﻿(function ($) {
    'use strict';
    $(function () {
        // Set idle time
        $(document).idleTimer(300000);

        $(document).on("idle.idleTimer", function (event, elem, obj, triggerevent) {
            window.location.assign("log-off.php");
        });

    });
}(jQuery))
