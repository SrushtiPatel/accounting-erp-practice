<?php
ob_start();
$Page = "Dashboard"; 
require_once('head.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Xenon ERP</title>
    
    <!-- Favicon -->
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="assets/plugins/bootstrap/dist/css/bootstrap.css" rel="stylesheet" />

    <!-- Bootstrap DateRangePicker Css -->
    <link href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />

    <!-- Animate.css Css -->
    <link href="assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Font Awesome Css -->
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

    <!-- iCheck Css -->
    <link href="assets/plugins/iCheck/skins/flat/_all.css" rel="stylesheet" />

    <!-- Switchery Css -->
    <link href="assets/plugins/switchery/dist/switchery.css" rel="stylesheet" />

    <!-- Metis Menu Css -->
    <link href="assets/plugins/metisMenu/dist/metisMenu.css" rel="stylesheet" />

    <!-- Pace Loader Css -->
    <link href="assets/plugins/pace/themes/white/pace-theme-flash.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="assets/css/style.css" rel="stylesheet" />
    
</head>
<body>
    <div class="all-content-wrapper">
        <!-- Top Bar -->
        <header>
            <?php include_once('header.php'); ?>
        </header>
        <?php
            $SelectProfile = "SELECT * FROM admin_master WHERE admin_id='".$_SESSION['AdminID']."'";
            $SelectProfileQuery = mysqli_query($con,$SelectProfile);
            if(!$SelectProfileQuery)
            {
                die(mysqli_error($con));
            }
            $Profile = mysqli_fetch_array($SelectProfileQuery);
			$CompanyDetail = $profileObject->selectCompanyProfile($CurrentCompanyID);
			$CompanyDetail = json_decode($CompanyDetail,true);
			$CompanyName = $CompanyDetail['CompanyName'];
			
        ?>
        <!-- #END# Top Bar -->
        <!-- Left Menu -->
        <aside class="sidebar">
            <?php include_once('menu.php'); ?>
        </aside>
        <!-- #END# Left Menu -->
        <section class="content">
            <div class="page-heading">
                <h1>Profile</h1>
                <ol class="breadcrumb">
                    <li><a href="dashboard.php">Home</a></li>
                    <li class="active">Profile</li>
                </ol>
            </div>
            
            <div class="page-body">
                <!-- -------------- ERROR SECTION START -------------- -->
                        
                    <div id="flash" class="alert alert hidden">
                        <strong>
                            <i class="fa fa-spinner fa-spin"></i>
                        </strong>
                        &nbsp; &nbsp;
                        <span></span>
                    </div>
                
                <!-- -------------- ERROR SECTION END -------------- -->
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">Admin Profile</div>
                            <div class="panel-body">
                                <table class="table table-striped table-bordered">
                                    <tbody>
                                        <tr>
                                            <th class="col-xs-6 col-sm-4">Admin Name</th>
                                            <td><?php echo $Profile['admin_name']; ?></td>
                                        </tr>
                                        <tr>
                                            <th class="col-xs-6 col-sm-4">Email ID</th>
                                            <td><?php echo $Profile['email_id']; ?></td>
                                        </tr>
                                        <tr>
                                            <th class="col-xs-6 col-sm-4">Mobile</th>
                                            <td><?php echo $Profile['mobile']; ?></td>
                                        </tr>
                                        <tr>
                                            <th class="col-xs-6 col-sm-4">Financial Year</th>
                                            <td><?php echo date("d-m-Y",strtotime($Profile['fyear_start'])); ?> to <?php echo date("d-m-Y",strtotime($Profile['fyear_end'])); ?></td>
                                        </tr>
                                        <?php
											if($Profile['license'] > 0)
											{
										?>
										<tr>
                                            <th class="col-xs-6 col-sm-4">Company Name</th>
                                            <td><?php echo $CompanyName; ?></td>
                                        </tr>
										<?php
											}
										?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- Horizontal Layout  -->
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">Update Admin Profile</div>
                            <div class="panel-body p-b-25">
                                <form id="Update-Profile-Form" method="post" class="form-horizontal" action="#">
                                    <input type="hidden" name="AdminID" value="<?php echo $_SESSION['AdminID']; ?>" required />
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Admin Name</label>
                                        <div class="col-sm-6">
                                            <input type="text" name="AdminName" value="<?php echo $Profile['admin_name']; ?>" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Email ID</label>
                                        <div class="col-sm-6">
                                            <input type="email" name="EmailID" value="<?php echo $Profile['email_id']; ?>" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Mobile</label>
                                        <div class="col-sm-6">
                                            <input type="text" name="Mobile" value="<?php echo $Profile['mobile']; ?>" onKeyPress="return NuMValidation(event);" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Financial Year</label>
                                    	<div class="col-sm-6">
                                    		<input type="text" class="form-control js-daterange-picker" id="FYear" name="FYear" value="<?php echo date("d/m/Y",strtotime($Profile['fyear_start'])); ?> - <?php echo date("d/m/Y",strtotime($Profile['fyear_end'])); ?>" required />
                                    		<span style="width: 50px;" class="glyphicon glyphicon-calendar form-control-feedback"></span>
		                               	</div>
                                    </div>
                                    <?php
										if($Profile['license'] > 0)
										{
									?>
                                  	<div class="form-group">
                                        <label class="col-sm-4 control-label">Company Name</label>
                                        <div class="col-sm-6">
                                        	<select id="CompanyID" name="CompanyID" class="form-control">
											<?php
												$SelectCompany = mysqli_query($con,"SELECT * FROM company_master");
												while($Company = mysqli_fetch_array($SelectCompany))
												{
													if($Profile['company_id'] == $Company['company_id'])
													{
														echo '<option value="'.$Company['company_id'].'" selected>'.$Company['company_name'].'</option>';
													}
													else
													{
														echo '<option value="'.$Company['company_id'].'">'.$Company['company_name'].'</option>';
													}
													
												}
											?>
                                        	</select>
										</div>
									</div>
                                   	<?php
										}
										else
										{
										?>
                                    	<input type="hidden" id="CompanyID" name="CompanyID" value="<?php echo $Profile['company_id']; ?>" required />
										<?php
										}
									?>
                                    <div class="form-group">
                                        <div class="col-sm-offset-4 col-sm-6">
                                            <button type="submit" class="btn btn-sm btn-success">
                                                <i class="fa fa-refresh bigger-110"></i>
                                                Update
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- #END# Horizontal Layout  -->
                </div>
            </div>
        </section>
        <!-- Footer -->
       
        <!-- #END# Footer -->
    </div>

    <!-- Jquery Core Js -->
    <script src="assets/plugins/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- MomentJs -->
    <script src="assets/plugins/moment/moment.js"></script>
    
    <!-- Bootstrap DateRangePicker Js -->
    <script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Pace Loader Js -->
    <script src="assets/plugins/pace/pace.js"></script>

    <!-- Screenfull Js -->
    <script src="assets/plugins/screenfull/src/screenfull.js"></script>

    <!-- Metis Menu Js -->
    <script src="assets/plugins/metisMenu/dist/metisMenu.js"></script>

    <!-- Jquery Slimscroll Js -->
    <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    

    <!-- Custom Js -->
    <script src="assets/js/admin.js"></script>
    

    <script type="text/javascript">
        jQuery(function($) {
			
			$('.js-daterange-picker').daterangepicker({
				opens: "left",
				drops: "up",
				applyClass: "btn-primary",
				locale: {
					format: 'DD/MM/YYYY'
				}
			});
        });
		
    </script>
    <script type="text/javascript">
        function NuMValidation(evt)
        {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
            return false;
    
            return true;
        }
    </script>
    <script type="text/javascript">
            $('#Update-Profile-Form').on('submit', function(event) {
                
                event.preventDefault();
                var Action = 'UpdateProfile';
                                
                var form_data = new FormData(this);
                form_data.append('Action',Action);
                                
                $("#flash").show();
                $("#flash i").addClass('fa-spinner');
                $("#flash i").addClass('fa-spin');
                $("#flash").removeClass('hidden');
                $('html,body').animate({ scrollTop: $(".content").offset().top},'slow');
                $("#flash span").html('Please Wait...');
                
                $.ajax({
                    url: 'includes/profile_script.php',
                    type: 'POST',
                    data: form_data,
                    contentType: false,
                    cache: false,
                    processData:false,
                    success: function(result)
                    {
                        //alert(result);
                        var obj = JSON.parse(result);
                        var Status = obj.Status;
                            
                        if(Status == '4')
                        {
                            $("#flash").removeClass('alert alert-danger');
                            $("#flash").addClass('alert alert-success');
                            $("#flash i").removeClass('fa-spinner');
                            $("#flash i").removeClass('fa-spin');
                            $("#flash i").removeClass('fa-times');
                            $("#flash i").addClass('fa-check');
                            $("#flash span").html('Profile Update Successfully.');
                            $('#flash').delay(2000).fadeOut(500);
                            setTimeout(function() {
                              window.location.href = "profile.php";
                            }, 1000);
                            return true;
                        }
                        else if(Status == '3')
                        {
                            $("#flash").removeClass('alert alert-success');
                            $("#flash").addClass('alert alert-danger');
                            $("#flash i").removeClass('fa-spinner');
                            $("#flash i").removeClass('fa-spin');
                            $("#flash i").removeClass('fa-check');
                            $("#flash i").addClass('fa-times');
                            $("#flash span").html('Profile Update Not Successfully.');
                            $('#flash').delay(3000).fadeOut(500);
                            return true;
                        }
                        else
                        {
                            $("#flash").removeClass('alert alert-success');
                            $("#flash").addClass('alert alert-danger');
                            $("#flash i").removeClass('fa-spinner');
                            $("#flash i").removeClass('fa-spin');
                            $("#flash i").removeClass('fa-check');
                            $("#flash i").addClass('fa-times');
                            $("#flash span").html(Status);
                            $('#flash').delay(4000).fadeOut(500);
                            return true;
                        }
                    }
                });
            });
        </script>
    
</body>
</html>
<?php
ob_flush();
?>