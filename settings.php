<?php
ob_start();
$Page = "Dashboard"; 
require_once('head.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Xenon ERP</title>
    
    <!-- Favicon -->
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="assets/plugins/bootstrap/dist/css/bootstrap.css" rel="stylesheet" />

    <!-- Bootstrap DateRangePicker Css -->
    <link href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />

    <!-- Animate.css Css -->
    <link href="assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Font Awesome Css -->
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

    <!-- iCheck Css -->
    <link href="assets/plugins/iCheck/skins/flat/_all.css" rel="stylesheet" />

    <!-- Switchery Css -->
    <link href="assets/plugins/switchery/dist/switchery.css" rel="stylesheet" />

    <!-- Metis Menu Css -->
    <link href="assets/plugins/metisMenu/dist/metisMenu.css" rel="stylesheet" />

    <!-- Pace Loader Css -->
    <link href="assets/plugins/pace/themes/white/pace-theme-flash.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="assets/css/style.css" rel="stylesheet" />
    
</head>
<body>
    <div class="all-content-wrapper">
        <!-- Top Bar -->
        <header>
            <?php include_once('header.php'); ?>
        </header>
        <!-- #END# Top Bar -->
        <!-- Left Menu -->
        <aside class="sidebar">
            <?php include_once('menu.php'); ?>
        </aside>
        <!-- #END# Left Menu -->
        <section class="content">
            <div class="page-heading">
                <h1>Setting</h1>
                <ol class="breadcrumb">
                    <li><a href="dashboard.php">Home</a></li>
                    <li class="active">Setting</li>
                </ol>
            </div>
            
            <div class="page-body">
                <!-- -------------- ERROR SECTION START -------------- -->
                        
                    <div id="flash" class="alert alert hidden">
                        <strong>
                            <i class="fa fa-spinner fa-spin"></i>
                        </strong>
                        &nbsp; &nbsp;
                        <span></span>
                    </div>
                
                <!-- -------------- ERROR SECTION END -------------- -->
                <div class="row clearfix">
                    <!-- Horizontal Layout  -->
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">Update Password</div>
                            <div class="panel-body p-b-25">
                                <form id="UpdatePWDForm" method="post" class="form-horizontal" action="#">
                                    <input type="hidden" id="AdminID" name="AdminID" value="<?php echo $_SESSION['AdminID']; ?>" required />
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Current Password</label>
                                        <div class="col-sm-6">
                                            <input type="password" id="OPWD" class="form-control" />
                                            <span id="opwd_error" style="color:#F00; padding-left:10px;"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">New Password</label>
                                        <div class="col-sm-6">
                                            <input type="password" id="NPWD" class="form-control" />
                                            <span id="npwd_error" style="color:#F00; padding-left:10px;"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Confirm Password</label>
                                        <div class="col-sm-6">
                                            <input type="password" id="CPWD" class="form-control" />
                                            <span id="cpwd_error" style="color:#F00; padding-left:10px;"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="text-center">
                                            <button type="submit" class="btn btn-sm btn-success">
                                                <i class="fa fa-refresh bigger-110"></i>
                                                Update
                                            </button>
                                            <span id="pwd_error" style="color:#F00; padding-left:10px;"></span>
                                            <span id="pwd_success" style="color:#3C0; padding-left:10px;"></span>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- #END# Horizontal Layout  -->
                </div>
            </div>
        </section>
        <!-- Footer -->
       
        <!-- #END# Footer -->
    </div>

    <!-- Jquery Core Js -->
    <script src="assets/plugins/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Bootstrap DateRangePicker Js -->
    <script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Pace Loader Js -->
    <script src="assets/plugins/pace/pace.js"></script>

    <!-- Screenfull Js -->
    <script src="assets/plugins/screenfull/src/screenfull.js"></script>

    <!-- Metis Menu Js -->
    <script src="assets/plugins/metisMenu/dist/metisMenu.js"></script>

    <!-- Jquery Slimscroll Js -->
    <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    

    <!-- Custom Js -->
    <script src="assets/js/admin.js"></script>
    

    
    <script type="text/javascript">
            $('#UpdatePWDForm').on('submit', function(event) {
                
                event.preventDefault();
                
                var admin_id = $('#AdminID').val();
                var s_admin_id = $('#AdminID').val();
                var opwd = $('#OPWD').val();
                var npwd = $('#NPWD').val();
                var cpwd = $('#CPWD').val();
                
                if(opwd == '')
                {
                    $("#opwd_error").show();
                    $("#opwd_error").html('Please Enter Current Password.');
                    $('#OPWD').focus();
                    return false;
                }
                if(npwd == '')
                {
                    $("#npwd_error").show();
                    $("#npwd_error").html('Please Enter New Password.');
                    $('#NPWD').focus();
                    return false;
                }
                if(cpwd == '')
                {
                    $("#cpwd_error").show();
                    $("#cpwd_error").html("New Password And Confirm Password Doesn't Match. Please Type Carefully.");
                    $('#CPWD').focus();
                    return false;
                }
                if(cpwd != npwd)
                {
                    $("#cpwd_error").show();
                    $("#cpwd_error").html("New Password And Confirm Password Doesn't Match. Please Type Carefully.");
                    $('#CPWD').focus();
                    return false;
                }
                
                //var form_data = new Array({'OldPwd': opwd, 'NewPwd': npwd, 'ConfPwd': cpwd, 'AdminID': admin_id, 'AdminID': s_admin_id});
                    $("#pwd_error").show();
                    $("#pwd_error").fadeIn(400).html('<img src="assets/img/loading.gif" title="loading.gif">');
                    
                    $.ajax({
                        type: 'POST',
                        url: "includes/settings_script.php",
                        data: ({'Action': 'UpdatePassword', 'OldPwd': opwd, 'NewPwd': npwd, 'ConfPwd': cpwd, 'AdminID': admin_id, 'AdminID': s_admin_id})
                        }).done(function(result) {
                            //alert(result);
                            var obj = JSON.parse(result);
                                                    
                            var Status = obj.Status;

                            if(Status == '1')
                            {
                                $('#pwd_error').fadeOut();
                                $('#npwd_error').fadeOut();
                                $('#cpwd_error').fadeOut();
                                $('#pwd_success').fadeOut();
                                $("#opwd_error").show();
                                $("#opwd_error").html("Wrong Current Password. Please Enter True Password.");
                                $("#OPWD").focus();
                                return false;
                            }
                            else if(Status == '2')
                            {
                                $('#pwd_error').fadeOut();
                                $('#npwd_error').fadeOut();
                                $('#cpwd_error').fadeOut();
                                $('#pwd_success').fadeOut();
                                $("#opwd_error").show();
                                $("#opwd_error").html("Wrong Current Password. Please Enter True Password.");
                                $("#OPWD").focus();
                                return false;
                            }
                            else if(Status == '3')
                            {
                                $('#pwd_error').fadeOut();
                                $('#npwd_error').fadeOut();
                                $('#opwd_error').fadeOut();
                                $('#pwd_success').fadeOut();
                                $("#cpwd_error").show();
                                $("#cpwd_error").html("New Password And Confirm Password Doesn't Match. Please Type Carefully.");
                                $("#CPWD").focus();
                                return false;
                            }
                            else if(Status == '4')
                            {
                                $('#pwd_error').fadeOut();
                                $('#npwd_error').fadeOut();
                                $('#opwd_error').fadeOut();
                                $('#cpwd_error').fadeOut();
                                $('#pwd_success').fadeOut();
                                $("#pwd_error").show();
                                $("#pwd_error").html("Password Change Failed. Please Try Again Later.");
                                $("#NPWD").focus();
                                return false;
                            }
                            else if(Status == '5')
                            {
                                $('#pwd_error').fadeOut();
                                $('#npwd_error').fadeOut();
                                $('#opwd_error').fadeOut();
                                $('#cpwd_error').fadeOut();
                                $("#pwd_success").show();
                                $("#pwd_success").html("Password Change Successfully.");
                                document.getElementById('UpdatePWDForm').reset();
                                $('#pwd_success').fadeOut(2000);
                                return true;
                            }
                            
                    });
            });
        </script>
    </body>
</html>
<?php
ob_flush();
?>