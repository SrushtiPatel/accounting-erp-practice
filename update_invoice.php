<?php
ob_start();
$Page = "Invoice"; 
require_once('head.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Xenon ERP</title>
    
    <!-- Favicon -->
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="assets/plugins/bootstrap/dist/css/bootstrap.css" rel="stylesheet" />

    <!-- Animate.css Css -->
    <link href="assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Font Awesome Css -->
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

    <!-- iCheck Css Libraries | You can choose a theme from plugins/iCheck/skins instead of get all themes -->
    <link href="assets/plugins/iCheck/skins/minimal/_all.css" rel="stylesheet" />
    <link href="assets/plugins/iCheck/skins/square/_all.css" rel="stylesheet" />
    <link href="assets/plugins/iCheck/skins/flat/_all.css" rel="stylesheet" />

    <!-- Switchery Css -->
    <link href="assets/plugins/switchery/dist/switchery.css" rel="stylesheet" />

    <!-- Metis Menu Css -->
    <link href="assets/plugins/metisMenu/dist/metisMenu.css" rel="stylesheet" />

    <!-- Pace Loader Css -->
    <link href="assets/plugins/pace/themes/white/pace-theme-flash.css" rel="stylesheet" />

    <!-- Bootstrap Select Css -->
    <link href="assets/plugins/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" />

    <!-- DateTimePicker Css -->
    <link href="assets/plugins/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="assets/css/style.css" rel="stylesheet" />

</head>
<body class="ls-fixed navbar-fixed">
    <div class="all-content-wrapper">
        <!-- Top Bar -->
        <header>
            <?php include_once('header.php'); ?>
        </header>
        <?php
            if(isset($_GET['invoice_id']))
            {
                $InvoiceID = $_GET['invoice_id'];
                $SelectInvoice = "SELECT * FROM invoice_master WHERE invoice_id='".$InvoiceID."'";
                $SelectInvoiceQuery = mysqli_query($con,$SelectInvoice);
                if(!$SelectInvoiceQuery)
                {
                    //die(mysqli_error($con));
                    header("Location: view_all_invoice.php");
                    exit();
                }
                $count = mysqli_num_rows($SelectInvoiceQuery);
                if($count != 1)
                {
                    header("Location: view_all_invoice.php");
                    exit();
                }
                
                $Invoice = mysqli_fetch_array($SelectInvoiceQuery);
                
                $InvoiceID = $Invoice['invoice_id'];
				$CompanyID = $Invoice['company_id'];
                $InvoiceNo = $Invoice['invoice_no'];
                $ClientID = $Invoice['client_id'];
                $InvoiceDate = $Invoice['invoice_date'];
                $ChallanNo = $Invoice['challan_no'];
				$ChallanDate = $Invoice['challan_date'];
				$PONo = $Invoice['po_no'];
                $PODate = $Invoice['po_date'];
				$DueDate = $Invoice['due_date'];
                $PlaceOfSupply = $Invoice['place_of_supply'];
                $TransportationMode = $Invoice['transportation_mode'];
                $VehicleNo = $Invoice['vehicle_no'];
                $DateOfSupply = $Invoice['date_of_supply'];
                $TotalAmountBeforeTax = $Invoice['total_amount_before_tax'];
                $Discount = $Invoice['discount'];
                $TotalTaxAmount = $Invoice['total_tax_amount'];
				$RoundOff = $Invoice['roundoff'];
                $GrandTotal = $Invoice['grand_total'];
                $TermsCondition = $Invoice['terms_conditions'];
				
				$BillingName = $Invoice['billing_name'];
				$BillingMobile = $Invoice['billing_mobile'];
                $BillingGSTIN = $Invoice['billing_gstin'];
                $BillingAddress = $Invoice['billing_address'];
                $BillingCity = $Invoice['billing_city'];
                $BillingState = $Invoice['billing_state'];
                $BillingStateCode = $Invoice['billing_state_code'];
                $BillingCountry = $Invoice['billing_country'];
                $BillingPinCode = $Invoice['billing_pin_code'];
				
				$ShippingName = $Invoice['shipping_name'];
                $ShippingGSTIN = $Invoice['shipping_gstin'];
                $ShippingAddress = $Invoice['shipping_address'];
                $ShippingCity = $Invoice['shipping_city'];
                $ShippingState = $Invoice['shipping_state'];
                $ShippingStateCode = $Invoice['shipping_state_code'];
                $ShippingCountry = $Invoice['shipping_country'];
                $ShippingPinCode = $Invoice['shipping_pin_code'];
				
				
				$OCName = $Invoice['oc_name'];
				$OCAddress = $Invoice['oc_address'];
				$OCPhone = $Invoice['oc_phone'];
				$OCMobile = $Invoice['oc_mobile'];
				$OCEmail = $Invoice['oc_email'];
				$OCPANNo = $Invoice['oc_pan_no'];
				$OCGSTIN = $Invoice['oc_gstin'];
				$OCState = $Invoice['oc_state'];
				$OCStateCode = $Invoice['oc_state_code'];
				$OCBankName = $Invoice['oc_bank_name'];
				$OCBankAcNo = $Invoice['oc_bank_ac_no'];
				$OCBankIFSC = $Invoice['oc_bank_ifsc'];
				$OCBankBranch = $Invoice['oc_bank_branch'];

                $ClientDetail = $ledgerObject->selectClientDetail($ClientID);
                $ClientDetail = json_decode($ClientDetail,true);
                $ClientName = $ClientDetail['ClientName'];
				
				if(!empty($Invoice['invoice_date']) && $Invoice['invoice_date'] != '0000-00-00') { $InvoiceDate = date("d-m-Y",strtotime($Invoice['invoice_date'])); }
				if(!empty($Invoice['challan_date']) && $Invoice['challan_date'] != '0000-00-00') { $ChallanDate = date("d-m-Y",strtotime($Invoice['challan_date'])); }
				if(!empty($Invoice['po_date']) && $Invoice['po_date'] != '0000-00-00') { $PODate = date("d-m-Y",strtotime($Invoice['po_date'])); }
				if(!empty($Invoice['due_date']) && $Invoice['due_date'] != '0000-00-00') { $DueDate = date("d-m-Y",strtotime($Invoice['due_date'])); }
				if(!empty($Invoice['date_of_supply']) && $Invoice['date_of_supply'] != '0000-00-00') { $DateOfSupply = date("d-m-Y",strtotime($Invoice['date_of_supply'])); }
				
            }
            else
            {
                header("Location: view_all_invoice.php");
                exit();
            }
        ?>
        <!-- #END# Top Bar -->
        <!-- Left Menu -->
        <aside class="sidebar">
            <?php include_once('menu.php'); ?>
        </aside>
        <!-- #END# Left Menu -->
        <section class="content">
            <div class="page-heading">
                <h1>
                    <a href="view_all_invoice.php">
                        Invoice
                    </a>
                    <small>
                        <i class="fa fa-angle-double-right"></i>
                        <a class="font-bold" href="update_invoice.php?invoice_id=<?php echo $InvoiceID; ?>">Update Invoice Detail</a>
                    </small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="dashboard.php">Home</a></li>
                    <li><a href="view_all_invoice.php">Invoice</a></li>
                </ol>
            </div>
            
            <div class="page-body">
            	<!-- -------------- ERROR SECTION START -------------- -->
                        
                <div id="flash" class="alert alert hidden">
                    <strong>
                        <i class="fa fa-spinner fa-spin"></i>
                    </strong>
                    &nbsp; &nbsp;
                    <span></span>
                </div>
            
            	<!-- -------------- ERROR SECTION END -------------- -->
                <div class="row clearfix">
                    <!-- Horizontal Layout  -->
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Update Invoice</div>
								<div class="panel-body p-b-25">
									<form id="Update-Invoice-Form" method="post" class="form-horizontal" action="#">
										<div class="row">
											<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
												<input type="hidden" id="AdminID" name="AdminID" value="<?php echo $_SESSION['AdminID']; ?>" required />
												<input type="hidden" id="InvoiceID" name="InvoiceID" value="<?php echo $InvoiceID; ?>"  required />
												<input type="hidden" id="CompanyID" name="CompanyID" value="<?php echo $CompanyID; ?>" required />
												<div class="form-group">
													<label class="col-sm-5 control-label">Client</label>
													<div class="col-sm-6">
														<input type="hidden" id="ClientID" name="ClientID" value="<?php echo $ClientID; ?>"  required />
														<input type="text" id="ClientName" name="ClientName" value="<?php echo $ClientName; ?>" class="form-control" readonly />
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-5 control-label">Invoice No</label>
													<div class="col-sm-6">
														<input type="text" id="InvoiceNo" name="InvoiceNo" class="form-control" value="<?php echo $InvoiceNo; ?>" readonly required />
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-5 control-label">Invoice Date</label>
													<div class="col-sm-6">
														<input type="text" id="InvoiceDate" name="InvoiceDate" value="<?php echo $InvoiceDate; ?>" placeholder="" data-format="DD-MM-YYYY" class="form-control js-dtp" required />
															<span style="width: 50px; " class="glyphicon glyphicon-calendar form-control-feedback"></span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-5 control-label">Challan No</label>
													<div class="col-sm-6">
														<input type="text" id="ChallanNo" name="ChallanNo" value="<?php echo $ChallanNo; ?>" class="form-control" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-5 control-label">Challan Date</label>
													<div class="col-sm-6">
														<input type="text" id="ChallanDate" name="ChallanDate" value="<?php echo $ChallanDate; ?>" data-format="DD-MM-YYYY" class="form-control js-dtp" />
														<span style="width: 50px;" class="glyphicon glyphicon-calendar form-control-feedback"></span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-5 control-label">PO No</label>
													<div class="col-sm-6">
														<input type="text" id="PONo" name="PONo" value="<?php echo $PONo; ?>" class="form-control" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-5 control-label">PO Date</label>
													<div class="col-sm-6">
														<input type="text" id="PODate" name="PODate" value="<?php echo $PODate; ?>" placeholder="" data-format="DD-MM-YYYY" class="form-control js-dtp" />
														<span style="width: 50px; " class="glyphicon glyphicon-calendar form-control-feedback"></span>
													</div>
												</div>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
												<div class="form-group">
													<label class="col-sm-5 control-label">Due Date</label>
													<div class="col-sm-6">
														<input type="text" id="DueDate" name="DueDate" value="<?php echo $DueDate; ?>" placeholder="" data-format="DD-MM-YYYY" class="form-control js-dtp" />
														<span style="width: 50px; " class="glyphicon glyphicon-calendar form-control-feedback"></span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-5 control-label">Place Of Supply</label>
													<div class="col-sm-6">
														<input type="text" id="PlaceOfSupply" name="PlaceOfSupply" value="<?php echo $PlaceOfSupply; ?>" class="form-control" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-5 control-label">Transportation Mode</label>
													<div class="col-sm-6">
														<input type="text" id="TransportationMode" name="TransportationMode" value="<?php echo $TransportationMode; ?>" class="form-control" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-5 control-label">Vehicle No</label>
													<div class="col-sm-6">
														<input type="text" id="VehicleNo" name="VehicleNo" value="<?php echo $VehicleNo; ?>" class="form-control" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-5 control-label">Date Of Supply</label>
													<div class="col-sm-6">
														<input type="text" id="DateOfSupply" name="DateOfSupply" value="<?php echo $DateOfSupply; ?>" placeholder="" data-format="DD-MM-YYYY" class="form-control js-dtp" />
															<span style="width: 50px; " class="glyphicon glyphicon-calendar form-control-feedback"></span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-5 control-label">Terms & Condition</label>
													<div class="col-sm-6">
														<textarea id="TermsCondition" name="TermsCondition" class="form-control no-resize" rows="5"><?php echo $TermsCondition; ?></textarea>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
												<div class="form-group">
													<label class="col-sm-9 control-label center" style="font-size: 15px;">Billing Details</label>
												</div>
												<div class="form-group">
													<label class="col-sm-5 control-label">Billing Name</label>
													<div class="col-sm-6">
														<input type="text" id="BillingName" name="BillingName" value="<?php echo $BillingName; ?>" class="form-control" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-5 control-label">Billing Mobile</label>
													<div class="col-sm-6">
														<input type="text" id="BillingMobile" name="BillingMobile" value="<?php echo $BillingMobile; ?>" class="form-control" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-5 control-label">Billing GSTIN</label>
													<div class="col-sm-6">
														<input type="text" id="BillingGSTIN" name="BillingGSTIN" value="<?php echo $BillingGSTIN; ?>" class="form-control" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-5 control-label">Billing Address</label>
													<div class="col-sm-6">
														<textarea id="BillingAddress" name="BillingAddress" class="form-control no-resize" rows="5"><?php echo $BillingAddress; ?></textarea>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-5 control-label">Billing City</label>
													<div class="col-sm-6">
														<input type="text" id="BillingCity" name="BillingCity" value="<?php echo $BillingCity; ?>" class="form-control" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-5 control-label">Billing State</label>
													<div class="col-sm-6">
														<input type="text" id="BillingState" name="BillingState" value="<?php echo $BillingState; ?>" class="form-control" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-5 control-label">Billing State Code</label>
													<div class="col-sm-6">
														<input type="text" id="BillingStateCode" name="BillingStateCode" value="<?php echo $BillingStateCode; ?>" class="form-control" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-5 control-label">Billing Country</label>
													<div class="col-sm-6">
														<input type="text" id="BillingCountry" name="BillingCountry" value="<?php echo $BillingCountry; ?>" class="form-control" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-5 control-label">Billing Pin Code</label>
													<div class="col-sm-6">
														<input type="text" id="BillingPinCode" name="BillingPinCode" value="<?php echo $BillingPinCode; ?>" class="form-control" />
													</div>
												</div>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
												<div class="form-group">
													<label class="col-sm-9 control-label no-pedding" style="font-size: 15px;">Shipping Details</label>
												</div>
												<div class="form-group">
													<label class="col-sm-5 control-label">Shipping Name</label>
													<div class="col-sm-6">
														<input type="text" id="ShippingName" name="ShippingName" value="<?php echo $ShippingName; ?>" class="form-control" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-5 control-label">Shipping GSTIN</label>
													<div class="col-sm-6">
														<input type="text" id="ShippingGSTIN" name="ShippingGSTIN" value="<?php echo $ShippingGSTIN; ?>" class="form-control" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-5 control-label">Shipping Address</label>
													<div class="col-sm-6">
														<textarea id="ShippingAddress" name="ShippingAddress" class="form-control no-resize" rows="5"><?php echo $ShippingAddress; ?></textarea>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-5 control-label">Shipping City</label>
													<div class="col-sm-6">
														<input type="text" id="ShippingCity" name="ShippingCity" value="<?php echo $ShippingCity; ?>" class="form-control" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-5 control-label">Shipping State</label>
													<div class="col-sm-6">
														<input type="text" id="ShippingState" name="ShippingState" value="<?php echo $ShippingState; ?>" class="form-control" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-5 control-label">Shipping State Code</label>
													<div class="col-sm-6">
														<input type="text" id="ShippingStateCode" name="ShippingStateCode" value="<?php echo $ShippingStateCode; ?>" class="form-control" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-5 control-label">Shipping Country</label>
													<div class="col-sm-6">
														<input type="text" id="ShippingCountry" name="ShippingCountry" value="<?php echo $ShippingCountry; ?>" class="form-control" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-5 control-label">Shipping Pin Code</label>
													<div class="col-sm-6">
														<input type="text" id="ShippingPinCode" name="ShippingPinCode" value="<?php echo $ShippingPinCode; ?>" class="form-control" />
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="form-group">
													<label class="col-xs-12 col-sm-12 control-label" style="font-size: 14px; text-align: center;">Our Details</label>
												</div>
											<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
												<div class="form-group">
													<label class="col-sm-5 control-label">Company Name</label>
													<div class="col-sm-6">
														<input type="text" id="OCName" name="OCName" value="<?php echo $OCName; ?>" class="form-control" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-5 control-label">Company Address</label>
													<div class="col-sm-6">
														<textarea id="OCAddress" name="OCAddress" class="form-control no-resize" rows="3" /><?php echo $OCAddress; ?></textarea>
													</div>
												</div>											
												<div class="form-group">
													<label class="col-sm-5 control-label">Phone</label>
													<div class="col-sm-6">
														<input type="text" id="OCPhone" name="OCPhone" value="<?php echo $OCPhone; ?>" class="form-control" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-5 control-label">Mobile</label>
													<div class="col-sm-6">
														<input type="text" id="OCMobile" name="OCMobile" value="<?php echo $OCMobile; ?>" class="form-control" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-5 control-label">Email</label>
													<div class="col-sm-6">
														<input type="text" id="OCEmail" name="OCEmail" value="<?php echo $OCEmail ?>" class="form-control" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-5 control-label">PAN No</label>
													<div class="col-sm-6">
														<input type="text" id="OCPANNo" name="OCPANNo" value="<?php echo $OCPANNo; ?>" class="form-control" />
													</div>
												</div>
												
											</div>
											<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
												<div class="form-group">
													<label class="col-sm-5 control-label">GSTIN</label>
													<div class="col-sm-6">
														<input type="text" id="GSTIN" name="GSTIN" value="<?php echo $OCGSTIN; ?>" class="form-control" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-5 control-label">State</label>
													<div class="col-sm-6">
														<input type="text" id="State" name="State" value="<?php echo $OCState; ?>" class="form-control" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-5 control-label">State Code</label>
													<div class="col-sm-6">
														<input type="text" id="StateCode" name="StateCode" value="<?php echo $OCStateCode; ?>" class="form-control" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-5 control-label">Bank Name</label>
													<div class="col-sm-6">
														<input type="text" id="OCBankName" name="OCBankName" value="<?php echo $OCBankName; ?>" class="form-control" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-5 control-label">Bank Ac No</label>
													<div class="col-sm-6">
														<input type="text" id="OCBankAcNo" name="OCBankAcNo" value="<?php echo $OCBankAcNo;; ?>" class="form-control" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-5 control-label">Bank IFSC</label>
													<div class="col-sm-6">
														<input type="text" id="OCBankIFSC" name="OCBankIFSC" value="<?php echo $OCBankIFSC; ?>" class="form-control" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-5 control-label">Bank Branch</label>
													<div class="col-sm-6">
														<input type="text" id="OCBankBranch" name="OCBankBranch" value="<?php echo $OCBankBranch; ?>" class="form-control" />
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-12 col-sm-12">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th colspan="10" class="text-center" width="100%">Item Detail</th>
														</tr>
														<tr>
															<th class="text-center" width="15%">Item Name</th>
															<th class="text-center" width="17%">Description</th>
															<th class="text-center" width="8%">Quantity</th>
															<th class="text-center" width="8%">Rate</th>
															<th class="text-center" width="5%">Discount</th>
															<th class="text-center" width="9%">SGST</th>
															<th class="text-center" width="9%">CGST</th>
															<th class="text-center" width="9%">IGST</th>
															<th class="text-center" width="12%">Total</th>
															<th class="text-center" width="8%">#</th>
														</tr>
													</thead>
													<tbody>
														<?php
															$SelectInvoiceDetails = mysqli_query($con,"SELECT * FROM `invoice_item_master` WHERE `invoice_id`='".$InvoiceID."'");
															$Count = 0;
															while($InvoiceDetail = mysqli_fetch_array($SelectInvoiceDetails))
															{
																$InvoiceItemID = $InvoiceDetail['invoice_item_id'];
																$ItemIndex = $InvoiceDetail['invoice_item_id'];
																$ItemID = $InvoiceDetail['item_id'];
																$Description = $InvoiceDetail['description'];
																$Quantity = $InvoiceDetail['qty'];
																$Rate = $InvoiceDetail['rate'];
																$Discount1 = $InvoiceDetail['discount'];
																$Amount = $InvoiceDetail['amount'];
																$TaxableValue = $InvoiceDetail['taxable_value'];
																$Tax1 = $InvoiceDetail['tax1_id'];
																$Tax1Rate = $InvoiceDetail['tax1_rate'];
																$Tax1Value = $InvoiceDetail['tax1_amount'];
																$Tax2 = $InvoiceDetail['tax2_id'];
																$Tax2Rate = $InvoiceDetail['tax2_rate'];
																$Tax2Value = $InvoiceDetail['tax2_amount'];
																$Tax3 = $InvoiceDetail['tax3_id'];
																$Tax3Rate = $InvoiceDetail['tax3_rate'];
																$Tax3Value = $InvoiceDetail['tax3_amount'];         
																$Total = $InvoiceDetail['total'];
																$ItemName = $InvoiceDetail['item_name'];
																$HSNCode = $InvoiceDetail['hsn_code'];
																$Unit = $InvoiceDetail['unit'];

														?>
														<tr data-item-index="<?php echo $ItemIndex; ?>">
															<td class="padding-0-impo">
															<input type="hidden" id="InvoiceItemID<?php echo $ItemIndex; ?>" name="InvoiceItemID[]" value="<?php echo $InvoiceItemID; ?>" required />
																<select id="ItemID[]" name="ItemID[]" class="form-control no-border item item_<?php echo $ItemIndex; ?>">
																	<?php
																		$SelectItem = mysqli_query($con,"SELECT * FROM item_master WHERE company_id='".$CurrentCompanyID."'");
																		while($Item = mysqli_fetch_array($SelectItem))
																		{
																			if($ItemID == $Item['item_id'])
																			echo '<option value="'.$Item['item_id'].'" selected>'.$Item['item_name'].'</option>';
																		}
																	?>
																</select>
																<input type="hidden" id="ItemName[]" name="ItemName[]" value="<?php echo $ItemName; ?>" />
																<input type="hidden" id="HSNCode[]" name="HSNCode[]" value="<?php echo $HSNCode; ?>" />
																<input type="hidden" id="Unit[]" name="Unit[]" value="<?php echo $Unit; ?>" />
															</td>
															<td class="padding-0-impo">
																<input type="text" id="Description[]" name="Description[]" value="<?php echo $Description; ?>" class="form-control no-border" />
															</td>
															<td class="padding-0-impo">
																<input type="text" id="Quantity[]" name="Quantity[]" value="<?php echo $Quantity; ?>" class="form-control no-border" onKeyPress="return NuMValidation(event);" onFocus="return grandTotal();" onKeyUp="return grandTotal();" required />
															</td>
															<td class="padding-0-impo">
																<input type="text" id="Rate[]" name="Rate[]" value="<?php echo $Rate; ?>" class="form-control no-border" onKeyPress="return NuMValidation(event);" onFocus="return grandTotal();" onKeyUp="return grandTotal();" required />
																<input type="hidden" id="Amount[]" name="Amount[]" value="<?php echo $Amount; ?>" />
															</td>
															<td class="padding-0-impo">
																<input type="text" id="Discount[]" name="Discount[]" value="<?php echo $Discount1; ?>" class="col-xs-12 form-control no-border" onKeyPress="return NuMValidation(event);" onFocus="return grandTotal();" onKeyUp="return grandTotal();" />
																<input type="hidden" id="TaxableValue[]" name="TaxableValue[]" value="<?php echo $TaxableValue; ?>" />
															</td>
															<td class="padding-0-impo">
																<select id="Tax1[]" name="Tax1[]" class="tax1 tax1_<?php echo $ItemIndex; ?> form-control no-border" onFocus="return grandTotal();">
																	<option value="-1">-- SELECT --</option>
																	<?php
																		$SelectTax = mysqli_query($con,"SELECT l.ledger_id,l.ledger_name, li.li_value FROM ledger_master l JOIN ledger_item_master li ON l.ledger_id=li.ledger_id WHERE l.group_id='27' AND li.lcf_id='36' AND li.li_value='SGST' AND company_id='".$CurrentCompanyID."'");
																		while($Tax = mysqli_fetch_array($SelectTax))
																		{
																			$Taxdetail = $taxObject->getTaxDetail($Tax['ledger_id']);
																			$Taxdetail = json_decode($Taxdetail,true);
																			$TaxRate = $Taxdetail['TaxValue'];
																			if($Tax1 == $Tax['ledger_id'])
																			{
																				echo '<option value="'.$Tax['ledger_id'].'" selected>'.$TaxRate.'</option>';	
																			}
																			else
																			{
																				echo '<option value="'.$Tax['ledger_id'].'">'.$TaxRate.'</option>';
																			}
																		}
																	?>
																</select>
																<input type="hidden" id="Tax1Rate[]" name="Tax1Rate[]" value="<?php echo $Tax1Rate; ?>" />
																<input type="hidden" id="Tax1Value[]" name="Tax1Value[]" value="<?php echo $Tax1Value; ?>" />
																<input type="hidden" id="OldTax1[]" name="OldTax1[]" value="<?php echo $Tax1; ?>" />
																<input type="hidden" id="OldTax1Value[]" name="OldTax1Value[]" value="<?php echo $Tax1Value; ?>" />
															</td>
															<td class="padding-0-impo">
																<select id="Tax2[]" name="Tax2[]" class="tax2 tax2_<?php echo $ItemIndex; ?> form-control no-border" onFocus="return grandTotal();">
																	<option value="-1">-- SELECT --</option>
																	<?php
																		$SelectTax = mysqli_query($con,"SELECT l.ledger_id,l.ledger_name, li.li_value FROM ledger_master l JOIN ledger_item_master li ON l.ledger_id=li.ledger_id WHERE l.group_id='27' AND li.lcf_id='36' AND li.li_value='CGST' AND company_id='".$CurrentCompanyID."'");
																		while($Tax = mysqli_fetch_array($SelectTax))
																		{
																			$Taxdetail = $taxObject->getTaxDetail($Tax['ledger_id']);
																			$Taxdetail = json_decode($Taxdetail,true);
																			$TaxRate = $Taxdetail['TaxValue'];
																			if($Tax2 == $Tax['ledger_id'])
																			{
																				echo '<option value="'.$Tax['ledger_id'].'" selected>'.$TaxRate.'</option>';	
																			}
																			else
																			{
																				echo '<option value="'.$Tax['ledger_id'].'">'.$TaxRate.'</option>';
																			}
																		}
																	?>
																</select>
																<input type="hidden" id="Tax2Rate[]" name="Tax2Rate[]" value="<?php echo $Tax2Rate; ?>" />
																<input type="hidden" id="Tax2Value[]" name="Tax2Value[]" value="<?php echo $Tax2Value; ?>" />
																<input type="hidden" id="OldTax2[]" name="OldTax2[]" value="<?php echo $Tax2; ?>" />
																<input type="hidden" id="OldTax2Value[]" name="OldTax2Value[]" value="<?php echo $Tax2Value; ?>" />
															</td>
															<td class="padding-0-impo">
																<select id="Tax3[]" name="Tax3[]" class="tax3 tax3_<?php echo $ItemIndex; ?> form-control no-border" onFocus="return grandTotal();">
																	<option value="-1">-- SELECT --</option>
																	<?php
																		$SelectTax = mysqli_query($con,"SELECT l.ledger_id,l.ledger_name, li.li_value FROM ledger_master l JOIN ledger_item_master li ON l.ledger_id=li.ledger_id WHERE l.group_id='27' AND li.lcf_id='36' AND li.li_value='IGST' AND company_id='".$CurrentCompanyID."'");
																		while($Tax = mysqli_fetch_array($SelectTax))
																		{
																			$Taxdetail = $taxObject->getTaxDetail($Tax['ledger_id']);
																			$Taxdetail = json_decode($Taxdetail,true);
																			$TaxRate = $Taxdetail['TaxValue'];
																			if($Tax3 == $Tax['ledger_id'])
																			{
																				echo '<option value="'.$Tax['ledger_id'].'" selected>'.$TaxRate.'</option>';	
																			}
																			else
																			{
																				echo '<option value="'.$Tax['ledger_id'].'">'.$TaxRate.'</option>';
																			}

																		}
																	?>
																</select>
																<input type="hidden" id="Tax3Rate[]" name="Tax3Rate[]" value="<?php echo $Tax3Rate; ?>" />
																<input type="hidden" id="Tax3Value[]" name="Tax3Value[]" value="<?php echo $Tax3Value; ?>" />
																<input type="hidden" id="OldTax3[]" name="OldTax3[]" value="<?php echo $Tax3; ?>" />
																<input type="hidden" id="OldTax3Value[]" name="OldTax3Value[]" value="<?php echo $Tax3Value; ?>" /> 
															</td>
															<td class="padding-0-impo">
																<input type="text" id="Total[]" name="Total[]" value="<?php echo $Total; ?>" class="form-control no-border" onFocus="return grandTotal();" readonly required/>
															</td>
															<td class="padding-0-impo align-center" style="padding-left: 5px !important; padding-top: 2px !important;">
																<?php if($Count == 0) { ?>
																	<button type="button" class="btn btn-default btn-sm btn-white addButton"><i class="fa fa-plus"></i></button>
																	<?php } else {?>
																	<button type="button" class="btn btn-default btn-sm btn-white addButton"><i class="fa fa-plus"></i></button>
																	<button type="button" class="btn btn-default btn-sm btn-white removeButton2"><i class="fa fa-minus"></i></button>
																<?php } ?>                                                      						
															</td>
														</tr>
														<?php
																$Count++;
															}
														?>
														<!-- Item Hide Start -->
														<tr class="hidden" id="itemTemplate">
															<td class="padding-0-impo center">
																<select id="item_id" name="item_id" class="form-control no-border item">
																	<option value="-1">-- SELECT --</option>
																	<?php
																		$SelectItem = mysqli_query($con,"SELECT * FROM item_master WHERE company_id='".$CurrentCompanyID."'");
																		while($Item = mysqli_fetch_array($SelectItem))
																		{
																			echo '<option value="'.$Item['item_id'].'">'.$Item['item_name'].'</option>';
																		}
																	?>
																</select>
																<input type="hidden" id="item_name" name="item_name" />
																<input type="hidden" id="hsn_code" name="hsn_code" />
																<input type="hidden" id="unit" name="unit" />
															</td>
															<td class="padding-0-impo center">
																<input type="text" id="description" name="description" class="form-control no-border" />
															</td>
															<td class="padding-0-impo center">
																<input type="text" id="quantity" name="quantity" class="form-control no-border" onKeyPress="return NuMValidation(event);" onFocus="return grandTotal();" onKeyUp="return grandTotal();" />
															</td>
															<td class="padding-0-impo center">
																<input type="text" id="rate" name="rate" class="form-control no-border" onKeyPress="return NuMValidation(event);" onFocus="return grandTotal();" onKeyUp="return grandTotal();" />
																<input type="hidden" id="amount" name="amount" />
															</td>
															<td class="padding-0-impo center">
																<input type="text" id="discount" name="discount" class="form-control no-border" onKeyPress="return NuMValidation(event);" onFocus="return grandTotal();" onKeyUp="return grandTotal();" />
																<input type="hidden" id="taxable_value" name="taxable_value" />
															</td>
															<td class="padding-0-impo center">
																<select id="tax1" name="tax1" class="tax1 form-control no-border" onFocus="return grandTotal();">
																	<option value="-1">-- SELECT --</option>
																	<?php
																		$SelectTax = mysqli_query($con,"SELECT l.ledger_id,l.ledger_name, li.li_value FROM ledger_master l JOIN ledger_item_master li ON l.ledger_id=li.ledger_id WHERE l.group_id='27' AND li.lcf_id='36' AND li.li_value='SGST' AND company_id='".$CurrentCompanyID."'");
																		while($Tax = mysqli_fetch_array($SelectTax))
																		{
																			$Taxdetail = $taxObject->getTaxDetail($Tax['ledger_id']);
																			$Taxdetail = json_decode($Taxdetail,true);
																			$TaxRate = $Taxdetail['TaxValue'];
																			echo '<option value="'.$Tax['ledger_id'].'">'.$TaxRate.'</option>';
																		}
																	?>
																</select>
																<input type="hidden" id="tax1rate" name="tax1rate" />
																<input type="hidden" id="tax1value" name="tax1value" />
															</td>
															<td class="padding-0-impo center">
																<select id="tax2" name="tax2" class="tax2 form-control no-border" onFocus="return grandTotal();">
																	<option value="-1">-- SELECT --</option>
																	<?php
																		$SelectTax = mysqli_query($con,"SELECT l.ledger_id,l.ledger_name, li.li_value FROM ledger_master l JOIN ledger_item_master li ON l.ledger_id=li.ledger_id WHERE l.group_id='27' AND li.lcf_id='36' AND li.li_value='CGST' AND company_id='".$CurrentCompanyID."'");
																		while($Tax = mysqli_fetch_array($SelectTax))
																		{
																			$Taxdetail = $taxObject->getTaxDetail($Tax['ledger_id']);
																			$Taxdetail = json_decode($Taxdetail,true);
																			$TaxRate = $Taxdetail['TaxValue'];
																			echo '<option value="'.$Tax['ledger_id'].'">'.$TaxRate.'</option>';
																		}
																	?>
																</select>
																<input type="hidden" id="tax2rate" name="tax2rate" />
																<input type="hidden" id="tax2value" name="tax2value" />
															</td>
															<td class="padding-0-impo center">
																<select id="tax3" name="tax3" class="tax3 form-control no-border" onFocus="return grandTotal();">
																	<option value="-1">-- SELECT --</option>
																	<?php
																		$SelectTax = mysqli_query($con,"SELECT l.ledger_id,l.ledger_name, li.li_value FROM ledger_master l JOIN ledger_item_master li ON l.ledger_id=li.ledger_id WHERE l.group_id='27' AND li.lcf_id='36' AND li.li_value='IGST' AND company_id='".$CurrentCompanyID."'");
																		while($Tax = mysqli_fetch_array($SelectTax))
																		{
																			$Taxdetail = $taxObject->getTaxDetail($Tax['ledger_id']);
																			$Taxdetail = json_decode($Taxdetail,true);
																			$TaxRate = $Taxdetail['TaxValue'];
																			echo '<option value="'.$Tax['ledger_id'].'">'.$TaxRate.'</option>';
																		}
																	?>
																</select>
																<input type="hidden" id="tax3rate" name="tax3rate" />
																<input type="hidden" id="tax3value" name="tax3value" />
															</td>
															<td class="padding-0-impo center">
																<input type="text" id="total" name="total" class="form-control no-border" onFocus="return grandTotal();" readonly required />
															</td>
															<td class="padding-0-impo align-center" style="padding-left: 5px !important; padding-top: 2px !important;">
																<button type="button" class="btn btn-default btn-sm addButton"><i class="fa fa-plus"></i></button>
																<button type="button" class="btn btn-default btn-sm removeButton"><i class="fa fa-minus"></i></button>
															</td>
														</tr>
														<!-- Item Hide End -->
														<tr>
															<td colspan="5"></td>
															<td colspan="3">Amount Before Tax</td>
															<td colspan="2" class="padding-0-impo center">
																<input type="text" id="TotalAmountBeforeTax" name="TotalAmountBeforeTax" value="<?php echo $TotalAmountBeforeTax; ?>" class="form-control no-border" onFocus="return grandTotal();" readonly required />
															</td>
														</tr>
														<tr>
															<td colspan="5"></td>
															<td colspan="3">Total Tax Amount</td>
															<td colspan="2" class="padding-0-impo center">
																<input type="text" id="TotalTaxAmount" name="TotalTaxAmount" value="<?php echo $TotalTaxAmount; ?>" class="form-control no-border" onFocus="return grandTotal();" readonly required />
															</td>
														</tr>
														<tr>
															<td colspan="5"></td>
															<td colspan="3">Discount</td>
															<td colspan="2" class="padding-0-impo center">
																<input type="text" id="TotalDiscount" name="TotalDiscount" value="<?php echo $Discount; ?>" class="form-control no-border" onFocus="return grandTotal();" readonly required />
															</td>
														</tr>
														<tr>
															<td colspan="5"></td>
															<td colspan="3">Round Off</td>
															<td colspan="2" class="padding-0-impo center">
																<input type="text" id="RoundOff" name="RoundOff" value="<?php echo $RoundOff; ?>" class="form-control no-border" onFocus="return grandTotal();" readonly required />
															</td>
														</tr>
														<tr>
															<td colspan="5"></td>
															<td colspan="3">Grand Total</td>
															<td colspan="2" class="padding-0-impo center">
																<input type="text" id="GrandTotal" name="GrandTotal" value="<?php echo $GrandTotal; ?>" class="form-control no-border" onFocus="return grandTotal();" readonly required />
																<input type="hidden" id="OldGrandTotal" name="OldGrandTotal" value="<?php echo $GrandTotal; ?>" class="form-control no-border" required />
															</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div class="row">    
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
												<div class="form-group">
													<div class="text-center">
														<button type="submit" class="btn btn-success" id="Update-Invoice">
															<i class="fa fa-check bigger-110"></i>
															Update
														</button>
													</div>
												</div>
											</div>
										</div>
                                	</form>
								</div>
                            </div>
                        </div>
                    </div>
                    <!-- #END# Horizontal Layout  -->
                </div>
            </div>
        </section>
        <!-- Footer -->
       
        <!-- #END# Footer -->
    </div>
        
    <!-- Jquery Core Js -->
    <script src="assets/plugins/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Pace Loader Js -->
    <script src="assets/plugins/pace/pace.js"></script>

    <!-- Screenfull Js -->
    <script src="assets/plugins/screenfull/src/screenfull.js"></script>

    <!-- Metis Menu Js -->
    <script src="assets/plugins/metisMenu/dist/metisMenu.js"></script>

    <!-- Jquery Slimscroll Js -->
    <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Switchery Js -->
    <script src="assets/plugins/switchery/dist/switchery.js"></script>

    <!-- iCheck Js -->
    <script src="assets/plugins/iCheck/icheck.js"></script>

    <!-- Autosize Js (Textarea auto growth plugin) -->
    <script src="assets/plugins/autosize/dist/autosize.js"></script>

    <!-- MomentJs Js -->
    <script src="assets/plugins/moment/moment.js"></script>

    <!-- DateTimePicker Js -->
    <script src="assets/plugins/eonasdan-bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js"></script>

    <!-- Bootstrap Select Js -->
    <script src="assets/plugins/bootstrap-select/dist/js/bootstrap-select.js"></script>

    <!-- Custom Js -->
    <script src="assets/js/admin.js"></script>
    <script src="assets/js/pages/bootbox.min.js"></script>
    

    
   <script>
        function NuMValidation(evt)
        {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
            return false;
    
            return true;
        } 
        function grandTotal()
        {
            var QtyArray = document.querySelectorAll("#Update-Invoice-Form input[id='Quantity[]']");
            var RateArray = document.querySelectorAll("#Update-Invoice-Form input[id='Rate[]']");
            var DiscountArray = document.querySelectorAll("#Update-Invoice-Form input[id='Discount[]']");
            var Tax1RateArray = document.querySelectorAll("#Update-Invoice-Form input[id='Tax1Rate[]']");
            var Tax2RateArray = document.querySelectorAll("#Update-Invoice-Form input[id='Tax2Rate[]']");
            var Tax3RateArray = document.querySelectorAll("#Update-Invoice-Form input[id='Tax3Rate[]']");
			
            var AmountArray = document.querySelectorAll("#Update-Invoice-Form input[id='Amount[]']");
            var TaxablevalueArray = document.querySelectorAll("#Update-Invoice-Form input[id='TaxableValue[]']");
            var TotalArray = document.querySelectorAll("#Update-Invoice-Form input[id='Total[]']");
            var Tax1ValueArray = document.querySelectorAll("#Update-Invoice-Form input[id='Tax1Value[]']");
            var Tax2ValueArray = document.querySelectorAll("#Update-Invoice-Form input[id='Tax2Value[]']");
            var Tax3ValueArray = document.querySelectorAll("#Update-Invoice-Form input[id='Tax3Value[]']");
            
            var TotalAmountBeforeTax = 0.00;
            var TotalDiscount = 0.00;
            var TotalTaxAmount = 0.00;
            var RounOff = 0.00;
            var GrandTotal2 = 0.00;
            var GrandTotal = 0.00;

            for (i = 0; i < QtyArray.length; i++) 
            {
                var Qty = QtyArray[i].value; 
                var Rate = RateArray[i].value;
                var Discount = DiscountArray[i].value;
                var Tax1Rate = Tax1RateArray[i].value;
                var Tax2Rate = Tax2RateArray[i].value;
                var Tax3Rate = Tax3RateArray[i].value;
                
                if(Qty == '' || isNaN(Qty)) { Qty = 0.00; }
                if(Rate == '' || isNaN(Rate)) { Rate = 0.00; }
                if(Discount == '' || isNaN(Discount)) { Discount = 0.00; }
                
                if(Tax1Rate == '' || isNaN(Tax1Rate)) { Tax1Rate = 0.00; }
                if(Tax2Rate == '' || isNaN(Tax2Rate)) { Tax2Rate = 0.00; }
                if(Tax3Rate == '' || isNaN(Tax3Rate)) { Tax3Rate = 0.00; }
                
                var Amount = parseFloat(Qty) * parseFloat(Rate);
                Amount = Math.round(Amount);
                Amount = Amount.toFixed(2);
                AmountArray[i].value = Amount;
                
                var TaxableValue = parseFloat(Amount) - parseFloat(Discount);
                TaxableValue = Math.round(TaxableValue);
                TaxableValue = TaxableValue.toFixed(2);
                TaxablevalueArray[i].value = TaxableValue;
                                                       
                var Tax1Value = parseFloat(TaxableValue) * parseFloat(Tax1Rate) / 100;
                Tax1Value = Math.round(Tax1Value);
                Tax1Value = Tax1Value.toFixed(2);
                Tax1ValueArray[i].value = Tax1Value;
                
                var Tax2Value = parseFloat(TaxableValue) * parseFloat(Tax2Rate) / 100;
               	Tax2Value = Math.round(Tax2Value);
                Tax2Value = Tax2Value.toFixed(2);
                Tax2ValueArray[i].value = Tax2Value;
                                                       
                var Tax3Value = parseFloat(TaxableValue) * parseFloat(Tax3Rate) / 100;
                Tax3Value = Math.round(Tax3Value);
                Tax3Value = Tax3Value.toFixed(2);
                Tax3ValueArray[i].value = Tax3Value;
                
                var Total = parseFloat(TaxableValue) + parseFloat(Tax1Value) + parseFloat(Tax2Value) + parseFloat(Tax3Value);
               	Total = Math.round(Total);
                Total = Total.toFixed(2);
                TotalArray[i].value = Total;
                
                TotalAmountBeforeTax += parseFloat(Amount);
                TotalDiscount += parseFloat(Discount);
                TotalTaxAmount += parseFloat(Tax1Value) + parseFloat(Tax2Value) + parseFloat(Tax3Value);
                GrandTotal2 += parseFloat(Total);
            }
            TotalAmountBeforeTax = Math.round(TotalAmountBeforeTax);
            TotalAmountBeforeTax = TotalAmountBeforeTax.toFixed(2);
            $("#TotalAmountBeforeTax").val(TotalAmountBeforeTax);
            
			TotalDiscount = Math.round(TotalDiscount);
            TotalDiscount = TotalDiscount.toFixed(2);
            $("#TotalDiscount").val(TotalDiscount);
			
            TotalTaxAmount = Math.round(TotalTaxAmount);
            TotalTaxAmount = TotalTaxAmount.toFixed(2);
            $("#TotalTaxAmount").val(TotalTaxAmount);
            
            GrandTotal2 = GrandTotal2.toFixed(2);
            GrandTotal = Math.round(GrandTotal2);
            GrandTotal = GrandTotal.toFixed(2);
            
            var Roundoff = GrandTotal - GrandTotal2;
            Roundoff = Roundoff.toFixed(2);

            $("#RoundOff").val(Roundoff);
            $("#GrandTotal").val(GrandTotal);
            
            return true;
        }

    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            
            var itemIndex = <?php echo $ItemIndex; ?>;
            
            $('#Update-Invoice-Form')
            
            // Add button click handler
            .on('click', '.addButton', function() {
				
				var checkItemIDValue = checkItemID();
				if(checkItemIDValue == -1)
				{
					return false;
				}
				
                itemIndex++;
                var $template = $('#itemTemplate'),
                    $clone    = $template
                                    .clone()
                                    .removeClass('hidden')
                                    .removeAttr('id')
                                    .attr('data-item-index', itemIndex)
                                    .insertBefore($template);
    
                // Update the name attributes
                $clone
                    .find('[name="item_id"]').attr('name', "ItemID[]").end()
					.find('[name="item_name"]').attr('name', "ItemName[]").end()
					.find('[name="hsn_code"]').attr('name', "HSNCode[]").end()
					.find('[name="unit"]').attr('name', "Unit[]").end()
                    .find('[name="item_name"]').attr('name', "ItemName[]").end()
                    .find('[name="description"]').attr('name', "Description[]").end()
                    .find('[name="quantity"]').attr('name', "Quantity[]").end()
                    .find('[name="rate"]').attr('name', "Rate[]").end()
                    .find('[name="amount"]').attr('name', "Amount[]").end()
                    .find('[name="discount"]').attr('name', "Discount[]").end()
                    .find('[name="taxable_value"]').attr('name', "TaxableValue[]").end()
                    .find('[name="tax1"]').attr('name', "Tax1[]").end()
                    .find('[name="tax1rate"]').attr('name', "Tax1Rate[]").end()
                    .find('[name="tax1value"]').attr('name', "Tax1Value[]").end()
                    .find('[name="tax2"]').attr('name', "Tax2[]").end()
                    .find('[name="tax2rate"]').attr('name', "Tax2Rate[]").end()
                    .find('[name="tax2value"]').attr('name', "Tax2Value[]").end()
                    .find('[name="tax3"]').attr('name', "Tax3[]").end()
                    .find('[name="tax3rate"]').attr('name', "Tax3Rate[]").end()
                    .find('[name="tax3value"]').attr('name', "Tax3Value[]").end()
                    .find('[name="total"]').attr('name', "Total[]").end()
                    .find('[id="item_id"]').attr('id', "ItemID[]").end()
					.find('[id="item_name"]').attr('id', "ItemName[]").end()
					.find('[id="hsn_code"]').attr('id', "HSNCode[]").end()
					.find('[id="unit"]').attr('id', "Unit[]").end()
                    .find('[id="item_name"]').attr('id', "ItemName[]").end()
                    .find('[id="description"]').attr('id', "Description[]").end()
                    .find('[id="quantity"]').attr('id', "Quantity[]").end()
                    .find('[id="rate"]').attr('id', "Rate[]").end()
                    .find('[id="amount"]').attr('id', "Amount[]").end()
                    .find('[id="discount"]').attr('id', "Discount[]").end()
                    .find('[id="taxable_value"]').attr('id', "TaxableValue[]").end()
                    .find('[id="tax1"]').attr('id', "Tax1[]").end()
                    .find('[id="tax1rate"]').attr('id', "Tax1Rate[]").end()
                    .find('[id="tax1value"]').attr('id', "Tax1Value[]").end()
                    .find('[id="tax2"]').attr('id', "Tax2[]").end()
                    .find('[id="tax2rate"]').attr('id', "Tax2Rate[]").end()
                    .find('[id="tax2value"]').attr('id', "Tax2Value[]").end()
                    .find('[id="tax3"]').attr('id', "Tax3[]").end()
                    .find('[id="tax3rate"]').attr('id', "Tax3Rate[]").end()
                    .find('[id="tax3value"]').attr('id', "Tax3Value[]").end()
                    .find('[id="total"]').attr('id', "Total[]").end()
                    .find('[name="Rate[]"]').attr('required', true).end()
                    .find('[name="Amount[]"]').attr('required', true).end()
                    .find('[name="Quantity[]"]').attr('required', true).end()
					.find('[id="ItemID[]"]').addClass("item_"+itemIndex).end()
                    .find('[id="Tax1[]"]').addClass("tax1_"+itemIndex).end()
                    .find('[id="Tax2[]"]').addClass("tax2_"+itemIndex).end()
                    .find('[id="Tax3[]"]').addClass("tax3_"+itemIndex).end()
                    .find('[name="ItemID[]"]').attr('required', true).end()
                    .find('[name="ItemName[]"]').attr('required', true).end();
                    
                    grandTotal();
                    
            })
    
            // Remove button click handler
            .on('click', '.removeButton', function() {
                var $row  = $(this).parents('tr'),
                    index = $row.attr('data-item-index');
                        
                // Remove element containing the fields
                $row.remove();
                grandTotal();
            });
        })
        // Remove button click handler
		.on('click', '.removeButton2', function() {
			var $row  = $(this).parents('tr'),
				index = $row.attr('data-item-index');
			
			var InvoiceItemID = $('#InvoiceItemID'+index).val();
			//alert(InvoiceItemID);
			bootbox.confirm("Are You Sure, You Want To Delete This Item ?", function(result) 
			{
				if(result == true) 
				{
					var Action = 'RemoveInvoiceItem';
											
					var form_data = 'Action='+ Action +'&InvoiceItemID='+ InvoiceItemID;
					//alert(form_data); 
					$.ajax({
						type: 'POST',
						url: 'includes/invoice_script.php',
						data: form_data,
						cache: false,
						success: function(result)
						{
							//alert(result);
							var obj = JSON.parse(result);
							var Status = obj.Status;
							if(Status == '6')
							{
								$row.remove();
								$("#Update-Invoice").trigger("click");
								return true;
							}
							else if(Status == '00')
							{
								alert("Cannot Remove Data, There is a Reference Available For This Item.");
								return false;
							}
							else
							{
								return true;
							}	
						}
					});
				}
			});
	  	});

        $('#Update-Invoice-Form')
            .on('change', '#ClientID', function() {
        
            var ClientID = $("#ClientID").val();
            
            if(ClientID == -1)
            {
                $("#Update-Invoice-Form #BillingGSTIN").val('');
                $("#Update-Invoice-Form #BillingAddress").val('');
                $("#Update-Invoice-Form #BillingCity").val('');
                $("#Update-Invoice-Form #BillingState").val('');
                $("#Update-Invoice-Form #BillingStateCode").val('');
                $("#Update-Invoice-Form #BillingCountry").val('');
                $("#Update-Invoice-Form #BillingPinCode").val('');
                $("#Update-Invoice-Form #ShippingGSTIN").val('');
                $("#Update-Invoice-Form #ShippingAddress").val('');
                $("#Update-Invoice-Form #ShippingCity").val('');
                $("#Update-Invoice-Form #ShippingState").val('');
                $("#Update-Invoice-Form #ShippingStateCode").val('');
                $("#Update-Invoice-Form #ShippingCountry").val('');
                $("#Update-Invoice-Form #ShippingPinCode").val('');
                $("#Update-Invoice-Form #TransporterName").val('');
                $("#Update-Invoice-Form #TransporterAddress").val('');
                $("#Update-Invoice-Form #TransporterState").val('');
                $("#Update-Invoice-Form #TransporterStateCode").val('');
                $("#Update-Invoice-Form #TransporterGSTIN").val('');
                $("#Update-Invoice-Form #TransporterPhone").val('');
                $("#Update-Invoice-Form #VendorID").focus();
            }
            else
            {
                var Action = 'GetClientDetail';
                var dataString = 'Action='+ Action +'&ClientID='+ ClientID;
                //alert(dataString);

                $("#data_loader").show();
                $("#data_loader").fadeIn(400).html('<img src="assets/img/loading.gif" />');
                $.ajax({
                    type: "POST",
                    url: "includes/loader_functions.php",
                    data: dataString,
                    cache: false,
                    success: function(result)
                    {
                        //alert(result);
                        $("#data_loader").hide();
                        var obj = JSON.parse(result);
                                
                        $("#Update-Invoice-Form #BillingGSTIN").val(obj.BillingGSTIN);
                        $("#Update-Invoice-Form #BillingAddress").val(obj.BillingAddress);
                        $("#Update-Invoice-Form #BillingCity").val(obj.BillingCity);
                        $("#Update-Invoice-Form #BillingState").val(obj.BillingState);
                        $("#Update-Invoice-Form #BillingStateCode").val(obj.BillingStateCode);
                        $("#Update-Invoice-Form #BillingCountry").val(obj.BillingCountry);
                        $("#Update-Invoice-Form #BillingPinCode").val(obj.BillingPinCode);
                        $("#Update-Invoice-Form #ShippingGSTIN").val(obj.ShippingGSTIN);
                        $("#Update-Invoice-Form #ShippingAddress").val(obj.ShippingAddress);
                        $("#Update-Invoice-Form #ShippingCity").val(obj.ShippingCity);
                        $("#Update-Invoice-Form #ShippingState").val(obj.ShippingState);
                        $("#Update-Invoice-Form #ShippingStateCode").val(obj.ShippingStateCode);
                        $("#Update-Invoice-Form #ShippingCountry").val(obj.ShippingCountry);
                        $("#Update-Invoice-Form #ShippingPinCode").val(obj.ShippingPinCode);
                        $("#Update-Invoice-Form #TransporterName").val(obj.TransporterName);
                        $("#Update-Invoice-Form #TransporterAddress").val(obj.TransporterAddress);
                        $("#Update-Invoice-Form #TransporterState").val(obj.TransporterState);
                        $("#Update-Invoice-Form #TransporterStateCode").val(obj.TransporterStateCode);
                        $("#Update-Invoice-Form #TransporterGSTIN").val(obj.TransporterGSTIN);
                        $("#Update-Invoice-Form #TransporterPhone").val(obj.TransporterPhone);
                        return true;
                    }
                });
            }
        });
        
        function checkClientID()
        {
            var checkClentID = '';
            $('select[name="ClientID"]').each(function() {
                var ClientID = $(this).val(); 
                if(ClientID < 1)
                {
                    checkClientID = ClientID;// $("#flash").removeClass('alert-danger');  style="border:1px solid #f2a696; color:#d68273;"
                    $(this).css({ border: "1px solid #f2a696", color: "#d68273" });
                    $(this).focus;
                }
                else
                {
                    $(this).css({ border: "", color: "" });
                }
            });
            return checkClientID;
        }
		
		function checkItemID()
        {
            var checkItemID = '';
            $('select[name="ItemID[]"]').each(function() {
                var ItemID = $(this).val(); 
                if(ItemID < 1)
                {
                    checkItemID = ItemID;
                    $(this).css({ border: "1px solid #f2a696", color: "#d68273" });
                    $(this).focus;
                }
                else
                {
                    $(this).css({ border: "", color: "" });
                }
            });
            return checkItemID;
        }
		$('#Update-Invoice-Form').on( 'change', 'tbody td select.item', function () {
            
            var $row  = $(this).parents("tr"),
                index = $row.attr('data-item-index');
            

            var ItemID = $('.item_'+index).val();
			
            if(ItemID < 0)
            {
                $row.find('[id="ItemName[]"]').attr('value', "").end();
				$row.find('[id="HSNCode[]"]').attr('value', "").end();
				$row.find('[id="Unit[]"]').attr('value', "").end();
				$row.find('[id="Rate[]"]').attr('value', "").end();
                
                return false;
            }
            else
            {
                var Action = 'GetItemDetail';
                var dataString = 'Action='+ Action +'&ItemID='+ ItemID;
                $.ajax({
                    type: "POST",
                    url: "includes/loader_functions.php",
                    data: dataString,
                    cache: false,
                    success: function(result)
                    {
                        //alert(result);
                        var obj = JSON.parse(result);
                        $row.find('[id="ItemName[]"]').attr('value', obj.ItemName).end();
						$row.find('[id="HSNCode[]"]').attr('value', obj.HSNCode).end();
						$row.find('[id="Unit[]"]').attr('value', obj.ItemUnit).end();
						$row.find('[id="Rate[]"]').attr('value', obj.ItemRate).end();
                      
                        return true;
                    }
                });
            }
        });
	  	// Select Tax Rate
        $('#Update-Invoice-Form').on( 'change', 'tbody td select.tax1', function () {
            
            var $row  = $(this).parents("tr"),
                index = $row.attr('data-item-index');
            

            var TaxID = $('.tax1_'+index).val();
			
            if(TaxID < 0)
            {
                $row.find('[id="Tax1Rate[]"]').attr('value', "").end();
                
                grandTotal();
                return false;
            }
            
            else
            {
                var Action = 'GetAllTaxDetail';
                var dataString = 'Action='+ Action +'&TaxID='+ TaxID;
                $.ajax({
                    type: "POST",
                    url: "includes/loader_functions.php",
                    data: dataString,
                    cache: false,
                    success: function(result)
                    {
                        //alert(result);
                        var obj = JSON.parse(result);
                        $row.find('[id="Tax1Rate[]"]').attr('value', obj.TaxValue).end();
                        
                        grandTotal();
                        return true;
                    }
                });
            }
        });
        
        $('#Update-Invoice-Form').on( 'change', 'tbody td select.tax2', function () {
            
            var $row  = $(this).parents("tr"),
                index = $row.attr('data-item-index');
            
            var TaxID = $('.tax2_'+index).val();
           
            if(TaxID < 0)
            {
                $row.find('[id="Tax2Rate[]"]').attr('value', "").end();
                
                grandTotal();
                return false;
            }
            
            else
            {
                var Action = 'GetAllTaxDetail';
                var dataString = 'Action='+ Action +'&TaxID='+ TaxID;
                //alert(dataString);
                $.ajax({
                    type: "POST",
                    url: "includes/loader_functions.php",
                    data: dataString,
                    cache: false,
                    success: function(result)
                    {
                        //alert(result);
                        var obj = JSON.parse(result);
                        $row.find('[id="Tax2Rate[]"]').attr('value', obj.TaxValue).end();
                        
                        grandTotal();
                        return true;
                    }
                });
            }
        });
        
        $('#Update-Invoice-Form').on( 'change', 'tbody td select.tax3', function () {
            
            var $row  = $(this).parents("tr"),
                index = $row.attr('data-item-index');
            
            var TaxID = $('.tax3_'+index).val();
			
            if(TaxID < 0)
            {
                $row.find('[id="Tax3Rate[]"]').attr('value', "").end();
                
                grandTotal();
                return false;
            }
            
            else
            {   
                var Action = 'GetAllTaxDetail';
                var dataString = 'Action='+ Action +'&TaxID='+ TaxID;
                $.ajax({
                    type: "POST",
                    url: "includes/loader_functions.php",
                    data: dataString,
                    cache: false,
                    success: function(result)
                    {
                        //alert(result);
                        var obj = JSON.parse(result);
                        $row.find('[id="Tax3Rate[]"]').attr('value', obj.TaxValue).end();
                        
                        grandTotal();
                        return true;
                    }
                });
            }
        });
    </script>
    <script type="text/javascript">

        jQuery(function ($) 
        {
            'use strict';
            $(document).ready(function () {
                //Init datetimepicker
                $('.js-dtp').each(function (i, key) {
                    var format = $(key).data('format');
                    $(key).datetimepicker({
                        format: format,
                        showClear: true
                    });
                });
            });
        });

    </script>

    <script type="text/javascript">
        $('#Update-Invoice-Form').on('submit', function(event) {
            
            event.preventDefault();
            var Action = 'UpdateInvoice';
            
            var ClientID = $("#ClientID").val();
            
            if(ClientID < 0)
            {
                $("#ClientID").focus();
                return false;
            }
            var checkItemIDValue = checkItemID();
			if(checkItemIDValue == -1)
			{
				return false;
			}
            grandTotal();

            //return false;
            var form_data = new FormData(this);
            form_data.append('Action',Action);
            
           	$("#flash").show();
            $("#flash i").addClass('fa-spinner');
            $("#flash i").addClass('fa-spin');
            $("#flash").removeClass('hidden');
            $('html,body').animate({ scrollTop: $(".content").offset().top},'slow');
            $("#flash span").html('Please Wait...');
            
            $.ajax({
                url: 'includes/invoice_script.php',
                type: 'POST',
                data: form_data,
                contentType: false,
                cache: false,
                processData:false,
                success: function(result)
                {
                    //alert(result);
                    var obj = JSON.parse(result);
                    var Status = obj.Status;
                    
                    if(Status == '4')
					{
						$("#flash").removeClass('alert-danger');
						$("#flash").addClass('alert-success');
						$("#flash i").removeClass('fa-spinner');
						$("#flash i").removeClass('fa-spin');
						$("#flash i").removeClass('fa fa-fw fa-times-circle');
						$("#flash i").addClass('fa fa-fw fa-check-circle');
						$("#flash span").html('Invoice Update Successfully.');
						document.getElementById("Update-Invoice-Form").reset();
						$('#flash').delay(2000).fadeOut(500);
						setTimeout(function() {
						  window.location.href = "update_invoice.php?invoice_id=<?php echo $InvoiceID;?>";
						}, 1000);
						return true;
					}
					else if(Status == '3')
					{
						$("#flash").removeClass('alert-success');
						$("#flash").addClass('alert-danger');
						$("#flash i").removeClass('fa-spinner');
						$("#flash i").removeClass('fa-spin');
						$("#flash i").removeClass('fa fa-fw fa-check-circle');
						$("#flash i").addClass('fa fa-fw fa-times-circle');
						$("#flash span").html('Invoice Update Not Successfully.');
						$('#flash').delay(3000).fadeOut(500);
						return true;
					}
					else if(Status == '0')
					{
						$("#flash").removeClass('alert-success');
						$("#flash").addClass('alert-danger');
						$("#flash i").removeClass('fa-spinner');
						$("#flash i").removeClass('fa-spin');
						$("#flash i").removeClass('fa fa-fw fa-check-circle');
						$("#flash i").addClass('fa fa-fw fa-times-circle');
						$("#flash span").html('Invoice No Already Exists.');
						$('#flash').delay(3000).fadeOut(500);
						return true;
					}
				}
           	});
  		});
		</script>
	</body>
</html>
<?php
ob_flush();
?>