<?php
ob_start();
$Page = "Item"; 
require_once('head.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Xenon ERP</title>
    
    <!-- Favicon -->
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="assets/plugins/bootstrap/dist/css/bootstrap.css" rel="stylesheet" />

    <!-- Animate.css Css -->
    <link href="assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Font Awesome Css -->
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

    <!-- iCheck Css -->
    <link href="assets/plugins/iCheck/skins/flat/_all.css" rel="stylesheet" />

    <!-- Switchery Css -->
    <link href="assets/plugins/switchery/dist/switchery.css" rel="stylesheet" />

    <!-- Metis Menu Css -->
    <link href="assets/plugins/metisMenu/dist/metisMenu.css" rel="stylesheet" />

    <!-- Pace Loader Css -->
    <link href="assets/plugins/pace/themes/white/pace-theme-flash.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="assets/css/style.css" rel="stylesheet" />

    <!-- Jquery Datatables Css -->
    <link href="assets/plugins/DataTables/media/css/dataTables.bootstrap.css" rel="stylesheet" />
    
</head>
<body class="ls-fixed navbar-fixed">
    <div class="all-content-wrapper">
        <!-- Top Bar -->
        <header>
            <?php include_once('header.php'); ?>
        </header>
        <!-- #END# Top Bar -->
        <!-- Left Menu -->
        <aside class="sidebar">
            <?php include_once('menu.php'); ?>
        </aside>
        <!-- #END# Left Menu -->
        <section class="content dashboard">
            <div class="page-heading">
                <h1>
                    <a href="item.php">
                        Item
                    </a>
                    <small>
                        <i class="fa fa-angle-double-right"></i>
                        <a class="font-bold" href="#myModal_Add_Item" data-toggle="modal">Add New Item</a>
                    </small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="dashboard.php">Home</a></li>
                    <li><a href="item.php">Item</a></li>
                </ol>
            </div>
            
            <div class="page-body">
                <!-- -------------- ERROR SECTION START -------------- -->
                            
                <div id="flash" class="alert alert hidden">
                    <strong>
                        <i class="fa fa-spinner fa-spin"></i>
                    </strong>
                    &nbsp; &nbsp;
                    <span></span>
                </div>
                
                <!-- -------------- ERROR SECTION END -------------- -->

                <div class="panel panel-default">
                    <div class="panel-heading">Item</div>
                    <div class="panel-body">
                        <table id="Item-Table" class="table table-striped table-hover js-basic-example dataTable">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Item Name</th>
                                    <th>Item Alias</th>
                                    <th>Item Code</th>
                                    <th>HSN Code</th>
                                    <th>Rate</th>
                                    <th>Unit </th>
                                    <th>Initial Stock </th>
                                    <th>Min Stock </th>
                                    <th>Max Stock</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $SelectItem = "SELECT * FROM item_master WHERE company_id='".$CurrentCompanyID."'";
                                    $SelectItemQuery = mysqli_query($con,$SelectItem);
                                    if(!$SelectItemQuery)
                                    {
                                        die(mysqli_error($con));
                                    }
                                    $count = 1;
                                    while($Item = mysqli_fetch_array($SelectItemQuery))
                                    {
                                        $ItemID = $Item['item_id'];
										$CompanyID = $Item['company_id'];
                                        $ItemName = $Item['item_name'];
                                        $ItemAlias = $Item['item_alias'];
                                        $ItemCode = $Item['item_code'];
										$HSNCode = $Item['hsn_code'];
                                        $ItemRate = $Item['item_rate'];
                                        $ItemUnit = $Item['item_unit'];
                                        $InitialStock = $Item['initial_stock'];
                                        $MinStock = $Item['min_stock'];
                                        $MaxStock = $Item['max_stock'];
                                    ?>
                                    <tr>
                                        <td class="center"><?php echo $count; $count++; ?></td>
                                        <td><?php echo $ItemName;?></td>
                                        <td><?php echo $ItemAlias;?></td>
                                        <td><?php echo $ItemCode;?></td>
                                        <td><?php echo $HSNCode;?></td>
                                        <td><?php echo $ItemRate;?></td>
                                        <td><?php echo $ItemUnit;?></td>
                                        <td><?php echo $InitialStock;?></td>
                                        <td><?php echo $MinStock;?></td>
                                        <td><?php echo $MaxStock;?></td>
                                        <td>
                                            <div class="action-buttons">
                                                <a data-toggle="modal" href="#myModal_Update_Item_<?php echo $ItemID; ?>" class="col-success" title="Edit"><i class="fa fa-pencil-square-o"></i></a>
                                                <a data-toggle="modal" href="#myModel_Remove_Item_<?php echo $ItemID; ?>" class="col-danger" title="Remove"><i class="fa fa-trash-o"></i></a>
                                            </div>
<!-- ------------------------------ Remove Model Start ------------------------------ -->
<div class="modal fade" id="myModel_Remove_Item_<?php echo $ItemID; ?>" tabindex="-1" role="dialog" aria-labelledby="myModelLable" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form id="Remove-Item-<?php echo $ItemID; ?>-Form" class="form-horizontal" method="post" action="#">
				<div class="modal-header">
					<h4 class="modal-title" id="myModelLable">Remove This Item ?</h4>
					<input type="hidden" id="ItemID" name="ItemID" value="<?php echo $ItemID; ?>" required />
					<br/>
				</div>
				<div class="modal-footer">
					<button class="m-w-150 btn btn-danger" type="submit" id="Remove-Item-<?php echo $ItemID; ?>" onClick="return RemoveItem(this.id);">
						<i class="fa fa-trash bigger-110"></i>
						Remove
					</button>
					<button type="button" class="m-w-150 btn btn-default" data-dismiss="modal">
						<i class="fa fa-remove bigger-110"></i>
						Close
					</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- ------------------------------ Remove Model End ------------------------------ -->
<!-- ------------------------------ Update Modal Start ------------------------------- -->
<div class="modal fade" id="myModal_Update_Item_<?php echo $ItemID; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" style="padding-top:100px;">
		<div class="modal-content">
			<form id="Update-Item-<?php echo $ItemID; ?>-Form" method="post" action="#" class="form-horizontal" role="form">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Update Item Detail</h4>
			</div>
			<div class="modal-body">
				<div id="flash3" class="alert hidden">
					<strong>
						<i class="fa fa-spinner fa-spin"></i>
					</strong>
					&nbsp; &nbsp;
					<span></span>
				</div>
				<input type="hidden" id="AdminID" name="AdminID" value="<?php echo $_SESSION['AdminID']; ?>" required />
				<input type="hidden" id="CompanyID" name="CompanyID" value="<?php echo $CompanyID; ?>" required />
				<input type="hidden" id="ItemID" value="<?php echo $ItemID; ?>" required />
				<div class="form-group" style="width:100%; margin-top: 15px;">
					<label class="col-sm-4 control-label">Item Name</label>
					<div class="col-sm-6">
						<input type="text" id="ItemName" name="ItemName" value="<?php echo $ItemName; ?>" class="form-control" style="width:100%;" required />
					</div>
				</div>
				<div class="form-group" style="width:100%; margin-top: 15px;">
					<label class="col-sm-4 control-label">Item Alias</label>
					<div class="col-sm-6">
						<input type="text" id="ItemAlias" name="ItemAlias" value="<?php echo $ItemAlias; ?>" class="form-control" style="width:100%;" />
					</div>
				</div>
				<div class="form-group" style="width:100%; margin-top: 15px;">
					<label class="col-sm-4 control-label">Item Code</label>
					<div class="col-sm-6">
						<input type="text" id="ItemCode" name="ItemCode" value="<?php echo $ItemCode; ?>" class="form-control" style="width:100%;" />
					</div>
				</div>
				<div class="form-group" style="width:100%; margin-top: 15px;">
					<label class="col-sm-4 control-label">HSN Code</label>
					<div class="col-sm-6">
						<input type="text" id="HSNCode" name="HSNCode" value="<?php echo $HSNCode; ?>" class="form-control" style="width:100%;" />
					</div>
				</div>
				<div class="form-group" style="width:100%; margin-top: 15px;">
					<label class="col-sm-4 control-label">Item Rate</label>
					<div class="col-sm-6">
						<input type="text" id="ItemRate" name="ItemRate" onKeyPress="return NuMValidation(event);" value="<?php echo $ItemRate; ?>" class="form-control" style="width:100%;" /> 
					</div>
				</div>
				<div class="form-group" style="width:100%; margin-top: 15px;">
					<label class="col-sm-4 control-label">Item Unit</label>
					<div class="col-sm-6">
						<input type="text" id="ItemUnit" name="ItemUnit" value="<?php echo $ItemUnit; ?>" class="form-control" style="width:100%;" />
					</div>
				</div>
				<div class="form-group" style="width:100%; margin-top: 15px;">
					<label class="col-sm-4 control-label">Initial Stock</label>
					<div class="col-sm-6">
						<input type="text" id="InitialStock" name="InitialStock" value="<?php echo $InitialStock; ?>" onKeyPress="return NuMValidation(event);" class="form-control" style="width:100%;" />
					</div>
				</div>
				<div class="form-group" style="width:100%; margin-top: 15px;">
					<label class="col-sm-4 control-label">Min. Stock</label>
					<div class="col-sm-6">
						<input type="text" id="MinStock" name="MinStock" value="<?php echo $MinStock; ?>" class="form-control" style="width:100%;" />
					</div>
				</div>
				<div class="form-group" style="width:100%; margin-top: 15px;">
					<label class="col-sm-4 control-label">Max. Stock</label>
					<div class="col-sm-6">
						<input type="text" id="MaxStock" name="MaxStock" value="<?php echo $MaxStock; ?>" class="form-control" style="width:100%;" />
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-success" id="Update-Item-<?php echo $ItemID; ?>" onClick="return UpdateItem(this.id);">
					<i class="fa fa-refresh bigger-110"></i>
					Update
				</button>
				<button class="btn btn-default" data-dismiss="modal">
					<i class="fa fa-remove bigger-110"></i>
					Close
				</button>
			</div>
			</form>
		</div>
	</div>      
</div>
<!-- ------------------------------ Update Modal End --------------------------------- -->
                                        </td>
                                    </tr>
                                <?php
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
<!-- ------------------------------ Add New Modal Start ------------------------------- -->
<div class="modal fade" id="myModal_Add_Item" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" style="padding-top:50px;">
		<div class="modal-content">
			<form id="Add-Item-Form" class="form-horizontal" role="form">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Add New Item</h4>
			</div>
			<div class="modal-body">
				<div id="flash2" class="alert hidden">
					<strong>
						<i class="fa fa-spinner fa-spin"></i>
					</strong>
					&nbsp; &nbsp;
					<span></span>
				</div>
				<input type="hidden" id="AdminID" name="AdminID" value="<?php echo $_SESSION['AdminID']; ?>" required />
				<input type="hidden" id="CompanyID" name="CompanyID" value="<?php echo $CurrentCompanyID; ?>" required />
				<div class="form-group">
					<label class="col-sm-4 control-label">Item Name</label>
					<div class="col-sm-6">
						<input type="text" id="ItemName" name="ItemName" class="form-control" required />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">Item Alias</label>
					<div class="col-sm-6">
						<input type="text" id="ItemAlias" name="ItemAlias" class="form-control" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">Item Code</label>
					<div class="col-sm-6">
						<input type="text" id="ItemCode" name="ItemCode" class="form-control" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">HSN Code</label>
					<div class="col-sm-6">
						<input type="text" id="HSNCode" name="HSNCode" class="form-control" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">Item Rate</label>
					<div class="col-sm-6">
						<input type="text" id="ItemRate" name="ItemRate" onKeyPress="return NuMValidation(event);" class="form-control" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">Item Unit</label>
					<div class="col-sm-6">
						<input type="text" id="ItemUnit" name="ItemUnit" class="form-control" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">Initial Stock</label>
					<div class="col-sm-6">
						<input type="text" id="InitialStock" name="InitialStock" onKeyPress="return NuMValidation(event);" class="form-control" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">Min. Stock</label>
					<div class="col-sm-6">
						<input type="text" id="MinStock" name="MinStock" onKeyPress="return NuMValidation(event);" class="form-control" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">Max. Stock</label>
					<div class="col-sm-6">
						<input type="text" id="MaxStock" name="MaxStock" onKeyPress="return NuMValidation(event);" class="form-control" />
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-success">
					<i class="fa fa-check bigger-110"></i>
					Submit
				</button>
				<button class="btn btn-default" data-dismiss="modal">
					<i class="fa fa-remove bigger-110"></i>
					Close
				</button>
			</div>
			</form>
		</div>
	</div>
</div>
<!-- ------------------------------ Add New Modal End --------------------------------- -->
                </div>
             </div>
        </section>
        <!-- Footer -->
       
        <!-- #END# Footer -->
    </div>

    <!-- Jquery Core Js -->
    <script src="assets/plugins/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Pace Loader Js -->
    <script src="assets/plugins/pace/pace.js"></script>

    <!-- Screenfull Js -->
    <script src="assets/plugins/screenfull/src/screenfull.js"></script>

    <!-- Metis Menu Js -->
    <script src="assets/plugins/metisMenu/dist/metisMenu.js"></script>

    <!-- Jquery Slimscroll Js -->
    <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- JQuery Datatables Js -->
    <script src="assets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
    <script src="assets/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>

    <!-- Custom Js -->
    <script src="assets/js/admin.js"></script>
    
    <script src="assets/js/pages/tables/jquery-datatables.js"></script>
    <script src="assets/js/pages/ui/modals.js"></script>

    <script type="text/javascript">
        jQuery(function($) {
            
        });
        
        function NuMValidation(evt)
        {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
            return false;
    
            return true;
        }
    </script>
    
    <script type="text/javascript">
        $('#Add-Item-Form').on('submit', function(event) {
            
            event.preventDefault();
            
            var AdminID = $('#Add-Item-Form #AdminID').val();
			var CompanyID = $('#Add-Item-Form #CompanyID').val();
            var ItemName = $('#Add-Item-Form #ItemName').val();
            var ItemAlias = $('#Add-Item-Form #ItemAlias').val();
            var ItemCode = $('#Add-Item-Form #ItemCode').val();
			var HSNCode = $('#Add-Item-Form #HSNCode').val();
            var ItemRate = $('#Add-Item-Form #ItemRate').val();
            var ItemUnit = $('#Add-Item-Form #ItemUnit').val();
            var InitialStock = $('#Add-Item-Form #InitialStock').val();
            var MinStock = $('#Add-Item-Form #MinStock').val();
            var MaxStock = $('#Add-Item-Form #MaxStock').val();
            
            var Action = 'AddItem';
            
           
            var form_data = 'Action='+ Action +'&CompanyID='+ CompanyID +'&ItemName='+ ItemName +'&ItemAlias='+ ItemAlias +'&ItemCode='+ ItemCode +'&HSNCode='+ HSNCode +'&ItemRate='+ ItemRate +'&ItemUnit='+ ItemUnit +'&InitialStock='+ InitialStock +'&MinStock='+ MinStock +'&MaxStock='+ MaxStock +'&AdminID='+ AdminID; 
            //alert(form_data);
            $("#flash2").show();
            $("#flash2 i").addClass('fa-spinner');
            $("#flash2 i").addClass('fa-spin');
            $("#flash2").removeClass('hidden');
            $('html,body').animate({ scrollTop: $(".content").offset().top},'slow');
            $("#flash2 span").html('Please Wait...');
            
            $.ajax({
                type: 'POST',
                url: 'includes/item_script.php',
                data: form_data,
                cache: false,
                success: function(result)
                {
                    //alert(result);
                    var obj = JSON.parse(result);
                    var Status = obj.Status;
                    
                    if(Status == '2')
                    {
                        $("#flash2").removeClass('alert-danger');
                        $("#flash2").addClass('alert-success');
                        $("#flash2 i").removeClass('fa-spinner');
						$("#flash2 i").removeClass('fa-spin');
                        $("#flash2 i").removeClass('fa fa-fw fa-times-circle');
                        $("#flash2 i").addClass('fa fa-fw fa-check-circle');
                        $("#flash2 span").html('Item Insert Successfully.');
                        document.getElementById("Add-Item-Form").reset();
                        $('#flash2').delay(3000).fadeOut(500);
                        $('#myModal_Add_Item').delay(500).fadeOut(500);
                        setTimeout(function() {
                          window.location.href = "item.php";
                        }, 1000);
                        return true;
                    }
                    else if(Status == '1')
                    {
                        $("#flash2").removeClass('alert-success');
                        $("#flash2").addClass('alert-danger');
                        $("#flash2 i").removeClass('fa-spinner');
						$("#flash2 i").removeClass('fa-spin');
                        $("#flash2 i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash2 i").addClass('fa fa-fw fa-times-circle');
                        $("#flash2 span").html('Item Insert Not Successfully.');
                        $('#flash2').delay(3000).fadeOut(500);
                        return true;
                    }
                    else if(Status == '0')
                    {
                        $("#flash2").removeClass('alert-success');
                        $("#flash2").addClass('alert-danger');
                        $("#flash2 i").removeClass('fa-spinner');
						$("#flash2 i").removeClass('fa-spin');
                        $("#flash2 i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash2 i").addClass('fa fa-fw fa-times-circle');
                        $("#flash2 span").html('Item Code Already Exists.');
                        $('#flash2').delay(3000).fadeOut(500);
                        return true;
                    }
                    else
                    {
                        $("#flash2").removeClass('alert-success');
                        $("#flash2").addClass('alert-danger');
                        $("#flash2 i").removeClass('fa-spinner');
						$("#flash2 i").removeClass('fa-spin');
                        $("#flash2 i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash2 i").addClass('fa fa-fw fa-times-circle');
                        $("#flash2 span").html(Status);
                        $('#flash2').delay(4000).fadeOut(500);
                        return true;
                    }
                }
            });
        });
        
        function UpdateItem(btnId) {
            
            var form_id = btnId+'-Form'; 
        
            $('#'+form_id).on('submit', function(event) {
                
            event.preventDefault();
            
            var ItemID = $('#'+form_id+' #ItemID').val();
			var CompanyID = $('#'+form_id+' #CompanyID').val();
            var AdminID = $('#'+form_id+' #AdminID').val();
            var ItemName = $('#'+form_id+' #ItemName').val();
            var ItemAlias = $('#'+form_id+' #ItemAlias').val();
            var ItemCode = $('#'+form_id+' #ItemCode').val();
			var HSNCode = $('#'+form_id+' #HSNCode').val();
            var ItemRate = $('#'+form_id+' #ItemRate').val();
            var ItemUnit = $('#'+form_id+' #ItemUnit').val();
            var InitialStock = $('#'+form_id+' #InitialStock').val();
            var MinStock = $('#'+form_id+' #MinStock').val();
            var MaxStock = $('#'+form_id+' #MaxStock').val();
            
            var Action = 'UpdateItem';
            
            var form_data = 'Action='+ Action +'&ItemID='+ ItemID +'&CompanyID='+ CompanyID +'&ItemName='+ ItemName +'&ItemAlias='+ ItemAlias +'&ItemCode='+ ItemCode +'&HSNCode='+ HSNCode +'&ItemRate='+ ItemRate +'&ItemUnit='+ ItemUnit +'&InitialStock='+ InitialStock +'&MinStock='+ MinStock +'&MaxStock='+ MaxStock +'&AdminID='+ AdminID; 
            //alert(form_data);
           
			$("#"+form_id+" #flash3").show();
            $("#"+form_id+" #flash3 i").addClass('fa-spinner');
            $("#"+form_id+" #flash3 i").addClass('fa-spin');
            $("#"+form_id+" #flash3").removeClass('hidden');
            $('html,body').animate({ scrollTop: $(".content").offset().top},'slow');
            $("#"+form_id+" #flash3 span").html('Please Wait...');
            
            $.ajax({
                type: 'POST',
                url: 'includes/item_script.php',
                data: form_data,
                cache: false,
                success: function(result)
                {
                    //alert(result);
                    var obj = JSON.parse(result);
                    var Status = obj.Status;
                    
                    if(Status == '4')
                    {
                        $("#"+form_id+" #flash3").removeClass('alert-danger');
                        $("#"+form_id+" #flash3").addClass('alert-success');
                        $("#"+form_id+" #flash3 i").removeClass('fa-spinner');
						$("#"+form_id+" #flash3 i").removeClass('fa-spin');
                        $("#"+form_id+" #flash3 i").removeClass('fa fa-fw fa-times-circle');
                        $("#"+form_id+" #flash3 i").addClass('fa fa-fw fa-check-circle');
                        $("#"+form_id+" #flash3 span").html('Item Update Successfully.');
                        $("#"+form_id+" #flash3").delay(3000).fadeOut(500);
                        $('#myModal_Update_item_'+ItemID).delay(500).fadeOut(500);
                        setTimeout(function() {
                          window.location.href = "item.php";
                        }, 1000);
                        return true;
                    }
                    else if(Status == '3')
                    {
                        $("#"+form_id+" #flash3").removeClass('alert-success');
                        $("#"+form_id+" #flash3").addClass('alert-danger');
                        $("#"+form_id+" #flash3 i").removeClass('fa-spinner');
						$("#"+form_id+" #flash3 i").removeClass('fa-spin');
                        $("#"+form_id+" #flash3 i").removeClass('fa fa-fw fa-check-circle');
                        $("#"+form_id+" #flash3 i").addClass('fa fa-fw fa-times-circle');
                        $("#"+form_id+" #flash3 span").html('Item Update Not Successfully.');
                        $("#"+form_id+" #flash3").delay(3000).fadeOut(500);
                        return true;
                    }
                    else if(Status == '0')
                    {
                        $("#"+form_id+" #flash3").removeClass('alert-success');
                        $("#"+form_id+" #flash3").addClass('alert-danger');
                        $("#"+form_id+" #flash3 i").removeClass('fa-spinner');
						$("#"+form_id+" #flash3 i").removeClass('fa-spin');
                        $("#"+form_id+" #flash3 i").removeClass('fa fa-fw fa-check-circle');
                        $("#"+form_id+" #flash3 i").addClass('fa fa-fw fa-times-circle');
                        $("#"+form_id+" #flash3 span").html('Item Code Already Exists.');
                        $("#"+form_id+" #flash3").delay(3000).fadeOut(500);
                        return true;
                    }
                    else
                    {
                        $("#"+form_id+" #flash3").removeClass('alert-success');
                        $("#"+form_id+" #flash3").addClass('alert-danger');
                        $("#"+form_id+" #flash3 i").removeClass('fa-spinner');
						$("#"+form_id+" #flash3 i").removeClass('fa-spin');
                        $("#"+form_id+" #flash3 i").removeClass('fa fa-fw fa-check-circle');
                        $("#"+form_id+" #flash3 i").addClass('fa fa-fw fa-times-circle');
                        $("#"+form_id+" #flash3 span").html(Status);
                        $("#"+form_id+" #flash3").delay(4000).fadeOut(500);
                        return true;
                    }
                }
            });
            
            });
        }
    
        function RemoveItem(btnId) {
            
            var form_id = btnId+'-Form'; 
        
            $('#'+form_id).on('submit', function(event) {
            
            event.preventDefault();
            
            var ItemID = $('#'+form_id+' #ItemID').val();
            var Action = 'RemoveItem';
            
            var form_data = 'Action='+ Action +'&ItemID='+ ItemID; 
            
            $("#flash").show();
            $("#flash i").addClass('fa-spinner');
            $("#flash i").addClass('fa-spin');
            $("#flash").removeClass('hidden');
            $('html,body').animate({ scrollTop: $(".content").offset().top},'slow');
            $("#flash span").html('Please Wait...');
            //alert(form_data);
            $.ajax({
                type: 'POST',
                url: 'includes/item_script.php',
                data: form_data,
                cache: false,
                success: function(result)
                {
                    //alert(result);
                    var obj = JSON.parse(result);
                    var Status = obj.Status;
                    
                    if(Status == '6')
                    {
                        $('.modal').fadeOut();
                        $("#flash").removeClass('alert-danger');
                        $("#flash").addClass('alert-success');
                        $("#flash i").removeClass('fa-spinner');
						$("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-times-circle');
                        $("#flash i").addClass('fa fa-fw fa-check-circle');
                        $("#flash span").html('Item Detail Remove Successfully.');
                        $("#flash").delay(2000).fadeOut(500);
                        setTimeout(function() {
                          window.location.href = "item.php";
                        }, 1000);
                        return true;
                    }
                    else if(Status == '5')
                    {
                        $('.modal').fadeOut();
                        $("#flash").removeClass('alert-success');
                        $("#flash").addClass('alert-danger');
                        $("#flash i").removeClass('fa-spinner');
						$("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash i").addClass('fa fa-fw fa-times-circle');
                        $("#flash span").html('Item Detail Remove Not Successfully.');
                        $("#flash").delay(3000).fadeOut(500);
                        return true;
                    }
                    else if(Status == '0')
                    {
                        $('.modal').fadeOut();
                        $("#flash").removeClass('alert-success');
                        $("#flash").addClass('alert-danger');
                        $("#flash i").removeClass('fa-spinner');
						$("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash i").addClass('fa fa-fw fa-times-circle');
                        $("#flash span").html('Cannot Delete Item, There is a Reference Available For This Item.');
                        $("#flash").delay(3000).fadeOut(500);
                        return true;
                    }
                    else
                    {
                        $('.modal').fadeOut();
                        $("#flash").removeClass('alert-success');
                        $("#flash").addClass('alert-danger');
                        $("#flash i").removeClass('fa-spinner');
						$("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash i").addClass('fa fa-fw fa-times-circle');
                        $("#flash span").html(Status);
                        $('#flash').delay(4000).fadeOut(500);
                        return true;
                    }
                }
            });
            
            });
        }
    </script>
    </body>
</html>
<?php
ob_flush();
?>