﻿<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Xenon ERP</title>
    
    <!-- Favicon -->
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet"/>

    <!-- Bootstrap Core Css -->
    <link href="assets/plugins/bootstrap/dist/css/bootstrap.css" rel="stylesheet"/>

    <!-- Font Awesome Css -->
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>

    <!-- iCheck Css -->
    <link href="assets/plugins/iCheck/skins/square/_all.css" rel="stylesheet"/>

    <!-- Custom Css -->
    <link href="assets/css/style.css" rel="stylesheet"/>
</head>
<body class="sign-in-page">
    <div class="signin-form-area">
       	
        <h1><b>Xenon</b> - ERP</h1>
        <div class="signin-top-info">Powerd By Nirant Enterprise Pvt. Ltd.</div>
        <div class="row">
            <div class="col-sm-2 col-md-4 col-lg-4"></div>
            <div class="col-sm-8 col-md-4 col-lg-4 p-l-50 p-r-50">
                
                <form id="Login-Form" method="post" action="#">
                    <div class="form-group has-feedback">
                        
                            <input type="text" class="form-control" placeholder="Username" name="UserName" id="UserName" />
                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        
                    </div>
                    <div class="form-group has-feedback">
                        
                            <input type="password" class="form-control" placeholder="Password" name="Password" id="Password" />
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        
                    </div>
                    <div class="row">
						<div class="col-xs-12 text-center m-b-10">
							<span id="login_error" class="col-danger"></span>
							<span id="login_success" class="col-success"></span>
						</div>

						<div class="col-xs-offset-4 col-xs-4">
							<button type="submit" class="btn btn-success btn-block btn-flat">Sign In </button>
						</div>
                    </div>
                </form>
            </div>
            <div class="col-sm-2 col-md-4 col-lg-4"></div>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="assets/plugins/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="assets/plugins/bootstrap/dist/js/bootstrap.js"></script>

    <!-- iCheck Js -->
    <script src="assets/plugins/iCheck/icheck.js"></script>

    <!-- Jquery Validation Js -->
    <script src="assets/plugins/jquery-validation/dist/jquery.validate.js"></script>

    <!-- Custom Js -->
    <script src="assets/js/pages/examples/signin.js"></script>

    <script type="text/javascript">
        $('#Login-Form').on('submit', function(event) {
            
            event.preventDefault();
    
            var UserName = $("#UserName").val();
            var Password = $("#Password").val();

            if(UserName == '')
            {
                $('#login_success').fadeOut();
                    $("#login_error").html("Please Enter Username.");
                    $("#UserName").focus();
                    return false;
            }
            if(Password == '')
            {
                $('#login_success').fadeOut();
                    $("#login_error").html("Please Enter Password.");
                    $("#Password").focus();
                    return false;
            }
            
            var Action = "AdminLogin";
            var form_data = 'Action='+ Action +'&UserName='+ UserName +'&Password='+ Password;
            
            $('#login_success').fadeOut();
                $("#login_error").show();
                $("#login_error").fadeIn(400).html('<img src="assets/img/loading.gif" title="loading.gif">');
            
            $.ajax({
                url: "includes/login_script.php",
                type: 'POST',
                data: form_data,
                cache: false,
                processData:false,
                }).done(function(result) {
                    //alert(result);
                    var obj = JSON.parse(result);
                                            
                    var Status = obj.Status;
                    
                    if(Status == '1')
                    {
                        $('#login_success').fadeOut();
                            $("#login_error").html("Login Failed. Please Check Your Username And Password.");
                            $("#Username").focus();
                            return false;
                    }
                    else if(Status == '2')
                    {
                        $('#login_success').fadeOut();
                            $("#login_error").html("Login Failed. Account Does Not Exist.");
                            $("#Username").focus();
                            return false;
                    }
                    else if(Status == '3')
                    {
                        $('#login_success').fadeOut();
                            $("#login_error").html("Login Failed. Account Not Active.");
                            $("#Username").focus();
                            return false;
                    }
                    else if(Status == '4')
                    {
                        $('#login_success').fadeOut();
                            $("#login_error").html("Login Failed. Please Try Again Later.");
                            $("#Username").focus();
                            return false;
                    }
                    else if(Status == '5')
                    {
                        $('#login_error').fadeOut();
                            $("#login_success").show();
                            $("#login_success").html("Login Successful. Redirecting...");
                            window.location.assign("dashboard.php");
                            return true;
                    }
                    else
                    {
                        $('#login_success').fadeOut();
                            $("#login_error").html(Status);
                            $("#Username").focus();
                            return false;
                    }   
                });
            
        });
    </script>
</body>
</html>
