<?php
ob_start();
$Page = "Company"; 
require_once('head.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Xenon ERP</title>
    
    <!-- Favicon -->
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="assets/plugins/bootstrap/dist/css/bootstrap.css" rel="stylesheet" />

    <!-- Animate.css Css -->
    <link href="assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Font Awesome Css -->
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

    <!-- iCheck Css -->
    <link href="assets/plugins/iCheck/skins/flat/_all.css" rel="stylesheet" />

    <!-- Switchery Css -->
    <link href="assets/plugins/switchery/dist/switchery.css" rel="stylesheet" />

    <!-- Metis Menu Css -->
    <link href="assets/plugins/metisMenu/dist/metisMenu.css" rel="stylesheet" />

    <!-- Pace Loader Css -->
    <link href="assets/plugins/pace/themes/white/pace-theme-flash.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="assets/css/style.css" rel="stylesheet" />

    <!-- Jquery Datatables Css -->
    <link href="assets/plugins/DataTables/media/css/dataTables.bootstrap.css" rel="stylesheet" />
    
</head>
<body class="ls-fixed navbar-fixed">
    <div class="all-content-wrapper">
        <!-- Top Bar -->
        <header>
            <?php include_once('header.php'); ?>
        </header>
        <!-- #END# Top Bar -->
        <!-- Left Menu -->
        <aside class="sidebar">
            <?php include_once('menu.php'); ?>
        </aside>
        <!-- #END# Left Menu -->
        <section class="content dashboard">
            <div class="page-heading">
                <h1>
                    <a href="company.php">
                        Company
                    </a>
                    <small>
                        <i class="fa fa-angle-double-right"></i>
                        <a class="font-bold" href="#myModal_Add_Company" data-toggle="modal">Add New Company</a>
                    </small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="dashboard.php">Home</a></li>
                    <li class="active">Add Company</li>
                </ol>
            </div>
            <div class="page-body">
            <!-- -------------- ERROR SECTION START -------------- -->
                        
            <div id="flash" class="alert alert hidden">
                <strong>
                    <i class="fa fa-spinner fa-spin"></i>
                </strong>
                &nbsp; &nbsp;
                <span></span>
            </div>
            
            <!-- -------------- ERROR SECTION END -------------- -->

            
                <div class="panel panel-default">
                    <div class="panel-heading">Company</div>
                    <div class="panel-body">
                        <table id="Company-Table" class="table table-striped table-hover js-basic-example dataTable">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Company Name</th>
                                    <th>Company Alias</th>
                                    <th>Company Code</th>
                                    <th>Address</th>
                                    <th>Phone </th>
                                    <th>Mobile </th>
                                    <th>Email </th>
                                    <th>PAN No </th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $SelectCompany = mysqli_query($con,"SELECT * FROM company_master");
                                    if(!$SelectCompany)
                                    {
                                        die(mysqli_error($con));
                                    }
                                    $count = 1;
                                    while($Company = mysqli_fetch_array($SelectCompany))
                                    {
                                        $CompanyID = $Company['company_id'];
                                        $CompanyName = $Company['company_name'];
                                        $CompanyAlias = $Company['company_alias'];
                                        $CompanyCode = $Company['company_code'];
                                        $Address = $Company['address'];
                                        $Phone = $Company['phone'];
                                        $Mobile = $Company['mobile'];
                                        $Email = $Company['email'];
                                        $PANNO = $Company['pan_no'];
										$GSTIN = $Company['gstin'];
										$State = $Company['state'];
										$StateCode = $Company['state_code'];
										$BankName = $Company['bank_name'];
										$BankAcNo = $Company['bank_ac_no'];
										$BankIFSC = $Company['bank_ifsc'];
										$BankBranch = $Company['bank_branch'];
                                    ?>
                            
                                    <tr>
                                        <td class="center"><?php echo $count; $count++; ?></td>
                                        <td><?php echo $CompanyName;?></td>
                                        <td><?php echo $CompanyAlias;?></td>
                                        <td><?php echo $CompanyCode;?></td>
                                        <td><?php echo $Address;?></td>
                                        <td><?php echo $Phone;?></td>
                                        <td><?php echo $Mobile;?></td>
                                        <td><?php echo $Email;?></td>
                                        <td><?php echo $PANNO;?></td>
                                        <td>
                                            <div class="action-buttons">
                                                <a data-toggle="modal" href="#myModal_Update_Company_<?php echo $CompanyID; ?>">
                                                    <i class="fa fa-pencil text-success m-r-10 col-success" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Update"></i>
                                                </a>
                                                <a data-toggle="modal" href="#myModel_Remove_Company_<?php echo $CompanyID; ?>" class="red tooltip-error" data-rel="tooltip" title="Remove">
                                                    <i class="fa fa-trash-o text-danger m-r-10 col-danger" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Delete"></i>
                                                </a>
                                            </div>
<!-- ------------------------------ Remove Model Start ------------------------------ -->
    <div class="modal fade" id="myModel_Remove_Company_<?php echo $CompanyID; ?>" tabindex="-1" role="dialog" aria-labelledby="myModelLable" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="Remove-Company-<?php echo $CompanyID; ?>-Form" class="form-horizontal" method="post" action="#">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModelLable">Remove This Company ?</h4>
                        <input type="hidden" id="CompanyID" name="CompanyID" value="<?php echo $CompanyID; ?>" required />
                        <br/>
                    </div>
                    <div class="modal-footer">
                        <button class="m-w-150 btn btn-danger" type="submit" id="Remove-Company-<?php echo $CompanyID; ?>" onClick="return RemoveCompany(this.id);">
                            <i class="fa fa-trash bigger-110"></i>
                            Remove
                        </button>
                        <button type="button" class="m-w-150 btn btn-default" data-dismiss="modal">
                            <i class="fa fa-remove bigger-110"></i>
                            Close
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>


 <!-- ------------------------------ Remove Model End ------------------------------ -->
 <!-- ------------------------------ Update Modal Start ------------------------------- -->
    <div class="modal fade" id="myModal_Update_Company_<?php echo $CompanyID; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="padding-top:100px;">
            <div class="modal-content">
                <form id="Update-Company-<?php echo $CompanyID; ?>-Form" method="post" action="#" class="form-horizontal" role="form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Update Company Detail</h4>
                </div>
                <div class="modal-body">
                    <div id="flash3" class="alert hidden">
                        <strong>
                            <i class="fa fa-spinner fa-spin"></i>
                        </strong>
                        &nbsp; &nbsp;
                        <span></span>
                    </div>
                    <input type="hidden" id="AdminID" name="AdminID" value="<?php echo $_SESSION['AdminID']; ?>" required />
                    <input type="hidden" id="CompanyID" value="<?php echo $CompanyID; ?>" required />
                    <div class="form-group" style="width:100%; margin-top: 15px;">
                        <label class="col-sm-4 control-label">Company Name</label>
                        <div class="col-sm-6">
                            <input type="text" id="CompanyName" name="CompanyName" value="<?php echo $CompanyName; ?>" class="form-control" style="width:100%;" required />
                        </div>
                    </div>
                    <div class="form-group" style="width:100%; margin-top: 15px;">
                        <label class="col-sm-4 control-label">Company Alias</label>
                        <div class="col-sm-6">
                            <input type="text" id="CompanyAlias" name="CompanyAlias" value="<?php echo $CompanyAlias; ?>" class="form-control" style="width:100%;" />
                        </div>
                    </div>
                    <div class="form-group" style="width:100%; margin-top: 15px;">
                        <label class="col-sm-4 control-label">Company Code</label>
                        <div class="col-sm-6">
                            <input type="text" id="CompanyCode" name="CompanyCode" value="<?php echo $CompanyCode; ?>" class="form-control" style="width:100%;" />
                        </div>
                    </div>
                    <div class="form-group" style="width:100%; margin-top: 15px;">
                        <label class="col-sm-4 control-label">Address</label>
                        <div class="col-sm-6">
                            <textarea id="Address" name="Address" class="form-control" style="width:100%;"><?php echo $Address; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group" style="width:100%; margin-top: 15px;">
                        <label class="col-sm-4 control-label">Phone</label>
                        <div class="col-sm-6">
                            <input type="text" id="Phone" name="Phone" value="<?php echo $Phone; ?>" onKeyPress="return NuMValidation(event);" class="form-control" style="width:100%;" />
                        </div>
                    </div>
                    <div class="form-group" style="width:100%; margin-top: 15px;">
                        <label class="col-sm-4 control-label">Mobile</label>
                        <div class="col-sm-6">
                            <input type="text" id="Mobile" name="Mobile" value="<?php echo $Mobile; ?>" onKeyPress="return NuMValidation(event);" class="form-control" style="width:100%;" />
                        </div>
                    </div>
                    <div class="form-group" style="width:100%; margin-top: 15px;">
                        <label class="col-sm-4 control-label">Email</label>
                        <div class="col-sm-6">
                            <input type="email" id="Email" name="Email" value="<?php echo $Email; ?>" class="form-control" style="width:100%;" />
                        </div>
                    </div>
                    <div class="form-group" style="width:100%; margin-top: 15px;">
                        <label class="col-sm-4 control-label">GSTIN</label>
                        <div class="col-sm-6">
                            <input type="text" id="GSTIN" name="GSTIN" value="<?php echo $GSTIN; ?>" class="form-control" style="width:100%;" />
                        </div>
                    </div>
                    <div class="form-group" style="width:100%; margin-top: 15px;">
                        <label class="col-sm-4 control-label">PAN NO</label>
                        <div class="col-sm-6">
                            <input type="text" id="PANNO" name="PANNO" value="<?php echo $PANNO; ?>" class="form-control" style="width:100%;" />
                        </div>
                    </div>
                    <div class="form-group" style="width:100%; margin-top: 15px;">
                        <label class="col-sm-4 control-label">State</label>
                        <div class="col-sm-6">
                            <input type="text" id="State" name="State" value="<?php echo $State; ?>" class="form-control" style="width:100%;" />
                        </div>
                    </div>
                    <div class="form-group" style="width:100%; margin-top: 15px;">
                        <label class="col-sm-4 control-label">State Code</label>
                        <div class="col-sm-6">
                            <input type="text" id="StateCode" name="StateCode" value="<?php echo $StateCode; ?>" class="form-control" style="width:100%;" />
                        </div>
                    </div>
                    <div class="form-group" style="width:100%; margin-top: 15px;">
                        <label class="col-sm-4 control-label">Bank Name</label>
                        <div class="col-sm-6">
                            <input type="text" id="BankName" name="BankName" value="<?php echo $BankName; ?>" class="form-control" style="width:100%;" />
                        </div>
                    </div>
                    <div class="form-group" style="width:100%; margin-top: 15px;">
                        <label class="col-sm-4 control-label">Bank Ac No</label>
                        <div class="col-sm-6">
                            <input type="text" id="BankAcNo" name="BankAcNo" value="<?php echo $BankAcNo; ?>" class="form-control" style="width:100%;" />
                        </div>
                    </div>
                    <div class="form-group" style="width:100%; margin-top: 15px;">
                        <label class="col-sm-4 control-label">Bank IFSC</label>
                        <div class="col-sm-6">
                            <input type="text" id="BankIFSC" name="BankIFSC" value="<?php echo $BankIFSC; ?>" class="form-control" style="width:100%;" />
                        </div>
                    </div>
                    <div class="form-group" style="width:100%; margin-top: 15px;">
                        <label class="col-sm-4 control-label">Bank Branch</label>
                        <div class="col-sm-6">
                            <input type="text" id="BankBranch" name="BankBranch" value="<?php echo $BankBranch; ?>" class="form-control" style="width:100%;" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-success" id="Update-Company-<?php echo $CompanyID; ?>" onClick="return UpdateCompany(this.id);">
                        <i class="fa fa-refresh bigger-110"></i>
                        Update
                    </button>
                    <button class="btn btn-sm btn-default" data-dismiss="modal">
                        <i class="fa fa-remove bigger-110"></i>
                        Close
                    </button>
                </div>
                </form>
            </div>
        </div>      
    </div>
<!-- ------------------------------ Update Modal End --------------------------------- -->
                                        </td>
                                    </tr>
                                <?php
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
<!-- ------------------------------ Add New Modal Start ------------------------------- -->
    <div class="modal fade" id="myModal_Add_Company" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="padding-top:50px;">
            <div class="modal-content">
                <form id="Add-Company-Form" class="form-horizontal" role="form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Add New Company</h4>
                </div>
                <div class="modal-body">
                    <div id="flash2" class="alert hidden">
                        <strong>
                            <i class="ace-icon fa fa-spinner"></i>
                        </strong>
                        &nbsp; &nbsp;
                        <span></span>
                    </div>
                    <input type="hidden" id="AdminID" name="AdminID" value="<?php echo $_SESSION['AdminID']; ?>" required />
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Company Name</label>
                        <div class="col-sm-6">
                            <input type="text" id="CompanyName" name="CompanyName" class="form-control" required />
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Company Alias</label>
                        <div class="col-sm-6">
                            <input type="text" id="CompanyAlias" name="CompanyAlias" class="form-control" />
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Company Code</label>
                        <div class="col-sm-6">
                            <input type="text" id="CompanyCode" name="CompanyCode" class="form-control" />
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Address</label>
                        <div class="col-sm-6">
                            <textarea id="Address" name="Address" class="form-control" ></textarea>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Phone</label>
                        <div class="col-sm-6">
                            <input type="text" id="Phone" name="Phone" onKeyPress="return NuMValidation(event);" class="form-control" />
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Mobile</label>
                        <div class="col-sm-6">
                            <input type="text" id="Mobile" name="Mobile" onKeyPress="return NuMValidation(event);" class="form-control" />
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Email</label>
                        <div class="col-sm-6">
                            <input type="email" id="Email" name="Email" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">GSTIN</label>
                        <div class="col-sm-6">
                            <input type="text" id="GSTIN" name="GSTIN" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">PAN NO</label>
                        <div class="col-sm-6">
                            <input type="text" id="PANNO" name="PANNO" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">State</label>
                        <div class="col-sm-6">
                            <input type="text" id="State" name="State" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">State Code</label>
                        <div class="col-sm-6">
                            <input type="text" id="StateCode" name="StateCode" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Bank Name</label>
                        <div class="col-sm-6">
                            <input type="text" id="BankName" name="BankName" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Bank Ac No</label>
                        <div class="col-sm-6">
                            <input type="text" id="BankAcNo" name="BankAcNo" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Bank IFSC</label>
                        <div class="col-sm-6">
                            <input type="text" id="BankIFSC" name="BankIFSC" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Bank Branch</label>
                        <div class="col-sm-6">
                            <input type="text" id="BankBranch" name="BankBranch" class="form-control" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-success">
                        <i class="fa fa-check bigger-110"></i>
                        Submit
                    </button>
                    <button class="btn btn-sm btn-default" data-dismiss="modal">
                        <i class="fa fa-remove bigger-110"></i>
                        Close
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div>
<!-- ------------------------------ Add New Modal End --------------------------------- -->
                </div>
             </div>
        </section>
        <!-- Footer -->
       
        <!-- #END# Footer -->
    </div>

    <!-- Jquery Core Js -->
    <script src="assets/plugins/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Pace Loader Js -->
    <script src="assets/plugins/pace/pace.js"></script>

    <!-- Screenfull Js -->
    <script src="assets/plugins/screenfull/src/screenfull.js"></script>

    <!-- Metis Menu Js -->
    <script src="assets/plugins/metisMenu/dist/metisMenu.js"></script>

    <!-- Jquery Slimscroll Js -->
    <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Idle Timer Js -->
    <script src="assets/plugins/jquery-idletimer/dist/idle-timer.js"></script>

    <!-- JQuery Datatables Js -->
    <script src="assets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
    <script src="assets/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>

    <!-- Custom Js -->
    <script src="assets/js/admin.js"></script>
    <script src="assets/js/pages/miscellaneous/idle-timer.js"></script>
    <script src="assets/js/pages/tables/jquery-datatables.js"></script>
    <script src="assets/js/pages/ui/modals.js"></script>

    <script type="text/javascript">
        jQuery(function($) {
            
        });
        
        function NuMValidation(evt)
        {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
            return false;
    
            return true;
        }
    </script>
    
    <script type="text/javascript">
        $('#Add-Company-Form').on('submit', function(event) {
            
            event.preventDefault();
            
            var AdminID = $('#Add-Company-Form #AdminID').val();
            var CompanyName = $('#Add-Company-Form #CompanyName').val();
            var CompanyAlias = $('#Add-Company-Form #CompanyAlias').val();
            var CompanyCode = $('#Add-Company-Form #CompanyCode').val();
            var Address = $('#Add-Company-Form #Address').val();
            var Phone = $('#Add-Company-Form #Phone').val();
            var Mobile = $('#Add-Company-Form #Mobile').val();
            var Email = $('#Add-Company-Form #Email').val();
			var GSTIN = $('#Add-Company-Form #GSTIN').val();
            var PANNO = $('#Add-Company-Form #PANNO').val();
            var State = $('#Add-Company-Form #State').val();
			var StateCode = $('#Add-Company-Form #StateCode').val();
			var BankName = $('#Add-Company-Form #BankName').val();
			var BankAcNo = $('#Add-Company-Form #BankAcNo').val();
			var BankIFSC = $('#Add-Company-Form #BankIFSC').val();
			var BankBranch = $('#Add-Company-Form #BankBranch').val();
            
            var Action = 'AddCompany';
            
           
            var form_data = 'Action='+ Action +'&CompanyName='+ CompanyName +'&CompanyAlias='+ CompanyAlias +'&CompanyCode='+ CompanyCode +'&Address='+ Address +'&Phone='+ Phone +'&Mobile='+ Mobile +'&Email='+ Email +'&PANNO='+ PANNO +'&GSTIN='+ GSTIN +'&AdminID='+ AdminID +'&State='+ State +'&StateCode='+ StateCode +'&BankName='+ BankName +'&BankAcNo='+ BankAcNo +'&BankIFSC='+ BankIFSC +'&BankBranch='+ BankBranch; 
            //alert(form_data);
            $("#flash2").show();
            $("#flash2 i").addClass('fa-spinner');
            $("#flash2 i").addClass('fa-spin');
            $("#flash2").removeClass('hidden');
            $('html,body').animate({ scrollTop: $(".content").offset().top},'slow');
            $("#flash2 span").html('Please Wait...');
            
            $.ajax({
                type: 'POST',
                url: 'includes/company_script.php',
                data: form_data,
                cache: false,
                success: function(result)
                {
                    //alert(result);
                    var obj = JSON.parse(result);
                    var Status = obj.Status;
                    
                    if(Status == '2')
                    {
                        $("#flash2").removeClass('alert-danger');
                        $("#flash2").addClass('alert-success');
                        $("#flash2 i").removeClass('fa-spinner');
						$("#flash2 i").removeClass('fa-spin');
                        $("#flash2 i").removeClass('fa fa-fw fa-times-circle');
                        $("#flash2 i").addClass('fa fa-fw fa-check-circle');
                        $("#flash2 span").html('Company Insert Successfully.');
                        document.getElementById("Add-Company-Form").reset();
                        $('#flash2').delay(3000).fadeOut(500);
                        $('#myModal_Add_Company').delay(500).fadeOut(500);
                        setTimeout(function() {
                          window.location.href = "companies.php";
                        }, 1000);
                        return true;
                    }
                    else if(Status == '1')
                    {
                        $("#flash2").removeClass('alert-success');
                        $("#flash2").addClass('alert-danger');
                        $("#flash2 i").removeClass('fa-spinner');
						$("#flash2 i").removeClass('fa-spin');
                        $("#flash2 i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash2 i").addClass('fa fa-fw fa-times-circle');
                        $("#flash2 span").html('Company Insert Not Successfully.');
                        $('#flash2').delay(3000).fadeOut(500);
                        return true;
                    }
                    else if(Status == '0')
                    {
                        $("#flash2").removeClass('alert-success');
                        $("#flash2").addClass('alert-danger');
                        $("#flash2 i").removeClass('fa-spinner');
						$("#flash2 i").removeClass('fa-spin');
                        $("#flash2 i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash2 i").addClass('fa fa-fw fa-times-circle');
                        $("#flash2 span").html('Company Code Already Exists.');
                        $('#flash2').delay(3000).fadeOut(500);
                        return true;
                    }
                    else
                    {
                        $("#flash2").removeClass('alert-success');
                        $("#flash2").addClass('alert-danger');
                        $("#flash2 i").removeClass('fa-spinner');
						$("#flash2 i").removeClass('fa-spin');
                        $("#flash2 i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash2 i").addClass('fa fa-fw fa-times-circle');
                        $("#flash2 span").html(Status);
                        $('#flash2').delay(4000).fadeOut(500);
                        return true;
                    }
                }
            });
        });
        
        function UpdateCompany(btnId) {
            
            var form_id = btnId+'-Form'; 
        
            $('#'+form_id).on('submit', function(event) {
                
            event.preventDefault();
            
            var CompanyID = $('#'+form_id+' #CompanyID').val();
            var AdminID = $('#'+form_id+' #AdminID').val();
            var CompanyName = $('#'+form_id+' #CompanyName').val();
            var CompanyAlias = $('#'+form_id+' #CompanyAlias').val();
            var CompanyCode = $('#'+form_id+' #CompanyCode').val();
            var Address = $('#'+form_id+' #Address').val();
            var Phone = $('#'+form_id+' #Phone').val();
            var Mobile = $('#'+form_id+' #Mobile').val();
            var Email = $('#'+form_id+' #Email').val();
            var GSTIN = $('#'+form_id+' #GSTIN').val();
            var PANNO = $('#'+form_id+' #PANNO').val();
            var State = $('#'+form_id+' #State').val();
			var StateCode = $('#'+form_id+' #StateCode').val();
			var BankName = $('#'+form_id+' #BankName').val();
			var BankAcNo = $('#'+form_id+' #BankAcNo').val();
			var BankIFSC = $('#'+form_id+' #BankIFSC').val();
			var BankBranch = $('#'+form_id+' #BankBranch').val();
            
            var Action = 'UpdateCompany';
            
            var form_data = 'Action='+ Action +'&CompanyID='+ CompanyID +'&CompanyName='+ CompanyName +'&CompanyAlias='+ CompanyAlias +'&CompanyCode='+ CompanyCode +'&Address='+ Address +'&Phone='+ Phone +'&Mobile='+ Mobile +'&Email='+ Email +'&PANNO='+ PANNO +'&GSTIN='+ GSTIN +'&AdminID='+ AdminID +'&State='+ State +'&StateCode='+ StateCode +'&BankName='+ BankName +'&BankAcNo='+ BankAcNo +'&BankIFSC='+ BankIFSC +'&BankBranch='+ BankBranch; 
            //alert(form_data);
            $("#"+form_id+" #flash3").show();
            $("#"+form_id+" #flash3 i").addClass('fa-spinner');
            $("#"+form_id+" #flash3 i").addClass('fa-spin');
            $("#"+form_id+" #flash3").removeClass('hidden');
            $('html,body').animate({ scrollTop: $(".content").offset().top},'slow');
            $("#"+form_id+" #flash3 span").html('Please Wait...');
            
            $.ajax({
                type: 'POST',
                url: 'includes/company_script.php',
                data: form_data,
                cache: false,
                success: function(result)
                {
                    //alert(result);
                    var obj = JSON.parse(result);
                    var Status = obj.Status;
                    
                    if(Status == '4')
                    {
                        $("#"+form_id+" #flash3").removeClass('alert-danger');
                        $("#"+form_id+" #flash3").addClass('alert-success');
                        $("#"+form_id+" #flash3 i").removeClass('fa-spinner');
						$("#"+form_id+" #flash3 i").removeClass('fa-spin');
                        $("#"+form_id+" #flash3 i").removeClass('fa fa-fw fa-times-circle');
                        $("#"+form_id+" #flash3 i").addClass('fa fa-fw fa-check-circle');
                        $("#"+form_id+" #flash3 span").html('Company Update Successfully.');
                        $("#"+form_id+" #flash3").delay(3000).fadeOut(500);
                        $('#myModal_Update_company_'+CompanyID).delay(500).fadeOut(500);
                        setTimeout(function() {
                          window.location.href = "companies.php";
                        }, 1000);
                        return true;
                    }
                    else if(Status == '3')
                    {
                        $("#"+form_id+" #flash3").removeClass('alert-success');
                        $("#"+form_id+" #flash3").addClass('alert-danger');
                        $("#"+form_id+" #flash3 i").removeClass('fa-spinner');
						$("#"+form_id+" #flash3 i").removeClass('fa-spin');
                        $("#"+form_id+" #flash3 i").removeClass('fa fa-fw fa-check-circle');
                        $("#"+form_id+" #flash3 i").addClass('fa fa-fw fa-times-circle');
                        $("#"+form_id+" #flash3 span").html('Company Update Not Successfully.');
                        $("#"+form_id+" #flash3").delay(3000).fadeOut(500);
                        return true;
                    }
                    else if(Status == '0')
                    {
                        $("#"+form_id+" #flash3").removeClass('alert-success');
                        $("#"+form_id+" #flash3").addClass('alert-danger');
                        $("#"+form_id+" #flash3 i").removeClass('fa-spinner');
                        $("#"+form_id+" #flash3 i").removeClass('fa fa-fw fa-check-circle');
                        $("#"+form_id+" #flash3 i").addClass('fa fa-fw fa-times-circle');
                        $("#"+form_id+" #flash3 span").html('Company Code Already Exists.');
                        $("#"+form_id+" #flash3").delay(3000).fadeOut(500);
                        return true;
                    }
                    else
                    {
                        $("#"+form_id+" #flash3").removeClass('alert-success');
                        $("#"+form_id+" #flash3").addClass('alert-danger');
                        $("#"+form_id+" #flash3 i").removeClass('fa-spinner');
						$("#"+form_id+" #flash3 i").removeClass('fa-spin');
                        $("#"+form_id+" #flash3 i").removeClass('fa fa-fw fa-check-circle');
                        $("#"+form_id+" #flash3 i").addClass('fa fa-fw fa-times-circle');
                        $("#"+form_id+" #flash3 span").html(Status);
                        $("#"+form_id+" #flash3").delay(4000).fadeOut(500);
                        return true;
                    }
                }
            });
            
            });
        }
    
        function RemoveCompany(btnId) {
            
            var form_id = btnId+'-Form'; 
        
            $('#'+form_id).on('submit', function(event) {
            
            event.preventDefault();
            
            var CompanyID = $('#'+form_id+' #CompanyID').val();
            var Action = 'RemoveCompany';
            
            var form_data = 'Action='+ Action +'&CompanyID='+ CompanyID; 
            
            $("#flash").show();
            $("#flash i").addClass('fa-spinner');
            $("#flash i").addClass('fa-spin');
            $("#flash").removeClass('hidden');
            $('html,body').animate({ scrollTop: $(".content").offset().top},'slow');
            $("#flash span").html('Please Wait...');
            //alert(form_data);
            $.ajax({
                type: 'POST',
                url: 'includes/company_script.php',
                data: form_data,
                cache: false,
                success: function(result)
                {
                    //alert(result);
                    var obj = JSON.parse(result);
                    var Status = obj.Status;
                    
                    if(Status == '6')
                    {
                        $('.modal').fadeOut();
                        $("#flash").removeClass('alert-danger');
                        $("#flash").addClass('alert-success');
                        $("#flash i").removeClass('fa-spinner');
						$("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-times-circle');
                        $("#flash i").addClass('fa fa-fw fa-check-circle');
                        $("#flash span").html('Company Detail Remove Successfully.');
                        $("#flash").delay(2000).fadeOut(500);
                        setTimeout(function() {
                          window.location.href = "companies.php";
                        }, 1000);
                        return true;
                    }
                    else if(Status == '5')
                    {
                        $('.modal').fadeOut();
                        $("#flash").removeClass('alert-success');
                        $("#flash").addClass('alert-danger');
                        $("#flash i").removeClass('fa-spinner');
						$("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash i").addClass('fa fa-fw fa-times-circle');
                        $("#flash span").html('Company Detail Remove Not Successfully.');
                        $("#flash").delay(3000).fadeOut(500);
                        return true;
                    }
                    else if(Status == '0')
                    {
                        $('.modal').fadeOut();
                        $("#flash").removeClass('alert-success');
                        $("#flash").addClass('alert-danger');
                        $("#flash i").removeClass('fa-spinner');
						$("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash i").addClass('fa fa-fw fa-times-circle');
                        $("#flash span").html('Cannot Delete Company, There is a Reference Available For This Company.');
                        $("#flash").delay(3000).fadeOut(500);
                        return true;
                    }
                    else
                    {
                        $('.modal').fadeOut();
                        $("#flash").removeClass('alert-success');
                        $("#flash").addClass('alert-danger');
                        $("#flash i").removeClass('fa-spinner');
						$("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash i").addClass('fa fa-fw fa-times-circle');
                        $("#flash span").html(Status);
                        $('#flash').delay(4000).fadeOut(500);
                        return true;
                    }
                }
            });
            
            });
        }
    </script>
        
        
    </body>
</html>
<?php
ob_flush();
?>