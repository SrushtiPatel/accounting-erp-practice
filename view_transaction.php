<?php
ob_start();
$Page = "ViewAllTransaction"; 
require_once('head.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Xenon ERP</title>
    
    <!-- Favicon -->
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="assets/plugins/bootstrap/dist/css/bootstrap.css" rel="stylesheet" />

    <!-- Bootstrap DateRangePicker Css -->
    <link href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />

    <!-- Animate.css Css -->
    <link href="assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Font Awesome Css -->
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

    <!-- iCheck Css -->
    <link href="assets/plugins/iCheck/skins/flat/_all.css" rel="stylesheet" />

    <!-- Switchery Css -->
    <link href="assets/plugins/switchery/dist/switchery.css" rel="stylesheet" />

    <!-- Metis Menu Css -->
    <link href="assets/plugins/metisMenu/dist/metisMenu.css" rel="stylesheet" />

    <!-- Pace Loader Css -->
    <link href="assets/plugins/pace/themes/white/pace-theme-flash.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="assets/css/style.css" rel="stylesheet" />
    
</head>
<body class="ls-fixed navbar-fixed">
    <div class="all-content-wrapper">
        <!-- Top Bar -->
        <header>
            <?php include_once('header.php'); ?>
        </header>
        <?php
            if(isset($_GET['banking_id']))
            {
                $BankingID = $_GET['banking_id'];
                $SelectBanking = "SELECT * FROM banking_master WHERE banking_id='".$BankingID."'";
                $SelectBankingQuery = mysqli_query($con,$SelectBanking);
                if(!$SelectBankingQuery)
                {
                    //die(mysqli_error($con));
                    header("Location: view_all_transactions.php");
                    exit();
                }
                $count = mysqli_num_rows($SelectBankingQuery);
                if($count != 1)
                {
                    header("Location: view_all_transactions.php");
                    exit();
                }
                
                $Banking = mysqli_fetch_array($SelectBankingQuery);
                
                $BVNo = $Banking['banking_id'];
                $TransactionDate = $Banking['transaction_date'];
                $PayFrom = $Banking['pay_from'];
                $PayTo = $Banking['pay_to'];
                $Amount = $Banking['amount'];
                $Description = $Banking['description'];

				$DebitBankAccount = $ledgerObject->selectLedgerDetail($PayFrom);
				$DebitBankAccount = json_decode($DebitBankAccount,true);
				$PayFromName = $DebitBankAccount['LedgerName'];

				$CreditBankAccount = $ledgerObject->selectLedgerDetail($PayTo);
				$CreditBankAccount = json_decode($CreditBankAccount,true);
				$PayToName = $CreditBankAccount['LedgerName'];
				
                $AddedBy = '';
                $AddedDate = '';
                $ModifiedBy = '';
                $ModifiedDate = '';

                if(!empty($Banking['addedby']))
                {
                    $AddedBy = $profileObject->selectUserName($Banking['addedby']);
                    $AddedDate = '( '.date("h:i:s A d-m-Y",strtotime($Banking['addeddate'])).' )';
                }
                if(!empty($Banking['modifiedby']))
                {
                    $ModifiedBy = $profileObject->selectUserName($Banking['modifiedby']);
                    $ModifiedDate = '( '.date("h:i:s A d-m-Y",strtotime($Banking['modifieddate'])).' )';
                }
            }
            else
            {
                header("Location: view_all_transactions.php");
                exit();
            }
        ?>
        <!-- #END# Top Bar -->
        <!-- Left Menu -->
        <aside class="sidebar">
            <?php include_once('menu.php'); ?>
        </aside>
        <!-- #END# Left Menu -->
        <section class="content">
            <div class="page-heading">
                <h1>
                	<a href="view_all_transactions.php">Transactions</a>
					<small>
						<i class="fa fa-angle-double-right"></i>
						<a class="font-bold" href="view_transaction.php?banking_id=<?php echo $BankingID; ?>">View Transaction Detail</a>
					</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="dashboard.php">Home</a></li>
                    <li><a href="view_all_transactions.php">Transaction</a></li>
                </ol>
            </div>
            
            <div class="page-body">
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">Transaction Details</div>
                            <div class="panel-body">
                                <div class="row">
                                   	<div class="col-xs-12 col-sm-12">
										<table class="table table-bordered table-striped table-hover">
											<tbody>
												<tr>
													<th width="40%">Banking Voucher No</th>
													<td width="60%"><?php echo $BVNo; ?></td>
												</tr>
												<tr>
													<th>Transaction Date</th>
													<td><?php echo $TransactionDate; ?></td>
												</tr>
												<tr>
													<th>Pay From</th>
													<td><?php echo $PayFromName; ?></td>
												</tr>
												<tr>
													<th>Pay To</th>
													<td><?php echo $PayToName; ?></td>
												</tr>
												<tr>
													<th>Amount</th>
													<td><?php echo $Amount; ?></td>
												</tr>
												<tr>
													<th>Description</th>
													<td><?php echo $Description; ?></td>
												</tr>
												<tr>
													<th>AddedBy</th>
													<td><?php echo $AddedBy; ?></td>
												</tr>
												<tr>
													<th>AddedDate</th>
													<td><?php echo $AddedDate; ?></td>
												</tr>
												<tr>
													<th>ModifiedBy</th>
													<td><?php echo $ModifiedBy; ?></td>
												</tr>
												<tr>
													<th>ModifiedDate</th>
													<td><?php echo $ModifiedDate; ?></td>
												</tr>
											</tbody>
										</table>
									</div>
                                </div>
                                <div class="form-group">
                                    <div class="text-center">
                                        <a class="btn btn-sm btn-success" href="update_transaction.php?banking_id=<?php echo $BankingID; ?>">
                                            <i class="fa fa-refresh bigger-110"></i>
                                            Update Transaction Detail
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Footer -->
        
        <!-- #END# Footer -->
    </div>

    <!-- Jquery Core Js -->
    <script src="assets/plugins/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Bootstrap DateRangePicker Js -->
    <script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Pace Loader Js -->
    <script src="assets/plugins/pace/pace.js"></script>

    <!-- Screenfull Js -->
    <script src="assets/plugins/screenfull/src/screenfull.js"></script>

    <!-- Metis Menu Js -->
    <script src="assets/plugins/metisMenu/dist/metisMenu.js"></script>

    <!-- Jquery Slimscroll Js -->
    <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    

    <!-- Custom Js -->
    <script src="assets/js/admin.js"></script>
    

    <script type="text/javascript">
        jQuery(function($) {
                                            
            $('.js-daterange-picker').daterangepicker({
                format: 'dd-mm-yyyy',
                autoclose:true
            });
            
        });
    </script>
    
    
</body>
</html>
<?php
ob_flush();
?>