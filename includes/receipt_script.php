<?php
ob_start();
session_start();
ini_set('error_reporting', 0);
ini_set('display_errors', 0);
require_once('dbconfig.php');
require_once('functions.php');
require_once('objects.php');
if($_SERVER["REQUEST_METHOD"] == "POST")
{
	$Action = $_POST['Action'];
	
	if($Action == 'AddReceipt')
	{
		$CompanyID = $_POST['CompanyID'];
		$RVType = $_POST['RVType'];
		$RVNo = $_POST['RVNo'];
		$RVNo = $salesObject->nextInvoiceReceiptNo($CurrentCompanyID);
		$RVDate = $_POST['RVDate'];
		$ReceivedFrom = $_POST['ReceivedFrom'];
		$ReceivedTo = $_POST['ReceivedTo'];
		$ReceiptMode = $_POST['ReceiptMode'];
		$Description = $_POST['Description'];
		$TotalAmount = $_POST['TotalAmount'];
		$TotalTDSAmount = $_POST['TotalTDSAmount'];
		
		$GrandTotal = $TotalAmount + $TotalTDSAmount;
		
		$BankName = '';
		$ChequeNo = '';
		$ChequeDate = '';
		$TransactionDate = '';
		$CardNo = '';
		$TransactionRef = '';
		$PaymentDate = '';
		
		if(isset($_POST['BankName']))
		{
			$BankName = $_POST['BankName'];
		}
		if(isset($_POST['ChequeNo']))
		{
			$ChequeNo = $_POST['ChequeNo'];
		}
		if(isset($_POST['ChequeDate']))
		{
			$ChequeDate = $_POST['ChequeDate'];
		}
		if(isset($_POST['TransactionDate']))
		{
			$TransactionDate = $_POST['TransactionDate'];
		}
		if(isset($_POST['CardNo']))
		{
			$CardNo = $_POST['CardNo'];
		}
		if(isset($_POST['TransactionRef']))
		{
			$TransactionRef = $_POST['TransactionRef'];
		}
		if(isset($_POST['PaymentDate']))
		{
			$PaymentDate = $_POST['PaymentDate'];
		}
		
		//Array 
		$InvoiceID = array();
		$TDSAmount = '';
		$Amount = '';
		
		if(isset($_POST['InvoiceID']))
		{
			$InvoiceID = $_POST['InvoiceID'];
		}
		if(isset($_POST['TDSAmount']))
		{
			$TDSAmount = $_POST['TDSAmount'];
		}
		if(isset($_POST['Amount']))
		{
			$Amount = $_POST['Amount'];
		}
		
		$Length = count($InvoiceID);
		
		if($Length < 1)
		{
			$jsonData = '{ 
				"Status":"00"
			}';
			echo $jsonData;
			exit();
		}
		
		if(!empty($ChequeDate)) { $ChequeDate = date("Y-m-d",strtotime($_POST['ChequeDate'])); }
		if(!empty($PaymentDate)) { $PaymentDate = date("Y-m-d",strtotime($_POST['PaymentDate'])); }
		if(!empty($RVDate)) { $RVDate = date("Y-m-d",strtotime($_POST['RVDate'])); }
		if(!empty($TransactionDate)) { $TransactionDate = date("Y-m-d",strtotime($_POST['TransactionDate'])); }
		
		$Insert = "INSERT INTO `receipt_vouchers`(`company_id`,`rv_no`, `rv_date`, `received_from`, `received_to`, `receipt_mode`, `bank_name`, `cheque_no`, `cheque_date`, `transaction_date`, `card_no`, `transaction_ref`, `payment_date`, `description`, `total_amount`, `total_tds_amount`, `rv_type`) VALUES ('$CompanyID','$RVNo','$RVDate','$ReceivedFrom','$ReceivedTo','$ReceiptMode','$BankName','$ChequeNo','$ChequeDate','$TransactionDate','$CardNo','$TransactionRef','$PaymentDate','$Description','$TotalAmount','$TotalTDSAmount','$RVType')";
		
		$InsertQuery = mysqli_query($con,$Insert);
		
		$RVID = mysqli_insert_id($con);
		
		if($InsertQuery)
		{
			$UpdateBalance = $ledgerObject->updateLedgerCurrentBalance($ReceivedFrom,$GrandTotal,'-');
			
			$UpdateCurrentBalance = $ledgerObject->updateLedgerCurrentBalance($ReceivedTo,$GrandTotal,'+');
			
			for($i = 0; $i < $Length; $i++)
			{
				$I_InvoiceID = $InvoiceID[$i];
				$I_TDSAmount = $TDSAmount[$i];
				$I_Amount = $Amount[$i];
				
				if(!empty($I_Amount))
				{
					$InsertItem = mysqli_query($con,"INSERT INTO `receipt_voucher_items`(`rv_id`, `invoice_id`, `tds_amount`, `amount`) VALUES ('$RVID','$I_InvoiceID','$I_TDSAmount','$I_Amount')");
					if(!$InsertItem)
					{
						$jsonData = '{ 
							"Status":"1"
						}';
						echo $jsonData;
						exit();
					}	
				}
				
			}
			if(!$InsertQuery)
			{
				$jsonData = '{ 
					"Status":"1"
				}';
				echo $jsonData;
				exit();
			}
			else
			{
				$jsonData = '{ 
					"Status":"2"
				}';
				echo $jsonData;
				exit();
			}
		}
		else
		{
			if(mysqli_errno($con)==1062)
			{
				$jsonData = '{ 
					"Status":"0"
				}';
				echo $jsonData;
				exit();
			}
			else
			{
				$jsonData = '{ 
					"Status":"1"
				}';
				echo $jsonData;
				exit();
			}
		}
			
	}
	else if($Action == 'UpdateReceipt')
	{
		$RVID = $_POST['RVID'];
		$CompanyID = $_POST['CompanyID'];
		$RVType = $_POST['RVType'];
		$RVNo = $_POST['RVNo'];
		$RVDate = $_POST['RVDate'];
		$ReceivedFrom = $_POST['ReceivedFrom'];
		$ReceivedTo = $_POST['ReceivedTo'];
		$ReceiptMode = $_POST['ReceiptMode'];
		$Description = $_POST['Description'];
		$TotalAmount = $_POST['TotalAmount'];
		$OldTotalAmount = $_POST['OldTotalAmount'];
		
		$TotalTDSAmount = $_POST['TotalTDSAmount'];
		$OldTotalTDSAmount = $_POST['OldTotalTDSAmount'];
		
		$AmountDiff = $TotalAmount - $OldTotalAmount;
		$TDSAmountDiff = $TotalTDSAmount - $OldTotalTDSAmount;
		
		$GrandTotal = $AmountDiff + $TDSAmountDiff;
		
		$BankName = '';
		$ChequeNo = '';
		$ChequeDate = '';
		$TransactionDate = '';
		$CardNo = '';
		$TransactionRef = '';
		$PaymentDate = '';
		
		if(isset($_POST['BankName']))
		{
			$BankName = $_POST['BankName'];
		}
		if(isset($_POST['ChequeNo']))
		{
			$ChequeNo = $_POST['ChequeNo'];
		}
		if(isset($_POST['ChequeDate']))
		{
			$ChequeDate = $_POST['ChequeDate'];
		}
		if(isset($_POST['TransactionDate']))
		{
			$TransactionDate = $_POST['TransactionDate'];
		}
		if(isset($_POST['CardNo']))
		{
			$CardNo = $_POST['CardNo'];
		}
		if(isset($_POST['TransactionRef']))
		{
			$TransactionRef = $_POST['TransactionRef'];
		}
		if(isset($_POST['PaymentDate']))
		{
			$PaymentDate = $_POST['PaymentDate'];
		}
		//Array
		$RVIID = '';
		$InvoiceID = '';
		$TDSAmount = '';
		$Amount = '';
		
		if(isset($_POST['RVIID']))
		{
			$RVIID = $_POST['RVIID'];
		}
		if(isset($_POST['InvoiceID']))
		{
			$InvoiceID = $_POST['InvoiceID'];
		}
		if(isset($_POST['TDSAmount']))
		{
			$TDSAmount = $_POST['TDSAmount'];
		}
		if(isset($_POST['Amount']))
		{
			$Amount = $_POST['Amount'];
		}
		
		$Length = count($Amount);
		
		if(!empty($ChequeDate)) { $ChequeDate = date("Y-m-d",strtotime($_POST['ChequeDate'])); }
		if(!empty($PaymentDate)) { $PaymentDate = date("Y-m-d",strtotime($_POST['PaymentDate'])); }
		if(!empty($RVDate)) { $RVDate = date("Y-m-d",strtotime($_POST['RVDate'])); }
		if(!empty($TransactionDate)) { $TransactionDate = date("Y-m-d",strtotime($_POST['TransactionDate'])); }
		
		$Update = "UPDATE `receipt_vouchers` SET `company_id`='$CompanyID',`rv_no`='$RVNo',`rv_date`='$RVDate',`received_from`='$ReceivedFrom',`received_to`='$ReceivedTo',`receipt_mode`='$ReceiptMode',`bank_name`='$BankName',`cheque_no`='$ChequeNo',`cheque_date`='$ChequeDate',`transaction_date`='$TransactionDate',`card_no`='$CardNo',`transaction_ref`='$TransactionRef',`payment_date`='$PaymentDate',`description`='$Description',`total_amount`='$TotalAmount',`total_tds_amount`='$TotalTDSAmount' WHERE rv_id='".$RVID."'";
		$UpdateQuery = mysqli_query($con,$Update);
		
		if($UpdateQuery)
		{
			if($OldTotalAmount != $TotalAmount || $OldTotalTDSAmount != $TotalTDSAmount)
			{
				$UpdateBalance = $ledgerObject->updateLedgerCurrentBalance($ReceivedFrom,$GrandTotal,'-');
			
				$UpdateCurrentBalance = $ledgerObject->updateLedgerCurrentBalance($ReceivedTo,$GrandTotal,'+');
			}
			for($i = 0; $i < $Length; $i++)
			{
				$I_RVIID = $RVIID[$i];
				$I_InvoiceID = $InvoiceID[$i];
				$I_TDSAmount = $TDSAmount[$i];
				$I_Amount = $Amount[$i];
				
				if(!empty($I_RVIID))
				{
					$UpdateItem = mysqli_query($con,"UPDATE `receipt_voucher_items` SET `tds_amount`='$I_TDSAmount',`amount`='$I_Amount' WHERE rvi_id='".$I_RVIID."'");
				}
			}
			if(!$UpdateQuery)
			{
				$jsonData = '{ 
					"Status":"3"
				}';
				echo $jsonData;
				exit();
			}
			else
			{
				$jsonData = '{ 
					"Status":"4"
				}';
				echo $jsonData;
				exit();
			}
		}
		else
		{
			if(mysqli_errno($con)==1062)
			{
				$jsonData = '{ 
					"Status":"0"
				}';
				echo $jsonData;
				exit();
			}
			else
			{
				$jsonData = '{ 
					"Status":"3"
				}';
				echo $jsonData;
				exit();
			}
		}
	}
	else if($Action == 'RemoveReceipt')
	{
		$RVID = $_POST['RVID'];
		$ReceivedTo = $_POST['ReceivedTo'];
		$ReceivedFrom = $_POST['ReceivedFrom'];
		$TotalAmount = $_POST['TotalAmount'];
		$TotalTDSAmount = $_POST['TotalTDSAmount'];
		
		
		$Total = $TotalAmount + $TotalTDSAmount;
		$UpdateBalance = $ledgerObject->updateLedgerCurrentBalance($ReceivedFrom,$Total,'+');
			
		$UpdateCurrentBalance = $ledgerObject->updateLedgerCurrentBalance($ReceivedTo,$Total,'-');
		
		$Remove = "DELETE FROM receipt_vouchers WHERE rv_id='".$RVID."'";
		$RemoveQuery = mysqli_query($con,$Remove);
		if($RemoveQuery)
		{
			$jsonData = '{ 
				"Status":"6"
			}';
			echo $jsonData;
			exit();
		}
		else
		{
			if(mysqli_errno($con)==1451)
			{
				$jsonData = '{ 
					"Status":"00"
				}';
				echo $jsonData;
				exit();
			}
			else
			{
				$jsonData = '{ 
					"Status":"5"
				}';
				echo $jsonData;
				exit();
			}	
		}
	}
	else if($Action == 'AddAdvanceReceipt')
	{
		$CompanyID = $_POST['CompanyID'];
		$RVType = $_POST['RVType'];
		$RVNo = $_POST['RVNo'];
		$RVNo = $salesObject->nextInvoiceReceiptNo();
		$RVDate = $_POST['RVDate'];
		$ReceivedFrom = $_POST['ReceivedFrom'];
		$ReceivedTo = $_POST['ReceivedTo'];
		$ReceiptMode = $_POST['ReceiptMode'];
		$Description = $_POST['Description'];
		$TotalAmount = $_POST['TotalAmount'];
		$TotalTDSAmount = $_POST['TotalTDSAmount'];
		
		$GrandTotal = $TotalAmount + $TotalTDSAmount;
		
		$BankName = '';
		$ChequeNo = '';
		$ChequeDate = '';
		$TransactionDate = '';
		$CardNo = '';
		$TransactionRef = '';
		$PaymentDate = '';
		
		if(isset($_POST['BankName']))
		{
			$BankName = $_POST['BankName'];
		}
		if(isset($_POST['ChequeNo']))
		{
			$ChequeNo = $_POST['ChequeNo'];
		}
		if(isset($_POST['ChequeDate']))
		{
			$ChequeDate = $_POST['ChequeDate'];
		}
		if(isset($_POST['TransactionDate']))
		{
			$TransactionDate = $_POST['TransactionDate'];
		}
		if(isset($_POST['CardNo']))
		{
			$CardNo = $_POST['CardNo'];
		}
		if(isset($_POST['TransactionRef']))
		{
			$TransactionRef = $_POST['TransactionRef'];
		}
		if(isset($_POST['PaymentDate']))
		{
			$PaymentDate = $_POST['PaymentDate'];
		}
		
		if(!empty($ChequeDate)) { $ChequeDate = date("Y-m-d",strtotime($_POST['ChequeDate'])); }
		if(!empty($PaymentDate)) { $PaymentDate = date("Y-m-d",strtotime($_POST['PaymentDate'])); }
		if(!empty($RVDate)) { $RVDate = date("Y-m-d",strtotime($_POST['RVDate'])); }
		if(!empty($TransactionDate)) { $TransactionDate = date("Y-m-d",strtotime($_POST['TransactionDate'])); }
		
		$Insert = "INSERT INTO `receipt_vouchers`(`company_id`,`rv_no`, `rv_date`, `received_from`, `received_to`, `receipt_mode`, `bank_name`, `cheque_no`, `cheque_date`, `transaction_date`, `card_no`, `transaction_ref`, `payment_date`, `description`, `total_amount`, `total_tds_amount`, `rv_type`) VALUES ('$CompanyID','$RVNo','$RVDate','$ReceivedFrom','$ReceivedTo','$ReceiptMode','$BankName','$ChequeNo','$ChequeDate','$TransactionDate','$CardNo','$TransactionRef','$PaymentDate','$Description','$TotalAmount','$TotalTDSAmount','$RVType')";
		
		$InsertQuery = mysqli_query($con,$Insert);
		
		if($InsertQuery)
		{
			$UpdateBalance = $ledgerObject->updateLedgerCurrentBalance($ReceivedFrom,$GrandTotal,'-');
			
			$UpdateCurrentBalance = $ledgerObject->updateLedgerCurrentBalance($ReceivedTo,$GrandTotal,'+');
			
			$jsonData = '{ 
				"Status":"2"
			}';
			echo $jsonData;
			exit();
		}
		else
		{
			if(mysqli_errno($con)==1062)
			{
				$jsonData = '{ 
					"Status":"0"
				}';
				echo $jsonData;
				exit();
			}
			else
			{
				$jsonData = '{ 
					"Status":"1"
				}';
				echo $jsonData;
				exit();
			}
		}
	}
	else if($Action == 'UpdateAdvanceReceipt')
	{
		$RVID = $_POST['RVID'];
		$CompanyID = $_POST['CompanyID'];
		$RVType = $_POST['RVType'];
		$RVNo = $_POST['RVNo'];
		$RVDate = $_POST['RVDate'];
		$ReceivedFrom = $_POST['ReceivedFrom'];
		$ReceivedTo = $_POST['ReceivedTo'];
		$ReceiptMode = $_POST['ReceiptMode'];
		$Description = $_POST['Description'];
		$TotalAmount = $_POST['TotalAmount'];
		$OldTotalAmount = $_POST['OldTotalAmount'];
		
		$TotalTDSAmount = $_POST['TotalTDSAmount'];
		$OldTotalTDSAmount = $_POST['OldTotalTDSAmount'];
		
		$AmountDiff = $TotalAmount - $OldTotalAmount;
		$TDSAmountDiff = $TotalTDSAmount - $OldTotalTDSAmount;
		
		$GrandTotal = $AmountDiff + $TDSAmountDiff;
		
		$BankName = '';
		$ChequeNo = '';
		$ChequeDate = '';
		$TransactionDate = '';
		$CardNo = '';
		$TransactionRef = '';
		$PaymentDate = '';
		
		if(isset($_POST['BankName']))
		{
			$BankName = $_POST['BankName'];
		}
		if(isset($_POST['ChequeNo']))
		{
			$ChequeNo = $_POST['ChequeNo'];
		}
		if(isset($_POST['ChequeDate']))
		{
			$ChequeDate = $_POST['ChequeDate'];
		}
		if(isset($_POST['TransactionDate']))
		{
			$TransactionDate = $_POST['TransactionDate'];
		}
		if(isset($_POST['CardNo']))
		{
			$CardNo = $_POST['CardNo'];
		}
		if(isset($_POST['TransactionRef']))
		{
			$TransactionRef = $_POST['TransactionRef'];
		}
		if(isset($_POST['PaymentDate']))
		{
			$PaymentDate = $_POST['PaymentDate'];
		}
		
		if(!empty($ChequeDate)) { $ChequeDate = date("Y-m-d",strtotime($_POST['ChequeDate'])); }
		if(!empty($PaymentDate)) { $PaymentDate = date("Y-m-d",strtotime($_POST['PaymentDate'])); }
		if(!empty($RVDate)) { $RVDate = date("Y-m-d",strtotime($_POST['RVDate'])); }
		if(!empty($TransactionDate)) { $TransactionDate = date("Y-m-d",strtotime($_POST['TransactionDate'])); }
		
		$Update = "UPDATE `receipt_vouchers` SET `company_id`='$CompanyID',`rv_no`='$RVNo',`rv_date`='$RVDate',`received_from`='$ReceivedFrom',`received_to`='$ReceivedTo',`receipt_mode`='$ReceiptMode',`bank_name`='$BankName',`cheque_no`='$ChequeNo',`cheque_date`='$ChequeDate',`transaction_date`='$TransactionDate',`card_no`='$CardNo',`transaction_ref`='$TransactionRef',`payment_date`='$PaymentDate',`description`='$Description',`total_amount`='$TotalAmount',`total_tds_amount`='$TotalTDSAmount' WHERE rv_id='".$RVID."'";
		$UpdateQuery = mysqli_query($con,$Update);
		if($UpdateQuery)
		{
			if($OldTotalAmount != $TotalAmount || $OldTotalTDSAmount != $TotalTDSAmount)
			{
				$UpdateBalance = $ledgerObject->updateLedgerCurrentBalance($ReceivedFrom,$GrandTotal,'-');
			
				$UpdateCurrentBalance = $ledgerObject->updateLedgerCurrentBalance($ReceivedTo,$GrandTotal,'+');
				
				$jsonData = '{ 
					"Status":"4"
				}';
				echo $jsonData;
				exit();
			}
		}
		else
		{
			if(mysqli_errno($con)==1062)
			{
				$jsonData = '{ 
					"Status":"0"
				}';
				echo $jsonData;
				exit();
			}
			else
			{
				$jsonData = '{ 
					"Status":"3"
				}';
				echo $jsonData;
				exit();
			}
		}
	}
	else if($Action == 'RemoveAdvanceReceipt')
	{
		$RVID = $_POST['RVID'];
		$ReceivedTo = $_POST['ReceivedTo'];
		$ReceivedFrom = $_POST['ReceivedFrom'];
		$TotalAmount = $_POST['TotalAmount'];
		$TotalTDSAmount = $_POST['TotalTDSAmount'];
		
		$Total = $TotalAmount + $TotalTDSAmount;
		$UpdateBalance = $ledgerObject->updateLedgerCurrentBalance($ReceivedFrom,$Total,'+');
			
		$UpdateCurrentBalance = $ledgerObject->updateLedgerCurrentBalance($ReceivedTo,$Total,'-');
		
		$Remove = "DELETE FROM receipt_vouchers WHERE rv_id='".$RVID."'";
		$RemoveQuery = mysqli_query($con,$Remove);
		if($RemoveQuery)
		{
			$jsonData = '{ 
				"Status":"6"
			}';
			echo $jsonData;
			exit();
		}
		else
		{
			if(mysqli_errno($con)==1451)
			{
				$jsonData = '{ 
					"Status":"00"
				}';
				echo $jsonData;
				exit();
			}
			else
			{
				$jsonData = '{ 
					"Status":"5"
				}';
				echo $jsonData;
				exit();
			}	
		}
	}
	else if($Action == 'AddOtherReceipt')
	{
		$CompanyID = $_POST['CompanyID'];
		$RVType = $_POST['RVType'];
		$RVNo = $_POST['RVNo'];
		$RVNo = $salesObject->nextInvoiceReceiptNo();
		$RVDate = $_POST['RVDate'];
		$ReceivedFrom = $_POST['ReceivedFrom'];
		$ReceivedTo = $_POST['ReceivedTo'];
		$ReceiptMode = $_POST['ReceiptMode'];
		$Description = $_POST['Description'];
		$TotalAmount = $_POST['TotalAmount'];
		
		$BankName = '';
		$ChequeNo = '';
		$ChequeDate = '';
		$TransactionDate = '';
		$CardNo = '';
		$TransactionRef = '';
		$PaymentDate = '';
		
		if(isset($_POST['BankName']))
		{
			$BankName = $_POST['BankName'];
		}
		if(isset($_POST['ChequeNo']))
		{
			$ChequeNo = $_POST['ChequeNo'];
		}
		if(isset($_POST['ChequeDate']))
		{
			$ChequeDate = $_POST['ChequeDate'];
		}
		if(isset($_POST['TransactionDate']))
		{
			$TransactionDate = $_POST['TransactionDate'];
		}
		if(isset($_POST['CardNo']))
		{
			$CardNo = $_POST['CardNo'];
		}
		if(isset($_POST['TransactionRef']))
		{
			$TransactionRef = $_POST['TransactionRef'];
		}
		if(isset($_POST['PaymentDate']))
		{
			$PaymentDate = $_POST['PaymentDate'];
		}
		
		if(!empty($ChequeDate)) { $ChequeDate = date("Y-m-d",strtotime($_POST['ChequeDate'])); }
		if(!empty($PaymentDate)) { $PaymentDate = date("Y-m-d",strtotime($_POST['PaymentDate'])); }
		if(!empty($RVDate)) { $RVDate = date("Y-m-d",strtotime($_POST['RVDate'])); }
		if(!empty($TransactionDate)) { $TransactionDate = date("Y-m-d",strtotime($_POST['TransactionDate'])); }
		
		$Insert = "INSERT INTO `receipt_vouchers`(`company_id`,`rv_no`, `rv_date`, `received_from`, `received_to`, `receipt_mode`, `bank_name`, `cheque_no`, `cheque_date`, `transaction_date`, `card_no`, `transaction_ref`, `payment_date`, `description`, `total_amount`, `rv_type`) VALUES ('$CompanyID','$RVNo','$RVDate','$ReceivedFrom','$ReceivedTo','$ReceiptMode','$BankName','$ChequeNo','$ChequeDate','$TransactionDate','$CardNo','$TransactionRef','$PaymentDate','$Description','$TotalAmount','$RVType')";
		
		$InsertQuery = mysqli_query($con,$Insert);
		
		if($InsertQuery)
		{
			$UpdateBalance = $ledgerObject->updateLedgerCurrentBalance($ReceivedFrom,$TotalAmount,'-');
			
			$UpdateCurrentBalance = $ledgerObject->updateLedgerCurrentBalance($ReceivedTo,$TotalAmount,'+');
			
			$jsonData = '{ 
				"Status":"2"
			}';
			echo $jsonData;
			exit();
		}
		else
		{
			if(mysqli_errno($con)==1062)
			{
				$jsonData = '{ 
					"Status":"0"
				}';
				echo $jsonData;
				exit();
			}
			else
			{
				$jsonData = '{ 
					"Status":"1"
				}';
				echo $jsonData;
				exit();
			}
		}
	}
	else if($Action == 'UpdateOtherReceipt')
	{
		$RVID = $_POST['RVID'];
		$CompanyID = $_POST['CompanyID'];
		$RVType = $_POST['RVType'];
		$RVNo = $_POST['RVNo'];
		$RVDate = $_POST['RVDate'];
		$ReceivedFrom = $_POST['ReceivedFrom'];
		$ReceivedTo = $_POST['ReceivedTo'];
		$ReceiptMode = $_POST['ReceiptMode'];
		$Description = $_POST['Description'];
		$TotalAmount = $_POST['TotalAmount'];
		$OldTotalAmount = $_POST['OldTotalAmount'];
		
		$AmountDiff = $TotalAmount - $OldTotalAmount;
		
		$BankName = '';
		$ChequeNo = '';
		$ChequeDate = '';
		$TransactionDate = '';
		$CardNo = '';
		$TransactionRef = '';
		$PaymentDate = '';
		
		if(isset($_POST['BankName']))
		{
			$BankName = $_POST['BankName'];
		}
		if(isset($_POST['ChequeNo']))
		{
			$ChequeNo = $_POST['ChequeNo'];
		}
		if(isset($_POST['ChequeDate']))
		{
			$ChequeDate = $_POST['ChequeDate'];
		}
		if(isset($_POST['TransactionDate']))
		{
			$TransactionDate = $_POST['TransactionDate'];
		}
		if(isset($_POST['CardNo']))
		{
			$CardNo = $_POST['CardNo'];
		}
		if(isset($_POST['TransactionRef']))
		{
			$TransactionRef = $_POST['TransactionRef'];
		}
		if(isset($_POST['PaymentDate']))
		{
			$PaymentDate = $_POST['PaymentDate'];
		}
		
		if(!empty($ChequeDate)) { $ChequeDate = date("Y-m-d",strtotime($_POST['ChequeDate'])); }
		if(!empty($PaymentDate)) { $PaymentDate = date("Y-m-d",strtotime($_POST['PaymentDate'])); }
		if(!empty($RVDate)) { $RVDate = date("Y-m-d",strtotime($_POST['RVDate'])); }
		if(!empty($TransactionDate)) { $TransactionDate = date("Y-m-d",strtotime($_POST['TransactionDate'])); }
		
		$Update = "UPDATE `receipt_vouchers` SET `company_id`='$CompanyID',`rv_no`='$RVNo',`rv_date`='$RVDate',`received_from`='$ReceivedFrom',`received_to`='$ReceivedTo',`receipt_mode`='$ReceiptMode',`bank_name`='$BankName',`cheque_no`='$ChequeNo',`cheque_date`='$ChequeDate',`transaction_date`='$TransactionDate',`card_no`='$CardNo',`transaction_ref`='$TransactionRef',`payment_date`='$PaymentDate',`description`='$Description',`total_amount`='$TotalAmount' WHERE rv_id='".$RVID."'";
		$UpdateQuery = mysqli_query($con,$Update);
		if($UpdateQuery)
		{
			if($OldTotalAmount != $TotalAmount)
			{
				$UpdateBalance = $ledgerObject->updateLedgerCurrentBalance($ReceivedFrom,$AmountDiff,'-');
			
				$UpdateCurrentBalance = $ledgerObject->updateLedgerCurrentBalance($ReceivedTo,$AmountDiff,'+');
				
				$jsonData = '{ 
					"Status":"4"
				}';
				echo $jsonData;
				exit();
			}
		}
		else
		{
			if(mysqli_errno($con)==1062)
			{
				$jsonData = '{ 
					"Status":"0"
				}';
				echo $jsonData;
				exit();
			}
			else
			{
				$jsonData = '{ 
					"Status":"3"
				}';
				echo $jsonData;
				exit();
			}
		}
	}
	else if($Action == 'RemoveOtherIncome')
	{
		$RVID = $_POST['RVID'];
		$ReceivedTo = $_POST['ReceivedTo'];
		$ReceivedFrom = $_POST['ReceivedFrom'];
		$TotalAmount = $_POST['TotalAmount'];
		
		$UpdateBalance = $ledgerObject->updateLedgerCurrentBalance($ReceivedFrom,$TotalAmount,'+');
			
		$UpdateCurrentBalance = $ledgerObject->updateLedgerCurrentBalance($ReceivedTo,$TotalAmount,'-');
		
		$Remove = "DELETE FROM receipt_vouchers WHERE rv_id='".$RVID."'";
		$RemoveQuery = mysqli_query($con,$Remove);
		if($RemoveQuery)
		{
			$jsonData = '{ 
				"Status":"6"
			}';
			echo $jsonData;
			exit();
		}
		else
		{
			if(mysqli_errno($con)==1451)
			{
				$jsonData = '{ 
					"Status":"00"
				}';
				echo $jsonData;
				exit();
			}
			else
			{
				$jsonData = '{ 
					"Status":"5"
				}';
				echo $jsonData;
				exit();
			}	
		}
	}
}
else
{
	$jsonData = '{ 
		"Status":"Unauthorised Access!"
	}';
	echo $jsonData;
	exit();
}
ob_flush();
?>