<?php
ob_start();
session_start();
require_once('dbconfig.php');
require_once('functions.php');
require_once('objects.php');

if($_SERVER["REQUEST_METHOD"] == "POST")
{
	$Action = $_POST['Action'];
	if($Action == 'UpdateProfile')
	{
		$AdminID = $_POST['AdminID'];
		$AdminName = mysqli_real_escape_string($con,$_POST['AdminName']);
		$EmailID = $_POST['EmailID'];
		$Mobile = $_POST['Mobile'];
		$CompanyID = $_POST['CompanyID'];
		
		$FYear = $_POST['FYear']; 
		$FYear = explode('-',$FYear);
		
		$FYearStart = str_replace('/', '-', $FYear[0]);
		$FYearEnd = str_replace('/', '-', $FYear[1]);
		
		$FYearStart = date("Y-m-d",strtotime($FYearStart)); 
		$FYearEnd = date("Y-m-d",strtotime($FYearEnd)); 
		
		$ModifiedBy = $_POST['AdminID'];
		$ModifiedDate = date("Y-m-d H:i:s");

		$Update = "UPDATE `admin_master` SET `admin_name`='$AdminName',`email_id`='$EmailID',`mobile`='$Mobile',`fyear_start`='$FYearStart',`fyear_end`='$FYearEnd',`company_id`='$CompanyID',`modifiedby`='$ModifiedBy',`modifieddate`='$ModifiedDate' WHERE admin_id='".$AdminID."'";
		$UpdateQuery = mysqli_query($con,$Update);

		if(!$UpdateQuery)
		{
			die(mysqli_error($con));
			$jsonData = '{ 
				"Status":"3"
			}';
			echo $jsonData;
			exit();
		}
		else
		{
			$jsonData = '{ 
				"Status":"4"
			}';
			echo $jsonData;
			exit();
		}
	}
	else
	{
		$jsonData = '{ 
			"Status":"Unauthorised Access!"
		}';
		echo $jsonData;
		exit();
	}
}
else
{
	$jsonData = '{ 
		"Status":"Unauthorised Access!"
	}';
	echo $jsonData;
	exit();
}
ob_flush();
?>