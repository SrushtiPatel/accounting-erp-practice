<?php
ob_start();
session_start();
require_once('dbconfig.php');
if($_SERVER["REQUEST_METHOD"] == "POST")
{
	$Action = $_POST['Action'];
	if($Action == 'AddItem')
	{
		$ItemName = mysqli_real_escape_string($con,$_POST['ItemName']);
		$ItemAlias = mysqli_real_escape_string($con,$_POST['ItemAlias']);
		$ItemCode = mysqli_real_escape_string($con,$_POST['ItemCode']);
		$HSNCode = mysqli_real_escape_string($con,$_POST['HSNCode']);
		$ItemRate = mysqli_real_escape_string($con,$_POST['ItemRate']);
		$ItemUnit = mysqli_real_escape_string($con,$_POST['ItemUnit']);
		$InitialStock = mysqli_real_escape_string($con,$_POST['InitialStock']);
		$MinStock = mysqli_real_escape_string($con,$_POST['MinStock']);
		$MaxStock = mysqli_real_escape_string($con,$_POST['MaxStock']);
		$CompanyID = $_POST['CompanyID'];
		
		$AddedBy = $_POST['AdminID'];
		$AddedDate = date("Y-m-d H:i:s");
		
		$Insert = "INSERT INTO `item_master`(`company_id`,`item_name`, `item_alias`, `item_code`,`hsn_code`, `item_rate`, `item_unit`, `initial_stock`, `min_stock`, `max_stock`, `addedby`, `addeddate`) VALUES ('$CompanyID','$ItemName','$ItemAlias','$ItemCode','$HSNCode','$ItemRate','$ItemUnit','$InitialStock','$MinStock','$MaxStock','$AddedBy','$AddedDate')";
		
		$InsertQuery = mysqli_query($con,$Insert);
		
		if(!$InsertQuery)
		{
			if(mysqli_errno($con)==1062)
			{
				$jsonData = '{ 
					"Status":"0"
				}';
				echo $jsonData;
				exit();
			}
			else
			{
				//die(mysqli_error($con));
				$jsonData = '{ 
					"Status":"1"
				}';
				echo $jsonData;
				exit();
			}
		}
		else
		{
			$jsonData = '{ 
				"Status":"2"
			}';
			echo $jsonData;
			exit();
		}
	}
	else if($Action == 'UpdateItem')
	{
		$ItemID = $_POST['ItemID'];
		$ItemName = mysqli_real_escape_string($con,$_POST['ItemName']);
		$ItemAlias = mysqli_real_escape_string($con,$_POST['ItemAlias']);
		$ItemCode = mysqli_real_escape_string($con,$_POST['ItemCode']);
		$HSNCode = mysqli_real_escape_string($con,$_POST['HSNCode']);
		$ItemRate = mysqli_real_escape_string($con,$_POST['ItemRate']);
		$ItemUnit = mysqli_real_escape_string($con,$_POST['ItemUnit']);
		$InitialStock = mysqli_real_escape_string($con,$_POST['InitialStock']);
		$MinStock = mysqli_real_escape_string($con,$_POST['MinStock']);
		$MaxStock = mysqli_real_escape_string($con,$_POST['MaxStock']);
		
		$CompanyID = $_POST['CompanyID'];
		
		$ModifiedBy = $_POST['AdminID'];
		$ModifiedDate = date("Y-m-d H:i:s");

		$Update = "UPDATE `item_master` SET `company_id`='$CompanyID', `item_name`='$ItemName',`item_alias`='$ItemAlias',`item_code`='$ItemCode',`hsn_code`='$HSNCode',`item_rate`='$ItemRate',`item_unit`='$ItemUnit',`initial_stock`='$InitialStock',`min_stock`='$MinStock',`max_stock`='$MaxStock',`modifiedby`='$ModifiedBy',`modifieddate`='$ModifiedDate' WHERE `item_id`='".$ItemID."'";

		$UpdateQuery = mysqli_query($con,$Update);
		
		if(!$UpdateQuery)
		{
			//die(mysqli_error($con));
			if(mysqli_errno($con)==1062)
			{
				$jsonData = '{ 
					"Status":"0"
				}';
				echo $jsonData;
				exit();
			}
			else
			{
				$jsonData = '{ 
					"Status":"3"
				}';
				echo $jsonData;
				exit();
			}
		}
		else
		{
			$jsonData = '{ 
				"Status":"4"
			}';
			echo $jsonData;
			exit();
		}
	}
	else if($Action == 'RemoveItem')
	{
		$ItemID = $_POST['ItemID'];
				
		$Remove = "DELETE FROM `item_master` WHERE `item_id`='".$ItemID."'";
		$RemoveQuery = mysqli_query($con,$Remove);
			
		if(!$RemoveQuery)
		{
			if(mysqli_errno($con)==1451)
			{
				$jsonData = '{ 
					"Status":"0"
				}';
				echo $jsonData;
				exit();
			}
			else
			{
				$jsonData = '{ 
					"Status":"5"
				}';
				echo $jsonData;
				exit();
			}
		}
		else
		{
			$jsonData = '{ 
				"Status":"6"
			}';
			echo $jsonData;
			exit();
		}
	}
	else
	{
		$jsonData = '{ 
			"Status":"Unauthorised Access!"
		}';
		echo $jsonData;
		exit();
	}
}
else
{
	$jsonData = '{ 
		"Status":"Unauthorised Access!"
	}';
	echo $jsonData;
	exit();
}
ob_flush();
?>