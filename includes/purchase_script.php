<?php
ob_start();
session_start();
ini_set('error_reporting', 0);
ini_set('display_errors', 0);
require_once('dbconfig.php');
require_once('functions.php');
require_once('objects.php');
if($_SERVER["REQUEST_METHOD"] == "POST")
{
	$Action = $_POST['Action'];
	
	if($Action == 'AddPurchase')
	{
		$CompanyID = $_POST['CompanyID'];
		$VoucherNo = $_POST['VoucherNo'];
		$VoucherNo = $purchaseObject->nextVoucherNo($CurrentCompanyID);
		$VendorID = $_POST['VendorID'];
		$InvoiceNo = $_POST['InvoiceNo'];
		$InvoiceDate = $_POST['InvoiceDate'];
		$ChallanNo = $_POST['ChallanNo'];
		$ChallanDate = $_POST['ChallanDate'];
		$PONo = $_POST['PONo'];
		$PODate = $_POST['PODate'];
		$DueDate = $_POST['DueDate'];
		$TermsCondition = $_POST['TermsCondition'];
		
		$VendorName = $_POST['VendorName'];
		$VendorGSTIN = $_POST['VendorGSTIN'];
		$VendorState = $_POST['VendorState'];
		$VendorStateCode = $_POST['VendorStateCode'];
		
		$TotalAmountBeforeTax = $_POST['TotalAmountBeforeTax'];
		$TotalDiscount = $_POST['TotalDiscount'];
		$TotalTaxAmount = $_POST['TotalTaxAmount'];
		$RoundOff = $_POST['RoundOff'];
		$GrandTotal = $_POST['GrandTotal'];
		
		//Array List
		
		$ItemID = $_POST['ItemID'];
		$ItemName = $_POST['ItemName'];
		$HSNCode = $_POST['HSNCode'];
		$Unit = $_POST['Unit'];
		$Description = $_POST['Description'];
		$Quantity = $_POST['Quantity'];
		$Rate = $_POST['Rate'];
		$Amount = $_POST['Amount'];
		$Discount = $_POST['Discount'];
		$TaxableValue  = $_POST['TaxableValue'];
		$Tax1  = $_POST['Tax1'];
		$Tax1Rate  = $_POST['Tax1Rate'];
		$Tax1Value = $_POST['Tax1Value'];
		$Tax2  = $_POST['Tax2'];
		$Tax2Rate  = $_POST['Tax2Rate'];
		$Tax2Value = $_POST['Tax2Value'];
		$Tax3  = $_POST['Tax3'];
		$Tax3Rate  = $_POST['Tax3Rate'];
		$Tax3Value = $_POST['Tax3Value'];
		$Total =$_POST['Total'];
		
		$Length = count($ItemID);
		
		if(!empty($InvoiceDate)) { $InvoiceDate = date("Y-m-d",strtotime($_POST['InvoiceDate'])); }
		if(!empty($ChallanDate)) { $ChallanDate = date("Y-m-d",strtotime($_POST['ChallanDate'])); }
		if(!empty($PODate)) { $PODate = date("Y-m-d",strtotime($_POST['PODate'])); }
		if(!empty($DueDate)) { $DueDate = date("Y-m-d",strtotime($_POST['DueDate'])); }
		
		$AddedBy = $_POST['AdminID'];
		$AddedDate = date("Y-m-d H:i:s");
		
		$Insert = "INSERT INTO `purchase_master`(`company_id`,`voucher_no`, `vendor_id`, `invoice_no`, `invoice_date`, `challan_no`, `challan_date`, `po_no`, `po_date`, `due_date`, `vendor_name`, `vendor_gstin`, `vendor_state`, `vendor_state_code`, `total_amount_before_tax`, `discount`, `total_tax_amount`, `roundoff`, `grand_total`, `terms_conditions`, `addedby`, `addeddate`) VALUES ('$CompanyID','$VoucherNo','$VendorID','$InvoiceNo','$InvoiceDate','$ChallanNo','$ChallanDate','$PONo','$PODate','$DueDate','$VendorName','$VendorGSTIN','$VendorState','$VendorStateCode','$TotalAmountBeforeTax','$Discount','$TotalTaxAmount','$RoundOff','$GrandTotal','$TermsCondition','$AddedBy','$AddedDate')";
		
		$InsertQuery = mysqli_query($con,$Insert);
		
		$PurchaseID = mysqli_insert_id($con);
		
		if($InsertQuery)
		{
			$UpdateCurrentBalance = $ledgerObject->updateLedgerCurrentBalance($VendorID,$GrandTotal,'-');
			
			for($i = 0; $i < $Length; $i++)
			{
				$I_ItemID = $ItemID[$i];
				$I_ItemName = $ItemName[$i];
				$I_HSNCode = $HSNCode[$i];
				$I_Unit = $Unit[$i];
				$I_Description = $Description[$i];
				$I_Quantity = $Quantity[$i];
				$I_Rate = $Rate[$i];
				$I_Amount = $Amount[$i];
				$I_Discount = $Discount[$i];
				$I_TaxableValue = $TaxableValue[$i];
				$I_Tax1 = $Tax1[$i];
				$I_Tax1Rate = $Tax1Rate[$i];
				$I_Tax1Value = $Tax1Value[$i];
				$I_Tax2 = $Tax2[$i];
				$I_Tax2Rate = $Tax2Rate[$i];
				$I_Tax2Value = $Tax2Value[$i];
				$I_Tax3 = $Tax3[$i];
				$I_Tax3Rate = $Tax3Rate[$i];
				$I_Tax3Value = $Tax3Value[$i];
				$I_Total = $Total[$i];
				
				$InsertItem = mysqli_query($con,"INSERT INTO `purchase_item_master`(`purchase_id`, `item_id`, `item_name`, `description`, `hsn_code`, `qty`, `unit`, `rate`, `discount`, `amount`, `taxable_value`, `tax1_id`, `tax1_rate`, `tax1_amount`, `tax2_id`, `tax2_rate`, `tax2_amount`, `tax3_id`, `tax3_rate`, `tax3_amount`, `total`) VALUES ('$PurchaseID','$I_ItemID','$I_ItemName','$I_Description','$I_HSNCode','$I_Quantity','$I_Unit','$I_Rate','$I_Discount','$I_Amount','$I_TaxableValue','$I_Tax1','$I_Tax1Rate','$I_Tax1Value','$I_Tax2','$I_Tax2Rate','$I_Tax2Value','$I_Tax3','$I_Tax3Rate','$I_Tax3Value','$I_Total')");
				if($InsertItem)
				{	
					if($I_Tax1 > 0)
					{
						$UpdateTax1 = $ledgerObject->updateLedgerCurrentBalance($I_Tax1,$I_Tax1Value,'+');
					}
					if($I_Tax2 > 0)
					{
						$UpdateTax2 = $ledgerObject->updateLedgerCurrentBalance($I_Tax2,$I_Tax2Value,'+');
					}
					if($I_Tax3 > 0)
					{
						$UpdateTax3 = $ledgerObject->updateLedgerCurrentBalance($I_Tax3,$I_Tax3Value,'+');
					}
				}
				else
				{
					$jsonData = '{ 
						"Status":"1"
					}';
					echo $jsonData;
					exit();
				}
			}
			if(!$InsertQuery)
			{
				$jsonData = '{ 
					"Status":"1"
				}';
				echo $jsonData;
				exit();
			}
			else
			{
				$jsonData = '{ 
					"Status":"2"
				}';
				echo $jsonData;
				exit();
			}
		}
		else
		{
			if(mysqli_errno($con)==1062)
			{
				$jsonData = '{ 
					"Status":"0"
				}';
				echo $jsonData;
				exit();
			}
			else
			{
				$jsonData = '{ 
					"Status":"1"
				}';
				echo $jsonData;
				exit();
			}
		}
			
	}
	else if($Action == 'UpdatePurchase')
	{
		$PurchaseID =$_POST['PurchaseID'];
		$CompanyID = $_POST['CompanyID'];
		$VoucherNo = $_POST['VoucherNo'];
		$VendorID = $_POST['VendorID'];
		$InvoiceNo = $_POST['InvoiceNo'];
		$InvoiceDate = $_POST['InvoiceDate'];
		$ChallanNo = $_POST['ChallanNo'];
		$ChallanDate = $_POST['ChallanDate'];
		$PONo = $_POST['PONo'];
		$PODate = $_POST['PODate'];
		$DueDate = $_POST['DueDate'];
		$TermsCondition = $_POST['TermsCondition'];
		
		$VendorName = $_POST['VendorName'];
		$VendorGSTIN = $_POST['VendorGSTIN'];
		$VendorState = $_POST['VendorState'];
		$VendorStateCode = $_POST['VendorStateCode'];
		
		$TotalAmountBeforeTax = $_POST['TotalAmountBeforeTax'];
		$TotalDiscount = $_POST['TotalDiscount'];
		$TotalTaxAmount = $_POST['TotalTaxAmount'];
		$RoundOff = $_POST['RoundOff'];
		$GrandTotal = $_POST['GrandTotal'];
		$OldGrandTotal = $_POST['OldGrandTotal'];
		$GrandTotalDiff = $GrandTotal - $OldGrandTotal;
		//Array List
		
		$PurchaseItemID = $_POST['PurchaseItemID'];
		$ItemID = $_POST['ItemID'];
		$ItemName = $_POST['ItemName'];
		$HSNCode = $_POST['HSNCode'];
		$Unit = $_POST['Unit'];
		$Description = $_POST['Description'];
		$Quantity = $_POST['Quantity'];
		$Rate = $_POST['Rate'];
		$Amount = $_POST['Amount'];
		$Discount = $_POST['Discount'];
		$TaxableValue  = $_POST['TaxableValue'];
		$Tax1  = $_POST['Tax1'];
		$Tax1Rate  = $_POST['Tax1Rate'];
		$Tax1Value = $_POST['Tax1Value'];
		$Tax2  = $_POST['Tax2'];
		$Tax2Rate  = $_POST['Tax2Rate'];
		$Tax2Value = $_POST['Tax2Value'];
		$Tax3  = $_POST['Tax3'];
		$Tax3Rate  = $_POST['Tax3Rate'];
		$Tax3Value = $_POST['Tax3Value'];
		$Total = $_POST['Total'];
		
		$OldTax1 = $_POST['OldTax1'];
		$OldTax2 = $_POST['OldTax2'];
		$OldTax3 = $_POST['OldTax3'];
		$OldTax1Value = $_POST['OldTax1Value'];
		$OldTax2Value = $_POST['OldTax2Value'];
		$OldTax3Value = $_POST['OldTax3Value'];
		
		$Length = count($ItemID);
		
		if(!empty($InvoiceDate)) { $InvoiceDate = date("Y-m-d",strtotime($_POST['InvoiceDate'])); }
		if(!empty($ChallanDate)) { $ChallanDate = date("Y-m-d",strtotime($_POST['ChallanDate'])); }
		if(!empty($PODate)) { $PODate = date("Y-m-d",strtotime($_POST['PODate'])); }
		if(!empty($DueDate)) { $DueDate = date("Y-m-d",strtotime($_POST['DueDate'])); }
		
		$ModifiedBy = $_POST['AdminID'];
		$ModifiedDate = date("Y-m-d H:i:s");
		
		$Update = "UPDATE `purchase_master` SET `company_id`='$CompanyID',`voucher_no`='$VoucherNo',`vendor_id`='$VendorID',`invoice_no`='$InvoiceNo',`invoice_date`='$InvoiceDate',`challan_no`='$ChallanNo',`challan_date`='$ChallanDate',`po_no`='$PONo',`po_date`='$PODate',`due_date`='$DueDate',`vendor_name`='$VendorName',`vendor_gstin`='$VendorGSTIN',`vendor_state`='$VendorState',`vendor_state_code`='$VendorStateCode',`total_amount_before_tax`='$TotalAmountBeforeTax',`discount`='$TotalDiscount',`total_tax_amount`='$TotalTaxAmount',`roundoff`='$RoundOff',`grand_total`='$GrandTotal',`terms_conditions`='$TermsCondition',`modifiedby`='$ModifiedBy',`modifieddate`='$ModifiedDate' WHERE purchase_id='".$PurchaseID."'";

		$UpdateQuery = mysqli_query($con,$Update);
		
		if($UpdateQuery)
		{
			$UpdateCurrentBalance = $ledgerObject->updateLedgerCurrentBalance($VendorID,$GrandTotalDiff,'-');
			
			for($i = 0; $i < $Length; $i++)
			{
				$I_PurchaseItemID = $PurchaseItemID[$i];
				$I_ItemID = $ItemID[$i];
				$I_ItemName = $ItemName[$i];
				$I_HSNCode = $HSNCode[$i];
				$I_Unit = $Unit[$i];
				$I_Description = $Description[$i];
				$I_Quantity = $Quantity[$i];
				$I_Rate = $Rate[$i];
				$I_Amount = $Amount[$i];
				$I_Discount = $Discount[$i];
				$I_TaxableValue = $TaxableValue[$i];
				$I_Tax1 = $Tax1[$i];
				$I_Tax1Rate = $Tax1Rate[$i];
				$I_Tax1Value = $Tax1Value[$i];
				$I_Tax2 = $Tax2[$i];
				$I_Tax2Rate = $Tax2Rate[$i];
				$I_Tax2Value = $Tax2Value[$i];
				$I_Tax3 = $Tax3[$i];
				$I_Tax3Rate = $Tax3Rate[$i];
				$I_Tax3Value = $Tax3Value[$i];
				$I_Total = $Total[$i];
				
				$I_OldTax1 = $OldTax1[$i];
				$I_OldTax2 = $OldTax2[$i];
				$I_OldTax3 = $OldTax3[$i];
				
				$I_OldTax1Value = $OldTax1Value[$i];
				$I_OldTax2Value = $OldTax2Value[$i];
				$I_OldTax3Value = $OldTax3Value[$i];
				
				if(!empty($I_PurchaseItemID))
				{
					$UpdateItem = mysqli_query($con,"UPDATE `purchase_item_master` SET `item_name`='$I_ItemName',`description`='$I_Description',`hsn_code`='$I_HSNCode',`qty`='$I_Quantity',`unit`='$I_Unit',`rate`='$I_Rate',`discount`='$I_Discount',`amount`='$I_Amount',`taxable_value`='$I_TaxableValue',`tax1_id`='$I_Tax1',`tax1_rate`='$I_Tax1Rate',`tax1_amount`='$I_Tax1Value',`tax2_id`='$I_Tax2',`tax2_rate`='$I_Tax2Rate',`tax2_amount`='$I_Tax2Value',`tax3_id`='$I_Tax3',`tax3_rate`='$I_Tax3Rate',`tax3_amount`='$I_Tax3Value',`total`='$I_Total' WHERE purchase_item_id='".$I_PurchaseItemID."'");
					if($UpdateItem)
					{
						if($I_Tax1 > 0)
						{
							$UpdateTax = $ledgerObject->updateLedgerCurrentBalance($I_Tax1,$I_Tax1Value,'+');
						}
						if($I_Tax2 > 0)
						{ 
							$UpdateTax = $ledgerObject->updateLedgerCurrentBalance($I_Tax2,$I_Tax2Value,'+');
						}
						if($I_Tax3 > 0)
						{
							$UpdateTax = $ledgerObject->updateLedgerCurrentBalance($I_Tax3,$I_Tax3Value,'+');
						}
						if($I_OldTax1 > 0)
						{
							$UpdateTax = $ledgerObject->updateLedgerCurrentBalance($I_OldTax1,$I_OldTax1Value,'-');
						}
						if($I_OldTax2 > 0)
						{
							$UpdateTax = $ledgerObject->updateLedgerCurrentBalance($I_OldTax2,$I_OldTax2Value,'-');
						}
						if($I_OldTax3 > 0)
						{
							$UpdateTax = $ledgerObject->updateLedgerCurrentBalance($I_OldTax3,$I_OldTax3Value,'-');
						}
					}
				}
				else
				{
					$InsertItem = mysqli_query($con,"INSERT INTO `purchase_item_master`(`purchase_id`, `item_id`,`item_name`, `description`, `hsn_code`, `qty`,`unit`, `rate`, `discount`, `amount`, `taxable_value`, `tax1_id`, `tax1_rate`, `tax1_amount`, `tax2_id`, `tax2_rate`, `tax2_amount`, `tax3_id`, `tax3_rate`, `tax3_amount`, `total`) VALUES ('$PurchaseID','$I_ItemID','$I_ItemName','$I_Description','$I_HSNCode','$I_Quantity','$I_Unit','$I_Rate','$I_Discount','$I_Amount','$I_TaxableValue','$I_Tax1','$I_Tax1Rate','$I_Tax1Value','$I_Tax2','$I_Tax2Rate','$I_Tax2Value','$I_Tax3','$I_Tax3Rate','$I_Tax3Value','$I_Total')");
					if($InsertItem)
					{	
						if($I_Tax1 > 0)
						{
							$UpdateTax1 = $ledgerObject->updateLedgerCurrentBalance($I_Tax1,$I_Tax1Value,'+');
						}
						if($I_Tax2 > 0)
						{
							$UpdateTax2 = $ledgerObject->updateLedgerCurrentBalance($I_Tax2,$I_Tax2Value,'+');
						}
						if($I_Tax3 > 0)
						{
							$UpdateTax3 = $ledgerObject->updateLedgerCurrentBalance($I_Tax3,$I_Tax3Value,'+');
						}
					}
				}
			}
			if(!$UpdateQuery)
			{
				$jsonData = '{ 
					"Status":"3"
				}';
				echo $jsonData;
				exit();
			}
			else
			{
				$jsonData = '{ 
					"Status":"4"
				}';
				echo $jsonData;
				exit();
			}
		}
		else
		{
			if(mysqli_errno($con)==1062)
			{
				$jsonData = '{ 
					"Status":"0"
				}';
				echo $jsonData;
				exit();
			}
			else
			{
				$jsonData = '{ 
					"Status":"3"
				}';
				echo $jsonData;
				exit();
			}
		}
	}
	else if($Action == 'RemovePurchaseItem')
	{
		$PurchaseItemID = $_POST['PurchaseItemID'];
		
		$SelectPurchaseItem = mysqli_query($con,"SELECT * FROM purchase_item_master WHERE purchase_item_id='".$PurchaseItemID."'");
		$PurchaseItem = mysqli_fetch_array($SelectPurchaseItem);
		$Tax1 = $PurchaseItem['tax1_id'];
		$Tax1Value = $PurchaseItem['tax1_amount'];

		$Tax2 = $PurchaseItem['tax2_id'];
		$Tax2Value = $PurchaseItem['tax2_amount'];

		$Tax3 = $PurchaseItem['tax3_id'];
		$Tax3Value = $PurchaseItem['tax3_amount'];

		if($Tax1 > 0)
		{
			$UpdateTax1 = $ledgerObject->updateLedgerCurrentBalance($Tax1,$Tax1Value,'-');
		}
		if($Tax2 > 0)
		{
			$UpdateTax2 = $ledgerObject->updateLedgerCurrentBalance($Tax2,$Tax2Value,'-');
		}
		if($Tax3 > 0)
		{
			$UpdateTax3 = $ledgerObject->updateLedgerCurrentBalance($Tax3,$Tax3Value,'-');
		}
		$Remove = "DELETE FROM purchase_item_master WHERE purchase_item_id='".$PurchaseItemID."'";
		$RemoveQuery = mysqli_query($con,$Remove);
		
		if(!$RemoveQuery)
		{
			$jsonData = '{ 
				"Status":"5"
			}';
			echo $jsonData;
			exit();	
		}
		else
		{
			$jsonData = '{ 
				"Status":"6"
			}';
			echo $jsonData;
			exit();
		}
	}
	else if($Action == 'RemovePurchase')
	{
		$PurchaseID = $_POST['PurchaseID'];
		
		$SelectPurchase = mysqli_query($con,"SELECT p.vendor_id, p.grand_total, pi.tax1_id, tax1_amount, pi.tax2_id, tax2_amount, pi.tax3_id, tax3_amount FROM purchase_master p JOIN purchase_item_master pi ON p.purchase_id=pi.purchase_id WHERE p.purchase_id='".$PurchaseID."'");
		while($Purchase = mysqli_fetch_array($SelectPurchase))
		{
			$VendorID = $Purchase['vendor_id'];
			$GrandTotal = $Purchase['grand_total'];
			$Tax1 = $Purchase['tax1_id'];
			$Tax1Value = $Purchase['tax1_amount'];

			$Tax2 = $Purchase['tax2_id'];
			$Tax2Value = $Purchase['tax2_amount'];

			$Tax3 = $Purchase['tax3_id'];
			$Tax3Value = $Purchase['tax3_amount'];
			
			$UpdateTax1 = $ledgerObject->updateLedgerCurrentBalance($Tax1,$Tax1Value,'-');

			$UpdateTax2 = $ledgerObject->updateLedgerCurrentBalance($Tax2,$Tax2Value,'-');

			$UpdateTax3 = $ledgerObject->updateLedgerCurrentBalance($Tax3,$Tax3Value,'-');
		}
		$UpdateCurrentBalance = $ledgerObject->updateLedgerCurrentBalance($VendorID,$GrandTotal,'+');

		$Remove = "DELETE FROM purchase_master WHERE purchase_id='".$PurchaseID."'";
		$RemoveQuery = mysqli_query($con,$Remove);
		
		if(!$RemoveQuery)
		{
			$jsonData = '{ 
				"Status":"5"
			}';
			echo $jsonData;
			exit();	
		}
		else
		{
			$jsonData = '{ 
				"Status":"6"
			}';
			echo $jsonData;
			exit();
		}
	}
}
else
{
	$jsonData = '{ 
		"Status":"Unauthorised Access!"
	}';
	echo $jsonData;
	exit();
}
ob_flush();
?>