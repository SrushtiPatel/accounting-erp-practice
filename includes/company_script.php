<?php
ob_start();
session_start();
require_once('dbconfig.php');
if($_SERVER["REQUEST_METHOD"] == "POST")
{
	$Action = $_POST['Action'];
	if($Action == 'AddCompany')
	{
		$CompanyName = mysqli_real_escape_string($con,$_POST['CompanyName']);
		$CompanyAlias = mysqli_real_escape_string($con,$_POST['CompanyAlias']);
		$CompanyCode = mysqli_real_escape_string($con,$_POST['CompanyCode']);
		$Address = mysqli_real_escape_string($con,$_POST['Address']);
		$Phone = mysqli_real_escape_string($con,$_POST['Phone']);
		$Mobile = mysqli_real_escape_string($con,$_POST['Mobile']);
		$Email = mysqli_real_escape_string($con,$_POST['Email']);
		$PANNO = mysqli_real_escape_string($con,$_POST['PANNO']);
		$GSTIN = mysqli_real_escape_string($con,$_POST['GSTIN']);
		$State = mysqli_real_escape_string($con,$_POST['State']);
		$StateCode = mysqli_real_escape_string($con,$_POST['StateCode']);
		$BankName = mysqli_real_escape_string($con,$_POST['BankName']);
		$BankAcNo = mysqli_real_escape_string($con,$_POST['BankAcNo']);
		$BankIFSC = mysqli_real_escape_string($con,$_POST['BankIFSC']);
		$BankBranch = mysqli_real_escape_string($con,$_POST['BankBranch']);
		
		$AddedBy = $_POST['AdminID'];
		$AddedDate = date("Y-m-d H:i:s");
		
		$Insert = "INSERT INTO `company_master`(`company_name`, `company_alias`, `company_code`, `address`, `phone`, `mobile`, `email`, `gstin`, `pan_no`, `state`, `state_code`, `bank_name`, `bank_ac_no`, `bank_ifsc`, `bank_branch`, `addedby`, `addeddate`) VALUES ('$CompanyName','$CompanyAlias','$CompanyCode','$Address','$Phone','$Mobile','$Email','$GSTIN','$PANNO','$State','$StateCode','$BankName','$BankAcNo','$BankIFSC','$BankBranch','$AddedBy','$AddedDate')";
		$InsertQuery = mysqli_query($con,$Insert);
		
		if(!$InsertQuery)
		{
			//die(mysqli_error($con));
			if(mysqli_errno($con)==1062)
			{
				$jsonData = '{ 
					"Status":"0"
				}';
				echo $jsonData;
				exit();
			}
			else
			{
				$jsonData = '{ 
					"Status":"1"
				}';
				echo $jsonData;
				exit();
			}
		}
		else
		{
			$jsonData = '{ 
				"Status":"2"
			}';
			echo $jsonData;
			exit();
		}
	}
	
	else if($Action == 'UpdateCompany')
	{
		$CompanyID = $_POST['CompanyID'];
		$CompanyName = mysqli_real_escape_string($con,$_POST['CompanyName']);
		$CompanyAlias = mysqli_real_escape_string($con,$_POST['CompanyAlias']);
		$CompanyCode = mysqli_real_escape_string($con,$_POST['CompanyCode']);
		$Address = mysqli_real_escape_string($con,$_POST['Address']);
		$Phone = mysqli_real_escape_string($con,$_POST['Phone']);
		$Mobile = mysqli_real_escape_string($con,$_POST['Mobile']);
		$Email = mysqli_real_escape_string($con,$_POST['Email']);
		$PANNO = mysqli_real_escape_string($con,$_POST['PANNO']);
		$GSTIN = mysqli_real_escape_string($con,$_POST['GSTIN']);
		$State = mysqli_real_escape_string($con,$_POST['State']);
		$StateCode = mysqli_real_escape_string($con,$_POST['StateCode']);
		$BankName = mysqli_real_escape_string($con,$_POST['BankName']);
		$BankAcNo = mysqli_real_escape_string($con,$_POST['BankAcNo']);
		$BankIFSC = mysqli_real_escape_string($con,$_POST['BankIFSC']);
		$BankBranch = mysqli_real_escape_string($con,$_POST['BankBranch']);
		
		$ModifiedBy = $_POST['AdminID'];
		$ModifiedDate = date("Y-m-d H:i:s");
		
		$Update = "UPDATE `company_master` SET `company_name`='$CompanyName',`company_alias`='$CompanyAlias',`company_code`='$CompanyCode',`address`='$Address',`phone`='$Phone',`mobile`='$Mobile',`email`='$Email',`gstin`='$GSTIN',`pan_no`='$PANNO',`state`='$State',`state_code`='$StateCode',`bank_name`='$BankName',`bank_ac_no`='$BankAcNo',`bank_ifsc`='$BankIFSC',`bank_branch`='$BankBranch',`modifiedby`='$ModifiedBy',`modifieddate`='$ModifiedDate' WHERE company_id='".$CompanyID."'";
		
		$UpdateQuery = mysqli_query($con,$Update);
		
		if(!$UpdateQuery)
		{
			//die(mysqli_error($con));
			if(mysqli_errno($con)==1062)
			{
				$jsonData = '{ 
					"Status":"0"
				}';
				echo $jsonData;
				exit();
			}
			else
			{
				$jsonData = '{ 
					"Status":"3"
				}';
				echo $jsonData;
				exit();
			}
		}
		else
		{
			$jsonData = '{ 
				"Status":"4"
			}';
			echo $jsonData;
			exit();
		}
	}
	else if($Action == 'RemoveCompany')
	{
		$CompanyID = $_POST['CompanyID'];
		
		$Remove = "DELETE FROM company_master WHERE company_id='".$CompanyID."'";
		$RemoveQuery = mysqli_query($con,$Remove);
		
		if(!$RemoveQuery)
		{
			if(mysqli_errno($con)==1451)
			{
				$jsonData = '{ 
					"Status":"0"
				}';
				echo $jsonData;
				exit();
			}
			else
			{
				$jsonData = '{ 
					"Status":"5"
				}';
				echo $jsonData;
				exit();
			}
		}
		else
		{
			$jsonData = '{ 
				"Status":"6"
			}';
			echo $jsonData;
			exit();
		}
	}
	else
	{
		$jsonData = '{ 
			"Status":"Unauthorised Access!"
		}';
		echo $jsonData;
		exit();
	}
}
else
{
	$jsonData = '{ 
		"Status":"Unauthorised Access!"
	}';
	echo $jsonData;
	exit();
}
ob_flush();
?>