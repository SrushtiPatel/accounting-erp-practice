<?php
ob_start();
session_start();
require_once('dbconfig.php');
require_once('functions.php');
require_once('objects.php');

if($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['Action']))
{
	$Action = $_POST['Action'];

	/*
	 *  For Class Client 
	 */

	if($Action == '')
	{
		
	}
	
	else if ($Action == 'GetCustomField')
	{
		$GroupID = $_POST['GroupID'];

		$CustomField = '';
		
		$SelectCustomField = mysqli_query($con,"SELECT * FROM ledger_custom_fields WHERE group_id='".$GroupID."'");
		$countCustomField = mysqli_num_rows($SelectCustomField);

		
		if($countCustomField > 0)
		{

			while($CustField = mysqli_fetch_array($SelectCustomField))
			{
				$LCFID = $CustField['lcf_id'];
				$LCFName = $CustField['lcf_name'];
				$LCFType = $CustField['lcf_type'];
				$LCFIsRequired = $CustField['is_required'];
				$LCFPlaceholder = $CustField['placeholder'];
				$LCFValues = $CustField['lcf_values'];


				$CustomField .= $ledgerObject->customerFieldsForLedger($LCFID,$LCFName,$LCFType,$LCFIsRequired,$LCFPlaceholder,$LCFValues);
												
			}
			
		}	
		echo $CustomField;
		exit();
	}
	else if($Action == 'GetBankCustomField')
	{
		$ReceiptMode = $_POST['ReceiptMode'];
		
		$BankCustomField = '';
		$BankCustomField .= $salesObject->getBankCustomFieldForReceipt($ReceiptMode);
		
		echo $BankCustomField;
		exit();
	}
	else if($Action == 'GetBankCustomPaymentField')
	{
		$PaymentMode = $_POST['PaymentMode'];
		
		$PaymentBankCustomField = '';
		$PaymentBankCustomField .= $purchaseObject->getBankCustomFieldForPayment($PaymentMode);
		
		echo $PaymentBankCustomField;
		exit();
	}

	else if($Action == 'GetAllTaxDetail')
	{
		$TaxID = $_POST['TaxID'];
		$jsonData = $taxObject->getTaxDetail($TaxID);
		
		echo $jsonData;
		exit();
	}
	else if($Action == 'GetItemDetail')
	{
		$ItemID = $_POST['ItemID'];
		$jsonData = $itemObject->selectItemDetail($ItemID);
		
		echo $jsonData;
		exit();
	}

	else if($Action == 'GetVendorDetail')
	{
		$VendorID = $_POST['VendorID'];

		$VendorDetail = $ledgerObject->getVendorDetail($VendorID);
		
		echo $VendorDetail;
		exit();
	}

	else if($Action == 'GetClientDetail')
	{
		$ClientID = $_POST['ClientID'];

		$ClientDetail = $ledgerObject->getClientDetail($ClientID);
		
		echo $ClientDetail;
		exit();
	}
	else if($Action == 'LoadInvoice')
	{
		$ClientID = $_POST['ClientID'];
		$InvoiceItem = '';
		
		if($ClientID > 0)
		{
			$SelectInvoice = mysqli_query($con,"SELECT * FROM invoice_master WHERE client_id='".$ClientID."'");
			$Count = mysqli_num_rows($SelectInvoice);
			if($Count > 0)
			{
				while($Invoice = mysqli_fetch_array($SelectInvoice))
				{
					$InvoiceID = $Invoice['invoice_id'];
					$ItemIndex = $Invoice['invoice_id'];
					$InvoiceNo = $Invoice['invoice_no'];
					$GrandTotal = $Invoice['grand_total'];

					$SelectReceiptItems = mysqli_query($con,"SELECT SUM(tds_amount), SUM(amount) FROM receipt_voucher_items WHERE invoice_id='".$InvoiceID."'");
					$ReceiptItems = mysqli_fetch_array($SelectReceiptItems);
					$TDSAmount = $ReceiptItems['SUM(tds_amount)'];
					$Amount = $ReceiptItems['SUM(amount)'];
					$Total = $TDSAmount + $Amount;
					$Received = $Total;
					$Outstanding = $GrandTotal - $Total;

					$InvoiceItem .= '<tr data-item-index="'.$ItemIndex.'">
										<td class="no-padding text-center">
											<input type = "hidden" id="InvoiceID[]" name="InvoiceID[]" value="'.$InvoiceID.'" required />
											<input type="text" id="InvoiceNo[]" name="InvoiceNo[]" value="'.$InvoiceNo.'" class="col-xs-12" style="border: none;" readonly />
										</td>
										<td class="no-padding text-center">
											<input type="text" id="Received[]" name="Received[]" class="col-xs-12" value="'.$Received.'" style="border: none;" readonly />
										</td>
										<td class="no-padding text-center">
											<input type="text" id="Outstanding[]" name="Outstanding[]" value="'.$Outstanding.'" class="col-xs-12" style="border: none;" readonly />
										</td>
										<td class="no-padding text-center">
											<input type="text" id="TDSAmount[]" name="TDSAmount[]" onKeyPress="return NuMValidation(event);" onKeyUp="return grandTotal();" class="col-xs-12" style="border: none;" />
										</td>
										<td class="no-padding text-center">
											<input type="text" id="Amount[]" name="Amount[]" onKeyPress="return NuMValidation(event);" onKeyUp="return grandTotal();" class="col-xs-12" style="border: none;" />
										</td>
									</tr>
					';
				}
					$InvoiceItem .='<tr>
										<td></td>
										<td></td>
										<td class="text-center">Total</td>
										<td class="no-padding text-center">
											<input type = "text" id="TotalTDSAmount" name="TotalTDSAmount" onFocus="return grandTotal();" class="col-xs-12" style="border: none;" readonly />
										</td>
										<td class="no-padding text-center">
											<input type = "text" id="TotalAmount" name="TotalAmount" onFocus="return grandTotal();" class="col-xs-12" style="border: none;" readonly />
										</td>
									</tr>
					';
			}
			else
			{
				$InvoiceItem .= '<tr>
								<th colspan="5" class="text-center">No Invoice Available For This Client.</th>
							</tr>
			';
			}
		}
		else
		{
			$InvoiceItem .= '<tr>
								<th colspan="5" class="text-center">Client is Not Select.</th>
							</tr>
			';
		}
		echo $InvoiceItem;
		exit();
	}
	else if($Action == 'LoadPurchase')
	{
		$VendorID = $_POST['VendorID'];
		$PurchaseItem = '';
		
		if($VendorID > 0)
		{
			$SelectPurchase = mysqli_query($con,"SELECT * FROM purchase_master WHERE vendor_id='".$VendorID."'");
			$Count = mysqli_num_rows($SelectPurchase);
			if($Count > 0)
			{
				while($Purchase = mysqli_fetch_array($SelectPurchase))
				{
					$PurchaseID = $Purchase['purchase_id'];
					$ItemIndex = $Purchase['purchase_id'];
					$VoucherNo = $Purchase['voucher_no'];
					$GrandTotal = $Purchase['grand_total'];

					$SelectPaymentItems = mysqli_query($con,"SELECT SUM(tds_amount), SUM(amount) FROM payment_voucher_items WHERE purchase_id='".$PurchaseID."'");
					$PaymentItems = mysqli_fetch_array($SelectPaymentItems);
					$TDSAmount = $PaymentItems['SUM(tds_amount)'];
					$Amount = $PaymentItems['SUM(amount)'];
					$Total = $TDSAmount + $Amount;
					$Paid = $Total;
					$Outstanding = $GrandTotal - $Total;

					$PurchaseItem .= '<tr data-item-index="'.$ItemIndex.'">
										<td class="no-padding text-center">
											<input type = "hidden" id="PurchaseID[]" name="PurchaseID[]" value="'.$PurchaseID.'" required />
											<input type="text" id="VoucherNo[]" name="VoucherNo[]" value="'.$VoucherNo.'" class="col-xs-12" style="border: none;" readonly />
										</td>
										<td class="no-padding text-center">
											<input type="text" id="Paid[]" name="Paid[]" class="col-xs-12" value="'.$Paid.'" style="border: none;" readonly />
										</td>
										<td class="no-padding text-center">
											<input type="text" id="Outstanding[]" name="Outstanding[]" value="'.$Outstanding.'" class="col-xs-12" style="border: none;" readonly />
										</td>
										<td class="no-padding text-center">
											<input type="text" id="TDSAmount[]" name="TDSAmount[]" onKeyPress="return NuMValidation(event);" onKeyUp="return grandTotal();" class="col-xs-12" style="border: none;" />
										</td>
										<td class="no-padding text-center">
											<input type="text" id="Amount[]" name="Amount[]" onKeyPress="return NuMValidation(event);" onKeyUp="return grandTotal();" class="col-xs-12" style="border: none;" />
										</td>
									</tr>
					';
				}
					$PurchaseItem .='<tr>
										<td></td>
										<td></td>
										<td class="text-center">Total</td>
										<td class="no-padding text-center">
											<input type = "text" id="TotalTDSAmount" name="TotalTDSAmount" onFocus="return grandTotal();" class="col-xs-12" style="border: none;" readonly />
										</td>
										<td class="no-padding text-center">
											<input type = "text" id="TotalAmount" name="TotalAmount" onFocus="return grandTotal();" class="col-xs-12" style="border: none;" readonly />
										</td>
									</tr>
					';
			}
			else
			{
				$PurchaseItem .= '<tr>
									<th colspan="5" class="text-center">No Invoice Available For This Vendor.</th>
								</tr>';
			}
		}
		else
		{
			$PurchaseItem .= '<tr>
								<th colspan="5" class="text-center"> Vendor is Not Select.</th>
							</tr>';
		}
		echo $PurchaseItem;
		exit();
	}
	else
	{
		$jsonData = '{ 
			"Status":"Unauthorised Access!"
		}';
		echo $jsonData;
		exit();
	}
}
if($_SERVER["REQUEST_METHOD"] == "GET" && isset($_GET['Action']))
{
	$Action = $_GET['Action'];
	
	if($Action == '')
	{
		
	}
	/*else if($Action == 'GetAllPurchase')
	{
		$jsonData = $purchaseObject->getAllPurchase();
		
		echo $jsonData;
		exit();
	}*/
	else if($Action == 'GetAllCategorys')
	{
		
		$jsonData = $categoryObject->getAllCategorys();
		
		echo $jsonData;
		exit();
	}
	else
	{
		$jsonData = '{ 
			"Status":"Unauthorised Access!"
		}';
		echo $jsonData;
		exit();
	}
}
else
{
	$jsonData = '{ 
		"Status":"Unauthorised Access!"
	}';
	echo $jsonData;
	exit();
}
ob_flush();
?>