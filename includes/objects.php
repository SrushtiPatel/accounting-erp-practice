<?php
// Objects & Basic Req
$profileObject = new Profile;
$groupObject = new Group;
$ledgerObject = new Ledger;
$taxObject = new Tax;
$itemObject = new Item;
$salesObject = new Sales;
$purchaseObject = new Purchase;
$bankingObject = new Banking;

$FinancialYear = $profileObject->selectFinancialYear($_SESSION['AdminID']);
$FinancialYear = json_decode($FinancialYear,true);
$FYearStart = $FinancialYear['FYearStart'];
$FYearEnd = $FinancialYear['FYearEnd'];
$FYearStart_dmY = date("d-m-Y",strtotime($FYearStart));
$FYearEnd_dmY = date("d-m-Y",strtotime($FYearEnd));

$CurrentCompanyID = $profileObject->selectAdminCompanyID($_SESSION['AdminID']);

/*$CurrentCompanyDetail = $profileObject->selectCompanyProfile($AdminCompanyID);
$CurrentCompanyDetail = json_decode($CurrentCompanyDetail,true);
$CurrentCompanyID = $CurrentCompanyDetail['CompanyID'];*/
?>