<?php
ob_start();
session_start();
require_once('dbconfig.php');
require_once('functions.php');
require_once('objects.php');

$LogOutTime = date("Y-m-d H:i:s");
$UpdateLoginHistory = mysqli_query($con,"UPDATE `admin_login_history` SET `logout_datetime`='$LogOutTime' WHERE `login_id` = '".$_SESSION['LoginID']."'");

$UpdateLive = mysqli_query($con,"UPDATE `admin_master` SET `live`='0' WHERE admin_id='".$_SESSION['AdminID']."'");

$_SESSION = array();
if(isset($_COOKIE["AdminID"]) && isset($_COOKIE["AdminName"]) && isset($_COOKIE["UserName"])) 
{
	setcookie("AdminID", '', strtotime( '-5 days' ), '/');
	setcookie("AdminName", '', strtotime( '-5 days' ), '/');
	setcookie("UserName", '', strtotime( '-5 days' ), '/');
}

session_destroy();

header("Location: ../index.php");
exit();

ob_flush();
?>