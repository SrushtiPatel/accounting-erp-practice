<?php
ob_start();
session_start();
require_once('dbconfig.php');
if($_SERVER["REQUEST_METHOD"] == "POST")
{
	$Action = $_POST['Action'];
	if($Action == 'AddGroup')
	{
		$GroupName = mysqli_real_escape_string($con,$_POST['GroupName']);
		$ParentID = mysqli_real_escape_string($con,$_POST['ParentID']);
		
		$AddedBy = $_POST['AdminID'];
		$AddedDate = date("Y-m-d H:i:s");

		$Insert = "INSERT INTO `group_master`(`group_name`, `parent_id`, `visible`, `edit`, `remove`, `addedby`, `addeddate`) VALUES ('$GroupName',$ParentID,'1','1','1','$AddedBy','$AddedDate')";
		
		$InsertQuery = mysqli_query($con,$Insert);
		
		if(!$InsertQuery)
		{
			if(mysqli_errno($con)==1062)
			{
				$jsonData = '{ 
					"Status":"0"
				}';
				echo $jsonData;
				exit();
			}
			else
			{
				//die(mysqli_error($con));
				$jsonData = '{ 
					"Status":"1"
				}';
				echo $jsonData;
				exit();
			}
		}
		else
		{
			$jsonData = '{ 
				"Status":"2"
			}';
			echo $jsonData;
			exit();
		}
	}
	else if($Action == 'UpdateGroup')
	{
		$GroupID = $_POST['GroupID'];
		$GroupName = mysqli_real_escape_string($con,$_POST['GroupName']);
		$ParentID = mysqli_real_escape_string($con,$_POST['ParentID']);
		
		$ModifiedBy = $_POST['AdminID'];
		$ModifiedDate = date("Y-m-d H:i:s");

		$Update = "UPDATE `group_master` SET `group_name`='$GroupName',`parent_id`=$ParentID,`modifiedby`='$ModifiedBy',`modifieddate`='$ModifiedDate' WHERE `group_id`='".$GroupID."'";

		$UpdateQuery = mysqli_query($con,$Update);
		
		if(!$UpdateQuery)
		{
			if(mysqli_errno($con)==1062)
			{
				$jsonData = '{ 
					"Status":"0"
				}';
				echo $jsonData;
				exit();
			}
			else if(mysqli_errno($con)==1451)
			{
				$jsonData = '{ 
					"Status":"00"
				}';
				echo $jsonData;
				exit();
			}
			else
			{
				$jsonData = '{ 
					"Status":"3"
				}';
				echo $jsonData;
				exit();
			}
		}
		else
		{
			$jsonData = '{ 
				"Status":"4"
			}';
			echo $jsonData;
			exit();
		}
	}
	else if($Action == 'RemoveGroup')
	{
		$GroupID = $_POST['GroupID'];
				
		$Remove = "DELETE FROM `group_master` WHERE `group_id`='".$GroupID."'";
		$RemoveQuery = mysqli_query($con,$Remove);
			
		if(!$RemoveQuery)
		{
			if(mysqli_errno($con)==1451)
			{
				$jsonData = '{ 
					"Status":"00"
				}';
				echo $jsonData;
				exit();
			}
			else
			{
				$jsonData = '{ 
					"Status":"5"
				}';
				echo $jsonData;
				exit();
			}
		}
		else
		{
			$jsonData = '{ 
				"Status":"6"
			}';
			echo $jsonData;
			exit();
		}
	}
	else
	{
		$jsonData = '{ 
			"Status":"Unauthorised Access!"
		}';
		echo $jsonData;
		exit();
	}
}
else
{
	$jsonData = '{ 
		"Status":"Unauthorised Access!"
	}';
	echo $jsonData;
	exit();
}
ob_flush();
?>