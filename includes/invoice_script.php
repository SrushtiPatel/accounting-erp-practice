<?php
ob_start();
session_start();
ini_set('error_reporting', 0);
ini_set('display_errors', 0);
require_once('dbconfig.php');
require_once('functions.php');
require_once('objects.php');
if($_SERVER["REQUEST_METHOD"] == "POST")
{
	$Action = $_POST['Action'];
	
	if($Action == 'AddInvoice')
	{
		$ClientID = $_POST['ClientID'];
		$CompanyID = $_POST['CompanyID'];
		$InvoiceNo = $_POST['InvoiceNo'];
		$InvoiceNo = $salesObject->nextInvoiceNo($CurrentCompanyID);
		$InvoiceDate = $_POST['InvoiceDate'];
		$ChallanNo = $_POST['ChallanNo'];
		$ChallanDate = $_POST['ChallanDate'];
		$PONo = $_POST['PONo'];
		$PODate = $_POST['PODate'];
		$DueDate = $_POST['DueDate'];
		$PlaceOfSupply = $_POST['PlaceOfSupply'];
		$TransportationMode = $_POST['TransportationMode'];
		$VehicleNo = $_POST['VehicleNo'];
		$DateOfSupply = $_POST['DateOfSupply'];
		$TermsCondition = $_POST['TermsCondition'];

		$BillingName = $_POST['BillingName'];
		$BillingMobile = $_POST['BillingMobile'];
		$BillingGSTIN = $_POST['BillingGSTIN'];
		$BillingAddress = $_POST['BillingAddress'];
		$BillingCity = $_POST['BillingCity'];
		$BillingState = $_POST['BillingState'];
		$BillingStateCode = $_POST['BillingStateCode'];
		$BillingCountry = $_POST['BillingCountry'];
		$BillingPinCode = $_POST['BillingPinCode'];
		
		$ShippingName = $_POST['ShippingName'];
		$ShippingGSTIN = $_POST['ShippingGSTIN'];
		$ShippingAddress = $_POST['ShippingAddress'];
		$ShippingCity = $_POST['ShippingCity'];
		$ShippingState = $_POST['ShippingState'];
		$ShippingStateCode = $_POST['ShippingStateCode'];
		$ShippingCountry = $_POST['ShippingCountry'];
		$ShippingPinCode = $_POST['ShippingPinCode'];

		$OCName = $_POST['OCName'];
		$OCAddress = $_POST['OCAddress'];
		$OCPhone = $_POST['OCPhone'];
		$OCMobile = $_POST['OCMobile'];
		$OCEmail = $_POST['OCEmail'];
		$OCPANNo = $_POST['OCPANNo'];
		$GSTIN = $_POST['GSTIN'];
		$State = $_POST['State'];
		$StateCode = $_POST['StateCode'];
		$OCBankName = $_POST['OCBankName'];
		$OCBankAcNo = $_POST['OCBankAcNo'];
		$OCBankIFSC = $_POST['OCBankIFSC'];
		$OCBankBranch = $_POST['OCBankBranch'];
		
		$TotalAmountBeforeTax = $_POST['TotalAmountBeforeTax'];
		$TotalDiscount = $_POST['TotalDiscount'];
		$TotalTaxAmount = $_POST['TotalTaxAmount'];
		$RoundOff = $_POST['RoundOff'];
		$GrandTotal = $_POST['GrandTotal'];
		
		//Array List
		$ItemID = $_POST['ItemID'];
		$ItemName = $_POST['ItemName'];
		$HSNCode = $_POST['HSNCode'];
		$Unit = $_POST['Unit'];
		$Description = $_POST['Description'];
		$Quantity = $_POST['Quantity'];
		$Rate = $_POST['Rate'];
		$Amount = $_POST['Amount'];
		$Discount = $_POST['Discount'];
		$TaxableValue = $_POST['TaxableValue'];
		$Tax1  = $_POST['Tax1'];
		$Tax1Rate  = $_POST['Tax1Rate'];
		$Tax1Value = $_POST['Tax1Value'];
		$Tax2  = $_POST['Tax2'];
		$Tax2Rate  = $_POST['Tax2Rate'];
		$Tax2Value = $_POST['Tax2Value'];
		$Tax3  = $_POST['Tax3'];
		$Tax3Rate  = $_POST['Tax3Rate'];
		$Tax3Value = $_POST['Tax3Value'];
		$Total =$_POST['Total'];
		
		$Length = count($ItemID);
		
		if(!empty($InvoiceDate)) { $InvoiceDate = date("Y-m-d",strtotime($_POST['InvoiceDate'])); }
		if(!empty($ChallanDate)) { $ChallanDate = date("Y-m-d",strtotime($_POST['ChallanDate'])); }
		if(!empty($PODate)) { $PODate = date("Y-m-d",strtotime($_POST['PODate'])); }
		if(!empty($DueDate)) { $DueDate = date("Y-m-d",strtotime($_POST['DueDate'])); }
		if(!empty($DateOfSupply)) { $DateOfSupply = date("Y-m-d",strtotime($_POST['DateOfSupply'])); }
		
		$AddedBy = $_POST['AdminID'];
		$AddedDate = date("Y-m-d H:i:s");
		
		$Insert = "INSERT INTO `invoice_master`(`company_id`,`invoice_no`, `invoice_date`, `challan_no`, `challan_date`, `po_no`, `po_date`, `place_of_supply`, `transportation_mode`, `vehicle_no`, `date_of_supply`, `due_date`, `client_id`, `billing_name`, `billing_mobile`, `billing_gstin`, `billing_address`, `billing_city`, `billing_state`, `billing_state_code`, `billing_country`, `billing_pin_code`, `shipping_name`, `shipping_gstin`, `shipping_address`, `shipping_city`, `shipping_state`, `shipping_state_code`, `shipping_country`, `shipping_pin_code`, `total_amount_before_tax`, `discount`, `total_tax_amount`, `roundoff`, `grand_total`, `terms_conditions`, `oc_name`, `oc_address`, `oc_phone`, `oc_mobile`, `oc_email`, `oc_pan_no`, `oc_gstin`, `oc_state`, `oc_state_code`,  `oc_bank_name`, `oc_bank_ac_no`, `oc_bank_ifsc`, `oc_bank_branch`, `addedby`, `addeddate`) VALUES ('$CompanyID','$InvoiceNo','$InvoiceDate','$ChallanNo','$ChallanDate','$PONo','$PODate','$PlaceOfSupply','$TransportationMode','$VehicleNo','$DateOfSupply','$DueDate','$ClientID','$BillingName','$BillingMobile','$BillingGSTIN','$BillingAddress','$BillingCity','$BillingState','$BillingStateCode','$BillingCountry','$BillingPinCode','$ShippingName','$ShippingGSTIN','$ShippingAddress','$ShippingCity','$ShippingState','$ShippingStateCode','$ShippingCountry','$ShippingPinCode','$TotalAmountBeforeTax','$TotalDiscount','$TotalTaxAmount','$RoundOff','$GrandTotal','$TermsCondition','$OCName','$OCAddress','$OCPhone','$OCMobile','$OCEmail','$OCPANNo','$GSTIN','$State','$StateCode','$OCBankName','$OCBankAcNo','$OCBankIFSC','$OCBankBranch','$AddedBy','$AddedDate')";
		
		$InsertQuery = mysqli_query($con,$Insert);
		
		$InvoiceID = mysqli_insert_id($con);
		
		if($InsertQuery)
		{
			$UpdateBalance = $ledgerObject->updateLedgerCurrentBalance($ClientID,$GrandTotal,'+');
			
			for($i = 0; $i < $Length; $i++)
			{
				$I_ItemID = $ItemID[$i];
				$I_ItemName = $ItemName[$i];
				$I_HSNCode = $HSNCode[$i];
				$I_Unit = $Unit[$i];
				$I_Description = $Description[$i];
				$I_Quantity = $Quantity[$i];
				$I_Rate = $Rate[$i];
				$I_Amount = $Amount[$i];
				$I_Discount = $Discount[$i];
				$I_TaxableValue = $TaxableValue[$i];
				$I_Tax1 = $Tax1[$i];
				$I_Tax1Rate = $Tax1Rate[$i];
				$I_Tax1Value = $Tax1Value[$i];
				$I_Tax2 = $Tax2[$i];
				$I_Tax2Rate = $Tax2Rate[$i];
				$I_Tax2Value = $Tax2Value[$i];
				$I_Tax3 = $Tax3[$i];
				$I_Tax3Rate = $Tax3Rate[$i];
				$I_Tax3Value = $Tax3Value[$i];
				$I_Total = $Total[$i];
				
				$InsertItem = mysqli_query($con,"INSERT INTO `invoice_item_master`(`invoice_id`, `item_id`,`item_name`, `description`, `hsn_code`, `qty`, `unit`, `rate`, `discount`, `amount`, `taxable_value`, `tax1_id`, `tax1_rate`, `tax1_amount`, `tax2_id`, `tax2_rate`, `tax2_amount`, `tax3_id`, `tax3_rate`, `tax3_amount`, `total`) VALUES ('$InvoiceID','$I_ItemID','$I_ItemName','$I_Description','$I_HSNCode','$I_Quantity','$I_Unit','$I_Rate','$I_Discount','$I_Amount','$I_TaxableValue','$I_Tax1','$I_Tax1Rate','$I_Tax1Value','$I_Tax2','$I_Tax2Rate','$I_Tax2Value','$I_Tax3','$I_Tax3Rate','$I_Tax3Value','$I_Total')");
				if($InsertItem)
				{
					if($I_Tax1 > 0)
					{
						$UpdateTax1 = $ledgerObject->updateLedgerCurrentBalance($I_Tax1,$I_Tax1Value,'-');
					}
					if($I_Tax2 > 0)
					{
						$UpdateTax2 = $ledgerObject->updateLedgerCurrentBalance($I_Tax2,$I_Tax2Value,'-');
					}
					if($I_Tax3 > 0)
					{
						$UpdateTax3 = $ledgerObject->updateLedgerCurrentBalance($I_Tax3,$I_Tax3Value,'-');
					}
				}
				else
				{
					$jsonData = '{ 
						"Status":"1"
					}';
					echo $jsonData;
					exit();
				}
			}
			if(!$InsertQuery)
			{
				$jsonData = '{ 
					"Status":"1"
				}';
				echo $jsonData;
				exit();
			}
			else
			{
				$jsonData = '{ 
					"Status":"2"
				}';
				echo $jsonData;
				exit();
			}
		}
		else
		{
			if(mysqli_errno($con)==1062)
			{
				$jsonData = '{ 
					"Status":"0"
				}';
				echo $jsonData;
				exit();
			}
			else
			{
				$jsonData = '{ 
					"Status":"1"
				}';
				echo $jsonData;
				exit();
			}
		}
			
	}
	else if($Action == 'UpdateInvoice')
	{
		$InvoiceID = $_POST['InvoiceID'];
		$CompanyID = $_POST['CompanyID'];
		$ClientID = $_POST['ClientID'];
		$InvoiceNo = $_POST['InvoiceNo'];
		$InvoiceDate = $_POST['InvoiceDate'];
		$ChallanNo = $_POST['ChallanNo'];
		$ChallanDate = $_POST['ChallanDate'];
		$PONo = $_POST['PONo'];
		$PODate = $_POST['PODate'];
		$DueDate = $_POST['DueDate'];
		$PlaceOfSupply = $_POST['PlaceOfSupply'];
		$TransportationMode = $_POST['TransportationMode'];
		$VehicleNo = $_POST['VehicleNo'];
		$DateOfSupply = $_POST['DateOfSupply'];
		$TermsCondition = $_POST['TermsCondition'];

		$BillingName = $_POST['BillingName'];
		$BillingMobile = $_POST['BillingMobile'];
		$BillingGSTIN = $_POST['BillingGSTIN'];
		$BillingAddress = $_POST['BillingAddress'];
		$BillingCity = $_POST['BillingCity'];
		$BillingState = $_POST['BillingState'];
		$BillingStateCode = $_POST['BillingStateCode'];
		$BillingCountry = $_POST['BillingCountry'];
		$BillingPinCode = $_POST['BillingPinCode'];
		
		$ShippingName = $_POST['ShippingName'];
		$ShippingGSTIN = $_POST['ShippingGSTIN'];
		$ShippingAddress = $_POST['ShippingAddress'];
		$ShippingCity = $_POST['ShippingCity'];
		$ShippingState = $_POST['ShippingState'];
		$ShippingStateCode = $_POST['ShippingStateCode'];
		$ShippingCountry = $_POST['ShippingCountry'];
		$ShippingPinCode = $_POST['ShippingPinCode'];
		

		$OCName = $_POST['OCName'];
		$OCAddress = $_POST['OCAddress'];
		$OCPhone = $_POST['OCPhone'];
		$OCMobile = $_POST['OCMobile'];
		$OCEmail = $_POST['OCEmail'];
		$OCPANNo = $_POST['OCPANNo'];
		$GSTIN = $_POST['GSTIN'];
		$State = $_POST['State'];
		$StateCode = $_POST['StateCode'];
		$OCBankName = $_POST['OCBankName'];
		$OCBankAcNo = $_POST['OCBankAcNo'];
		$OCBankIFSC = $_POST['OCBankIFSC'];
		$OCBankBranch = $_POST['OCBankBranch'];
		
		$TotalAmountBeforeTax = $_POST['TotalAmountBeforeTax'];
		$TotalDiscount = $_POST['TotalDiscount'];
		$TotalTaxAmount = $_POST['TotalTaxAmount'];
		$RoundOff = $_POST['RoundOff'];
		$GrandTotal = $_POST['GrandTotal'];
		$OldGrandTotal = $_POST['OldGrandTotal'];
		$GrandTotalDiff = $GrandTotal - $OldGrandTotal;
		//Array List
		
		$InvoiceItemID = $_POST['InvoiceItemID'];
		$ItemID = $_POST['ItemID'];
		$ItemName = $_POST['ItemName'];
		$HSNCode = $_POST['HSNCode'];
		$Unit = $_POST['Unit'];
		$Description = $_POST['Description'];
		$Quantity = $_POST['Quantity'];
		$Rate = $_POST['Rate'];
		$Amount = $_POST['Amount'];
		$Discount = $_POST['Discount'];
		$TaxableValue = $_POST['TaxableValue'];
		$Tax1  = $_POST['Tax1'];
		$Tax1Rate  = $_POST['Tax1Rate'];
		$Tax1Value = $_POST['Tax1Value'];
		$Tax2  = $_POST['Tax2'];
		$Tax2Rate  = $_POST['Tax2Rate'];
		$Tax2Value = $_POST['Tax2Value'];
		$Tax3  = $_POST['Tax3'];
		$Tax3Rate  = $_POST['Tax3Rate'];
		$Tax3Value = $_POST['Tax3Value'];
		$Total =$_POST['Total'];
		
		$OldTax1 = $_POST['OldTax1'];
		$OldTax2 = $_POST['OldTax2'];
		$OldTax3 = $_POST['OldTax3'];
		
		$OldTax1Value = $_POST['OldTax1Value'];
		$OldTax2Value = $_POST['OldTax2Value'];
		$OldTax3Value = $_POST['OldTax3Value'];
		
		$Length = count($ItemID);
		
		if(!empty($InvoiceDate)) { $InvoiceDate = date("Y-m-d",strtotime($_POST['InvoiceDate'])); }
		if(!empty($ChallanDate)) { $ChallanDate = date("Y-m-d",strtotime($_POST['ChallanDate'])); }
		if(!empty($PODate)) { $PODate = date("Y-m-d",strtotime($_POST['PODate'])); }
		if(!empty($DueDate)) { $DueDate = date("Y-m-d",strtotime($_POST['DueDate'])); }
		if(!empty($DateOfSupply)) { $DateOfSupply = date("Y-m-d",strtotime($_POST['DateOfSupply'])); }
		
		$ModifiedBy = $_POST['AdminID'];
		$ModifiedDate = date("Y-m-d H:i:s");
		
		$Update = "UPDATE `invoice_master` SET `company_id`='$CompanyID',`invoice_no`='$InvoiceNo',`invoice_date`='$InvoiceDate',`challan_no`='$ChallanNo',`challan_date`='$ChallanDate',`po_no`='$PONo',`po_date`='$PODate',`place_of_supply`='$PlaceOfSupply',`transportation_mode`='$TransportationMode',`vehicle_no`='$VehicleNo',`date_of_supply`='$DateOfSupply',`due_date`='$DueDate',`client_id`='$ClientID',`billing_name`='$BillingName',`billing_mobile`='$BillingMobile',`billing_gstin`='$BillingGSTIN',`billing_address`='$BillingAddress',`billing_city`='$BillingCity',`billing_state`='$BillingState',`billing_state_code`='$BillingStateCode',`billing_country`='$BillingCountry',`billing_pin_code`='$BillingPinCode',`shipping_name`='$ShippingName',`shipping_gstin`='$ShippingGSTIN',`shipping_address`='$ShippingAddress',`shipping_city`='$ShippingCity',`shipping_state`='$ShippingState',`shipping_state_code`='$ShippingStateCode',`shipping_country`='$ShippingCountry',`shipping_pin_code`='$ShippingPinCode',`total_amount_before_tax`='$TotalAmountBeforeTax',`discount`='$TotalDiscount',`total_tax_amount`='$TotalTaxAmount',`roundoff`='$RoundOff',`grand_total`='$GrandTotal',`terms_conditions`='$TermsCondition',`oc_name`='$OCName',`oc_address`='$OCAddress',`oc_phone`='$OCPhone',`oc_mobile`='$OCMobile',`oc_email`='$OCEmail',`oc_pan_no`='$OCPANNo',`oc_gstin`='$GSTIN',`oc_state`='$State',`oc_state_code`='$StateCode',`oc_bank_name`='$OCBankName',`oc_bank_ac_no`='$OCBankAcNo',`oc_bank_ifsc`='$OCBankIFSC',`oc_bank_branch`='$OCBankBranch',`modifiedby`='$ModifiedBy',`modifieddate`='$ModifiedDate' WHERE invoice_id='".$InvoiceID."'";

		$UpdateQuery = mysqli_query($con,$Update);
		
		if($UpdateQuery)
		{
			$UpdateBalance = $ledgerObject->updateLedgerCurrentBalance($ClientID,$GrandTotalDiff,'+');
			
			for($i = 0; $i < $Length; $i++)
			{
				$I_InvoiceItemID = $InvoiceItemID[$i];
				$I_ItemID = $ItemID[$i];
				$I_ItemName = $ItemName[$i];
				$I_HSNCode = $HSNCode[$i];
				$I_Unit = $Unit[$i];
				$I_Description = $Description[$i];
				$I_Quantity = $Quantity[$i];
				$I_Rate = $Rate[$i];
				$I_Amount = $Amount[$i];
				$I_Discount = $Discount[$i];
				$I_TaxableValue = $TaxableValue[$i];
				$I_Tax1 = $Tax1[$i];
				$I_Tax1Rate = $Tax1Rate[$i];
				$I_Tax1Value = $Tax1Value[$i];
				$I_Tax2 = $Tax2[$i];
				$I_Tax2Rate = $Tax2Rate[$i];
				$I_Tax2Value = $Tax2Value[$i];
				$I_Tax3 = $Tax3[$i];
				$I_Tax3Rate = $Tax3Rate[$i];
				$I_Tax3Value = $Tax3Value[$i];
				$I_Total = $Total[$i];
				$I_OldTax1Value = $OldTax1Value[$i];
				$I_OldTax2Value = $OldTax2Value[$i];
				$I_OldTax3Value = $OldTax3Value[$i];
				
				$I_OldTax1 = $OldTax1[$i];
				$I_OldTax2 = $OldTax2[$i];
				$I_OldTax3 = $OldTax3[$i];
				
				if(!empty($I_InvoiceItemID))
				{
					$UpdateItem = mysqli_query($con,"UPDATE `invoice_item_master` SET `description`='$I_Description',`qty`='$I_Quantity',`rate`='$I_Rate',`discount`='$I_Discount',`amount`='$I_Amount',`taxable_value`='$I_TaxableValue',`tax1_id`='$I_Tax1',`tax1_rate`='$I_Tax1Rate',`tax1_amount`='$I_Tax1Value',`tax2_id`='$I_Tax2',`tax2_rate`='$I_Tax2Rate',`tax2_amount`='$I_Tax2Value',`tax3_id`='$I_Tax3',`tax3_rate`='$I_Tax3Rate',`tax3_amount`='$I_Tax3Value',`total`='$I_Total' WHERE invoice_item_id='".$I_InvoiceItemID."'");
					if($UpdateItem)
					{
						if($I_Tax1 > 0)
						{
							$UpdateTax = $ledgerObject->updateLedgerCurrentBalance($I_Tax1,$I_Tax1Value,'-');
						}
						if($I_Tax2 > 0)
						{
							$UpdateTax = $ledgerObject->updateLedgerCurrentBalance($I_Tax2,$I_Tax2Value,'-');
						}
						if($I_Tax3 > 0)
						{
							$UpdateTax = $ledgerObject->updateLedgerCurrentBalance($I_Tax3,$I_Tax3Value,'-');
						}
						if($I_OldTax1 > 0)
						{
							$UpdateTax = $ledgerObject->updateLedgerCurrentBalance($I_OldTax1,$I_OldTax1Value,'+');
						}
						if($I_OldTax2 > 0)
						{
							$UpdateTax = $ledgerObject->updateLedgerCurrentBalance($I_OldTax2,$I_OldTax2Value,'+');
						}
						if($I_OldTax3 > 0)
						{
							$UpdateTax = $ledgerObject->updateLedgerCurrentBalance($I_OldTax3,$I_OldTax3Value,'+');
						}
					}

				}
				else
				{
					$InsertItem = mysqli_query($con,"INSERT INTO `invoice_item_master`(`invoice_id`, `item_id`, `item_name`, `description`, `hsn_code`, `qty`, `unit`, `rate`, `discount`, `amount`, `taxable_value`, `tax1_id`, `tax1_rate`, `tax1_amount`, `tax2_id`, `tax2_rate`, `tax2_amount`, `tax3_id`, `tax3_rate`, `tax3_amount`, `total`) VALUES ('$InvoiceID','$I_ItemID','$I_ItemName','$I_Description','$I_HSNCode','$I_Quantity','$I_Unit','$I_Rate','$I_Discount','$I_Amount','$I_TaxableValue','$I_Tax1','$I_Tax1Rate','$I_Tax1Value','$I_Tax2','$I_Tax2Rate','$I_Tax2Value','$I_Tax3','$I_Tax3Rate','$I_Tax3Value','$I_Total')");
					if($InsertItem)
					{
						if($I_Tax1 > 0)
						{
							$UpdateTax1 = $ledgerObject->updateLedgerCurrentBalance($I_Tax1,$I_Tax1Value,'-');
						}
						if($I_Tax2 > 0)
						{
							$UpdateTax2 = $ledgerObject->updateLedgerCurrentBalance($I_Tax2,$I_Tax2Value,'-');
						}
						if($I_Tax3 > 0)
						{
							$UpdateTax3 = $ledgerObject->updateLedgerCurrentBalance($I_Tax3,$I_Tax3Value,'-');
						}
					}
				}
				
			}
			if(!$UpdateQuery)
			{
				$jsonData = '{ 
					"Status":"3"
				}';
				echo $jsonData;
				exit();
			}
			else
			{
				$jsonData = '{ 
					"Status":"4"
				}';
				echo $jsonData;
				exit();
			}
		}
		else
		{
			if(mysqli_errno($con)==1062)
			{
				$jsonData = '{ 
					"Status":"0"
				}';
				echo $jsonData;
				exit();
			}
			else
			{
				$jsonData = '{ 
					"Status":"3"
				}';
				echo $jsonData;
				exit();
			}
		}
	}
	else if($Action == 'RemoveInvoiceItem')
	{
		$InvoiceItemID = $_POST['InvoiceItemID'];
		
		$SelectInvoiceItem = mysqli_query($con,"SELECT * FROM invoice_item_master WHERE invoice_item_id='".$InvoiceItemID."'");
		$InvoiceItem = mysqli_fetch_array($SelectInvoiceItem);
		$Tax1 = $InvoiceItem['tax1_id'];
		$Tax1Value = $InvoiceItem['tax1_amount'];

		$Tax2 = $InvoiceItem['tax2_id'];
		$Tax2Value = $InvoiceItem['tax2_amount'];

		$Tax3 = $InvoiceItem['tax3_id'];
		$Tax3Value = $InvoiceItem['tax3_amount'];
		
		if($Tax1 > 0)
		{
			$UpdateTax1 = $ledgerObject->updateLedgerCurrentBalance($Tax1,$Tax1Value,'+');
		}
		if($Tax2 > 0)
		{
			$UpdateTax2 = $ledgerObject->updateLedgerCurrentBalance($Tax2,$Tax2Value,'+');
		}
		if($Tax3 > 0)
		{
			$UpdateTax3 = $ledgerObject->updateLedgerCurrentBalance($Tax3,$Tax3Value,'+');
		}
		
		$Remove = "DELETE FROM invoice_item_master WHERE invoice_item_id='".$InvoiceItemID."'";
		$RemoveQuery = mysqli_query($con,$Remove);
		if(!$RemoveQuery)
		{
			$jsonData = '{ 
				"Status":"5"
			}';
			echo $jsonData;
			exit();	
		}
		else
		{
			$jsonData = '{ 
				"Status":"6"
			}';
			echo $jsonData;
			exit();
		}
	}
	else if($Action == 'RemoveInvoice')
	{
		$InvoiceID = $_POST['InvoiceID'];
		
		$SelectInvoice = mysqli_query($con,"SELECT i.client_id, i.grand_total, im.tax1_id, im.tax1_amount, im.tax2_id, im.tax2_amount, im.tax3_id, im.tax3_amount FROM invoice_master i JOIN invoice_item_master im ON i.invoice_id=im.invoice_id WHERE i.invoice_id='".$InvoiceID."'");
		while($Invoice = mysqli_fetch_array($SelectInvoice))
		{
			$ClientID = $Invoice['client_id'];
			$GrandTotal = $Invoice['grand_total'];

			$Tax1 = $Invoice['tax1_id'];
			$Tax1Value = $Invoice['tax1_amount'];

			$Tax2 = $Invoice['tax2_id'];
			$Tax2Value = $Invoice['tax2_amount'];

			$Tax3 = $Invoice['tax3_id'];
			$Tax3Value = $Invoice['tax3_amount'];
			
			$UpdateTax1 = $ledgerObject->updateLedgerCurrentBalance($Tax1,$Tax1Value,'+');

			$UpdateTax2 = $ledgerObject->updateLedgerCurrentBalance($Tax2,$Tax2Value,'+');

			$UpdateTax3 = $ledgerObject->updateLedgerCurrentBalance($Tax3,$Tax3Value,'+');
		}
		$UpdateBalance = $ledgerObject->updateLedgerCurrentBalance($ClientID,$GrandTotal,'-');
		
		$Remove = "DELETE FROM invoice_master WHERE invoice_id='".$InvoiceID."'";
		$RemoveQuery = mysqli_query($con,$Remove);
		if(!$RemoveQuery)
		{
			$jsonData = '{ 
				"Status":"5"
			}';
			echo $jsonData;
			exit();	
		}
		else
		{
			$jsonData = '{ 
				"Status":"6"
			}';
			echo $jsonData;
			exit();
		}
	}
}
else
{
	$jsonData = '{ 
		"Status":"Unauthorised Access!"
	}';
	echo $jsonData;
	exit();
}
ob_flush();
?>