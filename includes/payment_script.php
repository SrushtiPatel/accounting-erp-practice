<?php
ob_start();
session_start();
ini_set('error_reporting', 0);
ini_set('display_errors', 0);
require_once('dbconfig.php');
require_once('functions.php');
require_once('objects.php');
if($_SERVER["REQUEST_METHOD"] == "POST")
{
	$Action = $_POST['Action'];
	
	if($Action == 'AddPayment')
	{
		$CompanyID = $_POST['CompanyID'];
		$PVType = $_POST['PVType'];
		$PVNo = $_POST['PVNo'];
		$PVNo = $purchaseObject->nextInvoicePaymentNo($CurrentCompanyID);
		$PVDate = $_POST['PVDate'];
		$PayTo = $_POST['PayTo'];
		$PayFrom = $_POST['PayFrom'];
		$PaymentMode = $_POST['PaymentMode'];
		$Description = $_POST['Description'];
		$TotalAmount = $_POST['TotalAmount'];
		$TotalTDSAmount = $_POST['TotalTDSAmount'];
		
		$GrandTotal = $TotalTDSAmount + $TotalAmount;
		
		$BankName = '';
		$ChequeNo = '';
		$ChequeDate = '';
		$TransactionDate = '';
		$CardNo = '';
		$TransactionRef = '';
		$PaymentDate = '';
		
		if(isset($_POST['BankName']))
		{
			$BankName = $_POST['BankName'];
		}
		if(isset($_POST['ChequeNo']))
		{
			$ChequeNo = $_POST['ChequeNo'];
		}
		if(isset($_POST['ChequeDate']))
		{
			$ChequeDate = $_POST['ChequeDate'];
		}
		if(isset($_POST['TransactionDate']))
		{
			$TransactionDate = $_POST['TransactionDate'];
		}
		if(isset($_POST['CardNo']))
		{
			$CardNo = $_POST['CardNo'];
		}
		if(isset($_POST['TransactionRef']))
		{
			$TransactionRef = $_POST['TransactionRef'];
		}
		if(isset($_POST['PaymentDate']))
		{
			$PaymentDate = $_POST['PaymentDate'];
		}
		//Array
		
		$PurchaseID = array();
		$TDSAmount = '';
		$Amount = '';
		if(isset($_POST['PurchaseID']))
		{
			$PurchaseID = $_POST['PurchaseID'];
		}
		if(isset($_POST['TDSAmount']))
		{
			$TDSAmount = $_POST['TDSAmount'];
		}
		if(isset($_POST['Amount']))
		{
			$Amount = $_POST['Amount'];
		}
		
		$Length = count($PurchaseID);
		if($Length < 1)
		{
			$jsonData = '{ 
				"Status":"00"
			}';
			echo $jsonData;
			exit();
		}
		
		
		if(!empty($ChequeDate)) { $ChequeDate = date("Y-m-d",strtotime($_POST['ChequeDate'])); }
		if(!empty($PaymentDate)) { $PaymentDate = date("Y-m-d",strtotime($_POST['PaymentDate'])); }
		if(!empty($PVDate)) { $PVDate = date("Y-m-d",strtotime($_POST['PVDate'])); }
		if(!empty($TransactionDate)) { $TransactionDate = date("Y-m-d",strtotime($_POST['TransactionDate'])); }
		
		$AddedBy = $_POST['AdminID'];
		$AddedDate = date("Y-m-d H:i:s");
		
		$Insert = "INSERT INTO `payment_vouchers`(`company_id`,`pv_no`, `pv_date`, `pay_to`, `pay_from`, `payment_mode`, `bank_name`, `cheque_no`, `cheque_date`, `transaction_date`, `card_no`, `transaction_ref`, `payment_date`, `total_amount`, `total_tds_amount`, `pv_type`, `description`) VALUES ('$CompanyID','$PVNo','$PVDate','$PayTo','$PayFrom','$PaymentMode','$BankName','$ChequeNo','$ChequeDate','$TransactionDate','$CardNo','$TransactionRef','$PaymentDate','$TotalAmount','$TotalTDSAmount','$PVType','$Description')";
		
		$InsertQuery = mysqli_query($con,$Insert);
		
		$PVID = mysqli_insert_id($con);
		
		if($InsertQuery)
		{
			$UpdateBalance = $ledgerObject->updateLedgerCurrentBalance($PayFrom,$GrandTotal,'-');
			
			$UpdateCurrentBalance = $ledgerObject->updateLedgerCurrentBalance($PayTo,$GrandTotal,'+');
			
			for($i = 0; $i < $Length; $i++)
			{
				$I_PurchaseID = $PurchaseID[$i];
				$I_TDSAmount = $TDSAmount[$i];
				$I_Amount = $Amount[$i];
				
				if(!empty($I_Amount))
				{
					$InsertItem = mysqli_query($con,"INSERT INTO `payment_voucher_items`(`pv_id`, `purchase_id`, `tds_amount`, `amount`) VALUES ('$PVID','$I_PurchaseID','$I_TDSAmount','$I_Amount')");
					if(!$InsertItem)
					{
						$jsonData = '{ 
							"Status":"1"
						}';
						echo $jsonData;
						exit();
					}	
				}
				
			}
			if(!$InsertQuery)
			{
				$jsonData = '{ 
					"Status":"1"
				}';
				echo $jsonData;
				exit();
			}
			else
			{
				$jsonData = '{ 
					"Status":"2"
				}';
				echo $jsonData;
				exit();
			}
		}
		else
		{
			if(mysqli_errno($con)==1062)
			{
				$jsonData = '{ 
					"Status":"0"
				}';
				echo $jsonData;
				exit();
			}
			else
			{
				$jsonData = '{ 
					"Status":"1"
				}';
				echo $jsonData;
				exit();
			}
		}
			
	}
	else if($Action == 'UpdatePayment')
	{
		$CompanyID = $_POST['CompanyID'];
		$PVID = $_POST['PVID'];
		$PVType = $_POST['PVType'];
		$PVNo = $_POST['PVNo'];
		$PVDate = $_POST['PVDate'];
		$PayTo = $_POST['PayTo'];
		$PayFrom = $_POST['PayFrom'];
		$PaymentMode = $_POST['PaymentMode'];
		$Description = $_POST['Description'];
		$TotalAmount = $_POST['TotalAmount'];
		$OldTotalAmount = $_POST['OldTotalAmount'];
		
		$TotalTDSAmount = '';
		$OldTotalTDSAmount = '';
		
		if(isset($_POST['TotalTDSAmount']))
		{
			$TotalTDSAmount = $_POST['TotalTDSAmount'];
		}
		if(isset($_POST['OldTotalTDSAmount']))
		{
			$OldTotalTDSAmount = $_POST['OldTotalTDSAmount'];
		}
		
		$AmountDiff = $TotalAmount - $OldTotalAmount;
		$TDSAmountDiff = $TotalTDSAmount - $OldTotalTDSAmount;
		
		$GrandTotal = $AmountDiff + $TDSAmountDiff;
		
		$BankName = '';
		$ChequeNo = '';
		$ChequeDate = '';
		$TransactionDate = '';
		$CardNo = '';
		$TransactionRef = '';
		$PaymentDate = '';
		
		if(isset($_POST['BankName']))
		{
			$BankName = $_POST['BankName'];
		}
		if(isset($_POST['ChequeNo']))
		{
			$ChequeNo = $_POST['ChequeNo'];
		}
		if(isset($_POST['ChequeDate']))
		{
			$ChequeDate = $_POST['ChequeDate'];
		}
		if(isset($_POST['TransactionDate']))
		{
			$TransactionDate = $_POST['TransactionDate'];
		}
		if(isset($_POST['CardNo']))
		{
			$CardNo = $_POST['CardNo'];
		}
		if(isset($_POST['TransactionRef']))
		{
			$TransactionRef = $_POST['TransactionRef'];
		}
		if(isset($_POST['PaymentDate']))
		{
			$PaymentDate = $_POST['PaymentDate'];
		}
		//Array
		$PVIID = '';
		$PurchaseID = '';
		$TDSAmount = '';
		$Amount = '';
		if(isset($_POST['PVIID']))
		{
			$PVIID = $_POST['PVIID'];
		}
		if(isset($_POST['PurchaseID']))
		{
			$PurchaseID = $_POST['PurchaseID'];
		}
		if(isset($_POST['TDSAmount']))
		{
			$TDSAmount = $_POST['TDSAmount'];
		}
		if(isset($_POST['Amount']))
		{
			$Amount = $_POST['Amount'];
		}
		
		$Length = count($PurchaseID);
		
		if(!empty($ChequeDate)) { $ChequeDate = date("Y-m-d",strtotime($_POST['ChequeDate'])); }
		if(!empty($PaymentDate)) { $PaymentDate = date("Y-m-d",strtotime($_POST['PaymentDate'])); }
		if(!empty($PVDate)) { $PVDate = date("Y-m-d",strtotime($_POST['PVDate'])); }
		if(!empty($TransactionDate)) { $TransactionDate = date("Y-m-d",strtotime($_POST['TransactionDate'])); }
		
		
		$Update = "UPDATE `payment_vouchers` SET `company_id`='$CompanyID',`pv_no`='$PVNo',`pv_date`='$PVDate',`pay_to`='$PayTo',`pay_from`='$PayFrom',`payment_mode`='$PaymentMode',`bank_name`='$BankName',`cheque_no`='$ChequeNo',`cheque_date`='$ChequeDate',`transaction_date`='$TransactionDate',`card_no`='$CardNo',`transaction_ref`='$TransactionRef',`payment_date`='$PaymentDate',`total_amount`='$TotalAmount',`total_tds_amount`='$TotalTDSAmount',`description`='$Description' WHERE pv_id='".$PVID."'";
		$UpdateQuery = mysqli_query($con,$Update);
		
		if($UpdateQuery)
		{
			if($OldTotalAmount != $TotalAmount || $OldTotalTDSAmount != $TotalTDSAmount)
			{
				$UpdateBalance = $ledgerObject->updateLedgerCurrentBalance($PayFrom,$GrandTotal,'-');
			
				$UpdateCurrentBalance = $ledgerObject->updateLedgerCurrentBalance($PayTo,$GrandTotal,'+');
			}
			for($i = 0; $i < $Length; $i++)
			{
				$I_PVIID = $PVIID[$i];
				$I_PurchaseID = $PurchaseID[$i];
				$I_TDSAmount = $TDSAmount[$i];
				$I_Amount = $Amount[$i];
				
				if(!empty($I_PVIID))
				{
					$UpdateItem = mysqli_query($con,"UPDATE `payment_voucher_items` SET `tds_amount`='$I_TDSAmount',`amount`='$I_Amount' WHERE pvi_id='".$I_PVIID."'");
				}
			}
			if(!$UpdateQuery)
			{
				$jsonData = '{ 
					"Status":"3"
				}';
				echo $jsonData;
				exit();
			}
			else
			{
				$jsonData = '{ 
					"Status":"4"
				}';
				echo $jsonData;
				exit();
			}
		}
		else
		{
			if(mysqli_errno($con)==1062)
			{
				$jsonData = '{ 
					"Status":"0"
				}';
				echo $jsonData;
				exit();
			}
			else
			{
				$jsonData = '{ 
					"Status":"3"
				}';
				echo $jsonData;
				exit();
			}
		}
	}
	else if($Action == 'RemovePayment')
	{
		$PVID = $_POST['PVID'];
		$PayTo = $_POST['PayTo'];
		$PayFrom = $_POST['PayFrom'];
		$TotalAmount = $_POST['TotalAmount'];
		$TotalTDSAmount = '';
		if(isset($_POST['TotalTDSAmount']))
		{
			$TotalTDSAmount = $_POST['TotalTDSAmount'];
		}
		$Total = $TotalAmount + $TotalTDSAmount;
		
		$UpdateBalance = $ledgerObject->updateLedgerCurrentBalance($PayFrom,$Total,'+');
			
		$UpdateCurrentBalance = $ledgerObject->updateLedgerCurrentBalance($PayTo,$Total,'-');
		
		$Remove = "DELETE FROM payment_vouchers WHERE pv_id='".$PVID."'";
		$RemoveQuery = mysqli_query($con,$Remove);
		if($RemoveQuery)
		{
			$jsonData = '{ 
				"Status":"6"
			}';
			echo $jsonData;
			exit();
		}
		else
		{
			if(mysqli_errno($con)==1451)
			{
				$jsonData = '{ 
					"Status":"00"
				}';
				echo $jsonData;
				exit();
			}
			else
			{
				$jsonData = '{ 
					"Status":"5"
				}';
				echo $jsonData;
				exit();
			}	
		}
	}
	// Against Payment End
	//  Advance Payment Start 
	else if($Action == 'AddAdvancePayment')
	{
		$CompanyID = $_POST['CompanyID'];
		$PVType = $_POST['PVType'];
		$PVNo = $_POST['PVNo'];
		$PVNo = $purchaseObject->nextInvoicePaymentNo();
		$PVDate = $_POST['PVDate'];
		$PayTo = $_POST['PayTo'];
		$PayFrom = $_POST['PayFrom'];
		$PaymentMode = $_POST['PaymentMode'];
		$Description = $_POST['Description'];
		$TotalAmount = $_POST['TotalAmount'];
		$TotalTDSAmount = $_POST['TotalTDSAmount'];
		
		$GrandTotal = $TotalTDSAmount + $TotalAmount;
		
		$BankName = '';
		$ChequeNo = '';
		$ChequeDate = '';
		$TransactionDate = '';
		$CardNo = '';
		$TransactionRef = '';
		$PaymentDate = '';
		
		if(isset($_POST['BankName']))
		{
			$BankName = $_POST['BankName'];
		}
		if(isset($_POST['ChequeNo']))
		{
			$ChequeNo = $_POST['ChequeNo'];
		}
		if(isset($_POST['ChequeDate']))
		{
			$ChequeDate = $_POST['ChequeDate'];
		}
		if(isset($_POST['TransactionDate']))
		{
			$TransactionDate = $_POST['TransactionDate'];
		}
		if(isset($_POST['CardNo']))
		{
			$CardNo = $_POST['CardNo'];
		}
		if(isset($_POST['TransactionRef']))
		{
			$TransactionRef = $_POST['TransactionRef'];
		}
		if(isset($_POST['PaymentDate']))
		{
			$PaymentDate = $_POST['PaymentDate'];
		}
		
		if(!empty($ChequeDate)) { $ChequeDate = date("Y-m-d",strtotime($_POST['ChequeDate'])); }
		if(!empty($PaymentDate)) { $PaymentDate = date("Y-m-d",strtotime($_POST['PaymentDate'])); }
		if(!empty($PVDate)) { $PVDate = date("Y-m-d",strtotime($_POST['PVDate'])); }
		if(!empty($TransactionDate)) { $TransactionDate = date("Y-m-d",strtotime($_POST['TransactionDate'])); }
		
		$AddedBy = $_POST['AdminID'];
		$AddedDate = date("Y-m-d H:i:s");
		
		$Insert = "INSERT INTO `payment_vouchers`(`company_id`,`pv_no`, `pv_date`, `pay_to`, `pay_from`, `payment_mode`, `bank_name`, `cheque_no`, `cheque_date`, `transaction_date`, `card_no`, `transaction_ref`, `payment_date`, `total_amount`, `total_tds_amount`, `pv_type`, `description`) VALUES ('$CompanyID','$PVNo','$PVDate','$PayTo','$PayFrom','$PaymentMode','$BankName','$ChequeNo','$ChequeDate','$TransactionDate','$CardNo','$TransactionRef','$PaymentDate','$TotalAmount','$TotalTDSAmount','$PVType','$Description')";
		
		$InsertQuery = mysqli_query($con,$Insert);
		if($InsertQuery)
		{
			$UpdateBalance = $ledgerObject->updateLedgerCurrentBalance($PayFrom,$GrandTotal,'-');
			
			$UpdateCurrentBalance = $ledgerObject->updateLedgerCurrentBalance($PayTo,$GrandTotal,'+');
			
			$jsonData = '{ 
				"Status":"2"
			}';
			echo $jsonData;
			exit();
			
		}
		else
		{
			if(mysqli_errno($con)==1062)
			{
				$jsonData = '{ 
					"Status":"0"
				}';
				echo $jsonData;
				exit();
			}
			else
			{
				$jsonData = '{ 
					"Status":"1"
				}';
				echo $jsonData;
				exit();
			}
		}
		
	}
	else if($Action == 'UpdateAdvancePayment')
	{
		$CompanyID = $_POST['CompanyID'];
		$PVID = $_POST['PVID'];
		$PVType = $_POST['PVType'];
		$PVNo = $_POST['PVNo'];
		$PVDate = $_POST['PVDate'];
		$PayTo = $_POST['PayTo'];
		$PayFrom = $_POST['PayFrom'];
		$PaymentMode = $_POST['PaymentMode'];
		$Description = $_POST['Description'];
		$TotalAmount = $_POST['TotalAmount'];
		$OldTotalAmount = $_POST['OldTotalAmount'];
		
		$TotalTDSAmount = $_POST['TotalTDSAmount'];
		$OldTotalTDSAmount = $_POST['OldTotalTDSAmount'];
		
		$AmountDiff = $TotalAmount - $OldTotalAmount;
		$TDSAmountDiff = $TotalTDSAmount - $OldTotalTDSAmount;
		
		$GrandTotal = $AmountDiff + $TDSAmountDiff;
		
		$BankName = '';
		$ChequeNo = '';
		$ChequeDate = '';
		$TransactionDate = '';
		$CardNo = '';
		$TransactionRef = '';
		$PaymentDate = '';
		
		if(isset($_POST['BankName']))
		{
			$BankName = $_POST['BankName'];
		}
		if(isset($_POST['ChequeNo']))
		{
			$ChequeNo = $_POST['ChequeNo'];
		}
		if(isset($_POST['ChequeDate']))
		{
			$ChequeDate = $_POST['ChequeDate'];
		}
		if(isset($_POST['TransactionDate']))
		{
			$TransactionDate = $_POST['TransactionDate'];
		}
		if(isset($_POST['CardNo']))
		{
			$CardNo = $_POST['CardNo'];
		}
		if(isset($_POST['TransactionRef']))
		{
			$TransactionRef = $_POST['TransactionRef'];
		}
		if(isset($_POST['PaymentDate']))
		{
			$PaymentDate = $_POST['PaymentDate'];
		}
		
		if(!empty($ChequeDate)) { $ChequeDate = date("Y-m-d",strtotime($_POST['ChequeDate'])); }
		if(!empty($PaymentDate)) { $PaymentDate = date("Y-m-d",strtotime($_POST['PaymentDate'])); }
		if(!empty($PVDate)) { $PVDate = date("Y-m-d",strtotime($_POST['PVDate'])); }
		if(!empty($TransactionDate)) { $TransactionDate = date("Y-m-d",strtotime($_POST['TransactionDate'])); }
		
		
		$Update = "UPDATE `payment_vouchers` SET `company_id`='$CompanyID',`pv_no`='$PVNo',`pv_date`='$PVDate',`pay_to`='$PayTo',`pay_from`='$PayFrom',`payment_mode`='$PaymentMode',`bank_name`='$BankName',`cheque_no`='$ChequeNo',`cheque_date`='$ChequeDate',`transaction_date`='$TransactionDate',`card_no`='$CardNo',`transaction_ref`='$TransactionRef',`payment_date`='$PaymentDate',`total_amount`='$TotalAmount',`total_tds_amount`='$TotalTDSAmount',`description`='$Description' WHERE pv_id='".$PVID."'";
		$UpdateQuery = mysqli_query($con,$Update);
		
		if($UpdateQuery)
		{
			if($OldTotalAmount != $TotalAmount || $OldTotalTDSAmount != $TotalTDSAmount)
			{
				$UpdateBalance = $ledgerObject->updateLedgerCurrentBalance($PayFrom,$GrandTotal,'-');
			
				$UpdateCurrentBalance = $ledgerObject->updateLedgerCurrentBalance($PayTo,$GrandTotal,'+');
			}
			$jsonData = '{ 
				"Status":"4"
			}';
			echo $jsonData;
			exit();
		}
		else
		{
			if(mysqli_errno($con)==1062)
			{
				$jsonData = '{ 
					"Status":"0"
				}';
				echo $jsonData;
				exit();
			}
			else
			{
				$jsonData = '{ 
					"Status":"3"
				}';
				echo $jsonData;
				exit();
			}
		}
	}
	else if($Action == 'RemoveAdvancePayment')
	{
		$PVID = $_POST['PVID'];
		$PayTo = $_POST['PayTo'];
		$PayFrom = $_POST['PayFrom'];
		$TotalAmount = $_POST['TotalAmount'];
		$TotalTDSAmount = $_POST['TotalTDSAmount'];
		
		$Total = $TotalAmount + $TotalTDSAmount;
		
		$UpdateBalance = $ledgerObject->updateLedgerCurrentBalance($PayFrom,$Total,'+');
			
		$UpdateCurrentBalance = $ledgerObject->updateLedgerCurrentBalance($PayTo,$Total,'-');
		
		$Remove = "DELETE FROM payment_vouchers WHERE pv_id='".$PVID."'";
		$RemoveQuery = mysqli_query($con,$Remove);
		if($RemoveQuery)
		{
			$jsonData = '{ 
				"Status":"6"
			}';
			echo $jsonData;
			exit();
		}
		else
		{
			if(mysqli_errno($con)==1451)
			{
				$jsonData = '{ 
					"Status":"00"
				}';
				echo $jsonData;
				exit();
			}
			else
			{
				$jsonData = '{ 
					"Status":"5"
				}';
				echo $jsonData;
				exit();
			}	
		}
	}
	// Advance Payment End
	//Other Expense Start
	else if($Action == 'AddOtherExpense')
	{
		$CompanyID = $_POST['CompanyID'];
		$PVType = $_POST['PVType'];
		$PVNo = $_POST['PVNo'];
		$PVNo = $purchaseObject->nextInvoicePaymentNo();
		$PVDate = $_POST['PVDate'];
		$PayTo = $_POST['PayTo'];
		$PayFrom = $_POST['PayFrom'];
		$PaymentMode = $_POST['PaymentMode'];
		$Description = $_POST['Description'];
		$TotalAmount = $_POST['TotalAmount'];
		
		$BankName = '';
		$ChequeNo = '';
		$ChequeDate = '';
		$TransactionDate = '';
		$CardNo = '';
		$TransactionRef = '';
		$PaymentDate = '';
		
		if(isset($_POST['BankName']))
		{
			$BankName = $_POST['BankName'];
		}
		if(isset($_POST['ChequeNo']))
		{
			$ChequeNo = $_POST['ChequeNo'];
		}
		if(isset($_POST['ChequeDate']))
		{
			$ChequeDate = $_POST['ChequeDate'];
		}
		if(isset($_POST['TransactionDate']))
		{
			$TransactionDate = $_POST['TransactionDate'];
		}
		if(isset($_POST['CardNo']))
		{
			$CardNo = $_POST['CardNo'];
		}
		if(isset($_POST['TransactionRef']))
		{
			$TransactionRef = $_POST['TransactionRef'];
		}
		if(isset($_POST['PaymentDate']))
		{
			$PaymentDate = $_POST['PaymentDate'];
		}
		
		if(!empty($ChequeDate)) { $ChequeDate = date("Y-m-d",strtotime($_POST['ChequeDate'])); }
		if(!empty($PaymentDate)) { $PaymentDate = date("Y-m-d",strtotime($_POST['PaymentDate'])); }
		if(!empty($PVDate)) { $PVDate = date("Y-m-d",strtotime($_POST['PVDate'])); }
		if(!empty($TransactionDate)) { $TransactionDate = date("Y-m-d",strtotime($_POST['TransactionDate'])); }
		
		$AddedBy = $_POST['AdminID'];
		$AddedDate = date("Y-m-d H:i:s");
		
		$Insert = "INSERT INTO `payment_vouchers`(`company_id`,`pv_no`, `pv_date`, `pay_to`, `pay_from`, `payment_mode`, `bank_name`, `cheque_no`, `cheque_date`, `transaction_date`, `card_no`, `transaction_ref`, `payment_date`, `total_amount`,`pv_type`, `description`) VALUES ('$CompanyID','$PVNo','$PVDate','$PayTo','$PayFrom','$PaymentMode','$BankName','$ChequeNo','$ChequeDate','$TransactionDate','$CardNo','$TransactionRef','$PaymentDate','$TotalAmount','$PVType','$Description')";
		
		$InsertQuery = mysqli_query($con,$Insert);
		if($InsertQuery)
		{
			$UpdateBalance = $ledgerObject->updateLedgerCurrentBalance($PayFrom,$TotalAmount,'-');
			
			$UpdateCurrentBalance = $ledgerObject->updateLedgerCurrentBalance($PayTo,$TotalAmount,'+');
			
			$jsonData = '{ 
				"Status":"2"
			}';
			echo $jsonData;
			exit();
			
		}
		else
		{
			if(mysqli_errno($con)==1062)
			{
				$jsonData = '{ 
					"Status":"0"
				}';
				echo $jsonData;
				exit();
			}
			else
			{
				$jsonData = '{ 
					"Status":"1"
				}';
				echo $jsonData;
				exit();
			}
		}
	}
	else if($Action == 'UpdateOtherExpense')
	{
		$CompanyID = $_POST['CompanyID'];
		$PVID = $_POST['PVID'];
		$PVType = $_POST['PVType'];
		$PVNo = $_POST['PVNo'];
		$PVDate = $_POST['PVDate'];
		$PayTo = $_POST['PayTo'];
		$PayFrom = $_POST['PayFrom'];
		$PaymentMode = $_POST['PaymentMode'];
		$Description = $_POST['Description'];
		$TotalAmount = $_POST['TotalAmount'];
		$OldTotalAmount = $_POST['OldTotalAmount'];
		
		$AmountDiff = $TotalAmount - $OldTotalAmount;
		
		$BankName = '';
		$ChequeNo = '';
		$ChequeDate = '';
		$TransactionDate = '';
		$CardNo = '';
		$TransactionRef = '';
		$PaymentDate = '';
		
		if(isset($_POST['BankName']))
		{
			$BankName = $_POST['BankName'];
		}
		if(isset($_POST['ChequeNo']))
		{
			$ChequeNo = $_POST['ChequeNo'];
		}
		if(isset($_POST['ChequeDate']))
		{
			$ChequeDate = $_POST['ChequeDate'];
		}
		if(isset($_POST['TransactionDate']))
		{
			$TransactionDate = $_POST['TransactionDate'];
		}
		if(isset($_POST['CardNo']))
		{
			$CardNo = $_POST['CardNo'];
		}
		if(isset($_POST['TransactionRef']))
		{
			$TransactionRef = $_POST['TransactionRef'];
		}
		if(isset($_POST['PaymentDate']))
		{
			$PaymentDate = $_POST['PaymentDate'];
		}
		
		if(!empty($ChequeDate)) { $ChequeDate = date("Y-m-d",strtotime($_POST['ChequeDate'])); }
		if(!empty($PaymentDate)) { $PaymentDate = date("Y-m-d",strtotime($_POST['PaymentDate'])); }
		if(!empty($PVDate)) { $PVDate = date("Y-m-d",strtotime($_POST['PVDate'])); }
		if(!empty($TransactionDate)) { $TransactionDate = date("Y-m-d",strtotime($_POST['TransactionDate'])); }
		
		
		$Update = "UPDATE `payment_vouchers` SET `company_id`='$CompanyID',`pv_no`='$PVNo',`pv_date`='$PVDate',`pay_to`='$PayTo',`pay_from`='$PayFrom',`payment_mode`='$PaymentMode',`bank_name`='$BankName',`cheque_no`='$ChequeNo',`cheque_date`='$ChequeDate',`transaction_date`='$TransactionDate',`card_no`='$CardNo',`transaction_ref`='$TransactionRef',`payment_date`='$PaymentDate',`total_amount`='$TotalAmount',`description`='$Description' WHERE pv_id='".$PVID."'";
		$UpdateQuery = mysqli_query($con,$Update);
		
		if($UpdateQuery)
		{
			if($OldTotalAmount != $TotalAmount)
			{
				$UpdateBalance = $ledgerObject->updateLedgerCurrentBalance($PayFrom,$AmountDiff,'-');
			
				$UpdateCurrentBalance = $ledgerObject->updateLedgerCurrentBalance($PayTo,$AmountDiff,'+');
			}
			$jsonData = '{ 
				"Status":"4"
			}';
			echo $jsonData;
			exit();
		}
		else
		{
			if(mysqli_errno($con)==1062)
			{
				$jsonData = '{ 
					"Status":"0"
				}';
				echo $jsonData;
				exit();
			}
			else
			{
				$jsonData = '{ 
					"Status":"3"
				}';
				echo $jsonData;
				exit();
			}
		}
	}
	else if($Action == 'RemoveOtherExpense')
	{
		$PVID = $_POST['PVID'];
		$PayTo = $_POST['PayTo'];
		$PayFrom = $_POST['PayFrom'];
		$TotalAmount = $_POST['TotalAmount'];
		
		$UpdateBalance = $ledgerObject->updateLedgerCurrentBalance($PayFrom,$TotalAmount,'+');
			
		$UpdateCurrentBalance = $ledgerObject->updateLedgerCurrentBalance($PayTo,$TotalAmount,'-');
		
		$Remove = "DELETE FROM payment_vouchers WHERE pv_id='".$PVID."'";
		$RemoveQuery = mysqli_query($con,$Remove);
		if($RemoveQuery)
		{
			$jsonData = '{ 
				"Status":"6"
			}';
			echo $jsonData;
			exit();
		}
		else
		{
			if(mysqli_errno($con)==1451)
			{
				$jsonData = '{ 
					"Status":"00"
				}';
				echo $jsonData;
				exit();
			}
			else
			{
				$jsonData = '{ 
					"Status":"5"
				}';
				echo $jsonData;
				exit();
			}	
		}
	}
	//Other Expense End
}
else
{
	$jsonData = '{ 
		"Status":"Unauthorised Access!"
	}';
	echo $jsonData;
	exit();
}
ob_flush();
?>