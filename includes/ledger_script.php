<?php
ob_start();
session_start();
require_once('dbconfig.php');
if($_SERVER["REQUEST_METHOD"] == "POST")
{
	$Action = $_POST['Action'];
	if($Action == 'AddLedger')
	{
		
		$LedgerName = mysqli_real_escape_string($con,$_POST['LedgerName']);
		$LedgerAlias = mysqli_real_escape_string($con,$_POST['LedgerAlias']);
		$GroupID = mysqli_real_escape_string($con,$_POST['GroupID']);
		$OpeningBalance = mysqli_real_escape_string($con,$_POST['OpeningBalance']);
		$CurrentBalance = $OpeningBalance;
		$CompanyID = $_POST['CompanyID'];
		$LCF = '';
		if(isset($_POST['LCF']))
		{
			$LCF = $_POST['LCF'];
			$Length = count($LCF); 
		}
		
		$AddedBy = $_POST['AdminID'];
		$AddedDate = date("Y-m-d H:i:s");

		$Insert = "INSERT INTO `ledger_master`(`company_id`,`ledger_name`, `ledger_alias`, `group_id`, `opening_balance`, `current_balance`, `addedby`, `addeddate`) VALUES ('$CompanyID','$LedgerName','$LedgerAlias','$GroupID','$OpeningBalance','$CurrentBalance','$AddedBy','$AddedDate')";
		$InsertQuery = mysqli_query($con,$Insert);

		$LedgerID = mysqli_insert_id($con);
		
		if($InsertQuery)
		{
			if(!empty($LCF))
			{
				foreach($LCF as $LCFID => $LCF_Value)
				{
					if($LCFID > 0)
					{
						$LCF_Value = mysqli_real_escape_string($con,$LCF_Value);
						$InsertLIItem = mysqli_query($con,"INSERT INTO `ledger_item_master`(`ledger_id`, `lcf_id`, `li_value`) VALUES ('$LedgerID','$LCFID','$LCF_Value')");

						if(!$InsertLIItem)
						{ 
							$Remove = "DELETE FROM `ledger_master` WHERE `ledger_id`='".$LedgerID."'";
							$Remove = "DELETE FROM `ledger_item_master` WHERE `ledger_id`='".$LedgerID."'";

							if(mysqli_errno($con)==1062)
							{
								$jsonData = '{ 
									"Status":"0"
								}';
								echo $jsonData;
								exit();
							}
							else
							{
								$jsonData = '{ 
									"Status":"1"
								}';
								echo $jsonData;
								exit();
							}
						}
					}
				}
			}
			if(!$InsertQuery)
			{
				$Remove = "DELETE FROM `ledger_master` WHERE `ledger_id`='".$LedgerID."'";
				$Remove = "DELETE FROM `ledger_item_master` WHERE `ledger_id`='".$LedgerID."'";
				
				if(mysqli_errno($con)==1062)
				{
					$jsonData = '{ 
						"Status":"0"
					}';
					echo $jsonData;
					exit();
				}
				else
				{
					$jsonData = '{ 
						"Status":"1"
					}';
					echo $jsonData;
					exit();
				}
			}
			else
			{
				$jsonData = '{ 
					"Status":"2"
				}';
				echo $jsonData;
				exit();
			}
		}
		else
		{
			if(mysqli_errno($con)==1062)
			{
				$jsonData = '{ 
					"Status":"0"
				}';
				echo $jsonData;
				exit();
			}
			else
			{
				$jsonData = '{ 
					"Status":"1"
				}';
				echo $jsonData;
				exit();
			}
		}
	}
	else if($Action == 'UpdateLedger')
	{
		$LedgerID = $_POST['LedgerID'];
		$LedgerName = mysqli_real_escape_string($con,$_POST['LedgerName']);
		$LedgerAlias = mysqli_real_escape_string($con,$_POST['LedgerAlias']);
		$GroupID = mysqli_real_escape_string($con,$_POST['GroupID']);
		$OpeningBalance = mysqli_real_escape_string($con,$_POST['OpeningBalance']);
		$OldCurrentBalance = mysqli_real_escape_string($con,$_POST['OldCurrentBalance']);
		$OldOpeningBalance = mysqli_real_escape_string($con,$_POST['OldOpeningBalance']);
		$CompanyID = $_POST['CompanyID'];
		
		$Diff= 0;
		$CurrentBalance = 0;

		$Diff = $OpeningBalance-$OldOpeningBalance;
		$CurrentBalance = $OldCurrentBalance + $Diff; 
		
		$LCF = '';
		if(isset($_POST['LCF']))
		{
			$LCF = $_POST['LCF'];
			$Length = count($LCF); 
		}
		
		$ModifiedBy = $_POST['AdminID'];
		$ModifiedDate = date("Y-m-d H:i:s");

		$Update = "UPDATE `ledger_master` SET `company_id`='$CompanyID',`ledger_name`='$LedgerName',`ledger_alias`='$LedgerAlias',`group_id`='$GroupID',`opening_balance`='$OpeningBalance',`current_balance`='$CurrentBalance',`modifiedby`='$ModifiedBy',`modifieddate`='$ModifiedDate' WHERE `ledger_id`='".$LedgerID."'";
		
		$UpdateQuery = mysqli_query($con,$Update);
		
		if($UpdateQuery)
		{
			if(!empty($LCF))
			{
				foreach($LCF as $LCFID => $LCF_Value)
				{
					if(!empty($LCFID))
					{    
						$LCF_Value = mysqli_real_escape_string($con,$LCF_Value);
						$UpdateItem = mysqli_query($con,"UPDATE `ledger_item_master` SET `ledger_id`='$LedgerID',`lcf_id`='$LCFID',`li_value`='$LCF_Value'  WHERE `lcf_id` = '".$LCFID."' AND `ledger_id` = '".$LedgerID."'");
					}
					else  
					{
						$LCF_Value = mysqli_real_escape_string($con,$LCF_Value);
						$UpdateItem = mysqli_query($con,"INSERT INTO `ledger_item_master`(`ledger_id`, `lcf_id`, `li_value`) VALUES ('$LedgerID','$LCFID','$LCF_Value')");
					}
				}
			}
			
			if(!$UpdateQuery)
			{
				if(mysqli_errno($con)==1062)
				{
					$jsonData = '{ 
						"Status":"0"
					}';
					echo $jsonData;
					exit();
				}
				else if(mysqli_errno($con)==1451)
				{
					$jsonData = '{ 
						"Status":"00"
					}';
					echo $jsonData;
					exit();
				}
				else
				{
					$jsonData = '{ 
						"Status":"3"
					}';
					echo $jsonData;
					exit();
				}
			}
			else
			{
				$jsonData = '{ 
					"Status":"4"
				}';
				echo $jsonData;
				exit();
			}
		}
		else
		{
			if(mysqli_errno($con)==1062)
			{
				$jsonData = '{ 
					"Status":"0"
				}';
				echo $jsonData;
				exit();
			}
			else if(mysqli_errno($con)==1451)
			{
				$jsonData = '{ 
					"Status":"00"
				}';
				echo $jsonData;
				exit();
			}
			else
			{
				$jsonData = '{ 
					"Status":"3"
				}';
				echo $jsonData;
				exit();
			}
		}
	}
	else if($Action == 'RemoveLedger')
	{
		$LedgerID = $_POST['LedgerID'];
				
		$Remove = "DELETE FROM `ledger_master` WHERE `ledger_id`='".$LedgerID."'";
		$RemoveQuery = mysqli_query($con,$Remove);
			
		if(!$RemoveQuery)
		{
			if(mysqli_errno($con)==1451)
			{
				$jsonData = '{ 
					"Status":"00"
				}';
				echo $jsonData;
				exit();
			}
			else
			{
				$jsonData = '{ 
					"Status":"5"
				}';
				echo $jsonData;
				exit();
			}
		}
		else
		{
			$jsonData = '{ 
				"Status":"6"
			}';
			echo $jsonData;
			exit();
		}
	}
	else
	{
		$jsonData = '{ 
			"Status":"Unauthorised Access!"
		}';
		echo $jsonData;
		exit();
	}
}
else
{
	$jsonData = '{ 
		"Status":"Unauthorised Access!"
	}';
	echo $jsonData;
	exit();
}
ob_flush();
?>