<?php
class Profile
{
	public function selectFinancialYear($AdminID)
	{
		global $con;
		$SelectFinancialYear = mysqli_query($con,"SELECT fyear_start, fyear_end FROM admin_master WHERE admin_id='".$AdminID."'");
		$FinancialYearArr = mysqli_fetch_array($SelectFinancialYear);
		$FYearStart = $FinancialYearArr['fyear_start'];
		$FYearEnd = $FinancialYearArr['fyear_end'];
		
		$FinancialYear = '{ 
			"FYearStart":"'.$FYearStart.'",
			"FYearEnd":"'.$FYearEnd.'"
		}';
		return $FinancialYear;
		exit();
	}
	
	public function selectUserName($AdminID)
	{
		global $con;
		$SelectUserName = mysqli_query($con,"SELECT username FROM admin_master WHERE admin_id='".$AdminID."'");
		$UserNameArr = mysqli_fetch_array($SelectUserName);
		$UserName = $UserNameArr['username'];
		return $UserName;
		exit();
	}
	
	public function selectUserType($AdminTypeID)
	{
		global $con;
		$SelectUserType = mysqli_query($con,"SELECT admin_type FROM admin_type_master WHERE admin_type_id='".$AdminTypeID."'");
		$UserTypeArr = mysqli_fetch_array($SelectUserType);
		$UserType = $UserTypeArr['admin_type'];
		return $UserType;
		exit();
	}
	
	public function selectAdminName($AdminID)
	{
		global $con;
		$SelectAdminName = mysqli_query($con,"SELECT admin_name FROM admin_master WHERE admin_id='".$AdminID."'");
		$AdminNameArr = mysqli_fetch_array($SelectAdminName);
		$AdminName = $AdminNameArr['admin_name'];
		return $AdminName;
		exit();
	}
	public function selectAdminCompanyID($AdminID)
	{
		global $con;
		
		$SelectCompany = mysqli_query($con,"SELECT company_id FROM admin_master WHERE admin_id='".$AdminID."'");
		$CompanyArr = mysqli_fetch_array($SelectCompany);
		$CompanyID = $CompanyArr['company_id'];
		return $CompanyID;
		exit();
	}
	
	public function selectCompanyProfile($CompanyID)
	{
		global $con;
		
		$SelectCompany = mysqli_query($con,"SELECT * FROM company_master WHERE company_id='".$CompanyID."'");
		$Company = mysqli_fetch_array($SelectCompany);
		$CompanyID = $Company['company_id'];
		$CompanyName = $Company['company_name'];
		$CompanyAlias = $Company['company_alias'];
		$CompanyCode = $Company['company_code'];
		$Address = $Company['address'];
		$Phone = $Company['phone'];
		$Mobile = $Company['mobile'];
		$Email = $Company['email'];
		$GSTIN = $Company['gstin'];
		$PanNo = $Company['pan_no'];
		$State = $Company['state'];
		$StateCode = $Company['state_code'];
		$BankName = $Company['bank_name'];
		$BankAcNo = $Company['bank_ac_no'];
		$BankBranch = $Company['bank_branch'];
		$BankIFSC = $Company['bank_ifsc'];
		
		$CompanyProfile = array(
			"CompanyID"=>$CompanyID,
			"CompanyName"=>$CompanyName,
			"CompanyAlias"=>$CompanyAlias,
			"CompanyCode"=>$CompanyCode,
			"Address"=>$Address,
			"Phone"=>$Phone,
			"Mobile"=>$Mobile,
			"Email"=>$Email,
			"GSTIN"=>$GSTIN,
			"PanNo"=>$PanNo,
			"State"=>$State,
			"StateCode"=>$StateCode,
			"BankName"=>$BankName,
			"BankAcNo"=>$BankAcNo,
			"BankIFSC"=>$BankIFSC,
			"BankBranch"=>$BankBranch
		);
		$CompanyProfile = json_encode($CompanyProfile);
		
		return $CompanyProfile;
		exit();
	}
}

class Tax
{
	public function getTaxDetail($TaxID)
	{
		global $con;

		$SelectTax = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$TaxID."' AND lcf_id='37'");
		$Tax = mysqli_fetch_array($SelectTax);

		$TaxValue = $Tax['li_value'];

		$TaxDetail = array(
			"TaxValue"=>$TaxValue
		);
		$TaxDetail = json_encode($TaxDetail);
		
		return $TaxDetail;
		exit();
	}
}

class Group
{
	public function selectGroupDetail($GroupID)
	{
		global $con; 
		$SelectGroup = mysqli_query($con,"SELECT * FROM group_master WHERE group_id='".$GroupID."'");
		$Group = mysqli_fetch_array($SelectGroup);
		
		$GroupName = mysqli_real_escape_string($con,$Group['group_name']);
		
		$GroupDetail = '{
			"GroupName":"'.$GroupName.'"
		}';
		return $GroupDetail;
	}
}

class Item
{
	public function selectItemDetail($ItemID)
	{
		global $con; 
		$SelectItem = mysqli_query($con,"SELECT * FROM item_master WHERE item_id='".$ItemID."'");
		$Item = mysqli_fetch_array($SelectItem);
		
		$ItemName = mysqli_real_escape_string($con,$Item['item_name']);
		$ItemCode = mysqli_real_escape_string($con,$Item['item_code']);
		$HSNCode = mysqli_real_escape_string($con,$Item['hsn_code']);
		$ItemRate = mysqli_real_escape_string($con,$Item['item_rate']);
		$ItemUnit = mysqli_real_escape_string($con,$Item['item_unit']);
		
		$ItemDetail = '{
			"ItemName":"'.$ItemName.'",
			"ItemCode":"'.$ItemCode.'",
			"ItemUnit":"'.$ItemUnit.'",
			"HSNCode":"'.$HSNCode.'",
			"ItemRate":"'.$ItemRate.'"
		}';
		return $ItemDetail;
	}
}

class Ledger
{
	public function selectLedgerDetail($LedgerID)
	{
		global $con; 
		$SelectLedger = mysqli_query($con,"SELECT * FROM ledger_master WHERE ledger_id='".$LedgerID."'");
		$Ledger = mysqli_fetch_array($SelectLedger);
		
		$LedgerName = mysqli_real_escape_string($con,$Ledger['ledger_name']);
		
		$LedgerDetail = '{
			"LedgerName":"'.$LedgerName.'"
		}';
		return $LedgerDetail;
	}

	public function selectVendorDetail($VendorID)
	{
		global $con; 
		$SelectVendor = mysqli_query($con,"SELECT * FROM ledger_master WHERE ledger_id='".$VendorID."' AND group_id='34'");
		$Vendor = mysqli_fetch_array($SelectVendor);
		
		$VendorName = mysqli_real_escape_string($con,$Vendor['ledger_name']);
		
		$VendorDetail = '{
			"VendorName":"'.$VendorName.'"
		}';
		return $VendorDetail;
	}

	public function selectClientDetail($ClientID)
	{
		global $con; 
		$SelectClient = mysqli_query($con,"SELECT * FROM ledger_master WHERE ledger_id='".$ClientID."' AND group_id='33'");
		$Client = mysqli_fetch_array($SelectClient);
		
		$ClientName = mysqli_real_escape_string($con,$Client['ledger_name']);
		
		$ClientDetail = '{
			"ClientName":"'.$ClientName.'"
		}';
		return $ClientDetail;
	}
	
	public function selectBankDetail($LedgerID)
	{
		global $con; 
		
		$SelectLedger = mysqli_query($con,"SELECT * FROM ledger_master WHERE ledger_id='".$LedgerID."'");
		$Ledger = mysqli_fetch_array($SelectLedger);
		
		$LedgerName = mysqli_real_escape_string($con,$Ledger['ledger_name']);
		$LedgerAlias = mysqli_real_escape_string($con,$Ledger['ledger_alias']);
		$OpeningBalance = $Ledger['opening_balance'];
		$CurrentBalance = $Ledger['current_balance'];
		
		$BankAcNo = $this->selectLedgerItemValue(41,$LedgerID);
		$BankAcHolder = $this->selectLedgerItemValue(42,$LedgerID);
		$BankName = $this->selectLedgerItemValue(43,$LedgerID);
		$BankBranch = $this->selectLedgerItemValue(44,$LedgerID);
		$RTGSCode = $this->selectLedgerItemValue(45,$LedgerID);
		$MICRCode = $this->selectLedgerItemValue(46,$LedgerID);
		$IFSCCode = $this->selectLedgerItemValue(47,$LedgerID);
		
		$ClientDetail = array(
			"LedgerName"=>$LedgerName,
			"LedgerAlias"=>$LedgerAlias,
			"OpeningBalance"=>$OpeningBalance,
			"CurrentBalance"=>$CurrentBalance,
			"BankAcNo"=>$BankAcNo,
			"BankAcHolder"=>$BankAcHolder,
			"BankName"=>$BankName,
			"BankBranch"=>$BankBranch,
			"RTGSCode"=>$RTGSCode,
			"MICRCode"=>$MICRCode,
			"IFSCCode"=>$IFSCCode
		);
			
		$ClientDetail = json_encode($ClientDetail);
		
		return $ClientDetail;
	}
	
	public function selectLedgerItemValue($LCFID,$LedgerID)
	{
		global $con; 
		
		$SelectItem = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$LedgerID."' AND lcf_id='".$LCFID."'");
		$ItemArr = mysqli_fetch_array($SelectItem);
		
		$Value = $ItemArr['li_value'];
		return $Value;
	}

	public function selectLedgerItemDetail($LCFID)
	{
		global $con; 
		$SelectLCF = mysqli_query($con,"SELECT * FROM ledger_custom_fields WHERE lcf_id='".$LCFID."'");
		$LCF = mysqli_fetch_array($SelectLCF);
		
		$LCFName = mysqli_real_escape_string($con,$LCF['lcf_name']);
		$LCFType = mysqli_real_escape_string($con,$LCF['lcf_type']);
		$LCFIsRequired = mysqli_real_escape_string($con,$LCF['is_required']);
		$LCFPlaceholder = mysqli_real_escape_string($con,$LCF['placeholder']);
		$LCFValues = mysqli_real_escape_string($con,$LCF['lcf_values']);
		
		$LCFDetail = '{
			"LCFName":"'.$LCFName.'",
			"LCFType":"'.$LCFType.'",
			"LCFIsRequired":"'.$LCFIsRequired.'",
			"LCFPlaceholder":"'.$LCFPlaceholder.'",
			"LCFValues":"'.$LCFValues.'"
		}';
		return $LCFDetail;
	}

	public function getVendorDetail($VendorID)
	{

		global $con;

		$SelectBillingName = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$VendorID."' AND lcf_id='94'");
		$BillName = mysqli_fetch_array($SelectBillingName);
		$BillingName = $BillName['li_value'];

		$SelectBillingGSTIN = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$VendorID."' AND lcf_id='22'");
		$BillGSTIN = mysqli_fetch_array($SelectBillingGSTIN);
		$BillingGSTIN = $BillGSTIN['li_value'];

		$SelectBillingAddress = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$VendorID."' AND lcf_id='24'");
		$BillAddress = mysqli_fetch_array($SelectBillingAddress);
		$BillingAddress = $BillAddress['li_value'];

		$SelectBillingCity = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$VendorID."' AND lcf_id='25'");
		$BillCity = mysqli_fetch_array($SelectBillingCity);
		$BillingCity = $BillCity['li_value'];

		$SelectBillingState = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$VendorID."' AND lcf_id='26'");
		$BillState = mysqli_fetch_array($SelectBillingState);
		$BillingState = $BillState['li_value'];

		$SelectBillingStateCode = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$VendorID."' AND lcf_id='78'");
		$BillStateCode = mysqli_fetch_array($SelectBillingStateCode);
		$BillingStateCode = $BillStateCode['li_value'];

		$SelectBillingCountry = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$VendorID."' AND lcf_id='27'");
		$BillCountry = mysqli_fetch_array($SelectBillingCountry);
		$BillingCountry = $BillCountry['li_value'];

		$SelectBillingPinCode = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$VendorID."' AND lcf_id='28'");
		$BillPinCode = mysqli_fetch_array($SelectBillingPinCode);
		$BillingPinCode = $BillPinCode['li_value'];

		$SelectShippingName = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$VendorID."' AND lcf_id='95'");
		$ShippName = mysqli_fetch_array($SelectShippingName);
		$ShippingName = $ShippName['li_value'];

		$SelectShippingGSTIN = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$VendorID."' AND lcf_id='75'");
		$ShippGSTIN = mysqli_fetch_array($SelectShippingGSTIN);
		$ShippingGSTIN = $ShippGSTIN['li_value'];

		$SelectShippingAddress = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$VendorID."' AND lcf_id='29'");
		$ShippAddress = mysqli_fetch_array($SelectShippingAddress);
		$ShippingAddress = $ShippAddress['li_value'];

		$SelectShippingCity = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$VendorID."' AND lcf_id='30'");
		$ShippCity = mysqli_fetch_array($SelectShippingCity);
		$ShippingCity = $ShippCity['li_value'];

		$SelectShippingState = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$VendorID."' AND lcf_id='31'");
		$ShippState = mysqli_fetch_array($SelectShippingState);
		$ShippingState = $ShippState['li_value'];

		$SelectShippingStateCode = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$VendorID."' AND lcf_id='79'");
		$ShippStateCode = mysqli_fetch_array($SelectShippingStateCode);
		$ShippingStateCode = $ShippStateCode['li_value'];

		$SelectShippingCountry = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$VendorID."' AND lcf_id='32'");
		$ShippCountry = mysqli_fetch_array($SelectShippingCountry);
		$ShippingCountry = $ShippCountry['li_value'];

		$SelectShippingPinCode = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$VendorID."' AND lcf_id='33'");
		$ShippPinCode = mysqli_fetch_array($SelectShippingPinCode);
		$ShippingPinCode = $ShippPinCode['li_value'];

		$SelectTransporterName = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$VendorID."' AND lcf_id='86'");
		$TransName = mysqli_fetch_array($SelectTransporterName);
		$TransporterName = $TransName['li_value'];

		$SelectTransporterAddress = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$VendorID."' AND lcf_id='87'");
		$TransAddress = mysqli_fetch_array($SelectTransporterAddress);
		$TransporterAddress = $TransAddress['li_value'];

		$SelectTransporterState = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$VendorID."' AND lcf_id='88'");
		$TransState = mysqli_fetch_array($SelectTransporterState);
		$TransporterState = $TransState['li_value'];

		$SelectTransporterStateCode = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$VendorID."' AND lcf_id='89'");
		$TransStateCode = mysqli_fetch_array($SelectTransporterStateCode);
		$TransporterStateCode = $TransStateCode['li_value'];

		$SelectTransporterGSTIN = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$VendorID."' AND lcf_id='90'");
		$TransGSTIN = mysqli_fetch_array($SelectTransporterGSTIN);
		$TransporterGSTIN = $TransGSTIN['li_value'];

		$SelectTransporterPhone = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$VendorID."' AND lcf_id='91'");
		$TransPhone = mysqli_fetch_array($SelectTransporterPhone);
		$TransporterPhone = $TransPhone['li_value'];
		
		
		$VendorDetail = array(
			"BillingName"=>$BillingName,
			"BillingGSTIN"=>$BillingGSTIN,
			"BillingAddress"=>$BillingAddress,
			"BillingCity"=>$BillingCity,
			"BillingState"=>$BillingState,
			"BillingStateCode"=>$BillingStateCode,
			"BillingCountry"=>$BillingCountry,
			"BillingPinCode"=>$BillingPinCode,
			"ShippingName"=>$ShippingName,
			"ShippingGSTIN"=>$ShippingGSTIN,
			"ShippingAddress"=>$ShippingAddress,
			"ShippingCity"=>$ShippingCity,
			"ShippingState"=>$ShippingState,
			"ShippingStateCode"=>$ShippingStateCode,
			"ShippingCountry"=>$ShippingCountry,
			"ShippingPinCode"=>$ShippingPinCode,
			"TransporterName"=>$TransporterName,
			"TransporterAddress"=>$TransporterAddress,
			"TransporterState"=>$TransporterState,
			"TransporterStateCode"=>$TransporterStateCode,
			"TransporterGSTIN"=>$TransporterGSTIN,
			"TransporterPhone"=>$TransporterPhone
		);
		$VendorDetail = json_encode($VendorDetail);
		
		return $VendorDetail;
		exit();
				
	}

	public function getClientDetail($ClientID)
	{

		global $con;
		
		$SelectBillingName = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$ClientID."' AND lcf_id='92'");
		$BillName = mysqli_fetch_array($SelectBillingName);
		$BillingName = $BillName['li_value'];
		
		$SelectBillingMobile = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$ClientID."' AND lcf_id='1'");
		$BillMobile = mysqli_fetch_array($SelectBillingMobile);
		$BillingMobile = $BillMobile['li_value'];
		
		$SelectBillingGSTIN = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$ClientID."' AND lcf_id='5'");
		$BillGSTIN = mysqli_fetch_array($SelectBillingGSTIN);
		$BillingGSTIN = $BillGSTIN['li_value'];

		$SelectBillingAddress = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$ClientID."' AND lcf_id='7'");
		$BillAddress = mysqli_fetch_array($SelectBillingAddress);
		$BillingAddress = $BillAddress['li_value'];

		$SelectBillingCity = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$ClientID."' AND lcf_id='8'");
		$BillCity = mysqli_fetch_array($SelectBillingCity);
		$BillingCity = $BillCity['li_value'];

		$SelectBillingState = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$ClientID."' AND lcf_id='9'");
		$BillState = mysqli_fetch_array($SelectBillingState);
		$BillingState = $BillState['li_value'];

		$SelectBillingStateCode = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$ClientID."' AND lcf_id='76'");
		$BillStateCode = mysqli_fetch_array($SelectBillingStateCode);
		$BillingStateCode = $BillStateCode['li_value'];

		$SelectBillingCountry = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$ClientID."' AND lcf_id='10'");
		$BillCountry = mysqli_fetch_array($SelectBillingCountry);
		$BillingCountry = $BillCountry['li_value'];

		$SelectBillingPinCode = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$ClientID."' AND lcf_id='11'");
		$BillPinCode = mysqli_fetch_array($SelectBillingPinCode);
		$BillingPinCode = $BillPinCode['li_value'];

		$SelectShippingName = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$ClientID."' AND lcf_id='93'");
		$ShippName = mysqli_fetch_array($SelectShippingName);
		$ShippingName = $ShippName['li_value'];

		$SelectShippingGSTIN = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$ClientID."' AND lcf_id='74'");
		$ShippGSTIN = mysqli_fetch_array($SelectShippingGSTIN);
		$ShippingGSTIN = $ShippGSTIN['li_value'];

		$SelectShippingAddress = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$ClientID."' AND lcf_id='12'");
		$ShippAddress = mysqli_fetch_array($SelectShippingAddress);
		$ShippingAddress = $ShippAddress['li_value'];

		$SelectShippingCity = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$ClientID."' AND lcf_id='13'");
		$ShippCity = mysqli_fetch_array($SelectShippingCity);
		$ShippingCity = $ShippCity['li_value'];

		$SelectShippingState = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$ClientID."' AND lcf_id='14'");
		$ShippState = mysqli_fetch_array($SelectShippingState);
		$ShippingState = $ShippState['li_value'];

		$SelectShippingStateCode = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$ClientID."' AND lcf_id='77'");
		$ShippStateCode = mysqli_fetch_array($SelectShippingStateCode);
		$ShippingStateCode = $ShippStateCode['li_value'];

		$SelectShippingCountry = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$ClientID."' AND lcf_id='15'");
		$ShippCountry = mysqli_fetch_array($SelectShippingCountry);
		$ShippingCountry = $ShippCountry['li_value'];

		$SelectShippingPinCode = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$ClientID."' AND lcf_id='16'");
		$ShippPinCode = mysqli_fetch_array($SelectShippingPinCode);
		$ShippingPinCode = $ShippPinCode['li_value'];

		$SelectTransporterName = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$ClientID."' AND lcf_id='80'");
		$TransName = mysqli_fetch_array($SelectTransporterName);
		$TransporterName = $TransName['li_value'];

		$SelectTransporterAddress = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$ClientID."' AND lcf_id='81'");
		$TransAddress = mysqli_fetch_array($SelectTransporterAddress);
		$TransporterAddress = $TransAddress['li_value'];

		$SelectTransporterState = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$ClientID."' AND lcf_id='82'");
		$TransState = mysqli_fetch_array($SelectTransporterState);
		$TransporterState = $TransState['li_value'];

		$SelectTransporterStateCode = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$ClientID."' AND lcf_id='83'");
		$TransStateCode = mysqli_fetch_array($SelectTransporterStateCode);
		$TransporterStateCode = $TransStateCode['li_value'];

		$SelectTransporterGSTIN = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$ClientID."' AND lcf_id='84'");
		$TransGSTIN = mysqli_fetch_array($SelectTransporterGSTIN);
		$TransporterGSTIN = $TransGSTIN['li_value'];

		$SelectTransporterPhone = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$ClientID."' AND lcf_id='85'");
		$TransPhone = mysqli_fetch_array($SelectTransporterPhone);
		$TransporterPhone = $TransPhone['li_value'];
		
		$ClientDetail = array(
			"BillingName"=>$BillingName,
			"BillingMobile"=>$BillingMobile,
			"BillingGSTIN"=>$BillingGSTIN,
			"BillingAddress"=>$BillingAddress,
			"BillingCity"=>$BillingCity,
			"BillingState"=>$BillingState,
			"BillingStateCode"=>$BillingStateCode,
			"BillingCountry"=>$BillingCountry,
			"BillingPinCode"=>$BillingPinCode,
			"ShippingName"=>$ShippingName,
			"ShippingGSTIN"=>$ShippingGSTIN,
			"ShippingAddress"=>$ShippingAddress,
			"ShippingCity"=>$ShippingCity,
			"ShippingState"=>$ShippingState,
			"ShippingStateCode"=>$ShippingStateCode,
			"ShippingCountry"=>$ShippingCountry,
			"ShippingPinCode"=>$ShippingPinCode,
			"TransporterName"=>$TransporterName,
			"TransporterAddress"=>$TransporterAddress,
			"TransporterState"=>$TransporterState,
			"TransporterStateCode"=>$TransporterStateCode,
			"TransporterGSTIN"=>$TransporterGSTIN,
			"TransporterPhone"=>$TransporterPhone
		);
		$ClientDetail = json_encode($ClientDetail);
		
		return $ClientDetail;
		exit();
				
	}

	public function customerFieldsForLedger($LCFID,$LCFName,$LCFType,$LCFIsRequired,$LCFPlaceholder,$LCFValues)
	{
		global $con; 
		$CustomField = '';

		$LCFIsRequired2 = '';
		if ($LCFIsRequired == 1)
		{
			$LCFIsRequired2 = "required";
		}
		
		if ($LCFType == 'text') 
		{
			

			$CustomField .= '<div class="form-group" style="width:100%;">
			                    <label class="col-sm-5 control-label" style="width:44%;"> '.$LCFName.'</label>
			                    <div class="col-sm-6" style="width:54%;">
			                        <input type="'.$LCFType.'" id="LCF['.$LCFID.']" name="LCF['.$LCFID.']" placeholder="'.$LCFPlaceholder.'" class="form-control" '.$LCFIsRequired2.' />
			                    </div>
			                </div>';
		}
		elseif ($LCFType == 'number') 
		{
			$CustomField .= '<div class="form-group" style="width:100%;">
			                    <label class="col-sm-5 control-label" style="width:44%;"> '.$LCFName.'</label>
			                    <div class="col-sm-6" style="width:54%;">
			                        <input type="'.$LCFType.'" id="LCF['.$LCFID.']" name="LCF['.$LCFID.']" placeholder="'.$LCFPlaceholder.'" class="form-control" '.$LCFIsRequired2.' />
			                    </div>
			                </div>';
		}
		elseif ($LCFType == 'email') 
		{
			$CustomField .= '<div class="form-group" style="width:100%;">
			                    <label class="col-sm-5 control-label" style="width:44%;"> '.$LCFName.'</label>
			                    <div class="col-sm-6" style="width:54%;">
			                        <input type="'.$LCFType.'" id="LCF['.$LCFID.']" name="LCF['.$LCFID.']" placeholder="'.$LCFPlaceholder.'" class="form-control" '.$LCFIsRequired2.' />
			                    </div>
			                </div>';
		}
		elseif ($LCFType == 'textarea') 
		{
			$CustomField .= '<div class="form-group" style="width:100%;">
			                    <label class="col-sm-5 control-label" style="width:44%;">'.$LCFName.'</label>
			                    <div class="col-sm-6" style="width:54%;">
			                        <textarea id="LCF['.$LCFID.']" name="LCF['.$LCFID.']" placeholder="'.$LCFPlaceholder.'" class="form-control no-resize" rows="5" '.$LCFIsRequired2.' />
			                        </textarea>
			                    </div>
			                </div>';
		}
		elseif ($LCFType == 'date') 
		{
			$CustomField .=	'<div class="form-group" style="width:100%;">
		                        <label class="col-sm-5 control-label" style="width:44%;">'.$LCFName.'</label>
		                        <div class="col-sm-6" style="width:54%;">
		                            <input type="text" id="LCF['.$LCFID.']" name="LCF['.$LCFID.']" placeholder="'.$LCFPlaceholder.'" data-format="DD-MM-YYYY" class="form-control js-dtp" '.$LCFIsRequired2.' />
		                                <span style="width: 50px; " class="glyphicon glyphicon-calendar form-control-feedback"></span>
		                        </div>
		                    </div>';
		}
		elseif ($LCFType == 'select') 
		{
			$LCFValues = json_decode($LCFValues);
			
			$CustomField .= '<div class="form-group" style="width:100%;">
                                <label class="col-sm-5 control-label" style="width:44%;">'.$LCFName.'</label>
                                <div class="col-sm-6" style="width:54%;">
                                    <select class="selectpicker form-control show-tick" id="LCF['.$LCFID.']" name="LCF['.$LCFID.']">';
                                    	
                                    	foreach($LCFValues as $LCF_Value => $LCFID)
                                    	{
                                        	$CustomField .='<option value="'.$LCFID.'">'.$LCF_Value.'</option>';
                                        }
                    $CustomField .='</select>
                                </div>
                            </div>';
		}
		/*elseif ($LCFType == 'radio') 
		{
			$LCFValues = json_decode($LCFValues);

			$CustomField .= '<div class="form-group" style="width:100%;">
                                <label class="col-sm-5 control-label" style="width:44%;">'.$LCFName.'</label>
                                <div class="col-sm-2" style="width:54%;">
                                    <input type="checkbox" class="js-switch" id="LCF['.$LCFID.']" name="LCF['.$LCFID.']" placeholder="'.$LCFPlaceholder.'" value='1' data-size="small" '.$LCFIsRequired2.'  />
                                </div>
                            </div>';
		}*/
		else 
		{
			$CustomField .= '';

		}
		
		
		return $CustomField;
	}
	
	public function getcustomerFieldsFor1Ledger($LCFID,$LedgerID)
	{
		global $con; 
		
		$CustomField = '';

		$SelectLCF1Item = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$LedgerID."' AND lcf_id='".$LCFID."'");
		$LCF1ItemArr = mysqli_fetch_array($SelectLCF1Item);
		$Value = $LCF1ItemArr['li_value'];
		return $Value;
		exit();
	}
	
	public function getcustomerFieldsForLedger($LCFID,$LCFName,$LCFType,$LCFIsRequired,$LCFPlaceholder,$LCFValues,$LIID,$Value)
	{
		global $con; 
		$CustomField = '';
		
		$LCFIsRequired2 = '';
		if ($LCFIsRequired == 1)
		{
			$LCFIsRequired2 = "required";
		}
		
		if ($LCFType == 'text') 
		{
			

			$CustomField .= '<div class="form-group" style="width:100%;">
			                    <label class="col-sm-5 control-label" style="width:44%;"> '.$LCFName.'</label>
			                    <div class="col-sm-6" style="width:54%;">
			                        <input type="'.$LCFType.'" id="LCF['.$LCFID.']" name="LCF['.$LCFID.']" placeholder="'.$LCFPlaceholder.'" value="'.$Value.'" class="form-control" '.$LCFIsRequired2.' />
			                    </div>
			                </div>';
		}
		elseif ($LCFType == 'number') 
		{
			$CustomField .= '<div class="form-group" style="width:100%;">
			                    <label class="col-sm-5 control-label" style="width:44%;"> '.$LCFName.'</label>
			                    <div class="col-sm-6" style="width:54%;">
			                        <input type="'.$LCFType.'" id="LCF['.$LCFID.']" name="LCF['.$LCFID.']" value="'.$Value.'" placeholder="'.$LCFPlaceholder.'" class="form-control" '.$LCFIsRequired2.' />
			                    </div>
			                </div>';
		}
		elseif ($LCFType == 'email') 
		{
			$CustomField .= '<div class="form-group" style="width:100%;">
			                    <label class="col-sm-5 control-label" style="width:44%;"> '.$LCFName.'</label>
			                    <div class="col-sm-6" style="width:54%;">
			                        <input type="'.$LCFType.'" id="LCF['.$LCFID.']" name="LCF['.$LCFID.']" value="'.$Value.'" placeholder="'.$LCFPlaceholder.'" class="form-control" '.$LCFIsRequired2.' />
			                    </div>
			                </div>';
		}
		elseif ($LCFType == 'textarea') 
		{
			$CustomField .= '<div class="form-group" style="width:100%;">
			                    <label class="col-sm-5 control-label" style="width:44%;">'.$LCFName.'</label>
			                    <div class="col-sm-6" style="width:54%;">
			                        <textarea id="LCF['.$LCFID.']" name="LCF['.$LCFID.']" placeholder="'.$LCFPlaceholder.'" class="form-control no-resize" rows="5" '.$LCFIsRequired2.' />'.$Value.'
			                        </textarea>
			                    </div>
			                </div>';
		}
		elseif ($LCFType == 'date') 
		{
				$CustomField .=	'<div class="form-group" style="width:100%;">
			                        <label class="col-sm-5 control-label" style="width:44%;">'.$LCFName.'</label>
			                        <div class="col-sm-6" style="width:54%;">
			                            <input type="text" id="LCF['.$LCFID.']" name="LCF['.$LCFID.']" value="'.$Value.'" placeholder="'.$LCFPlaceholder.'" data-format="DD-MM-YYYY" class="form-control js-dtp" '.$LCFIsRequired2.' />
			                                <span style="width: 50px; " class="glyphicon glyphicon-calendar form-control-feedback"></span>
			                        </div>
			                    </div>';
		}
		elseif ($LCFType == 'select') 
		{
			
			$LCFValues = json_decode($LCFValues,true);
			
			$CustomField .= '<div class="form-group" style="width:100%;">
                                <label class="col-sm-5 control-label" style="width:44%;">'.$LCFName.'</label>
                                <div class="col-sm-6" style="width:54%;">
                                    <select class="selectpicker form-control show-tick" id="LCF['.$LCFID.']" name="LCF['.$LCFID.']">';
                                    	foreach($LCFValues as $LCFID => $LCF_Value)
                                    	{
                                    		if ($Value == $LCF_Value) 
                                    		{
                                    			$CustomField .= '<option value="'.$LCFID.'" selected>'.$LCF_Value.'</option>';
                                    		}
                                    		else
                                    		{
                                        		$CustomField .='<option value="'.$LCFID.'">'.$LCF_Value.'</option>';
                                        	}
                                        }
                    $CustomField .='</select>
                                </div>
                            </div>';
		}
		/*elseif ($LCFType == 'radio') 
		{
			$LCFValues = json_decode($LCFValues);

			$CustomField .= '<div class="form-group" style="width:100%;">
                                <label class="col-sm-5 control-label" style="width:44%;">'.$LCFName.'</label>
                                <div class="col-sm-2" style="width:54%;">
                                    <input type="checkbox" class="js-switch" id="LCF['.$LCFID.']" name="LCF['.$LCFID.']" value="'.$Value.'" placeholder="'.$LCFPlaceholder.'" value='1' data-size="small" '.$LCFIsRequired2.'  />
                                </div>
                            </div>';
		}*/
		else 
		{
			$CustomField .= '';

		}
		
		
		return $CustomField;
	}
	public function updateLedgerCurrentBalance($LedgerID,$CurrentBalance,$Operator)
	{
		global $con;
		
		if($Operator == '+')
		{
			$UpdateLedger = mysqli_query($con,"UPDATE ledger_master SET `current_balance`= current_balance + '$CurrentBalance' WHERE ledger_id='".$LedgerID."'");	
		}
		else if($Operator == '-')
		{
			$UpdateLedger = mysqli_query($con,"UPDATE ledger_master SET `current_balance`= current_balance - '$CurrentBalance' WHERE ledger_id='".$LedgerID."'");
		}
		if($UpdateLedger)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
class Sales
{
	public function nextInvoiceNo($CompanyID)
	{
		global $con;
		
		$InvoiceNo = '';
		$SelectInvoiceNo = mysqli_query($con,"SELECT MAX(CONVERT(`invoice_no`,UNSIGNED )) AS InvoiceNo FROM invoice_master WHERE company_id='".$CompanyID."'");
		$InvoiceNoArr = mysqli_fetch_array($SelectInvoiceNo);
		$InvoiceNo = $InvoiceNoArr['InvoiceNo'];
		if(empty($InvoiceNo)) { $InvoiceNo = 0; }
		$InvoiceNo++;
		return $InvoiceNo;
	}
	
	public function nextInvoiceReceiptNo($CompanyID)
	{
		global $con;
		
		$InvoiceReceiptNo = '';
		
		$SelectInvoiceReceiptNo = mysqli_query($con,"SELECT MAX(CONVERT(`rv_no`,UNSIGNED)) AS RVNO FROM receipt_vouchers WHERE company_id='".$CompanyID."'");
		$InvoiceReceiptNoArr = mysqli_fetch_array($SelectInvoiceReceiptNo);
		$InvoiceReceiptNo = $InvoiceReceiptNoArr['RVNO'];
		if(empty($InvoiceReceiptNo)) { $InvoiceReceiptNo = 0; }
		$InvoiceReceiptNo++;
		return $InvoiceReceiptNo;
	}
	
	public function getBankCustomFieldForReceipt($ReceiptMode)
	{
		global $con;
		
		$CustomField = '';
		
		if($ReceiptMode == 'CHEQUE')
		{
			$CustomField .= '<div class="form-group">
				<label class="col-sm-5 control-label">Bank Name</label>
				<div class="col-sm-6">
					<input type="text" id="BankName" name="BankName" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-5 control-label">Cheque No</label>
				<div class="col-sm-6">
					<input type="text" id="ChequeNo" name="ChequeNo" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-5 control-label">Cheque Date</label>
				<div class="col-sm-6">
					<input type="text" id="ChequeDate" name="ChequeDate" data-format="DD-MM-YYYY" class="form-control js-dtp" />
					<span style="width: 50px;" class="glyphicon glyphicon-calendar form-control-feedback"></span>
				</div>
			</div>';
		}
		else if($ReceiptMode == 'CREDIT/DEBIT CARD')
		{
			$CustomField .= '<div class="form-group">
					<label class="col-sm-5 control-label">Transaction Date</label>
					<div class="col-sm-6">
						<input type="text" id="TransactionDate" name="TransactionDate" data-format="DD-MM-YYYY" class="form-control js-dtp" />
						<span style="width: 50px;" class="glyphicon glyphicon-calendar form-control-feedback"></span>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-5 control-label">Card No</label>
					<div class="col-sm-6">
						<input type="text" id="CardNo" name="CardNo" class="form-control" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-5 control-label">Transaction Ref</label>
					<div class="col-sm-6">
						<input type="text" id="TransactionRef" name="TransactionRef" class="form-control" />
					</div>
				</div>
			';
		}
		else if($ReceiptMode == 'INTERNET BANKING')
		{
			$CustomField .= '<div class="form-group">
					<label class="col-sm-5 control-label">Bank Name</label>
					<div class="col-sm-6">
						<input type="text" id="BankName" name="BankName" class="form-control" />
					</div>
				</div> 
				<div class="form-group">
					<label class="col-sm-5 control-label">Payment Date</label>
					<div class="col-sm-6">
						<input type="text" id="PaymentDate" name="PaymentDate" data-format="DD-MM-YYYY" class="form-control js-dtp" />
						<span style="width: 50px;" class="glyphicon glyphicon-calendar form-control-feedback"></span>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-5 control-label">Transaction Ref</label>
					<div class="col-sm-6">
						<input type="text" id="TransactionRef" name="TransactionRef" class="form-control" />
					</div>
				</div>
			';
		}
		else
		{
			$CustomField .= '';
		}
		return $CustomField;
	}
	public function getTotalGST1($InvoiceID)
	{
		global $con;
		
		$TotalTaxableAmount = 0;
		$TotalSGST = 0;
		$TotalCGST = 0;
		$TotalIGST = 0;
		
		$SelectTax = mysqli_query($con,"SELECT SUM(taxable_value), SUM(tax1_amount), SUM(tax2_amount), SUM(tax3_amount) FROM invoice_item_master WHERE invoice_id='".$InvoiceID."'");
		$Tax = mysqli_fetch_array($SelectTax);
		
		$TotalTaxableAmount = $Tax['SUM(taxable_value)'];
		$TotalSGST = $Tax['SUM(tax1_amount)'];
		$TotalCGST = $Tax['SUM(tax2_amount)'];
		$TotalIGST = $Tax['SUM(tax3_amount)'];
		
		$TaxArr = array(
			"TotalTaxableAmount"=>$TotalTaxableAmount,
			"TotalSGST"=>$TotalSGST,
			"TotalCGST"=>$TotalCGST,
			"TotalIGST"=>$TotalIGST
		);
		$TaxData = json_encode($TaxArr);
		return $TaxData;
	}
}

class Purchase
{
	public function nextVoucherNo($CompanyID)
	{
		global $con;
		
		$VoucherNo = '';
		$SelectVoucherNo = mysqli_query($con,"SELECT MAX(CONVERT(`voucher_no`,UNSIGNED)) AS VoucherNo FROM purchase_master WHERE company_id='".$CompanyID."'");
		$VoucherNoArr = mysqli_fetch_array($SelectVoucherNo);
		$VoucherNo = $VoucherNoArr['VoucherNo'];
		if(empty($VoucherNo)) { $VoucherNo = 0; }
		$VoucherNo++;
		return $VoucherNo;
	}
	
	public function nextInvoicePaymentNo($CompanyID)
	{
		global $con;
		
		$InvoicePaymentNo = '';
		$SelectInvoicePaymentNo = mysqli_query($con,"SELECT MAX(CONVERT(`pv_no`,UNSIGNED)) AS PVNO FROM payment_vouchers WHERE company_id='".$CompanyID."'");
		$InvoicePaymentNoArr = mysqli_fetch_array($SelectInvoicePaymentNo);
		$InvoicePaymentNo = $InvoicePaymentNoArr['PVNO'];
		if(empty($InvoicePaymentNo)) { $InvoicePaymentNo = 0; }
		$InvoicePaymentNo++;
		return $InvoicePaymentNo;
	}
	
	public function getBankCustomFieldForPayment($PaymentMode)
	{
		global $con;
		
		$CustomField = '';
		
		if($PaymentMode == 'CHEQUE')
		{
			$CustomField .= '<div class="form-group">
				<label class="col-sm-5 control-label">Bank Name</label>
				<div class="col-sm-6">
					<input type="text" id="BankName" name="BankName" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-5 control-label">Cheque No</label>
				<div class="col-sm-6">
					<input type="text" id="ChequeNo" name="ChequeNo" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-5 control-label">Cheque Date</label>
				<div class="col-sm-6">
					<input type="text" id="ChequeDate" name="ChequeDate" data-format="DD-MM-YYYY" class="form-control js-dtp" />
					<span style="width: 50px;" class="glyphicon glyphicon-calendar form-control-feedback"></span>
				</div>
			</div>';
		}
		else if($PaymentMode == 'CREDIT/DEBIT CARD')
		{
			$CustomField .= '<div class="form-group">
					<label class="col-sm-5 control-label">Transaction Date</label>
					<div class="col-sm-6">
						<input type="text" id="TransactionDate" name="TransactionDate" data-format="DD-MM-YYYY" class="form-control js-dtp" />
						<span style="width: 50px;" class="glyphicon glyphicon-calendar form-control-feedback"></span>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-5 control-label">Card No</label>
					<div class="col-sm-6">
						<input type="text" id="CardNo" name="CardNo" class="form-control" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-5 control-label">Transaction Ref</label>
					<div class="col-sm-6">
						<input type="text" id="TransactionRef" name="TransactionRef" class="form-control" />
					</div>
				</div>
			';
		}
		else if($PaymentMode == 'INTERNET BANKING')
		{
			$CustomField .= '<div class="form-group">
					<label class="col-sm-5 control-label">Bank Name</label>
					<div class="col-sm-6">
						<input type="text" id="BankName" name="BankName" class="form-control" />
					</div>
				</div> 
				<div class="form-group">
					<label class="col-sm-5 control-label">Payment Date</label>
					<div class="col-sm-6">
						<input type="text" id="PaymentDate" name="PaymentDate" data-format="DD-MM-YYYY" class="form-control js-dtp" />
						<span style="width: 50px;" class="glyphicon glyphicon-calendar form-control-feedback"></span>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-5 control-label">Transaction Ref</label>
					<div class="col-sm-6">
						<input type="text" id="TransactionRef" name="TransactionRef" class="form-control" />
					</div>
				</div>
			';
		}
		else
		{
			$CustomField .= '';
		}
		return $CustomField;
	}
	
	public function getTotalGST($PurchaseID)
	{
		global $con;
		
		$TotalTaxableAmount = 0;
		$TotalSGST = 0;
		$TotalCGST = 0;
		$TotalIGST = 0;
		
		$SelectTax = mysqli_query($con,"SELECT SUM(taxable_value), SUM(tax1_amount), SUM(tax2_amount), SUM(tax3_amount) FROM purchase_item_master WHERE purchase_id='".$PurchaseID."'");
		$Tax = mysqli_fetch_array($SelectTax);
		
		$TotalTaxableAmount = $Tax['SUM(taxable_value)'];
		$TotalSGST = $Tax['SUM(tax1_amount)'];
		$TotalCGST = $Tax['SUM(tax2_amount)'];
		$TotalIGST = $Tax['SUM(tax3_amount)'];
		
		$TaxArr = array(
			"TotalTaxableAmount"=>$TotalTaxableAmount,
			"TotalSGST"=>$TotalSGST,
			"TotalCGST"=>$TotalCGST,
			"TotalIGST"=>$TotalIGST
		);
		$TaxData = json_encode($TaxArr);
		return $TaxData;
	}
	
}
class Banking
{
	public function nextBankVoucherNo($CompanyID)
	{
		global $con;
		
		$BVNO = '';
		$SelectBVNO = mysqli_query($con,"SELECT MAX(CONVERT(`bv_no`,UNSIGNED )) AS BVNO FROM banking_master WHERE company_id='".$CompanyID."'");
		$BVNOArr = mysqli_fetch_array($SelectBVNO);
		$BVNO = $BVNOArr['BVNO'];
		if(empty($BVNO)) { $BVNO = 0; }
		$BVNO++;
		return $BVNO;
	}
}
?>
<?php
$words = array('0'=> '' ,'1'=> 'one' ,'2'=> 'two' ,'3' => 'three','4' => 'four','5' => 'five','6' => 'six','7' => 'seven','8' => 'eight','9' => 'nine','10' => 'ten','11' => 'eleven','12' => 'twelve','13' => 'thirteen','14' => 'fouteen','15' => 'fifteen','16' => 'sixteen','17' => 'seventeen','18' => 'eighteen','19' => 'nineteen','20' => 'twenty','30' => 'thirty','40' => 'fourty','50' => 'fifty','60' => 'sixty','70' => 'seventy','80' => 'eighty','90' => 'ninty','100' => 'hundred','1000' => 'thousand','100000' => 'lakh','10000000' => 'crore');
function no_to_words($no)
{    
	global $words;
    if($no == 0)
	{
   		return '';
	}
	else 
	{
		$novalue = '';
		$highno = $no;
		$remainno = 0;
		$value = 100;
		$value1 = 1000;        
     	while($no >= 100)    
		{
       		if(($value <= $no) &&($no < $value1))    
			{
            	$novalue = $words["$value"];
             	$highno = (int)($no/$value);
               	$remainno = $no % $value;
               	break;
          	}
          	$value = $value1;
          	$value1 = $value * 100;
     	}        
      	if(array_key_exists("$highno",$words))
		{
        	return $words["$highno"]." ".$novalue." ".no_to_words($remainno);
		}
		else 
		{ 
        	$unit = $highno%10;
          	$ten = (int)($highno/10)*10;             
          	return $words["$ten"]." ".$words["$unit"]." ".$novalue." ".no_to_words($remainno);
      	}
  	}
}
?>