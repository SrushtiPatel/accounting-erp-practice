<?php
ob_start();
session_start();
require_once('dbconfig.php');

if($_SERVER["REQUEST_METHOD"] == "POST")
{
	// If Old Session Available...
	if(isset($_SESSION['AdminID']))
	{
		session_destroy();
		session_start(); 
	}
	$Action = $_POST['Action'];

	// Admin Login
	if($Action == 'AdminLogin')
	{
		$UserName = $_POST['UserName'];
		$Password = $_POST['Password'];
		
		$UserName = mysqli_real_escape_string($con,$UserName); 
		$Password = mysqli_real_escape_string($con,$Password);
				
		$EncPassword = md5($Password);
		
		$Select = "SELECT admin_id, admin_name, username, password, admin_type_id, status FROM admin_master WHERE BINARY username='".$UserName."' AND password='".$EncPassword."'";
		$SelectQuery = mysqli_query($con,$Select);
		
		if(!$SelectQuery)
		{
			$jsonData = '{ 
				"Status":"1"
			}';
			echo $jsonData;
			exit();
		}
		
		$existAccount = mysqli_num_rows($SelectQuery);
		$Admin = mysqli_fetch_array($SelectQuery);
		
		if($existAccount==1)
		{
			if($Admin['status']==1)
			{
				$AdminID = $Admin['admin_id'];
				$AdminName = $Admin['admin_name'];
				$UserName = $Admin['username'];
				$AdminType = $Admin['admin_type_id'];
				$LogInTime = date("Y-m-d H:i:s");
				$SessionID = session_id();
				
				ini_set('session_save_path','/');
				ini_set('session.gc_probability', 1);
				
				$_SESSION['AdminID'] = $AdminID;
				$_SESSION['AdminName'] = $AdminName;
				$_SESSION['UserName'] = $UserName;
				$_SESSION['AdminType'] = $AdminType;
				$_SESSION['SessionID']= $SessionID;
				
				setcookie("AdminID", $AdminID, strtotime( '+30 days' ), "/", "", "", TRUE);
				setcookie("AdminName", $AdminName, strtotime( '+30 days' ), "/", "", "", TRUE);
				setcookie("UserName", $UserName, strtotime( '+30 days' ), "/", "", "", TRUE);
				setcookie("AdminType", $AdminType, strtotime( '+30 days' ), "/", "", "", TRUE);
				setcookie("LoginStatus", 1, strtotime( '+30 days' ), "/", "", "", TRUE);
				
				function get_client_ip() {
					$ipaddress = '';
					if (getenv('HTTP_CLIENT_IP'))
						$ipaddress = getenv('HTTP_CLIENT_IP');
					else if(getenv('HTTP_X_FORWARDED_FOR'))
						$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
					else if(getenv('HTTP_X_FORWARDED'))
						$ipaddress = getenv('HTTP_X_FORWARDED');
					else if(getenv('HTTP_FORWARDED_FOR'))
						$ipaddress = getenv('HTTP_FORWARDED_FOR');
					else if(getenv('HTTP_FORWARDED'))
						$ipaddress = getenv('HTTP_FORWARDED');
					else if(getenv('REMOTE_ADDR'))
						$ipaddress = getenv('REMOTE_ADDR');
					else
						$ipaddress = 'UNKNOWN';
				 
					return $ipaddress;
				}
		
				$identifier = get_client_ip();
				
				$InsertLoginHistory = mysqli_query($con,"INSERT INTO `admin_login_history`(`admin_id`, `ip`, `session_id`, `login_datetime`) VALUES ('$AdminID','$identifier','$SessionID','$LogInTime')");
				
				$LoginID = mysqli_insert_id($con);
				$_SESSION['LoginID'] = $LoginID;
				setcookie("LoginID", $LoginID, strtotime( '+30 days' ), "/", "", "", TRUE);
				
				$UpdateLive = mysqli_query($con,"UPDATE `admin_master` SET `live`='1' WHERE admin_id='".$_SESSION['AdminID']."'");
				
				if(isset($_SESSION['AdminID']))
				{
					$jsonData = '{ 
						"Status":"5"
					}';
					echo $jsonData;
					exit();
				}
				else
				{
					session_destroy();
					$jsonData = '{ 
						"Status":"4"
					}';
					echo $jsonData;
					exit();
				}
			}
			else
			{
				$jsonData = '{ 
					"Status":"3"
				}';
				echo $jsonData;
				exit();
			}
		}
		else
		{
			$jsonData = '{ 
				"Status":"2"
			}';
			echo $jsonData;
			exit();
		}
	}
	else
	{
		$jsonData = '{ 
			"Status":"Unauthorised Access!"
		}';
		echo $jsonData;
		exit();
	}
}
else
{
	$jsonData = '{ 
		"Status":"Unauthorised Access!"
	}';
	echo $jsonData;
	exit();
}
ob_flush();
?>