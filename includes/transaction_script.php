<?php
ob_start();
session_start();
require_once('dbconfig.php');
require_once('functions.php');
require_once('objects.php');
if($_SERVER["REQUEST_METHOD"] == "POST")
{
	$Action = $_POST['Action'];
	
	if($Action == 'AddTransaction')
	{
		$CompanyID = $_POST['CompanyID'];
		$VoucherNo = $_POST['VoucherNo'];
		$VoucherNo = $bankingObject->nextBankVoucherNo($CurrentCompanyID);
		$TransactionDate = $_POST['TransactionDate'];
		$PayFrom = $_POST['PayFrom'];
		$PayTo = $_POST['PayTo'];
		$Amount = $_POST['Amount'];
		$Description = $_POST['Description'];
		
		if(!empty($TransactionDate)) { $TransactionDate = date("Y-m-d",strtotime($_POST['TransactionDate'])); }
		
		$AddedBy = $_POST['AdminID'];
		$AddedDate = date("Y-m-d H:i:s");
		
		$Insert = "INSERT INTO `banking_master`(`company_id`, `bv_no`, `transaction_date`, `pay_from`, `pay_to`, `amount`, `description`, `addedby`, `addeddate`) VALUES ('$CompanyID','$VoucherNo','$TransactionDate','$PayFrom','$PayTo','$Amount','$Description','$AddedBy','$AddedDate')";
		
		$InsertQuery = mysqli_query($con,$Insert);
		
		if($InsertQuery)
		{
			$UpdateDebitBankAccount = $ledgerObject->updateLedgerCurrentBalance($PayFrom,$Amount,'-');
			
			$UpdateCreditBankAccount = $ledgerObject->updateLedgerCurrentBalance($PayTo,$Amount,'+');
			
			if(!$InsertQuery)
			{
				$jsonData = '{ 
					"Status":"1"
				}';
				echo $jsonData;
				exit();
			}
			else
			{
				$jsonData = '{ 
					"Status":"2"
				}';
				echo $jsonData;
				exit();
			}
		}
		else
		{
			if(mysqli_errno($con)==1062)
			{
				$jsonData = '{ 
					"Status":"0"
				}';
				echo $jsonData;
				exit();
			}
			else
			{
				$jsonData = '{ 
					"Status":"1"
				}';
				echo $jsonData;
				exit();
			}
		}
			
	}
	else if($Action == 'UpdateTransaction')
	{
		$BankingID = $_POST['BankingID'];
		$CompanyID = $_POST['CompanyID'];
		$VoucherNo = $_POST['VoucherNo'];
		$TransactionDate = $_POST['TransactionDate'];
		$PayFrom = $_POST['PayFrom'];
		$PayTo = $_POST['PayTo'];
		$Amount = $_POST['Amount'];
		$Description = $_POST['Description'];
		
		$OldPayFrom = $_POST['OldPayFrom'];
		$OldPayTo = $_POST['OldPayTo'];
		$OldAmount = $_POST['OldAmount'];
		
		if(!empty($TransactionDate)) { $TransactionDate = date("Y-m-d",strtotime($_POST['TransactionDate'])); }
		
		$ModifiedBy = $_POST['AdminID'];
		$ModifiedDate = date("Y-m-d H:i:s");
		
		$Update = "UPDATE `banking_master` SET `transaction_date`='$TransactionDate',`pay_from`='$PayFrom',`pay_to`='$PayTo',`amount`='$Amount',`description`='$Description',`modifiedby`='$ModifiedBy',`modifieddate`='$ModifiedDate' WHERE banking_id='".$BankingID."'";
		$UpdateQuery = mysqli_query($con,$Update);
		
		if($UpdateQuery)
		{
			$UpdateDebitBankAccount = $ledgerObject->updateLedgerCurrentBalance($PayFrom,$Amount,'-');
			$UpdateCreditBankAccount = $ledgerObject->updateLedgerCurrentBalance($PayTo,$Amount,'+');
			
			$UpdateOldDebitBankAccount = $ledgerObject->updateLedgerCurrentBalance($OldPayFrom,$OldAmount,'+');
			$UpdateOldCreditBankAccount = $ledgerObject->updateLedgerCurrentBalance($OldPayTo,$OldAmount,'-');
			
			if(!$UpdateQuery)
			{
				$jsonData = '{ 
					"Status":"3"
				}';
				echo $jsonData;
				exit();
			}
			else
			{
				$jsonData = '{ 
					"Status":"4"
				}';
				echo $jsonData;
				exit();
			}
		}
		else
		{
			if(mysqli_errno($con)==1062)
			{
				$jsonData = '{ 
					"Status":"0"
				}';
				echo $jsonData;
				exit();
			}
			else
			{
				$jsonData = '{ 
					"Status":"3"
				}';
				echo $jsonData;
				exit();
			}
		}
	}
	else if($Action == 'RemoveTransaction')
	{
		$BankingID = $_POST['BankingID'];
		$PayFrom = $_POST['PayFrom'];
		$PayTo = $_POST['PayTo'];
		$Amount = $_POST['Amount'];
		
		$Remove = "DELETE FROM banking_master WHERE banking_id='".$BankingID."'";
		$RemoveQuery = mysqli_query($con,$Remove);
		if($RemoveQuery)
		{
			$UpdateDebitBankAccount = $ledgerObject->updateLedgerCurrentBalance($PayFrom,$Amount,'+');
			
			$UpdateCreditBankAccount = $ledgerObject->updateLedgerCurrentBalance($PayTo,$Amount,'-');
			
			if($RemoveQuery)
			{
				$jsonData = '{ 
					"Status":"6"
				}';
				echo $jsonData;
				exit();
			}
			else
			{
				$jsonData = '{ 
					"Status":"5"
				}';
				echo $jsonData;
				exit();
			}
		}
		else
		{
			$jsonData = '{ 
				"Status":"5"
			}';
			echo $jsonData;
			exit();
		}
	}
	
}
else
{
	$jsonData = '{ 
		"Status":"Unauthorised Access!"
	}';
	echo $jsonData;
	exit();
}
ob_flush();
?>