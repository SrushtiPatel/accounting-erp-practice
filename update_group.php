<?php
ob_start();
$Page = "Group"; 
require_once('head.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Xenon ERP</title>
    
    <!-- Favicon -->
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="assets/plugins/bootstrap/dist/css/bootstrap.css" rel="stylesheet" />

    <!-- Animate.css Css -->
    <link href="assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Font Awesome Css -->
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

    <!-- Switchery Css -->
    <link href="assets/plugins/switchery/dist/switchery.css" rel="stylesheet" />

    <!-- Metis Menu Css -->
    <link href="assets/plugins/metisMenu/dist/metisMenu.css" rel="stylesheet" />

    <!-- Bootstrap Select Css -->
    <link href="assets/plugins/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" />

    <!-- Colorpicker Css -->
    <link href="assets/plugins/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.css" rel="stylesheet" />

    <!-- Dropzone Css -->
    <link href="assets/plugins/dropzone/dist/dropzone.css" rel="stylesheet" />

    <!-- Multiselect Css -->
    <link href="assets/plugins/multiselect/css/multi-select.css" rel="stylesheet" />

    <!-- Spinners Css -->
    <link href="assets/plugins/jquery.spinner/dist/css/bootstrap-spinner.css" rel="stylesheet" />

    <!-- Bootstrap TagsInput Css -->
    <link href="assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />

    <!-- Select 2 Css -->
    <link href="assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />

    <!-- Chosen Css -->
    <link href="assets/plugins/chosen/chosen.css" rel="stylesheet" />

    <!-- Bootstrap DateRangePicker Css -->
    <link href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />

    <!-- noUISlider Css -->
    <link href="assets/plugins/nouislider/distribute/nouislider.css" rel="stylesheet" />

    <!-- Pace Loader Css -->
    <link href="assets/plugins/pace/themes/white/pace-theme-flash.css" rel="stylesheet" />

    <!-- Summernote Css -->
    <link href="assets/plugins/summernote/dist/summernote.css" rel="stylesheet" />

    <!-- Ion Range Slider Css -->
    <link href="assets/plugins/ionrangeslider/css/ion.rangeSlider.css" rel="stylesheet" />
    <link href="assets/plugins/ionrangeslider/css/ion.rangeSlider.skinFlat.css" rel="stylesheet" />


    <!-- Pace Loader Css -->
    <link href="assets/plugins/pace/themes/white/pace-theme-flash.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="assets/css/style.css" rel="stylesheet" />

</head>
<body class="ls-fixed navbar-fixed">
    <div class="all-content-wrapper">
        <!-- Top Bar -->
        <header>
            <?php include_once('header.php'); ?>
        </header>
        <?php
            if(isset($_GET['group_id']))
            {
                $GroupID = $_GET['group_id'];
                $SelectGroup = "SELECT * FROM group_master WHERE group_id='".$GroupID."'";
                $SelectGroupQuery = mysqli_query($con,$SelectGroup);
                if(!$SelectGroupQuery)
                {
                    //die(mysqli_error($con));
                    header("Location: view_all_group.php");
                    exit();
                }
                $count = mysqli_num_rows($SelectGroupQuery);
                if($count != 1)
                {
                    header("Location: view_all_group.php");
                    exit();
                }
                
                $Group = mysqli_fetch_array($SelectGroupQuery);
                
                $GroupID = $Group['group_id'];
                $GroupName = $Group['group_name'];
                $ParentID = $Group['parent_id'];

                $GroupDetail = $groupObject->selectGroupDetail($ParentID);
                $GroupDetail = json_decode($GroupDetail,true);
                $PGroupName = $GroupDetail['GroupName'];
            }
            else
            {
                header("Location: view_all_group.php");
                exit();
            }
        ?>
        <!-- #END# Top Bar -->
        <!-- Left Menu -->
        <aside class="sidebar">
            <?php include_once('menu.php'); ?>
        </aside>
        <!-- #END# Left Menu -->
        <section class="content">
            <div class="page-heading">
                <h1>
                    <a href="view_all_group.php">
                        Group
                    </a>
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        <a class="font-bold" href="update_group.php?group_id=<?php echo $GroupID; ?>">Update Group Detail</a>
                    </small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="dashboard.php">Home</a></li>
                    <li><a href="view_all_group.php">Group</a></li>
                </ol>
            </div>
            
            <div class="page-body">
                <!-- -------------- ERROR SECTION START -------------- -->
                        
                <div id="flash" class="alert alert hidden">
                    <strong>
                        <i class="ace-icon fa fa-spinner"></i>
                    </strong>
                    &nbsp; &nbsp;
                    <span></span>
                </div>
            
            	<!-- -------------- ERROR SECTION END -------------- -->
                <div class="row clearfix">
                    <!-- Horizontal Layout  -->
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">Update Group</div>
                            <div class="panel-body p-b-25">
                                <form id="Update-Group-Form" method="post" class="form-horizontal" action="#">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <input type="hidden" id="AdminID" name="AdminID" value="<?php echo $_SESSION['AdminID']; ?>" required />
                                        <input type="hidden" id="GroupID" name="GroupID" value="<?php echo $GroupID; ?>" required />
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Group Name</label>
                                            <div class="col-sm-6">
                                                <input type="text" id="GroupName" name="GroupName" value="<?php echo $GroupName; ?>" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Parent Group</label>
                                            <div class="col-sm-6">
                                                <select class="col-xs-10 col-md-8 selectpicker form-control show-tick" id="ParentID" name="ParentID">
                                                    <option value="NULL">-- SELECT --</option>
                                                    <?php
                                                        $SelectPGroup = "SELECT * FROM group_master WHERE visible='1' ORDER BY group_id";
                                                        $SelectPGroupQuery = mysqli_query($con,$SelectPGroup);
                                                                            
                                                        while($PGroup = mysqli_fetch_array($SelectPGroupQuery))
                                                        {
                                                            if($ParentID == $PGroup['group_id'])
                                                            {
                                                                echo '<option value="'.$PGroup['group_id'].'" selected>'.$PGroup['group_name'].'</option>';
                                                            }
                                                            else
                                                            {
                                                                echo '<option value="'.$PGroup['group_id'].'">'.$PGroup['group_name'].'</option>';
                                                            }
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="form-group">
                                            <div class="col-sm-offset-4 col-sm-6">
                                                <button type="submit" class="btn btn-success">
                                                    <i class="fa fa-refresh bigger-110"></i>
                                                    Update
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- #END# Horizontal Layout  -->
                </div>
            </div>
        </section>
        <!-- Footer -->
       
        <!-- #END# Footer -->
    </div>
        
    <!-- Jquery Core Js -->
    <script src="assets/plugins/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="assets/plugins/bootstrap/dist/js/bootstrap.js"></script>

    <!-- Pace Loader Js -->
    <script src="assets/plugins/pace/pace.js"></script>

    <!-- Screenfull Js -->
    <script src="assets/plugins/screenfull/src/screenfull.js"></script>

    <!-- Metis Menu Js -->
    <script src="assets/plugins/metisMenu/dist/metisMenu.js"></script>

    <!-- Jquery Slimscroll Js -->
    <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- iCheck Js -->
    <script src="assets/plugins/iCheck/icheck.js"></script>

    <!-- MomentJs -->
    <script src="assets/plugins/moment/moment.js"></script>

    <!-- Autosize Js (Textarea auto growth plugin) -->
    <script src="assets/plugins/autosize/dist/autosize.js"></script>

    <!-- Bootstrap Select Js -->
    <script src="assets/plugins/bootstrap-select/dist/js/bootstrap-select.js"></script>

    <!-- Dropzone Js -->
    <script src="assets/plugins/dropzone/dist/dropzone.js"></script>

    <!-- Masked Input Js -->
    <script src="assets/plugins/jquery.inputmask/dist/jquery.inputmask.bundle.js"></script>

    <!-- Multiselect Js -->
    <script src="assets/plugins/multiselect/js/jquery.multi-select.js"></script>

    <!-- Spinners Js -->
    <script src="assets/plugins/jquery.spinner/dist/js/jquery.spinner.js"></script>

    <!-- Bootstrap TagsInput Js -->
    <script src="assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.js"></script>

    <!-- TinyMCE Js -->
    <script src="assets/plugins/tinymce/tinymce.js"></script>

    <!-- Chosen Js -->
    <script src="assets/plugins/chosen/chosen.jquery.js"></script>

    <!-- Cropit Js -->
    <script src="assets/plugins/cropit/dist/jquery.cropit.js"></script>

    <!-- Custom Js -->
    <script src="assets/js/admin.js"></script>
    

    
   <script type="text/javascript">
        $('#Update-Group-Form').on('submit', function(event) {
            
            event.preventDefault();
             
			var ParentID = $("#ParentID").val();
			if(ParentID == 'NULL')
			{
				alert("Please Select Any Parent Group.");
				return false;
			}
			
            var Action = 'UpdateGroup';
			
            var form_data = new FormData(this);
            form_data.append('Action',Action); 
                
            $("#flash").show();
            $("#flash i").addClass('fa-spinner');
            $("#flash i").addClass('fa-spin');
            $("#flash").removeClass('hidden');
            $('html,body').animate({ scrollTop: $(".content").offset().top},'slow');
            $("#flash span").html('Please Wait...');
                
            $.ajax({
                url: 'includes/group_script.php',
                type: 'POST',
                data: form_data,
                contentType: false,
                cache: false,
                processData:false,
                success: function(result)
                {
                    //alert(result);
                    var obj = JSON.parse(result);
                    var Status = obj.Status;
                        
                    if(Status == '4')
                    {
                        $("#flash").removeClass('alert alert-danger');
                        $("#flash").addClass('alert alert-success');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-times-circle');
                        $("#flash i").addClass('fa fa-fw fa-check-circle');
                        $("#flash span").html('Group Update Successfully.');
                        document.getElementById("Update-Group-Form").reset();
                        $('#flash').delay(3000).fadeOut(500);
                        setTimeout(function() {
                          window.location.href = "update_group.php?group_id=<?php echo $GroupID; ?>";
                        }, 1000);
                        return true;                            
                    }
                    else if(Status == '3')
                    {
                        $("#flash").removeClass('alert alert-success');
                        $("#flash").addClass('alert alert-danger');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash i").addClass('fa fa-fw fa-times-circle');
                        $("#flash span").html('Group Update Not Successfully.');
                        $('#flash').delay(3000).fadeOut(500);
                        return true;
                    }
                    else if(Status == '0')
                    {
                        $("#flash").removeClass('alert alert-success');
                        $("#flash").addClass('alert alert-danger');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash i").addClass('fa fa-fw fa-times-circle');
                        $("#flash span").html('Group Already Exist.');
                        $('#flash').delay(4000).fadeOut(500);
                        return true;
                    }
                    else if(Status == '00')
                    {
                        $("#flash").removeClass('alert alert-success');
                        $("#flash").addClass('alert alert-danger');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash i").addClass('fa fa-fw fa-times-circle');
                        $("#flash span").html('Cannot Update Group, There is a Reference Available For This Group.');
                        $('#flash').delay(4000).fadeOut(500);
                        return true;
                    }
                    else
                    {
                        $("#flash").removeClass('alert alert-success');
                        $("#flash").addClass('alert alert-danger');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash i").addClass('fa fa-fw fa-times-circle');
                        $("#flash span").html(Status);
                        $('#flash').delay(4000).fadeOut(500);
                        return true;
                    }
                }
            });
        });
    </script>
        
    </body>
</html>
<?php
ob_flush();
?>