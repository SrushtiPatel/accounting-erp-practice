<?php
ob_start();
$Page = "Company"; 
require_once('head.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Xenon ERP</title>
    
    <!-- Favicon -->
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="assets/plugins/bootstrap/dist/css/bootstrap.css" rel="stylesheet" />

    <!-- Animate.css Css -->
    <link href="assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Font Awesome Css -->
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

    <!-- iCheck Css -->
    <link href="assets/plugins/iCheck/skins/flat/_all.css" rel="stylesheet" />

    <!-- Switchery Css -->
    <link href="assets/plugins/switchery/dist/switchery.css" rel="stylesheet" />

    <!-- Metis Menu Css -->
    <link href="assets/plugins/metisMenu/dist/metisMenu.css" rel="stylesheet" />

    <!-- Pace Loader Css -->
    <link href="assets/plugins/pace/themes/white/pace-theme-flash.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="assets/css/style.css" rel="stylesheet" />

    <!-- Jquery Datatables Css -->
    <link href="assets/plugins/DataTables/media/css/dataTables.bootstrap.css" rel="stylesheet" />
    
</head>
<body class="ls-fixed navbar-fixed">
    <div class="all-content-wrapper">
        <!-- Top Bar -->
        <header>
            <?php include_once('header.php'); ?>
        </header>
        <?php
            $SelectCompany = "SELECT * FROM company_master WHERE company_id='1'";
            $SelectCompanyQuery = mysqli_query($con,$SelectCompany);
            if(!$SelectCompanyQuery)
            {
                die(mysqli_error($con));
            }
            if(!$SelectCompanyQuery)
            {
                //die(mysqli_error($con));
                header("Location: dashboard.php");
                exit();
            }
            $count = mysqli_num_rows($SelectCompanyQuery);
            if($count != 1)
            {
                header("Location: dashboard.php");
                exit();
            }

            $Company = mysqli_fetch_array($SelectCompanyQuery);
            
            $CompanyID = $Company['company_id'];
            $CompanyName = $Company['company_name'];
            $CompanyAlias = $Company['company_alias'];
            $CompanyCode = $Company['company_code'];
            $Address = $Company['address'];
            $Phone = $Company['phone'];
            $Mobile = $Company['mobile'];
            $Email = $Company['email'];
            $GSTIN = $Company['gstin'];
            $State = $Company['state'];
            $StateCode = $Company['state_code'];
            $PANNO = $Company['pan_no'];
			$BankName = $Company['bank_name'];
			$BankAcNo = $Company['bank_ac_no'];
			$BankIFSC = $Company['bank_ifsc'];
			$BankBranch = $Company['bank_branch'];
            
        ?>
        <!-- #END# Top Bar -->
        <!-- Left Menu -->
        <aside class="sidebar">
            <?php include_once('menu.php'); ?>
        </aside>
        <!-- #END# Left Menu -->
        <section class="content dashboard">
            <div class="page-heading">
                <h1>
                    <a href="company.php">
                        Company
                    </a>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="dashboard.php">Home</a></li>
                    <li><a href="company.php">Company</a></li>
                </ol>
            </div>
            
            <div class="page-body">
                <!-- -------------- ERROR SECTION START -------------- -->
                        
                    <div id="flash" class="alert alert hidden">
                        <strong>
                            <i class="fa fa-spinner fa-spin"></i>
                        </strong>
                        &nbsp; &nbsp;
                        <span></span>
                    </div>
                
                <!-- -------------- ERROR SECTION END -------------- -->
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">Admin Profile</div>
                            <div class="panel-body">
                                <table class="table table-striped table-bordered">
                                    <tbody>
                                        <tr>
                                            <th class="col-xs-6 col-sm-4">Company Name</th>
                                            <td><?php echo $CompanyName; ?></td>
                                        </tr>
                                        <tr>
                                            <th class="col-xs-6 col-sm-4">Company Alias</th>
                                            <td><?php echo $CompanyAlias; ?></td>
                                        </tr>
                                        <tr>
                                            <th class="col-xs-6 col-sm-4">Company Code</th>
                                            <td><?php echo $CompanyCode; ?></td>
                                        </tr>
                                        <tr>
                                            <th class="col-xs-6 col-sm-4">Address</th>
                                            <td><?php echo $Address; ?></td>
                                        </tr>
                                        <tr>
                                            <th class="col-xs-6 col-sm-4">Phone</th>
                                            <td><?php echo $Phone; ?></td>
                                        </tr>
                                        <tr>
                                            <th class="col-xs-6 col-sm-4">Mobile</th>
                                            <td><?php echo $Mobile; ?></td>
                                        </tr>
                                        <tr>
                                            <th class="col-xs-6 col-sm-4">Email</th>
                                            <td><?php echo $Email; ?></td>
                                        </tr>
                                        <tr>
                                            <th class="col-xs-6 col-sm-4">PAN NO</th>
                                            <td><?php echo $PANNO; ?></td>
                                        </tr>
                                        <tr>
                                            <th class="col-xs-6 col-sm-4">GSTIN</th>
                                            <td><?php echo $GSTIN; ?></td>
                                        </tr>
                                        <tr>
                                            <th class="col-xs-6 col-sm-4">State</th>
                                            <td><?php echo $State; ?></td>
                                        </tr>
                                        <tr>
                                            <th class="col-xs-6 col-sm-4">State Code</th>
                                            <td><?php echo $StateCode; ?></td>
                                        </tr>
                                        <tr>
                                            <th class="col-xs-6 col-sm-4">Bank Name</th>
                                            <td><?php echo $BankName; ?></td>
                                        </tr>
                                        <tr>
                                            <th class="col-xs-6 col-sm-4">Bank Ac No</th>
                                            <td><?php echo $BankAcNo; ?></td>
                                        </tr>
                                        <tr>
                                            <th class="col-xs-6 col-sm-4">Bank IFSC</th>
                                            <td><?php echo $BankIFSC; ?></td>
                                        </tr>
                                        <tr>
                                            <th class="col-xs-6 col-sm-4">Bank Branch</th>
                                            <td><?php echo $BankBranch; ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- Horizontal Layout  -->
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">Update Company Profile</div>
                            <div class="panel-body p-b-25">
                                <form id="Update-Company-Form" method="post" class="form-horizontal" action="#">
                                    <input type="hidden" id="AdminID" name="AdminID" value="<?php echo $_SESSION['AdminID']; ?>" required />
                                    <input type="hidden" id="CompanyID" name="CompanyID" value="<?php echo $CompanyID; ?>" required />
                                    <div class="form-group" style="width:100%;">
                                        <label class="col-sm-4 control-label">Company Name</label>
                                        <div class="col-sm-6">
                                            <input type="text" id="CompanyName" name="CompanyName" value="<?php echo $CompanyName; ?>" class="form-control" required />
                                        </div>
                                    </div>
                                    <div class="form-group" style="width:100%;">
                                        <label class="col-sm-4 control-label">Company Alias</label>
                                        <div class="col-sm-6">
                                            <input type="text" id="CompanyAlias" name="CompanyAlias" value="<?php echo $CompanyAlias; ?>" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group" style="width:100%;">
                                        <label class="col-sm-4 control-label">Company Code</label>
                                        <div class="col-sm-6">
                                            <input type="text" id="CompanyCode" name="CompanyCode" value="<?php echo $CompanyCode; ?>" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group" style="width:100%;">
                                        <label class="col-sm-4 control-label">Address</label>
                                        <div class="col-sm-6">
                                            <textarea id="Address" name="Address" class="form-control" rows="4"><?php echo $Address; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group" style="width:100%;">
                                        <label class="col-sm-4 control-label">Phone</label>
                                        <div class="col-sm-6">
                                            <input type="text" id="Phone" name="Phone" value="<?php echo $Phone; ?>" onKeyPress="return NuMValidation(event);" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group" style="width:100%;">
                                        <label class="col-sm-4 control-label">Mobile</label>
                                        <div class="col-sm-6">
                                            <input type="text" id="Mobile" name="Mobile" value="<?php echo $Mobile; ?>" onKeyPress="return NuMValidation(event);" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group" style="width:100%;">
                                        <label class="col-sm-4 control-label">Email</label>
                                        <div class="col-sm-6">
                                            <input type="email" id="Email" name="Email" value="<?php echo $Email; ?>" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group" style="width:100%;">
                                        <label class="col-sm-4 control-label">PAN NO</label>
                                        <div class="col-sm-6">
                                            <input type="text" id="PANNO" name="PANNO" value="<?php echo $PANNO; ?>" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group" style="width:100%;">
                                        <label class="col-sm-4 control-label">GSTIN</label>
                                        <div class="col-sm-6">
                                            <input type="text" id="GSTIN" name="GSTIN" value="<?php echo $GSTIN; ?>" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group" style="width:100%;">
                                        <label class="col-sm-4 control-label">State</label>
                                        <div class="col-sm-6">
                                            <input type="text" id="State" name="State" value="<?php echo $State; ?>" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group" style="width:100%;">
                                        <label class="col-sm-4 control-label">State Code</label>
                                        <div class="col-sm-6">
                                            <input type="text" id="StateCode" name="StateCode" value="<?php echo $StateCode; ?>" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group" style="width:100%;">
                                        <label class="col-sm-4 control-label">Bank Name</label>
                                        <div class="col-sm-6">
                                            <input type="text" id="BankName" name="BankName" value="<?php echo $BankName; ?>" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group" style="width:100%;">
                                        <label class="col-sm-4 control-label">Bank Ac No</label>
                                        <div class="col-sm-6">
                                            <input type="text" id="BankAcNo" name="BankAcNo" value="<?php echo $BankAcNo; ?>" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group" style="width:100%;">
                                        <label class="col-sm-4 control-label">Bank IFSC</label>
                                        <div class="col-sm-6">
                                            <input type="text" id="BankIFSC" name="BankIFSC" value="<?php echo $BankIFSC; ?>" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group" style="width:100%;">
                                        <label class="col-sm-4 control-label">Bank Branch</label>
                                        <div class="col-sm-6">
                                            <input type="text" id="BankBranch" name="BankBranch" value="<?php echo $BankBranch; ?>" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-4 col-sm-6">
                                            <button type="submit" class="btn btn-success">
                                                <i class="fa fa-refresh bigger-110"></i>
                                                Update
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- #END# Horizontal Layout  -->
                </div>
            </div>
        </section>
        <!-- Footer -->
       
        <!-- #END# Footer -->
    </div>

    <!-- Jquery Core Js -->
    <script src="assets/plugins/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Pace Loader Js -->
    <script src="assets/plugins/pace/pace.js"></script>

    <!-- Screenfull Js -->
    <script src="assets/plugins/screenfull/src/screenfull.js"></script>

    <!-- Metis Menu Js -->
    <script src="assets/plugins/metisMenu/dist/metisMenu.js"></script>

    <!-- Jquery Slimscroll Js -->
    <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    

    <!-- JQuery Datatables Js -->
    <script src="assets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
    <script src="assets/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>

    <!-- Custom Js -->
    <script src="assets/js/admin.js"></script>
    
    <script src="assets/js/pages/tables/jquery-datatables.js"></script>
    <script src="assets/js/pages/ui/modals.js"></script>

    <script type="text/javascript">
        jQuery(function($) {
            
        });
        
        function NuMValidation(evt)
        {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
            return false;
    
            return true;
        }
    </script>
    
    <script type="text/javascript">
        $('#Update-Company-Form').on('submit', function(event) {
            
            event.preventDefault();
            var Action = 'UpdateCompany';
                            
            var form_data = new FormData(this);
            form_data.append('Action',Action);
                            
            $("#flash").show();
            $("#flash i").addClass('fa-spinner');
            $("#flash i").addClass('fa-spin');
            $("#flash").removeClass('hidden');
            $('html,body').animate({ scrollTop: $(".content").offset().top},'slow');
            $("#flash span").html('Please Wait...');
            
            $.ajax({
                url: 'includes/company_script.php',
                type: 'POST',
                data: form_data,
                contentType: false,
                cache: false,
                processData:false,
                success: function(result)
                {
                    //alert(result);
                    var obj = JSON.parse(result);
                    var Status = obj.Status;
                        
                    if(Status == '4')
                    {
                        $("#flash").removeClass('alert alert-danger');
                        $("#flash").addClass('alert alert-success');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-times-circle');
                        $("#flash i").addClass('fa fa-fw fa-check-circle');
                        $("#flash span").html('Company Update Successfully.');
                        $('#flash').delay(2000).fadeOut(500);
                        setTimeout(function() {
                          window.location.href = "company.php";
                        }, 1000);
                        return true;
                    }
                    else if(Status == '3')
                    {
                        $("#flash").removeClass('alert alert-success');
                        $("#flash").addClass('alert alert-danger');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash i").addClass('fa fa-fw fa-times-circle');
                        $("#flash span").html('Company Update Not Successfully.');
                        $('#flash').delay(3000).fadeOut(500);
                        return true;
                    }
                    else
                    {
                        $("#flash").removeClass('alert alert-success');
                        $("#flash").addClass('alert alert-danger');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash i").addClass('fa fa-fw fa-times-circle');
                        $("#flash span").html(Status);
                        $('#flash').delay(4000).fadeOut(500);
                        return true;
                    }
                }
            });
        });
    </script>
        
        
    </body>
</html>
<?php
ob_flush();
?>