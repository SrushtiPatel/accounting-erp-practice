<?php
ob_start();
$Page = "ViewAllTransaction"; 
require_once('head.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Xenon ERP</title>
    
    <!-- Favicon -->
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="assets/plugins/bootstrap/dist/css/bootstrap.css" rel="stylesheet" />

    <!-- Animate.css Css -->
    <link href="assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Font Awesome Css -->
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

    <!-- iCheck Css -->
    <link href="assets/plugins/iCheck/skins/flat/_all.css" rel="stylesheet" />

    <!-- Switchery Css -->
    <link href="assets/plugins/switchery/dist/switchery.css" rel="stylesheet" />

    <!-- Metis Menu Css -->
    <link href="assets/plugins/metisMenu/dist/metisMenu.css" rel="stylesheet" />

    <!-- Pace Loader Css -->
    <link href="assets/plugins/pace/themes/white/pace-theme-flash.css" rel="stylesheet" />
	
  	<!-- Bootstrap Select Css -->
    <link href="assets/plugins/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" />

   	<!-- DateTimePicker Css -->
    <link href="assets/plugins/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="assets/css/style.css" rel="stylesheet" />
    
</head>
<body class="ls-fixed navbar-fixed">
    <div class="all-content-wrapper">
        <!-- Top Bar -->
        <header>
            <?php include_once('header.php'); ?>
            <?php
            if(isset($_GET['banking_id']))
            {
                $BankingID = $_GET['banking_id'];
                $SelectBanking = "SELECT * FROM banking_master WHERE banking_id='".$BankingID."'";
                $SelectBankingQuery = mysqli_query($con,$SelectBanking);
                if(!$SelectBankingQuery)
                {
                    //die(mysqli_error($con));
                    header("Location: view_all_transactions.php");
                    exit();
                }
                $count = mysqli_num_rows($SelectBankingQuery);
                if($count != 1)
                {
                    header("Location: view_all_transactions.php");
                    exit();
                }
                
                $Banking = mysqli_fetch_array($SelectBankingQuery);
                
                $BVNo = $Banking['bv_no'];
                $TransactionDate = $Banking['transaction_date'];
                $PayFrom = $Banking['pay_from'];
                $PayTo = $Banking['pay_to'];
                $Amount = $Banking['amount'];
                $Description = $Banking['description'];
				
				if(!empty($Banking['transaction_date']) && $Banking['transaction_date'] != '0000-00-00') { $TransactionDate = date("d-m-Y",strtotime($Banking['transaction_date'])); }
				
            }
            else
            {
                header("Location: view_all_transactions.php");
                exit();
            }
        ?>
        </header>
        <!-- #END# Top Bar -->
        <!-- Left Menu -->
        <aside class="sidebar">
            <?php include_once('menu.php'); ?>
        </aside>
        <!-- #END# Left Menu -->
        <section class="content dashboard">
            <div class="page-heading">
                <h1>
                	<a href="view_all_transactions.php">Transactions</a>
					<small>
						<i class="fa fa-angle-double-right"></i>
						<a class="font-bold" href="update_transaction.php?banking_id=<?php echo $BankingID; ?>">Update Transaction Detail</a>
					</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="dashboard.php">Home</a></li>
                    <li><a href="view_all_transactions.php">Transaction</a></li>
                </ol>
            </div>
            <div class="page-body">
            <!-- -------------- ERROR SECTION START -------------- -->
			<div id="flash" class="alert alert hidden">
				<strong>
					<i class="fa fa-spinner fa-spin"></i>
				</strong>
				&nbsp; &nbsp;
				<span></span>
			</div>
			<!-- -------------- ERROR SECTION END -------------- -->
            	<div class="row clearfix">
                    <!-- Horizontal Layout  -->
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">Update Transaction Detail</div>
                            <div class="panel-body p-b-25">
                                <form id="Update-Transaction-Form" method="post" class="form-horizontal" action="#">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <input type="hidden" id="AdminID" name="AdminID" value="<?php echo $_SESSION['AdminID']; ?>" required />
                                            <input type="hidden" id="CompanyID" name="CompanyID" value="<?php echo $CurrentCompanyID; ?>" required />
                                            <input type="hidden" id="BankingID" name="BankingID" value="<?php echo $BankingID; ?>" required />
                                            <input type="hidden" id="OldPayFrom" name="OldPayFrom" value="<?php echo $PayFrom; ?>" required />
                                            <input type="hidden" id="OldPayTo" name="OldPayTo" value="<?php echo $PayTo; ?>" required />
                                            <input type="hidden" id="OldAmount" name="OldAmount" value="<?php echo $Amount; ?>" required />
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Voucher No</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="VoucherNo" name="VoucherNo" value="<?php echo $BVNo; ?>" class="form-control" required readonly />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Transaction Date</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="TransactionDate" name="TransactionDate" value="<?php echo $TransactionDate; ?>" placeholder="" data-format="DD-MM-YYYY" class="form-control js-dtp" required />
                                        			<span style="width: 50px; " class="glyphicon glyphicon-calendar form-control-feedback"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Pay From</label>
                                                <div class="col-sm-6">
                                                    <select class="col-xs-10 col-md-8 selectpicker form-control show-tick" id="PayFrom" name="PayFrom">
                                                        <option value="-1">-- SELECT --</option>
                                                        <?php
                                                            $SelectDebitBankAccount = mysqli_query($con,"SELECT * FROM ledger_master WHERE group_id IN('20','21') AND company_id='".$CurrentCompanyID."'  ORDER BY group_id");
															while($DebitBankAccount = mysqli_fetch_array($SelectDebitBankAccount))
                                                            {
																if($PayFrom == $DebitBankAccount['ledger_id'])
																{
																	echo '<option value="'.$DebitBankAccount['ledger_id'].'" selected>'.$DebitBankAccount['ledger_name'].'</option>';
																}
																else
																{
																	echo '<option value="'.$DebitBankAccount['ledger_id'].'">'.$DebitBankAccount['ledger_name'].'</option>';
																}
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Pay To</label>
                                                <div class="col-sm-6">
                                                    <select class="col-xs-10 col-md-8 selectpicker form-control show-tick" id="PayTo" name="PayTo">
                                                        <option value="-1">-- SELECT --</option>
                                                        <?php
                                                            $SelectCreditBankAccount = mysqli_query($con,"SELECT * FROM ledger_master WHERE group_id IN('20','21') AND company_id='".$CurrentCompanyID."'  ORDER BY group_id");
															while($CreditBankAccount = mysqli_fetch_array($SelectCreditBankAccount))
                                                            {
																if($PayTo == $CreditBankAccount['ledger_id'])
																{
																	echo '<option value="'.$CreditBankAccount['ledger_id'].'" selected>'.$CreditBankAccount['ledger_name'].'</option>';
																}
                                                                else
																{
																	echo '<option value="'.$CreditBankAccount['ledger_id'].'">'.$CreditBankAccount['ledger_name'].'</option>';
																}
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Amount</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="Amount" name="Amount" value="<?php echo $Amount; ?>" onKeyPress="return NuMValidation(event);" class="form-control" required />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Description</label>
                                                <div class="col-sm-6">
                                                    <textarea id="Description" name="Description" class="form-control no-resize" rows="4" /><?php echo $Description; ?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                               	<label class="col-sm-5 control-label"></label>
                                                <div class="col-sm-6">
                                                    <button type="submit" id="Submit" name="Submit" class="btn btn-success">
                                                        <i class="fa fa-refresh bigger-110"></i>
                                                        Update
                                                    </button>
                                                </div>
                                            </div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>

    <!-- Jquery Core Js -->
    <script src="assets/plugins/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Pace Loader Js -->
    <script src="assets/plugins/pace/pace.js"></script>

    <!-- Screenfull Js -->
    <script src="assets/plugins/screenfull/src/screenfull.js"></script>

    <!-- Metis Menu Js -->
    <script src="assets/plugins/metisMenu/dist/metisMenu.js"></script>

    <!-- Jquery Slimscroll Js -->
    <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   	
	<!-- Switchery Js -->
    <script src="assets/plugins/switchery/dist/switchery.js"></script>
    
    <!-- iCheck Js -->
    <script src="assets/plugins/iCheck/icheck.js"></script>

    <!-- Autosize Js (Textarea auto growth plugin) -->
    <script src="assets/plugins/autosize/dist/autosize.js"></script>

    <!-- MomentJs Js -->
    <script src="assets/plugins/moment/moment.js"></script>

    <!-- DateTimePicker Js -->
    <script src="assets/plugins/eonasdan-bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js"></script>

    <!-- Bootstrap Select Js -->
    <script src="assets/plugins/bootstrap-select/dist/js/bootstrap-select.js"></script>

    <!-- Custom Js -->
    <script src="assets/js/admin.js"></script>
    <script>
        function NuMValidation(evt)
        {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
            return false;
    
            return true;
        } 
    </script>
    <script type="text/javascript">
        jQuery(function ($) 
        {
            'use strict';
            $(document).ready(function () {
                //Init datetimepicker
                $('.js-dtp').each(function (i, key) {
                    var format = $(key).data('format');
                    $(key).datetimepicker({
                        format: format,
                        showClear: true
                    });
                });
            });
        });

    </script>
    <script type="text/javascript">
        $('#Update-Transaction-Form').on('submit', function(event) {
            
            event.preventDefault();
			
            var Action = 'UpdateTransaction';
			
			var PayTo = $("#PayTo").val();
			var PayFrom = $("#PayFrom").val();
			
			if(PayFrom < 0)
			{
				alert("Please Select Any Debit Bank Accounts");
				$("#PayFrom").focus();
                return false;
			}
			
			if(PayTo < 0)
            {
				alert("Please Select Any Credit Bank Accounts.");
                $("#PayTo").focus();
                return false;
            }
            
			if(PayFrom == PayTo)
			{
				alert("Please Select Differnt Bank Accounts");
                return false;
			}
			
			
            var form_data = new FormData(this);
            form_data.append('Action',Action);
            
			$("#Submit").attr('disabled',true);
            $("#flash").show();
            $("#flash i").addClass('fa-spinner');
            $("#flash i").addClass('fa-spin');
            $("#flash").removeClass('hidden');
            $('html,body').animate({ scrollTop: $(".content").offset().top},'slow');
            $("#flash span").html('Please Wait...');
           	//return false;
            $.ajax({
                url: 'includes/transaction_script.php',
                type: 'POST',
                data: form_data,
                contentType: false,
                cache: false,
                processData:false,
                success: function(result)
                {
                    //alert(result);
                    var obj = JSON.parse(result);
                    var Status = obj.Status;
                    
                    if(Status == '4')
                    {
						$("#Submit").attr('disabled',false);
                        $("#flash").removeClass('alert alert-danger');
                        $("#flash").addClass('alert alert-success');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-times-circle');
                        $("#flash i").addClass('fa fa-fw fa-check-circle');
                        $("#flash span").html('Transaction Update Successfully.');
                        document.getElementById("Update-Transaction-Form").reset();
                        $('#flash').delay(3000).fadeOut(500);
                        setTimeout(function() {
                          window.location.href = "update_transaction.php?banking_id=<?php echo $BankingID; ?>";
                        }, 1000);
                        return true;
                    }
                    else if(Status == '3')
                    {
						$("#Submit").attr('disabled',false);
                        $("#flash").removeClass('alert alert-success');
                        $("#flash").addClass('alert alert-danger');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash i").addClass('fa fa-fw fa-times-circle');
                        $("#flash span").html('Transaction Update Not Successfully.');
                        $('#flash').delay(3000).fadeOut(500);
                        return true;
                    }
                    else if(Status == '0')
                    {
						$("#Submit").attr('disabled',false);
                        $("#flash").removeClass('alert alert-success');
                        $("#flash").addClass('alert alert-danger');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash i").addClass('fa fa-fw fa-times-circle');
                        $("#flash span").html('Transaction Already Exists.');
                        $('#flash').delay(3000).fadeOut(500);
                        return true;
                    }
					else if(Status == '00')
                    {
						$("#Submit").attr('disabled',false);
                        $("#flash").removeClass('alert alert-success');
                        $("#flash").addClass('alert alert-danger');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash i").addClass('fa fa-fw fa-times-circle');
                        $("#flash span").html('Transaction Not Available.');
                        $('#flash').delay(3000).fadeOut(500);
                        return true;
                    }
                    else
                    {
						$("#Submit").attr('disabled',false);
                        $("#flash").removeClass('alert alert-success');
                        $("#flash").addClass('alert alert-danger');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash i").addClass('fa fa-fw fa-times-circle');
                        $("#flash span").html(result);
                        $('#flash').delay(3000).fadeOut(500);
                        return true;
                    }
                }
            });
        });
    </script>
    
</body>
</html>
<?php
ob_flush();
?>