<?php
session_start();
if(!isset($_SESSION['AdminID']))
{
	header("Location: index.php");
	exit();
}
if($_COOKIE['LoginStatus']==0)
{
	header("Location: log-off.php");
	exit();
}
require_once('includes/dbconfig.php');
require_once('includes/functions.php');
require_once('includes/objects.php');
?>