<?php
ob_start();
$Page = "Group"; 
require_once('head.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Xenon ERP</title>
    
    <!-- Favicon -->
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="assets/plugins/bootstrap/dist/css/bootstrap.css" rel="stylesheet" />

    <!-- Bootstrap DateRangePicker Css -->
    <link href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />

    <!-- Animate.css Css -->
    <link href="assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Font Awesome Css -->
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

    <!-- iCheck Css -->
    <link href="assets/plugins/iCheck/skins/flat/_all.css" rel="stylesheet" />

    <!-- Switchery Css -->
    <link href="assets/plugins/switchery/dist/switchery.css" rel="stylesheet" />

    <!-- Metis Menu Css -->
    <link href="assets/plugins/metisMenu/dist/metisMenu.css" rel="stylesheet" />

    <!-- Pace Loader Css -->
    <link href="assets/plugins/pace/themes/white/pace-theme-flash.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="assets/css/style.css" rel="stylesheet" />
    
</head>
<body class="ls-fixed navbar-fixed">
    <div class="all-content-wrapper">
        <!-- Top Bar -->
        <header>
            <?php include_once('header.php'); ?>
        </header>
        <?php
            if(isset($_GET['group_id']))
            {
                $GroupID = $_GET['group_id'];
                $SelectGroup = "SELECT * FROM group_master WHERE group_id='".$GroupID."'";
                $SelectGroupQuery = mysqli_query($con,$SelectGroup);
                if(!$SelectGroupQuery)
                {
                    //die(mysqli_error($con));
                    header("Location: view_all_group.php");
                    exit();
                }
                $count = mysqli_num_rows($SelectGroupQuery);
                if($count != 1)
                {
                    header("Location: view_all_group.php");
                    exit();
                }
                
                $Group = mysqli_fetch_array($SelectGroupQuery);
                
                $GroupID = $Group['group_id'];
                $GroupName = $Group['group_name'];
                $ParentID = $Group['parent_id'];

                $GroupDetail = $groupObject->selectGroupDetail($ParentID);
                $GroupDetail = json_decode($GroupDetail,true);
                $PGroupName = $GroupDetail['GroupName'];
                
                $AddedBy = '';
                $AddedDate = '';
                $ModifiedBy = '';
                $ModifiedDate = '';

                if(!empty($Group['addedby']))
                {
                    $AddedBy = $profileObject->selectUserName($Group['addedby']);
                    $AddedDate = '( '.date("h:i:s A d-m-Y",strtotime($Group['addeddate'])).' )';
                }
                if(!empty($Group['modifiedby']))
                {
                    $ModifiedBy = $profileObject->selectUserName($Group['modifiedby']);
                    $ModifiedDate = '( '.date("h:i:s A d-m-Y",strtotime($Group['modifieddate'])).' )';
                }
            }
            else
            {
                header("Location: view_all_group.php");
                exit();
            }
        ?>
        <!-- #END# Top Bar -->
        <!-- Left Menu -->
        <aside class="sidebar">
            <?php include_once('menu.php'); ?>
        </aside>
        <!-- #END# Left Menu -->
        <section class="content">
            <div class="page-heading">
                <h1>Group Details</h1>
                <ol class="breadcrumb">
                    <li><a href="dashboard.php">Home</a></li>
                    <li><a href="view_all_group.php">Group</a></li>
                    <li class="active">Group Details</li>
                </ol>
            </div>
            
            <div class="page-body">
                <!-- -------------- ERROR SECTION START -------------- -->
                        
                <div id="flash" class="alert alert hidden">
                    <strong>
                        <i class="ace-icon fa fa-spinner"></i>
                    </strong>
                    &nbsp; &nbsp;
                    <span></span>
                </div>
                
                <!-- -------------- ERROR SECTION END -------------- -->
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">Group Details</div>
                            <div class="panel-body">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <th>Group Name</th>
                                            <td><?php echo $GroupName; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Parent Group</th>
                                            <td><?php echo $PGroupName; ?></td>
                                        </tr>
                                        <tr>
                                            <th>AddedBy</th>
                                            <td><?php echo $AddedBy; ?></td>
                                        </tr>
                                        <tr>
                                            <th>AddedDate</th>
                                            <td><?php echo $AddedDate; ?></td>
                                        </tr>
                                        <tr>
                                            <th>ModifiedBy</th>
                                            <td><?php echo $ModifiedBy; ?></td>
                                        </tr>
                                        <tr>
                                            <th>ModifiedDate</th>
                                            <td><?php echo $ModifiedDate; ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="form-group">
                                    <div class="text-center">
                                        <a class="btn btn-sm btn-success" href="update_group.php?group_id=<?php echo $GroupID; ?>">
                                            <i class="fa fa-refresh bigger-110"></i>
                                            Update Group Detail
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Footer -->
        
        <!-- #END# Footer -->
    </div>

    <!-- Jquery Core Js -->
    <script src="assets/plugins/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Bootstrap DateRangePicker Js -->
    <script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Pace Loader Js -->
    <script src="assets/plugins/pace/pace.js"></script>

    <!-- Screenfull Js -->
    <script src="assets/plugins/screenfull/src/screenfull.js"></script>

    <!-- Metis Menu Js -->
    <script src="assets/plugins/metisMenu/dist/metisMenu.js"></script>

    <!-- Jquery Slimscroll Js -->
    <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    

    <!-- Custom Js -->
    <script src="assets/js/admin.js"></script>
    

    <script type="text/javascript">
        jQuery(function($) {
                                            
            $('.js-daterange-picker').daterangepicker({
                format: 'dd-mm-yyyy',
                autoclose:true
            });
            
        });
    </script>
    
    
</body>
</html>
<?php
ob_flush();
?>