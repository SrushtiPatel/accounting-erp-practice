<?php
ob_start();
$Page = "ViewAdvanceReceipt"; 
require_once('head.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Xenon ERP</title>
    
    <!-- Favicon -->
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="assets/plugins/bootstrap/dist/css/bootstrap.css" rel="stylesheet" />

    <!-- Animate.css Css -->
    <link href="assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Font Awesome Css -->
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

    <!-- iCheck Css -->
    <link href="assets/plugins/iCheck/skins/flat/_all.css" rel="stylesheet" />

    <!-- Switchery Css -->
    <link href="assets/plugins/switchery/dist/switchery.css" rel="stylesheet" />

    <!-- Metis Menu Css -->
    <link href="assets/plugins/metisMenu/dist/metisMenu.css" rel="stylesheet" />

    <!-- Pace Loader Css -->
    <link href="assets/plugins/pace/themes/white/pace-theme-flash.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="assets/css/style.css" rel="stylesheet" />

    <!-- Jquery Datatables Css -->
    <link href="assets/plugins/DataTables/media/css/dataTables.bootstrap.css" rel="stylesheet" />
    
</head>
<body class="ls-fixed navbar-fixed">
    <div class="all-content-wrapper">
        <!-- Top Bar -->
        <header>
            <?php include_once('header.php'); ?>
        </header>
        <!-- #END# Top Bar -->
        <!-- Left Menu -->
        <aside class="sidebar">
            <?php include_once('menu.php'); ?>
        </aside>
        <!-- #END# Left Menu -->
        <section class="content dashboard">
             <div class="page-heading">
                <h1>
                    <a href="view_all_advance_receipt.php">
                        Advance Receipt
                    </a>
                    <small>
                        <i class="fa fa-angle-double-right"></i>
                        <a class="font-bold" href="add_advance_receipt.php">Add New Advance Receipt</a>
                    </small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="dashboard.php">Home</a></li>
                    <li><a href="view_all_advance_receipt.php">Advance Receipt</a></li>
                </ol>
            </div>

            <div class="page-body">
            	<!-- -------------- ERROR SECTION START -------------- -->
                        
	            <div id="flash" class="alert alert hidden">
	                <strong>
	                    <i class="fa fa-spinner fa-spin"></i>
	                </strong>
	                &nbsp; &nbsp;
	                <span></span>
	            </div>
            
            	<!-- -------------- ERROR SECTION END -------------- -->
                <div class="panel panel-default">
                    <div class="panel-heading">Advance Receipt</div>
                    <div class="panel-body">
                        <table id="AdvanceReceipt-Table" class="table table-striped table-hover js-basic-example dataTable">
                            <thead>
                                <tr>
                                    <th>#</th>
									<th>Receipt No</th>
									<th>Receipt Date</th>
									<th>Received From</th>
									<th>Receipt Mode</th>
									<th>Total Amount</th>
									<th></th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                    $SelectAdvanceReceipt = mysqli_query($con,"SELECT * FROM receipt_vouchers WHERE rv_type='2' AND rv_date>='".$FYearStart."' AND rv_date<='".$FYearEnd."' AND company_id='".$CurrentCompanyID."'");
                                   	if(!$SelectAdvanceReceipt)
                                    {
                                        die(mysqli_error($con));
                                    }
                                    $count = 1;
                                    while($AdvanceReceipt = mysqli_fetch_array($SelectAdvanceReceipt))
                                    {
                                        $RVID = $AdvanceReceipt['rv_id'];
										$RVNo = $AdvanceReceipt['rv_no'];
										$RVDate = $AdvanceReceipt['rv_date'];
										$ReceivedFrom = $AdvanceReceipt['received_from'];
										$ReceiptMode = $AdvanceReceipt['receipt_mode'];
										$ReceivedTo = $AdvanceReceipt['received_to'];
										$TotalAmount = $AdvanceReceipt['total_amount'];
										$TotalTDSAmount = $AdvanceReceipt['total_tds_amount'];
										
										$ClientDetail = $ledgerObject->selectClientDetail($ReceivedFrom);
							            $ClientDetail = json_decode($ClientDetail,true);
							            $ClientName = $ClientDetail['ClientName'];
                                        if(!empty($RVDate)) { $RVDate = date("d-m-Y",strtotime($AdvanceReceipt['rv_date'])); }
                                    ?>
                                
                                    <tr>
                                        <td class="center"><?php echo $count; $count++; ?></td>
                                        <td><?php echo $RVNo;?></td>
                                        <td><?php echo $RVDate;?></td>
                                        <td><?php echo $ClientName;?></td>
                                        <td><?php echo $ReceiptMode;?></td>
                                        <td><?php echo $TotalAmount;?></td>
                                        <td>
											<div class="action-buttons">
<!--                                                <a id="InvoicePrint" href="print_invoice.php?invoice_id=<?php echo $RVID; ?>" class="col-primary" title="Print"><i class="fa fa-print"></i></a>-->
                                                <a href="update_advance_receipt.php?rv_id=<?php echo $RVID; ?>" class="col-success" title="Edit"><i class="fa fa-pencil-square-o"></i></a>
                                                <a data-toggle="modal" href="#myModel_Remove_Receipt_<?php echo $RVID; ?>" class="col-danger" title="Remove"><i class="fa fa-trash-o"></i></a>
                                            </div>
<!-- ------------------------------ Remove Model Start ------------------------------ -->
    <div class="modal fade" id="myModel_Remove_Receipt_<?php echo $RVID; ?>" tabindex="-1" role="dialog" aria-labelledby="myModelLable" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="Remove-Receipt-<?php echo $RVID; ?>-Form" class="form-horizontal" method="post" action="#">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModelLable">Remove This Receipt ?</h4>
                        <input type="hidden" id="RVID" name="RVID" value="<?php echo $RVID; ?>" required />
                        <input type="hidden" id="ReceivedTo" name="ReceivedTo" value="<?php echo $ReceivedTo; ?>" required />
                        <input type="hidden" id="ReceivedFrom" name="ReceivedFrom" value="<?php echo $ReceivedFrom; ?>" required />
                        <input type="hidden" id="TotalAmount" name="TotalAmount" value="<?php echo $TotalAmount; ?>" required />
                        <input type="hidden" id="TotalTDSAmount" name="TotalTDSAmount" value="<?php echo $TotalTDSAmount; ?>" required />
                        <br/>
                    </div>
                    <div class="modal-footer">
                        <button class="m-w-150 btn btn-danger" type="submit" id="Remove-Receipt-<?php echo $RVID; ?>" onClick="return RemoveReceipt(this.id);">
                            <i class="fa fa-trash bigger-110"></i>
                            Remove
                        </button>
                        <button type="button" class="m-w-150 btn btn-default" data-dismiss="modal">
                            <i class="fa fa-remove bigger-110"></i>
                            Close
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

 <!-- ------------------------------ Remove Model End ------------------------------ -->
                                        </td>
                                    </tr>
                                <?php
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
             </div>
        </section>
        <!-- Footer -->
       
        <!-- #END# Footer -->
    </div>

    <!-- Jquery Core Js -->
    <script src="assets/plugins/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Pace Loader Js -->
    <script src="assets/plugins/pace/pace.js"></script>

    <!-- Screenfull Js -->
    <script src="assets/plugins/screenfull/src/screenfull.js"></script>

    <!-- Metis Menu Js -->
    <script src="assets/plugins/metisMenu/dist/metisMenu.js"></script>

    <!-- Jquery Slimscroll Js -->
    <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- JQuery Datatables Js -->
    <script src="assets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
    <script src="assets/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>

    <!-- Custom Js -->
    <script src="assets/js/admin.js"></script>
    <script src="assets/js/pages/tables/jquery-datatables.js"></script>
    <script src="assets/js/pages/ui/modals.js"></script>

    <script type="text/javascript">
        function RemoveReceipt(btnId) {
            
            var form_id = btnId+'-Form'; 
        
            $('#'+form_id).on('submit', function(event) {
            
            event.preventDefault();
            
            var RVID = $('#'+form_id+' #RVID').val();
            var TotalAmount = $('#'+form_id+' #TotalAmount').val();
			var TotalTDSAmount = $('#'+form_id+' #TotalTDSAmount').val();
			var ReceivedTo = $('#'+form_id+' #ReceivedTo').val();
			var ReceivedFrom = $('#'+form_id+' #ReceivedFrom').val();
            	
            var Action = 'RemoveAdvanceReceipt';
            
            var form_data = 'Action='+ Action +'&RVID='+ RVID +'&TotalAmount='+ TotalAmount +'&TotalTDSAmount='+ TotalTDSAmount +'&ReceivedTo='+ ReceivedTo +'&ReceivedFrom='+ ReceivedFrom;
            
           	$("#flash").show();
			$("#flash i").addClass('fa-spinner');
			$("#flash i").addClass('fa-spin');
			$("#flash").removeClass('hidden');
			$('html,body').animate({ scrollTop: $(".content").offset().top},'slow');
			$("#flash span").html('Please Wait...');
           // alert(form_data);
            $.ajax({
                type: 'POST',
                url: 'includes/receipt_script.php',
                data: form_data,
                cache: false,
                success: function(result)
                {
                    //alert(result);
                    var obj = JSON.parse(result);
                    var Status = obj.Status;
                    
                    if(Status == '6')
                    {
                        $('.modal').fadeOut();
                        $("#flash").removeClass('alert alert-danger');
                        $("#flash").addClass('alert alert-success');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-times-circle');
                        $("#flash i").addClass('fa fa-fw fa-check-circle');
                        $("#flash span").html('Receipt Detail Remove Successfully.');
                        $("#flash").delay(2000).fadeOut(500);
                        setTimeout(function() {
                          window.location.href = "view_all_advance_receipt.php";
                        }, 1000);
                        return true;
                    }
                    else if(Status == '5')
                    {
                        $('.modal').fadeOut();
                        $("#flash").removeClass('alert alert-success');
                        $("#flash").addClass('alert alert-danger');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash i").addClass('fa fa-fw fa-times-circle');
                        $("#flash span").html('Receipt Detail Remove Not Successfully.');
                        $("#flash").delay(3000).fadeOut(500);
                        return true;
                    }
                    else if(Status == '00')
                    {
                        $('.modal').fadeOut();
                        $("#flash").removeClass('alert alert-success');
                        $("#flash").addClass('alert alert-danger');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash i").addClass('fa fa-fw fa-times-circle');
                        $("#flash span").html('Cannot Delete Receipt, There is a Reference Available For This Receipt.');
                        $("#flash").delay(3000).fadeOut(500);
                        return true;
                    }
                    else
                    {
                        $("#flash").removeClass('alert alert-success');
                        $("#flash").addClass('alert alert-danger');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash i").addClass('fa fa-fw fa-times-circle');
                        $("#flash span").html(Status);
                        $('#flash').delay(4000).fadeOut(500);
                        return true;
                    }
                }
            });
            
            });
        }
    </script>
        
        
    </body>
</html>
<?php
ob_flush();
?>