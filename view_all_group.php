<?php
ob_start();
$Page = "ViewAllGroup"; 
require_once('head.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Xenon ERP</title>
    
    <!-- Favicon -->
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="assets/plugins/bootstrap/dist/css/bootstrap.css" rel="stylesheet" />

    <!-- Animate.css Css -->
    <link href="assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Font Awesome Css -->
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

    <!-- iCheck Css -->
    <link href="assets/plugins/iCheck/skins/flat/_all.css" rel="stylesheet" />

    <!-- Switchery Css -->
    <link href="assets/plugins/switchery/dist/switchery.css" rel="stylesheet" />

    <!-- Metis Menu Css -->
    <link href="assets/plugins/metisMenu/dist/metisMenu.css" rel="stylesheet" />

    <!-- Pace Loader Css -->
    <link href="assets/plugins/pace/themes/white/pace-theme-flash.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="assets/css/style.css" rel="stylesheet" />

    <!-- Jquery Datatables Css -->
    <link href="assets/plugins/DataTables/media/css/dataTables.bootstrap.css" rel="stylesheet" />
    
</head>
<body class="ls-fixed navbar-fixed">
    <div class="all-content-wrapper">
        <!-- Top Bar -->
        <header>
            <?php include_once('header.php'); ?>
        </header>
        <!-- #END# Top Bar -->
        <!-- Left Menu -->
        <aside class="sidebar">
            <?php include_once('menu.php'); ?>
        </aside>
        <!-- #END# Left Menu -->
        <section class="content dashboard">
             <div class="page-heading">
                <h1>
                    <a href="view_all_group.php">
                        Group
                    </a>
                    <small>
                        <i class="fa fa-angle-double-right"></i>
                        <a class="font-bold" href="add_group.php">Add New Group</a>
                    </small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="dashboard.php">Home</a></li>
                    <li><a href="view_all_group.php">Group</a></li>
                </ol>
            </div>

            <div class="page-body">
                <!-- -------------- ERROR SECTION START -------------- -->
                        
                <div id="flash" class="alert alert hidden">
                    <strong>
                        <i class="fa fa-spinner fa-spin"></i>
                    </strong>
                    &nbsp; &nbsp;
                    <span></span>
                </div>
                
                <!-- -------------- ERROR SECTION END -------------- -->

                <div class="panel panel-default">
                    <div class="panel-heading">Group</div>
                    <div class="panel-body">
                        <table id="Group-Table" class="table table-striped table-hover js-basic-example dataTable">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Group Name</th>
                                    <th>Parent Group</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $SelectGroup = "SELECT * FROM group_master";
                                    $SelectGroupQuery = mysqli_query($con,$SelectGroup);
                                    if(!$SelectGroupQuery)
                                    {
                                        die(mysqli_error($con));
                                    }
                                    $count = 1;
                                    while($Group = mysqli_fetch_array($SelectGroupQuery))
                                    {
                                        $GroupID = $Group['group_id'];
                                        $GroupName = $Group['group_name'];
                                        $ParentID = $Group['parent_id'];
                                        $Visible = $Group['visible'];
                                        $Edit = $Group['edit'];
                                        $Remove = $Group['remove'];

                                        $GroupDetail = $groupObject->selectGroupDetail($ParentID);
                                        $GroupDetail = json_decode($GroupDetail,true);
                                        $PGroupName = $GroupDetail['GroupName'];
                                    ?>
                                
                                    <tr>
                                        <td class="center"><?php echo $count; $count++; ?></td>
                                        <td><?php echo $GroupName;?></td>
                                        <td><?php echo $PGroupName;?></td>
                                        <td>
                                            <div class="action-buttons">
                                                <?php
                                                    if ($Edit == '1') 
                                                    {
                                                        echo '<a href="update_group.php?group_id= '.$GroupID.'" class="col-success" title="Edit"><i class="fa fa-pencil-square-o""></i></a>';
                                                    }
                                                ?>
                                                <?php
                                                    if ($Remove == '1') 
                                                    {
                                                        echo '<a data-toggle="modal" href="#myModel_Remove_Group_'.$GroupID.'" class="col-danger" title="Remove"><i class="fa fa-trash-o"></i></a>';
                                                    }
                                                ?>
                                            </div>
<!-- ------------------------------ Remove Model Start ------------------------------ -->
    <div class="modal fade" id="myModel_Remove_Group_<?php echo $GroupID; ?>" tabindex="-1" role="dialog" aria-labelledby="myModelLable" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="Remove-Group-<?php echo $GroupID; ?>-Form" class="form-horizontal" method="post" action="#">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModelLable">Remove This Group ?</h4>
                        <input type="hidden" id="GroupID" name="GroupID" value="<?php echo $GroupID; ?>" required />
                        <br/>
                    </div>
                    <div class="modal-footer">
                        <button class="m-w-150 btn btn-danger" type="submit" id="Remove-Group-<?php echo $GroupID; ?>" onClick="return RemoveGroup(this.id);">
                            <i class="fa fa-trash bigger-110"></i>
                            Remove
                        </button>
                        <button type="button" class="m-w-150 btn btn-default" data-dismiss="modal">
                            <i class="fa fa-remove bigger-110"></i>
                            Close
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>


 <!-- ------------------------------ Remove Model End ------------------------------ -->
                                        </td>
                                    </tr>
                                <?php
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
             </div>
        </section>
        <!-- Footer -->
       
        <!-- #END# Footer -->
    </div>

    <!-- Jquery Core Js -->
    <script src="assets/plugins/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Pace Loader Js -->
    <script src="assets/plugins/pace/pace.js"></script>

    <!-- Screenfull Js -->
    <script src="assets/plugins/screenfull/src/screenfull.js"></script>

    <!-- Metis Menu Js -->
    <script src="assets/plugins/metisMenu/dist/metisMenu.js"></script>

    <!-- Jquery Slimscroll Js -->
    <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- JQuery Datatables Js -->
    <script src="assets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
    <script src="assets/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>

    <!-- Custom Js -->
    <script src="assets/js/admin.js"></script>
    <script src="assets/js/pages/tables/jquery-datatables.js"></script>
    <script src="assets/js/pages/ui/modals.js"></script>

    <script type="text/javascript">
		function RemoveGroup(btnId) {

			var form_id = btnId+'-Form'; 

			$('#'+form_id).on('submit', function(event) {

			event.preventDefault();

			var GroupID = $('#'+form_id+' #GroupID').val();

			var Action = 'RemoveGroup';

			var form_data = 'Action='+ Action +'&GroupID='+ GroupID;

			$("#flash").show();
            $("#flash i").addClass('fa-spinner');
            $("#flash i").addClass('fa-spin');
            $("#flash").removeClass('hidden');
            $('html,body').animate({ scrollTop: $(".content").offset().top},'slow');
            $("#flash span").html('Please Wait...');
			//alert(form_data);
			$.ajax({
				type: 'POST',
				url: 'includes/group_script.php',
				data: form_data,
				cache: false,
				success: function(result)
				{
					//alert(result);
					var obj = JSON.parse(result);
					var Status = obj.Status;

					if(Status == '6')
					{
						$('.modal').fadeOut();
						$("#flash").removeClass('alert alert-danger');
						$("#flash").addClass('alert alert-success');
						$("#flash i").removeClass('fa-spinner');
						$("#flash i").removeClass('fa-spin');
						$("#flash i").removeClass('fa fa-fw fa-times-circle');
						$("#flash i").addClass('fa fa-fw fa-check-circle');
						$("#flash span").html('Group Detail Remove Successfully.');
						$("#flash").delay(2000).fadeOut(500);
						setTimeout(function() {
						  window.location.href = "view_all_group.php";
						}, 1000);
						return true;
					}
					else if(Status == '5')
					{
						$('.modal').fadeOut();
						$("#flash").removeClass('alert alert-success');
						$("#flash").addClass('alert alert-danger');
						$("#flash i").removeClass('fa-spinner');
						$("#flash i").removeClass('fa-spin');
						$("#flash i").removeClass('fa fa-fw fa-check-circle');
						$("#flash i").addClass('fa fa-fw fa-times-circle');
						$("#flash span").html('Group Detail Remove Not Successfully.');
						$("#flash").delay(3000).fadeOut(500);
						return true;
					}
					else if(Status == '00')
					{
						$('.modal').fadeOut();
						$("#flash").removeClass('alert alert-success');
						$("#flash").addClass('alert alert-danger');
						$("#flash i").removeClass('fa-spinner');
						$("#flash i").removeClass('fa-spin');
						$("#flash i").removeClass('fa fa-fw fa-check-circle');
						$("#flash i").addClass('fa fa-fw fa-times-circle');
						$("#flash span").html('Cannot Delete Group, There is a Reference Available For This Group.');
						$("#flash").delay(3000).fadeOut(500);
						return true;
					}
					else
					{
						$("#flash").removeClass('alert alert-success');
						$("#flash").addClass('alert alert-danger');
						$("#flash i").removeClass('fa-spinner');
						$("#flash i").removeClass('fa-spin');
						$("#flash i").removeClass('fa fa-fw fa-check-circle');
						$("#flash i").addClass('fa fa-fw fa-times-circle');
						$("#flash span").html(Status);
						$('#flash').delay(4000).fadeOut(500);
						return true;
					}
				}
			});

			});
		}
	</script>
        
        
    </body>
</html>
<?php
ob_flush();
?>