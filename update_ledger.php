<?php
ob_start();
$Page = "Ledger"; 
require_once('head.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Xenon ERP</title>
    
    <!-- Favicon -->
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="assets/plugins/bootstrap/dist/css/bootstrap.css" rel="stylesheet" />

    <!-- Animate.css Css -->
    <link href="assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Font Awesome Css -->
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

    <!-- iCheck Css Libraries | You can choose a theme from plugins/iCheck/skins instead of get all themes -->
    <link href="assets/plugins/iCheck/skins/minimal/_all.css" rel="stylesheet" />
    <link href="assets/plugins/iCheck/skins/square/_all.css" rel="stylesheet" />
    <link href="assets/plugins/iCheck/skins/flat/_all.css" rel="stylesheet" />

    <!-- Switchery Css -->
    <link href="assets/plugins/switchery/dist/switchery.css" rel="stylesheet" />

    <!-- Metis Menu Css -->
    <link href="assets/plugins/metisMenu/dist/metisMenu.css" rel="stylesheet" />

    <!-- Pace Loader Css -->
    <link href="assets/plugins/pace/themes/white/pace-theme-flash.css" rel="stylesheet" />

    <!-- Bootstrap Select Css -->
    <link href="assets/plugins/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" />

    <!-- DateTimePicker Css -->
    <link href="assets/plugins/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="assets/css/style.css" rel="stylesheet" />

</head>
<body class="ls-fixed navbar-fixed">
    <div class="all-content-wrapper">
        <!-- Top Bar -->
        <header>
            <?php include_once('header.php'); ?>
        </header>
        <?php
            if(isset($_GET['ledger_id']))
            {
                $LedgerID = $_GET['ledger_id'];
                $SelectLedger = "SELECT * FROM ledger_master WHERE ledger_id='".$LedgerID."'";
                $SelectLedgerQuery = mysqli_query($con,$SelectLedger);
                if(!$SelectLedgerQuery)
                {
                    //die(mysqli_error($con));
                    header("Location: view_all_ledger.php");
                    exit();
                }
                $count = mysqli_num_rows($SelectLedgerQuery);
                if($count != 1)
                {
                    header("Location: view_all_ledger.php");
                    exit();
                }
                
                $Ledger = mysqli_fetch_array($SelectLedgerQuery);
                
                $LedgerID = $Ledger['ledger_id'];
				$CompanyID = $Ledger['company_id'];
                $LedgerName = $Ledger['ledger_name'];
                $GroupID = $Ledger['group_id'];
                $LedgerAlias = $Ledger['ledger_alias'];
                $OpeningBalance = $Ledger['opening_balance'];
                $OldCurrentBalance = $Ledger['current_balance'];
                $OldOpeningBalance = $Ledger['opening_balance'];

                $GroupDetail = $groupObject->selectGroupDetail($GroupID);
                $GroupDetail = json_decode($GroupDetail,true);
                $GroupName = $GroupDetail['GroupName'];
                
            }
            else
            {
                header("Location: view_all_ledger.php");
                exit();
            }
        ?>
        <!-- #END# Top Bar -->
        <!-- Left Menu -->
        <aside class="sidebar">
            <?php include_once('menu.php'); ?>
        </aside>
        <!-- #END# Left Menu -->
        <section class="content">
            <div class="page-heading">
                <h1>
                    <a href="view_all_ledger.php">
                        Ledger
                    </a>
                    <small>
                        <i class="fa fa-angle-double-right"></i>
                        <a class="font-bold" href="update_ledger.php?ledger_id=<?php echo $LedgerID; ?>">Update Ledger Detail</a>
                    </small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="dashboard.php">Home</a></li>
                    <li><a href="view_all_ledger.php">Ledger</a></li>
                </ol>
            </div>
            
            <div class="page-body">
                <!-- -------------- ERROR SECTION START -------------- -->
                        
                <div id="flash" class="alert alert hidden">
                    <strong>
                        <i class="fa fa-spinner fa-spin"></i>
                    </strong>
                    &nbsp; &nbsp;
                    <span></span>
                </div>
                
                <!-- -------------- ERROR SECTION END -------------- -->
                <div class="row clearfix">
                    <!-- Horizontal Layout  -->
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">Update Ledger</div>
                            <div class="panel-body p-b-25">
                                <form id="Update-Ledger-Form" method="post" class="form-horizontal" action="#">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <input type="hidden" id="AdminID" name="AdminID" value="<?php echo $_SESSION['AdminID']; ?>" required />
                                        <input type="hidden" id="LedgerID" name="LedgerID" value="<?php echo $LedgerID; ?>" required />
                                        <input type="hidden" id="CompanyID" name="CompanyID" value="<?php echo $CompanyID; ?>" required />
                                        <div class="form-group" style="width:100%;">
                                            <label class="col-sm-5 control-label" style="width:44%;">Ledger Name</label>
                                            <div class="col-sm-6" style="width:54%;">
                                                <input type="text" id="LedgerName" name="LedgerName" value="<?php echo $LedgerName; ?>" class="col-xs-10 col-md-8 form-control" required />
                                            </div>
                                        </div>
                                        
                                        <div class="form-group" style="width:100%;">
                                            <label class="col-sm-5 control-label" style="width:44%;">Ledger Alias</label>
                                            <div class="col-sm-6" style="width:54%;">
                                                <input type="text" id="LedgerAlias" name="LedgerAlias" value="<?php echo $LedgerAlias; ?>" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="form-group" style="width:100%;">
                                            <label class="col-sm-5 control-label" style="width:44%;">Parent Group</label>
                                            <div class="col-sm-6" style="width:54%;">
                                                <input type="hidden" id=GroupID name=GroupID value="<?php echo $GroupID; ?>" required />
                                                <input type="text" id="GroupName" name="GroupName" value="<?php echo $GroupName; ?>" class="form-control" readonly required />
                                            </div>
                                        </div>
                                        
                                        <div class="form-group" style="width:100%;">
                                            <label class="col-sm-5 control-label" style="width:44%;">Opening Balance</label>
                                            <div class="col-sm-6" style="width:54%;">
                                                <input type="hidden" id="OldOpeningBalance" name="OldOpeningBalance" value="<?php echo $OldOpeningBalance; ?>" class="form-control" required />
                                                <input type="text" id="OpeningBalance" name="OpeningBalance" value="<?php echo $OpeningBalance; ?>" class="form-control" onKeyPress="return NuMValidation2(event);" required />
                                                <input type="hidden" id="OldCurrentBalance" name="OldCurrentBalance" value="<?php echo $OldCurrentBalance; ?>" class="form-control" required />
                                            </div>
                                        </div>
                                        <div id="CustomFields">
                                        <?php
                                        	$SelectLCFItem = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$LedgerID."'");
                                            
                                           	while($LCFItem = mysqli_fetch_array($SelectLCFItem))
                                            {
												$LIID = $LCFItem['li_id'];
                                           		$LCFID = $LCFItem['lcf_id'];
                                                $Value = $LCFItem['li_value'];

                                                $LCFDetail = $ledgerObject->selectLedgerItemDetail($LCFID);
                                                $LCFDetail = json_decode($LCFDetail,true);
                                                $LCFName = $LCFDetail['LCFName'];
                                                $LCFType = $LCFDetail['LCFType'];
                                                $LCFIsRequired = $LCFDetail['LCFIsRequired'];
                                                $LCFPlaceholder = $LCFDetail['LCFPlaceholder'];
                                                $LCFValues = $LCFDetail['LCFValues'];

                                                if ($LCFItem>0) 
                                                {
                                                    echo $CustomField = $ledgerObject->getcustomerFieldsForLedger($LCFID,$LCFName,$LCFType,$LCFIsRequired,$LCFPlaceholder,$LCFValues,$LIID,$Value);
                                                }
                                            }
                                        ?>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="form-group">
                                            <div class="col-sm-offset-5 col-sm-6">
                                                <button type="submit" class="btn btn-success">
                                                    <i class="fa fa-refresh bigger-110"></i>
                                                    Update
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- #END# Horizontal Layout  -->
                </div>
            </div>
        </section>
        <!-- Footer -->
       
        <!-- #END# Footer -->
    </div>
        
    <!-- Jquery Core Js -->
    <script src="assets/plugins/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Pace Loader Js -->
    <script src="assets/plugins/pace/pace.js"></script>

    <!-- Screenfull Js -->
    <script src="assets/plugins/screenfull/src/screenfull.js"></script>

    <!-- Metis Menu Js -->
    <script src="assets/plugins/metisMenu/dist/metisMenu.js"></script>

    <!-- Jquery Slimscroll Js -->
    <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Switchery Js -->
    <script src="assets/plugins/switchery/dist/switchery.js"></script>

    <!-- iCheck Js -->
    <script src="assets/plugins/iCheck/icheck.js"></script>

    <!-- Autosize Js (Textarea auto growth plugin) -->
    <script src="assets/plugins/autosize/dist/autosize.js"></script>

    <!-- MomentJs Js -->
    <script src="assets/plugins/moment/moment.js"></script>

    <!-- DateTimePicker Js -->
    <script src="assets/plugins/eonasdan-bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js"></script>

    <!-- Bootstrap Select Js -->
    <script src="assets/plugins/bootstrap-select/dist/js/bootstrap-select.js"></script>

    <!-- Custom Js -->
    <script src="assets/js/admin.js"></script>

    <script>
        function NuMValidation(evt)
        {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
            return false;
    
            return true;
        } 
	   
	   	function NuMValidation2(evt)
		{
			var charCode = (evt.which) ? evt.which : evt.keyCode;
			if (charCode != 46 && charCode != 43 && charCode != 45 && charCode > 31 
			&& (charCode < 48 || charCode > 57))
			return false;

			return true;
		}
		
        function BasivElement(evt)
        {
            //Init switch button
                var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
                elems.forEach(function (e) {
                    var size = $(e).data('size');
                    var options = {};
                    options['color'] = '#009688';
                    if (size !== undefined) options['size'] = size;

                    var switchery = new Switchery(e, options);
                });

                //Init datetimepicker
                $('.js-dtp').each(function (i, key) {
                    var format = $(key).data('format');
                    $(key).datetimepicker({
                        format: format,
                        showClear: true
                    });
                });
        }

    </script>
    <script type="text/javascript">

        jQuery(function ($) 
        {
            'use strict';
            $(document).ready(function () {
                //Init switch button
                var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
                elems.forEach(function (e) {
                    var size = $(e).data('size');
                    var options = {};
                    options['color'] = '#009688';
                    if (size !== undefined) options['size'] = size;

                    var switchery = new Switchery(e, options);
                });

                //Init datetimepicker
                $('.js-dtp').each(function (i, key) {
                    var format = $(key).data('format');
                    $(key).datetimepicker({
                        format: format,
                        showClear: true
                    });
                });
            });
        });

    </script>

    <script type="text/javascript">
                    
        function fetchData()
        {
            var GroupID = $("#GroupID").val();

            if(GroupID < 0)
            {
                $("#CustomFields").html('');
                $("#GroupID").focus();
                return false;
            }
            
            else
            { 
                var Action = 'GetCustomField';
                var dataString = 'Action='+ Action +'&GroupID='+ GroupID;
                
                $.ajax({
                    type: "POST",
                    url: "includes/loader_functions.php",
                    data: dataString,
                    cache: false,
                    success: function(result)
                    {
                        //alert(result);
                        $("#CustomFields").html(result);
                        BasivElement();
                        return true;
                    }
                });
            }
        }       

    </script>
    
   <script type="text/javascript">
        $('#Update-Ledger-Form').on('submit', function(event) {
            
            event.preventDefault();
            
			var GroupID = $("#GroupID").val();
			
			if(GroupID < 1)
			{
				alert("Please Select Any Group.");
				return false;
			}
			   
            var Action = 'UpdateLedger';
                           
            var form_data = new FormData(this);
            form_data.append('Action',Action); 
                
            $("#flash").show();
            $("#flash i").addClass('fa-spinner');
            $("#flash i").addClass('fa-spin');
            $("#flash").removeClass('hidden');
            $('html,body').animate({ scrollTop: $(".content").offset().top},'slow');
            $("#flash span").html('Please Wait...');
                
            $.ajax({
                url: 'includes/ledger_script.php',
                type: 'POST',
                data: form_data,
                contentType: false,
                cache: false,
                processData:false,
                success: function(result)
                {
                    //alert(result);
                    var obj = JSON.parse(result);
                    var Status = obj.Status;
                        
                    if(Status == '4')
                    {
                        $("#flash").removeClass('alert alert-danger');
                        $("#flash").addClass('alert alert-success');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-times-circle');
                        $("#flash i").addClass('fa fa-fw fa-check-circle');
                        $("#flash span").html('Ledger Update Successfully.');
                        document.getElementById("Update-Ledger-Form").reset();
                        $('#flash').delay(3000).fadeOut(500);
                        setTimeout(function() {
                          window.location.href = "update_ledger.php?ledger_id=<?php echo $LedgerID; ?>";
                        }, 1000);
                        return true;                            
                    }
                    else if(Status == '3')
                    {
                        $("#flash").removeClass('alert alert-success');
                        $("#flash").addClass('alert alert-danger');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash i").addClass('fa fa-fw fa-times-circle');
                        $("#flash span").html('Ledger Update Not Successfully.');
                        $('#flash').delay(3000).fadeOut(500);
                        return true;
                    }
                    else if(Status == '0')
                    {
                        $("#flash").removeClass('alert alert-success');
                        $("#flash").addClass('alert alert-danger');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash i").addClass('fa fa-fw fa-times-circle');
                        $("#flash span").html('Ledger Already Exist.');
                        $('#flash').delay(4000).fadeOut(500);
                        return true;
                    }
                    else if(Status == '00')
                    {
                        $("#flash").removeClass('alert alert-success');
                        $("#flash").addClass('alert alert-danger');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash i").addClass('fa fa-fw fa-times-circle');
                        $("#flash span").html('Cannot Update Ledger, There is a Reference Available For This Ledger.');
                        $('#flash').delay(4000).fadeOut(500);
                        return true;
                    }
                    else
                    {
                        $("#flash").removeClass('alert alert-success');
                        $("#flash").addClass('alert alert-danger');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash i").addClass('fa fa-fw fa-times-circle');
                        $("#flash span").html(result);
                        $('#flash').delay(4000).fadeOut(500);
                        return true;
                    }
                }
            });
        });
    </script>
        
    </body>
</html>
<?php
ob_flush();
?>