<nav class="sidebar-nav">
	<ul class="metismenu">
		<li class="<?php if($Page == "Dashboard") { echo 'active'; } ?> hover">
			<a href="dashboard.php">
				<i class="menu-icon fa fa-tachometer"></i>
				<span class="nav-label">Dashboard</span>
			</a>
		</li>
		<?php
			$SelectAdmin = mysqli_query($con,"SELECT license FROM admin_master WHERE admin_id='".$_SESSION['AdminID']."'");
			$Admin = mysqli_fetch_array($SelectAdmin);
			$License = $Admin['license'];
			if($License > 0)
			{
		?>
		<li class="<?php if($Page == "Company") { echo 'active'; } ?> hover">
			<a href="companies.php">
				<i class="menu-icon fa fa-th"></i>
				<span class="nav-label">Company</span>
			</a>
		</li>
		<?php } ?>
		<li class="<?php if($Page == "Item") { echo 'active'; } ?> hover">
			<a href="item.php">
				<i class="menu-icon fa fa-th"></i>
				<span class="nav-label">Item</span>
			</a>
		</li>
		<li class="<?php if($Page == 'Group' || $Page == 'AddGroup' || $Page == 'ViewAllGroup') { echo 'active open'; } ?>">
			<a href="javascript:void(0);" class="menu-toggle">
				<i class="menu-icon fa fa-group"></i>
				<span class="nav-label">Group</span>
			</a>
			<ul>
				<li class="<?php if($Page == 'AddGroup') { echo 'active'; } ?>">
					<a href="add_group.php">
						Add New Group
					</a>
				</li>
				<li class="<?php if($Page == 'ViewAllGroup') { echo 'active'; } ?>">
					<a href="view_all_group.php">
						View All Group
					</a>
				</li>
			</ul>
		</li>
		<li class="<?php if($Page == 'Ledger' || $Page == 'AddLedger' || $Page == 'ViewAllLedger') { echo 'active open'; } ?>">
			<a href="javascript:void(0);" class="menu-toggle">
				<i class="menu-icon fa fa-book"></i>
				<span class="nav-label">Ledger</span>
			</a>
			<ul>
				<li class="<?php if($Page == 'AddLedger') { echo 'active'; } ?>">
					<a href="add_ledger.php">
						Add New Ledger
					</a>
				</li>
				<li class="<?php if($Page == 'ViewAllLedger') { echo 'active'; } ?>">
					<a href="view_all_ledger.php">
						View All Ledger
					</a>
				</li>
			</ul>
		</li>
		<li class="<?php if($Page == 'Client' || $Page == 'AddClient' || $Page == 'ViewAllClient') { echo 'active open'; } ?>">
			<a href="javascript:void(0);" class="menu-toggle">
				<i class="menu-icon fa fa-user-o"></i>
				<span class="nav-label">Client</span>
			</a>
			<ul>
				<li class="<?php if($Page == 'AddClient') { echo 'active'; } ?>">
					<a href="add_client.php">
						Add New Client
					</a>
				</li>
				<li class="<?php if($Page == 'ViewAllClient') { echo 'active'; } ?>">
					<a href="view_all_client.php">
						View All Client
					</a>
				</li>
			</ul>
		</li>
		<li class="<?php if($Page == 'Vendor' || $Page == 'AddVendor' || $Page == 'ViewAllVendor') { echo 'active open'; } ?>">
			<a href="javascript:void(0);" class="menu-toggle">
				<i class="menu-icon fa fa-user-o"></i>
				<span class="nav-label">Vendor</span>
			</a>
			<ul>
				<li class="<?php if($Page == 'AddVendor') { echo 'active'; } ?>">
					<a href="add_vendor.php">
						Add New Vendor
					</a>
				</li>
				<li class="<?php if($Page == 'ViewAllVendor') { echo 'active'; } ?>">
					<a href="view_all_vendor.php">
						View All Vendor
					</a>
				</li>
			</ul>
		</li>
		<li class="<?php if($Page == 'DutiesTaxes' || $Page == 'AddTaxes' || $Page == 'ViewAllTaxes') { echo 'active open'; } ?>">
			<a href="javascript:void(0);" class="menu-toggle">
				<i class="menu-icon fa fa-percent"></i>
				<span class="nav-label">Duties & Taxes</span>
			</a>
			<ul>
				<li class="<?php if($Page == 'AddTaxes') { echo 'active'; } ?>">
					<a href="add_taxes.php">
						Add New Taxes
					</a>
				</li>
				<li class="<?php if($Page == 'ViewAllTaxes') { echo 'active'; } ?>">
					<a href="view_all_taxes.php">
						View All Taxes
					</a>
				</li>
			</ul>
		</li>
		<li class="<?php if($Page == 'Purchase' || $Page == 'AgainstPayment' || $Page == 'AdvancePayment' || $Page == 'OtherPayment' || $Page == 'AddPurchase' || $Page == 'ViewAllPurchase' || $Page == 'AddAgainstPayment' || $Page == 'ViewAgainstPayment' || $Page == 'AddAdvancePayment' || $Page == 'ViewAdvancePayment' || $Page == 'AddOtherPayment' || $Page == 'ViewOtherPayment') { echo 'active open'; } ?>">
			<a href="javascript:void(0);" class="menu-toggle">
				<i class="menu-icon fa fa-credit-card"></i>
				<span class="nav-label">Purchase</span>
			</a>
			<ul>
				<li class="<?php if($Page == 'AddPurchase' || $Page == 'ViewAllPurchase') { echo 'active open'; } ?>">
					<a href="javascript:void(0);" class="menu-toggle">
						<span class="">Purchase</span>
					</a>
					<ul>
						<li class="<?php if($Page == 'AddPurchase') { echo 'active'; } ?>">
							<a href="add_purchase.php">
								Add New Purchase
							</a>
						</li>
						<li class="<?php if($Page == 'ViewAllPurchase') { echo 'active'; } ?>">
							<a href="view_all_purchase.php">
								View All Purchase
							</a>
						</li>
					</ul>
				</li>
				<li class="<?php if($Page == 'AgainstPayment' || $Page == 'AddAgainstPayment' || $Page == 'ViewAgainstPayment') { echo 'active open'; } ?>">
					<a href="javascript:void(0);" class="menu-toggle">
						<span class="">Payment Against Invoice</span>
					</a>
					<ul>
						<li class="<?php if($Page == 'AddAgainstPayment') { echo 'active'; } ?>">
							<a href="add_payment_against_invoice.php">
								Add New Payment
							</a>
						</li>
						<li class="<?php if($Page == 'ViewAgainstPayment') { echo 'active'; } ?>">
							<a href="view_all_against_payment.php">
								View All Payment 
							</a>
						</li>
					</ul>
				</li>
				<li class="<?php if($Page == 'AdvancePayment' || $Page == 'AddAdvancePayment' || $Page == 'ViewAdvancePayment') { echo 'active open'; } ?>">
					<a href="javascript:void(0);" class="menu-toggle">
						<span class="">Advance Payment</span>
					</a>
					<ul>
						<li class="<?php if($Page == 'AddAdvancePayment') { echo 'active'; } ?>">
							<a href="add_advance_payment.php">
								Add New Payment
							</a>
						</li>
						<li class="<?php if($Page == 'ViewAdvancePayment') { echo 'active'; } ?>">
							<a href="view_all_advance_payment.php">
								View All Payment
							</a>
						</li>
					</ul>
				</li>
				<li class="<?php if($Page == 'OtherPayment' || $Page == 'AddOtherPayment' || $Page == 'ViewOtherPayment') { echo 'active open'; } ?>">
					<a href="javascript:void(0);" class="menu-toggle">
						<span class="">Other Expense</span>
					</a>
					<ul>
						<li class="<?php if($Page == 'AddOtherPayment') { echo 'active'; } ?>">
							<a href="add_other_expense.php">
								Add New Expense
							</a>
						</li>
						<li class="<?php if($Page == 'ViewOtherPayment') { echo 'active'; } ?>">
							<a href="view_all_other_expense.php">
								View All Expense
							</a>
						</li>
					</ul>
				</li>
			</ul>
		</li>
		<li class="<?php if($Page == 'Invoice' || $Page == 'AddInvoice' || $Page == 'ViewAllInvoice' || $Page == 'AddAgainstReceipt' || $Page == 'ViewAgainstReceipt' || $Page == 'AddAdvanceReceipt' || $Page == 'ViewAdvanceReceipt' || $Page == 'AddOtherReceipt' || $Page == 'ViewOtherReceipt') { echo 'active open'; } ?>">
			<a href="javascript:void(0);" class="menu-toggle">
				<i class="menu-icon fa fa-money"></i>
				<span class="nav-label">Sales</span>
			</a>
			<ul>
				<li class="<?php if($Page == 'AddInvoice' || $Page == 'ViewAllInvoice') { echo 'active open'; } ?>">
					<a href="javascript:void(0);" class="menu-toggle">
						<span class="">Invoice</span>
					</a>
					<ul>
						<li class="<?php if($Page == 'AddInvoice') { echo 'active'; } ?>">
							<a href="add_invoice.php">
								Add New Invoice
							</a>
						</li>
						<li class="<?php if($Page == 'ViewAllInvoice') { echo 'active'; } ?>">
							<a href="view_all_invoice.php">
								View All Invoice
							</a>
						</li>
					</ul>
				</li>
				<li class="<?php if($Page == 'AddAgainstReceipt' || $Page == 'ViewAgainstReceipt') { echo 'active open'; } ?>">
					<a href="javascript:void(0);" class="menu-toggle">
						<span class="">Receipt Against Invoice</span>
					</a>
					<ul>
						<li class="<?php if($Page == 'AddAgainstReceipt') { echo 'active'; } ?>">
							<a href="add_receipt_against_invoice.php">
								Add New Receipt
							</a>
						</li>
						<li class="<?php if($Page == 'ViewAgainstReceipt') { echo 'active'; } ?>">
							<a href="view_all_against_receipt.php">
								View All Receipt
							</a>
						</li>
					</ul>
				</li>
				<li class="<?php if($Page == 'AddAdvanceReceipt' || $Page == 'ViewAdvanceReceipt') { echo 'active open'; } ?>">
					<a href="javascript:void(0);" class="menu-toggle">
						<span class="">Advance Receipt</span>
					</a>
					<ul>
						<li class="<?php if($Page == 'AddAdvanceReceipt') { echo 'active'; } ?>">
							<a href="add_advance_receipt.php">
								Add New Receipt
							</a>
						</li>
						<li class="<?php if($Page == 'ViewAdvanceReceipt') { echo 'active'; } ?>">
							<a href="view_all_advance_receipt.php">
								View All Receipt
							</a>
						</li>
					</ul>
				</li>
				<li class="<?php if($Page == 'AddOtherReceipt' || $Page == 'ViewOtherReceipt') { echo 'active open'; } ?>">
					<a href="javascript:void(0);" class="menu-toggle">
						<span class="">Other Income</span>
					</a>
					<ul>
						<li class="<?php if($Page == 'AddOtherReceipt') { echo 'active'; } ?>">
							<a href="add_other_income.php">
								Add New Income
							</a>
						</li>
						<li class="<?php if($Page == 'ViewOtherReceipt') { echo 'active'; } ?>">
							<a href="view_all_other_income.php">
								View All Income
							</a>
						</li>
					</ul>
				</li>
			</ul>
		</li>
		<li class="<?php if($Page == 'Transaction' || $Page == 'AddTransaction' || $Page == 'ViewAllTransaction') { echo 'active open'; } ?>">
			<a href="javascript:void(0);" class="menu-toggle">
				<i class="menu-icon fa fa-building-o"></i>
				<span class="nav-label">Banking</span>
			</a>
			<ul>
				<li class="<?php if($Page == 'AddTransaction') { echo 'active'; } ?>">
					<a href="add_transaction.php">
						Add New Transaction
					</a>
				</li>
				<li class="<?php if($Page == 'ViewAllTransaction') { echo 'active'; } ?>">
					<a href="view_all_transactions.php">
						View All Transactions
					</a>
				</li>
			</ul>
		</li>
		<li class="<?php if($Page == 'Stock' || $Page == 'AddSO' || $Page == 'ViewAllSO') { echo 'active open'; } ?>">
			<a href="javascript:void(0);" class="menu-toggle">
				<i class="menu-icon fa fa-sign-out"></i>
				<span class="nav-label">Stock Outward</span>
			</a>
			<ul>
				<li class="<?php if($Page == 'AddSO') { echo 'active'; } ?>">
					<a href="add_so.php">
						Add New Stock Outward
					</a>
				</li>
				<li class="<?php if($Page == 'ViewAllSO') { echo 'active'; } ?>">
					<a href="view_all_so.php">
						View All Stock Outward
					</a>
				</li>
			</ul>
		</li>
		<li class="<?php if($Page == 'Report' || $Page == 'SalesRegister' || $Page == 'PurchaseRegister'  || $Page == 'StockReport') { echo 'active open'; } ?>">
			<a href="javascript:void(0);" class="menu-toggle">
				<i class="menu-icon fa fa-line-chart"></i>
				<span class="nav-label">Report</span>
			</a>
			<ul>
				<li class="<?php if($Page == 'SalesRegister') { echo 'active'; } ?>">
					<a href="sales_register.php">
						Sales Register
					</a>
				</li>
				<li class="<?php if($Page == 'PurchaseRegister') { echo 'active'; } ?>">
					<a href="purchase_register.php">
						Purchase Register
					</a>
				</li>
				<li class="<?php if($Page == 'LedgerWiseReport' || $Page == 'VendorWiseReport' || $Page == 'ClientWiseReport' || $Page == 'CashBankTansaction' || $Page == 'TaxReport') { echo 'active open'; } ?>">
					<a href="javascript:void(0);" class="menu-toggle">
						<span class="">Ledger Wise Report</span>
					</a>
					<ul>
						<li class="<?php if($Page == 'VendorWiseReport') { echo 'active'; } ?>">
							<a href="vendor_wise_report.php">
								Vendor Wise Report
							</a>
						</li>
						<li class="<?php if($Page == 'ClientWiseReport') { echo 'active'; } ?>">
							<a href="client_wise_report.php">
								Client Wise Report
							</a>
						</li>
						<li class="<?php if($Page == 'CashBankTansaction') { echo 'active'; } ?>">
							<a href="cash_bank_transaction.php">
								Cash/Bank Transaction Report
							</a>
						</li>
						<li class="<?php if($Page == 'TaxReport') { echo 'active'; } ?>">
							<a href="tax_report.php">
								Tax Report
							</a>
						</li>
					</ul>
				</li>
			</ul>
		</li>
	</ul>
</nav>