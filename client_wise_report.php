<?php
ob_start();
$Page = "ClientWiseReport"; 
require_once('head.php');
$CompanyID = '';
$ClientID = '';
$Date = '';
$StartDate = ''; 
$EndDate = ''; 
$StartDate1 = ''; 
$EndDate1 = ''; 

if(isset($_GET['CompanyID']))
{
	$CompanyID = $_GET['CompanyID'];
}
if(isset($_GET['ClientID']))
{
	$ClientID = $_GET['ClientID'];
}
if(isset($_GET['Date']))
{
	$Date = $_GET['Date'];
	$Date = explode('-',$Date);
		
	$StartDate = str_replace('/', '-', $Date[0]);
	$EndDate = str_replace('/', '-', $Date[1]);

	$StartDate = date("Y-m-d",strtotime($StartDate)); 
	$EndDate = date("Y-m-d",strtotime($EndDate)); 
	
	$StartDate1 = date("d-m-Y",strtotime($StartDate)); 
	$EndDate1 = date("d-m-Y",strtotime($EndDate));
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Xenon ERP</title>
    
    <!-- Favicon -->
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="assets/plugins/bootstrap/dist/css/bootstrap.css" rel="stylesheet" />

    <!-- Bootstrap Select Css -->
    <link href="assets/plugins/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" />

    <!-- DateTimePicker Css -->
    <link href="assets/plugins/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet" />
	
   	<!-- Bootstrap DateRangePicker Css -->
    <link href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />

    <!-- Animate.css Css -->
    <link href="assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Font Awesome Css -->
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

    
    <!-- iCheck Css Libraries | You can choose a theme from plugins/iCheck/skins instead of get all themes -->
    <link href="assets/plugins/iCheck/skins/minimal/_all.css" rel="stylesheet" />

    <!-- Switchery Css -->
    <link href="assets/plugins/switchery/dist/switchery.css" rel="stylesheet" />

    <!-- Metis Menu Css -->
    <link href="assets/plugins/metisMenu/dist/metisMenu.css" rel="stylesheet" />

    <!-- Pace Loader Css -->
    <link href="assets/plugins/pace/themes/white/pace-theme-flash.css" rel="stylesheet" />
    
    <!-- Custom Css -->
    <link href="assets/css/style.css" rel="stylesheet" />
    
    <style type="text/css">
		#PrintReportDiv {
			/*border:2px solid #000;
			border-radius:10px;*/
		}
		.PrintReportTable {
			border-collapse:collapse;
			/*text-transform:uppercase;*/
		}
		.PrintReportTable tr td, .PrintReportTable tr th {
			font-family:Arial, Helvetica, sans-serif;
		}
		.b_l 
		{
			border-left:none !important;
		}
		.b_r 
		{
			border-right:none !important;
		}
		.b_b 
		{
			border-bottom:none !important;
		}
		.b_t 
		{
			border-top:none !important;
		}
	</style>
	<style type="text/css" media="print">
		@media print {		
			@page {
				/*size: A4 landscape;*/
				margin:0.25cm;
			}
			table tr td, table tr th {
				font-family:Arial, Helvetica, sans-serif;
				font-size:10px;
				padding:3px 4px;				
			}
			.PrintReportTable {
				border-collapse:collapse;
				border:1px solid #000 !important;
				/*page-break-inside:auto;*/
			}
			.PrintReportTable tr td table {
				min-width: 50%;
				border-collapse:collapse;
				border:1px solid #000 !important;
				/*page-break-inside:auto;*/
			}
			.PrintReportTable tr td table tr td {
				border-collapse:collapse;
			}
			.PrintReportTable tr {
				/*page-break-inside:auto;*/
			}
			.PrintReportTable tr td, .PrintReportTable tr th {
				font-family:Arial, Helvetica, sans-serif;
				font-size:10px;
				padding:3px 4px;
			}
			.page-break { display:block; page-break-before:always; }
			.hidden { display:none !important; }
			.hidden-print { display:none !important; }
			
			.b_l 
			{
				border-left:none !important;
			}
			.b_r 
			{
				border-right:none !important;
			}
			.b_b 
			{
				border-bottom:none !important;
			}
			.b_t 
			{
				border-top:none !important;
			}
		}
	</style>
    
</head>
<body class="ls-toggled">
    <div class="all-content-wrapper">
        <!-- Top Bar -->
        <header>
            <?php include_once('header.php'); ?>
        </header>
        <!-- #END# Top Bar -->
        <!-- Left Menu -->
        <aside class="sidebar">
            <?php include_once('menu.php'); ?>
        </aside>
        <!-- #END# Left Menu -->
        <section class="content dashboard">
            <div class="page-heading">
                <h1>
                    <a href="client_wise_report.php">
                        Client Wise Report
                    </a>
                </h1>
            </div>
            <div class="page-body">
             	<div class="row clearfix">
                    <div class="col-xs-12 col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Client Wise Report</div>
                            <div class="panel-body p-b-25">
                                <form id="#" method="get" class="form-horizontal" action="client_wise_report.php">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <input type="hidden" id="CompanyID" name="CompanyID" value="<?php echo $CurrentCompanyID; ?>" required />
											<div class="form-group">
												<label class="col-sm-3 col-md-2 control-label">Client</label>
												<div class="col-sm-4 col-md-3">
													<select class="col-xs-10 col-md-6 selectpicker form-control show-tick" id="ClientID" name="ClientID">
														<option value="-1">-- SELECT --</option>
														<?php
															$SelectClient = mysqli_query($con,"SELECT * FROM ledger_master WHERE group_id='33' AND company_id='".$CurrentCompanyID."' ORDER BY group_id");
															while($Client = mysqli_fetch_array($SelectClient))
															{
																if($ClientID == $Client['ledger_id'])
																{
																	echo '<option value="'.$Client['ledger_id'].'" selected>'.$Client['ledger_name'].'</option>';
																}
																else
																{
																	echo '<option value="'.$Client['ledger_id'].'">'.$Client['ledger_name'].'</option>';	
																}
																
															}
														?>
													</select>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-3 col-md-2 control-label">Financial Year</label>
												<div class="col-sm-4 col-md-3">
													<input type="text" class="form-control js-daterange-picker" id="Date" name="Date" value="<?php if(!empty($StartDate1)) { echo $StartDate1.' - '.$EndDate1; } ?>" required />
													<span style="width: 50px;" class="glyphicon glyphicon-calendar form-control-feedback"></span>
												</div>
											</div>
                                       		<div class="form-group">
                                                <div class="col-sm-offset-3 col-sm-4 col-md-offset-2 col-md-3">
                                                    <button type="submit" class="btn btn-success">
                                                        <i class="fa fa-check bigger-110"></i>
                                                        Submit
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
				<br />
				<?php
				if($CompanyID>0 && $ClientID>0 && !empty($StartDate) && !empty($EndDate))
				{
					
					$ClientDetail = $ledgerObject->selectClientDetail($ClientID);
					$ClientDetail = json_decode($ClientDetail,true);
					$ClientName = $ClientDetail['ClientName'];
					
					$OldInvoiceAmount = 0;
					$OldReceivedAmount = 0;
					
					$SelectOldInvoiceAmount = mysqli_query($con,"SELECT SUM(grand_total) FROM invoice_master WHERE client_id='".$ClientID."' AND invoice_date < '".$StartDate."' AND company_id='".$CompanyID."'");
					$OldInvoiceAmountArr = mysqli_fetch_array($SelectOldInvoiceAmount);
					$OldInvoiceAmount = $OldInvoiceAmountArr['SUM(grand_total)'];
					if(empty($OldInvoiceAmount)) { $OldInvoiceAmount = 0; }
					
					$SelectOldReceivedAmount = mysqli_query($con,"SELECT SUM(total_amount), SUM(total_tds_amount) FROM receipt_vouchers WHERE received_from='".$ClientID."' AND rv_date < '".$StartDate."' AND rv_type='1' AND company_id='".$CompanyID."'");
					$OldReceivedAmountArr = mysqli_fetch_array($SelectOldReceivedAmount);
					$OldTotalAmount = $OldReceivedAmountArr['SUM(total_amount)'];
					$OldTotalTDSAmount = $OldReceivedAmountArr['SUM(total_tds_amount)'];
					if(empty($OldTotalAmount)) { $OldTotalAmount = 0; }
					if(empty($OldTotalTDSAmount)) { $OldTotalTDSAmount = 0; }
					$OldReceivedAmount = $OldTotalAmount + $OldTotalTDSAmount;
												 
				?>
                <div class="row clearfix">
                    <div class="col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Ledger Wise Report Of : <?php echo $ClientName; ?>  [<?php echo $StartDate1.' To '.$EndDate1; ?>]</div>
                            <div class="panel-body p-b-25">
                            	 <div class="row clearfix">
                    				<div class="col-xs-12"> 
                      					<button id="btnPrint" class="btn btn-raised btn-primary">
											<i class="ace-icon fa fa-print bigger-125"></i>
											PRINT
										</button>
                     					<a id="btnXLS" onClick="javascript:fnExcelReport();" class="btn btn-raised btn-success">
											<i class="ace-icon fa fa-file-excel-o bigger-125"></i>
											EXCEL
										</a>
                      				
                      					<div id="PrintReport" style="margin-top:20px;">
											<div id="PrintReportDiv">
												<table width="100%" class="table">
													<tr>
														<th colspan="2" class="b_b b_l b_r b_t" style="text-align: center;font-size: 14px;">
															Ledger Wise Report Of &nbsp; : <?php echo '&nbsp;'.$ClientName; ?>
														</th>
													</tr>	
													<tr>
														<th class="b_b b_l b_r b_t" style="text-align: left">
														 Date : <?php echo ' '.$StartDate1.' '.'To'.' '.$EndDate1.' '?>
														</th>
														<th class="b_b b_l b_r b_t" style="text-align: right">Report Generated On : <?php echo date("d-m-Y"); ?></th>
													</tr>		
												</table> 
												<table id="ReportTable" class="PrintReportTable table table-striped table-bordered table-hover" width="100%" border="1" style="margin-bottom:0px;">
													<thead>
														<tr>
															<th width="10%" class="text-center">#</th>
															<th width="15%" class="text-center">Date</th>
															<th width="35%" class="text-center">Detail</th>
															<th width="20%" class="text-center">Total Invoicing</th>
															<th width="20%" class="text-center">Total Recevied</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<th colspan="3" style="text-align:right !important;">Brought Forward</th>
                                                            <th style="text-align:right !important;"><?php echo number_format($OldInvoiceAmount,2); ?></th>
                                                            <th style="text-align:right !important;"><?php echo number_format($OldReceivedAmount,2); ?></th>
														</tr>
														<?php
															$Invoicing = array();
															$Received = array();
															
															$SelectInvoiceDetail = mysqli_query($con,"SELECT * FROM invoice_master WHERE client_id='".$ClientID."' AND invoice_date>='".$StartDate."' AND invoice_date<='".$EndDate."' AND company_id='".$CompanyID."'");
															while($InvoiceDetail = mysqli_fetch_array($SelectInvoiceDetail))
															{
																$InvoiceNo = $InvoiceDetail['invoice_no'];
																$InvoiceDate = $InvoiceDetail['invoice_date'];
																$GrandTotal = $InvoiceDetail['grand_total'];
																$Description = 'Invoice No : '.$InvoiceNo;
																
																$Invoicing[] = array('Date' => $InvoiceDate, 'Description' => $Description, 'Amount' => $GrandTotal, 'Type' => 'Invoice');
															}
															
															$SelectPaymentDetail = mysqli_query($con,"SELECT * FROM receipt_vouchers WHERE received_from='".$ClientID."' AND rv_date>='".$StartDate."' AND rv_date<='".$EndDate."' AND rv_type='1' AND company_id='".$CompanyID."'");
															while($PaymentDetail = mysqli_fetch_array($SelectPaymentDetail))
															{
																$RVNo = $PaymentDetail['rv_no'];
																$RVDate = $PaymentDetail['rv_date'];
																$TotalAmount = $PaymentDetail['total_amount'];
																$TotalTDSAmount = $PaymentDetail['total_tds_amount'];
																$GrandTotal = $TotalAmount + $TotalTDSAmount;
																
																$Description = 'Receipt Voucher No :'.$RVNo;
																$PaymentMethod = $PaymentDetail['receipt_mode'];
																if($PaymentMethod == 'CHEQUE')
																{
																	$Description = 'Bank Name :'.$PaymentDetail['bank_name'].' CHEQUE No :'.$PaymentDetail['cheque_no'];
																}
																if($PaymentMethod == 'CREDIT/DEBIT CARD')
																{
																	$Description = 'Card No :'.$PaymentDetail['card_no'].' Transaction Ref :'.$PaymentDetail['transaction_ref'];
																}
																if($PaymentMethod == 'INTERNET BANKING')
																{
																	$Description = 'Bank Name :'.$PaymentDetail['bank_name'].' Transaction Ref :'.$PaymentDetail['transaction_ref'];
																}
																
																$Received[] = array('Date' => $RVDate, 'Description' => $Description, 'Amount' => $GrandTotal, 'Type' => 'Received');
															}
															//var_dump($Billing);								
															if(empty($Invoicing))
															{
																$Output = $Received;
															}
															else if(empty($Received))
															{
																$Output = $Invoicing;
															}
															else
															{
																$Output = array_merge($Received,$Invoicing);
															}
															//var_dump($Output);
															$length = sizeof($Output);
															$sortArray = array(); 

															if($length > 0)
															{
															foreach($Output as $Output2){ 
																foreach($Output2 as $key=>$value){ 
																	if(!isset($sortArray[$key])){ 
																		$sortArray[$key] = array(); 
																	} 
																	$sortArray[$key][] = $value; 
																} 
															} 

															$orderby = "Date";

															array_multisort($sortArray[$orderby],SORT_ASC,$Output);
															}
															$TotalInvoiceAmount = $OldInvoiceAmount;
															$TotalReceivedAmount = $OldReceivedAmount;

															for($i=0;$i<$length;$i++)
															{

														?>
                                                    	<tr>
                                                        	<td style="text-align:center!important;"><?php echo $i+1; ?></td>
															<td style="text-align:left !important;"><?php echo date("d-m-Y",strtotime($Output[$i]['Date'])); ?></td>
                                                            <td style="text-align:left !important;"><?php echo $Output[$i]['Description']; ?></td>
                                                            <td style="text-align:right !important;">
                                                            <?php
																if($Output[$i]['Type']=='Invoice')
																{
																	echo number_format($Output[$i]['Amount'],2);
																	$TotalInvoiceAmount += $Output[$i]['Amount'];
																}
															?>
                                                            </td>
                                                            <td style="text-align:right !important;">
                                                            <?php
																if($Output[$i]['Type']=='Received')
																{
																	echo number_format($Output[$i]['Amount'],2);
																	$TotalReceivedAmount += $Output[$i]['Amount'];
																}
															?>
                                                            </td>
                                                        </tr>
														<?php
															if((($i+1)%28 == 0 && $i>30) || $i == 22)
															{
															?>
																</tbody>
																</table>
																<div class="page-break"></div>
																<table id="ReportTable" class="PrintReportTable table table-striped table-bordered table-hover" width="100%" border="1" style="margin-bottom:0px;">
																<thead>
																	<tr>
																		<th width="10%" class="text-center">#</th>
																		<th width="15%" class="text-center">Date</th>
																		<th width="35%" class="text-center">Detail</th>
																		<th width="20%" class="text-center">Total Billing</th>
																		<th width="20%" class="text-center">Total Payment</th>
																	</tr>
																</thead>
																<tbody>
															<?php
															}
															}
															$TotalRemainAmount = $TotalInvoiceAmount - $TotalReceivedAmount;
														?>
                                                        <tr>
                                                        	<th colspan="3" style="text-align:right !important;">Total</th>
                                                            <th style="text-align:right !important;"><?php echo number_format($TotalInvoiceAmount,2); ?></th>
                                                            <th style="text-align:right !important;"><?php echo number_format($TotalReceivedAmount,2); ?></th>
                                                        </tr>
                                                        <tr>
                                                        	<th colspan="3" style="text-align:right !important;">Remain Amount</th>
                                                            <th colspan="2" style="text-align:right !important;"><?php echo number_format($TotalRemainAmount,2); ?></th>
                                                        </tr>
													</tbody>
												</table>
											</div>
										</div>
									 </div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php
				}
				?>								
            </div>
        </section>
        <!-- Footer -->
       
        <!-- #END# Footer -->
    </div>

    <!-- Jquery Core Js -->
    <script src="assets/plugins/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
    
    <!-- MomentJs Js -->
    <script src="assets/plugins/moment/moment.js"></script>
    
    <!-- DateTimePicker Js -->
    <script src="assets/plugins/eonasdan-bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js"></script>
    
    <!-- Bootstrap DateRangePicker Js -->
    <script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>


    <!-- Pace Loader Js -->
    <script src="assets/plugins/pace/pace.js"></script>

    <!-- Screenfull Js -->
    <script src="assets/plugins/screenfull/src/screenfull.js"></script>

    <!-- Metis Menu Js -->
    <script src="assets/plugins/metisMenu/dist/metisMenu.js"></script>

    <!-- Jquery Slimscroll Js -->
    <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   	
	<!-- Switchery Js -->
    <script src="assets/plugins/switchery/dist/switchery.js"></script>
    
    <!-- iCheck Js -->
    <script src="assets/plugins/iCheck/icheck.js"></script>
    
    <!-- Bootstrap Select Js -->
    <script src="assets/plugins/bootstrap-select/dist/js/bootstrap-select.js"></script>

    <!-- Custom Js -->
    <script src="assets/js/admin.js"></script>
    <script src="assets/js/jquery.print.js"></script>	
	<script src="assets/js/jquery.table2excel.min.js"></script>
    
    <script type="text/javascript">
        jQuery(function($) {
			
			$('.js-daterange-picker').daterangepicker({
				opens: "right",
				drops: "down",
				applyClass: "btn-primary",
				locale: {
					format: 'DD/MM/YYYY'
				}
			});
			
			$("#btnPrint").click(function() {
				$("#PrintReport").print();
				return (false);
			});
			
			//Init checkboxes and radios
			$('input[data-icheck-theme]').each(function (i, key) {
				var color = $(key).data('icheckColor');
				var theme = $(key).data('icheckTheme');
				var baseCheckboxClass = 'icheckbox_' + theme;
				var baseRadioClass = 'iradio_' + theme;

				$(key).iCheck({
					checkboxClass: color === theme ? baseCheckboxClass : baseCheckboxClass + '-' + color,
					radioClass: color === theme ? baseRadioClass : baseRadioClass + '-' + color
				});
			});
			
			$("#SelectAll").on('ifClicked', function (event) 
			{
				var status = $("#SelectAll").prop('checked');
				
				if(status == false)
				{
					$("input[type=checkbox]").prop('checked',true);
					$(".icheckbox_minimal-grey").addClass('checked');
				}
				else if(status == true)
				{
					$("input[type=checkbox]").prop('checked',false);
					$(".icheckbox_minimal-grey").removeClass('checked');
				}
			});
        });
		
		function fnExcelReport() {
			
			$("#ReportTable").table2excel({
				exclude: ".noExl",
				name: "Sales Report",
				filename: "Sales Report",
				fileext: ".xls",
				exclude_img: true,
				exclude_links: true,
				exclude_inputs: true
			});
		}
		
    </script>
</body>
</html>
<?php
ob_flush();
?>