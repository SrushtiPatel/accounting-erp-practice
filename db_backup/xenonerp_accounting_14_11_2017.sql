-- phpMyAdmin SQL Dump
-- version 4.7.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 14, 2017 at 05:16 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `xenonerp_accounting`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_login_history`
--

CREATE TABLE `admin_login_history` (
  `login_id` int(11) NOT NULL,
  `admin_id` int(11) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `login_datetime` timestamp NULL DEFAULT NULL,
  `logout_datetime` timestamp NULL DEFAULT NULL,
  `session_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_login_history`
--

INSERT INTO `admin_login_history` (`login_id`, `admin_id`, `ip`, `login_datetime`, `logout_datetime`, `session_id`) VALUES
(1, 1, '::1', '2017-07-18 05:20:12', '2017-07-18 06:57:50', 'o6rcofcjoebut6feemsj2euk86'),
(2, 1, '::1', '2017-07-18 07:47:58', '2017-07-18 14:28:03', 'o6rcofcjoebut6feemsj2euk86'),
(3, 1, '::1', '2017-07-19 08:30:51', '2017-07-19 08:44:35', '3nm848qoi1g05heg9jhl6kuc45'),
(4, 1, '::1', '2017-07-19 09:03:04', '2017-07-19 09:42:25', '3nm848qoi1g05heg9jhl6kuc45'),
(5, 1, '::1', '2017-07-19 10:01:27', '2017-07-19 10:59:16', '3nm848qoi1g05heg9jhl6kuc45'),
(6, 1, '::1', '2017-07-20 08:24:24', NULL, 'g03h2p699vn278tm8t60bcf9h5'),
(7, 1, '::1', '2017-07-21 07:08:58', '2017-07-21 07:10:24', '8cu9fh8ds94vh2kljmsjlm1ci7'),
(8, 1, '::1', '2017-07-21 10:23:53', '2017-07-21 11:36:07', 'e9rurs2jru5ftqe9lm767fqeu4'),
(9, 1, '::1', '2017-07-21 11:46:03', NULL, 'e9rurs2jru5ftqe9lm767fqeu4'),
(10, 1, '::1', '2017-07-28 05:00:20', '2017-07-28 13:32:47', '2fn6n94s9r8d2jm0qmq5karei1'),
(11, 1, '::1', '2017-07-29 05:17:27', '2017-07-29 05:46:32', 'qb1k7hp939jnm2firvpu3toga5'),
(12, 1, '::1', '2017-07-29 10:23:41', '2017-07-29 12:56:34', 'c6c3bnbnihsa4qdsokhfqgkoa2'),
(13, 1, '::1', '2017-08-01 04:55:14', '2017-08-01 11:33:01', '0esqvj42j9or8bv0bcaon9ur86'),
(14, 1, '::1', '2017-08-02 04:38:12', '2017-08-02 05:40:14', 'jma558habe8g1q1tjpg86u1jk3'),
(15, 2, '::1', '2017-08-02 05:40:21', '2017-08-02 05:54:22', 'jma558habe8g1q1tjpg86u1jk3'),
(16, 1, '::1', '2017-08-02 05:54:33', '2017-08-02 05:54:56', 'jma558habe8g1q1tjpg86u1jk3'),
(17, 2, '::1', '2017-08-02 05:55:07', '2017-08-02 06:34:07', 'jma558habe8g1q1tjpg86u1jk3'),
(18, 2, '::1', '2017-08-02 06:35:45', '2017-08-02 06:54:12', 'jma558habe8g1q1tjpg86u1jk3'),
(19, 2, '::1', '2017-08-02 06:54:15', '2017-08-02 07:46:18', 'jma558habe8g1q1tjpg86u1jk3'),
(20, 1, '::1', '2017-08-02 06:59:17', '2017-08-02 07:22:17', '87hcpqiu9rvm3hbr4ousk8ldr3'),
(21, 1, '::1', '2017-08-02 07:46:25', '2017-08-02 07:53:27', 'jma558habe8g1q1tjpg86u1jk3'),
(22, 2, '::1', '2017-08-02 07:53:33', '2017-08-02 08:27:11', 'jma558habe8g1q1tjpg86u1jk3'),
(23, 1, '::1', '2017-08-02 08:27:16', '2017-08-02 08:31:25', 'jma558habe8g1q1tjpg86u1jk3'),
(24, 2, '::1', '2017-08-02 08:31:34', '2017-08-02 08:44:24', 'jma558habe8g1q1tjpg86u1jk3'),
(25, 2, '::1', '2017-08-02 08:45:25', '2017-08-02 11:17:57', 'jma558habe8g1q1tjpg86u1jk3'),
(26, 3, '::1', '2017-08-02 08:47:49', '2017-08-02 08:48:39', 'q1hvhi7m0t1ufk13lic46v7i54'),
(27, 1, '::1', '2017-08-02 09:16:10', '2017-08-02 09:16:51', '8p0v7eubapaalopgcvn921fdv4'),
(28, 1, '::1', '2017-08-02 11:18:02', '2017-08-02 12:36:00', 'jma558habe8g1q1tjpg86u1jk3'),
(29, 2, '::1', '2017-08-02 12:36:07', '2017-08-02 13:17:35', 'jma558habe8g1q1tjpg86u1jk3'),
(30, 1, '::1', '2017-08-03 04:41:34', NULL, '59m8dh4kt1sj07lqm2v2mg6gh1'),
(31, 1, '::1', '2017-08-03 05:01:20', '2017-08-03 05:04:32', '5fhgp21m6ec3ob3jlaajj5lp53'),
(32, 2, '::1', '2017-08-03 05:04:38', '2017-08-03 05:59:01', '5fhgp21m6ec3ob3jlaajj5lp53'),
(33, 2, '::1', '2017-08-03 06:08:06', '2017-08-03 13:30:45', '5fhgp21m6ec3ob3jlaajj5lp53'),
(34, 2, '::1', '2017-08-04 05:01:47', '2017-08-04 10:35:19', '2a1ifm4ubistjvir0ntd9g01k4'),
(35, 1, '::1', '2017-08-08 05:13:02', '2017-08-08 05:20:18', '6h9db7osdfpuq3iai91m6obti7'),
(36, 2, '::1', '2017-08-08 05:20:24', '2017-08-08 05:27:32', '6h9db7osdfpuq3iai91m6obti7'),
(37, 1, '127.0.0.1', '2017-08-21 07:27:37', '2017-08-21 07:45:50', 'u5b17oa4e9c8764jgj6l0pd9n0'),
(38, 2, '127.0.0.1', '2017-08-21 07:45:57', '2017-08-21 07:47:29', 'u5b17oa4e9c8764jgj6l0pd9n0'),
(39, 2, '127.0.0.1', '2017-08-22 04:05:04', '2017-08-22 04:05:42', 'ah4c61s91jdjsub1khlkhdfam3'),
(40, 2, '127.0.0.1', '2017-08-25 05:38:40', '2017-08-25 05:42:36', 'lj868ab760unru6e2mcjufi3h6'),
(41, 2, '::1', '2017-08-25 08:11:03', '2017-08-25 08:38:17', '5bfqdu0uiah0in78kn8u5ml961'),
(42, 1, '::1', '2017-08-25 13:00:51', '2017-08-25 13:06:39', '5bfqdu0uiah0in78kn8u5ml961'),
(43, 1, '::1', '2017-08-26 04:36:07', '2017-08-26 07:30:12', 'ft384h2lf1a40a8u9u9c3304h2'),
(44, 2, '::1', '2017-08-26 07:01:42', '2017-08-26 07:12:09', '5ean92g1ol9kcpg25i26hjq221'),
(45, 1, '::1', '2017-08-26 07:40:55', '2017-08-26 12:04:18', 'ft384h2lf1a40a8u9u9c3304h2'),
(46, 1, '::1', '2017-08-28 12:56:09', '2017-08-28 12:56:16', '6g41u104moeho2212h9gmm2vf6'),
(47, 1, '127.0.0.1', '2017-08-31 08:26:42', '2017-08-31 08:26:54', 'it3hmct4eq4tjne6mbfesjc3f2'),
(48, 1, '::1', '2017-09-04 06:21:34', '2017-09-04 06:24:18', 'fhkkgqdthdvphcb934ecpjl7o2'),
(49, 2, '::1', '2017-09-04 06:24:29', '2017-09-04 06:27:42', 'fhkkgqdthdvphcb934ecpjl7o2'),
(50, 1, '::1', '2017-09-06 05:50:50', '2017-09-06 10:01:57', '2pnp8034itnb7e8923he3pt573'),
(51, 1, '::1', '2017-09-06 10:55:12', '2017-09-06 11:34:37', '2pnp8034itnb7e8923he3pt573'),
(52, 1, '::1', '2017-09-09 03:54:04', '2017-09-09 07:13:32', 'moa747bf0ekjav2ciqueal4972'),
(53, 1, '::1', '2017-09-09 07:25:22', '2017-09-09 07:25:37', 'moa747bf0ekjav2ciqueal4972'),
(54, 1, '::1', '2017-09-14 10:29:37', '2017-09-14 10:51:22', '4ao6mkj5287shnc9ucqico6um6'),
(55, 1, '::1', '2017-09-14 10:51:44', '2017-09-14 10:53:48', '4ao6mkj5287shnc9ucqico6um6'),
(56, 1, '::1', '2017-09-14 11:10:27', '2017-09-14 11:10:46', '4ao6mkj5287shnc9ucqico6um6'),
(57, 1, '::1', '2017-09-14 11:44:32', '2017-09-14 12:35:20', '4ao6mkj5287shnc9ucqico6um6'),
(58, 1, '::1', '2017-09-15 11:36:50', '2017-09-15 12:06:22', 'qq23u99pp7pru1s79uvrrhdsk6'),
(59, 1, '::1', '2017-09-18 06:28:13', '2017-09-18 06:48:00', 's9m25h7qgdnesr7pvru590ltr0'),
(60, 1, '::1', '2017-09-18 08:09:24', '2017-09-18 08:11:44', 's9m25h7qgdnesr7pvru590ltr0'),
(61, 1, '::1', '2017-09-18 08:11:53', '2017-09-18 08:18:09', 's9m25h7qgdnesr7pvru590ltr0'),
(62, 1, '::1', '2017-09-18 10:25:56', '2017-09-18 10:26:24', '70ivmh6uhfigjafjru9qcsdjv6'),
(63, 1, '127.0.0.1', '2017-09-19 05:40:23', '2017-09-19 07:20:15', 'k20adttjl8skvrtua4qeurrj01'),
(64, 1, '::1', '2017-09-20 06:33:10', '2017-09-20 06:33:58', 'hpgjrd54cfck12r5l8g987h4h2'),
(65, 1, '::1', '2017-09-23 05:55:04', NULL, 'igkmgoda3oi70dbcf7441rmql5'),
(66, 1, '::1', '2017-09-23 06:03:18', '2017-09-23 06:13:06', 'igkmgoda3oi70dbcf7441rmql5'),
(67, 1, '::1', '2017-10-03 05:10:55', '2017-10-03 05:11:06', '5ttrvegg6bnohf409v835pv8e7'),
(68, 1, '::1', '2017-10-07 10:14:04', '2017-10-07 10:14:44', 'h6u5qdb29804n1rt7j26ovgbr4'),
(69, 1, '::1', '2017-10-07 10:24:00', '2017-10-07 10:33:59', 'h6u5qdb29804n1rt7j26ovgbr4'),
(70, 1, '::1', '2017-10-10 10:52:16', '2017-10-10 10:53:06', '5sqrh8ekvdrm4i8817vhpadkb7'),
(71, 1, '::1', '2017-10-10 11:59:24', '2017-10-10 11:59:44', '4ek99h5aj16cb7kh65jcok7ej2'),
(72, 1, '::1', '2017-10-11 08:24:35', '2017-10-11 08:31:37', '52pp2ogs4eub3dbr8bia43u4b1'),
(73, 1, '::1', '2017-10-12 04:46:51', '2017-10-12 04:47:01', '9o6s106s8d27d2io1ou4e4uu23'),
(74, 1, '::1', '2017-10-12 07:33:19', '2017-10-12 13:32:25', 'iaib8l3gfen5t0rb0gsh2ons00'),
(75, 1, '::1', '2017-10-14 09:35:31', '2017-10-14 09:38:18', 'ue3ctv98sdr76a8cmtreuf8ct4'),
(76, 1, '::1', '2017-10-14 09:57:12', '2017-10-14 09:57:27', 'gf0gaa3elgfmt58d6uf01i2gq1'),
(77, 1, '::1', '2017-10-14 10:05:13', '2017-10-14 11:55:12', 'gf0gaa3elgfmt58d6uf01i2gq1'),
(78, 1, '::1', '2017-10-16 04:53:34', '2017-10-16 06:03:54', 'p7vcta5gn8ude0ekbjm908fbn7'),
(79, 1, '::1', '2017-10-16 13:02:51', '2017-10-16 13:22:57', 'p7vcta5gn8ude0ekbjm908fbn7'),
(80, 1, '::1', '2017-10-17 05:47:00', '2017-10-17 08:56:52', '3gvmk9pulr397aq4ve472tgbr6'),
(81, 1, '::1', '2017-10-17 08:57:02', '2017-10-17 09:37:32', '3gvmk9pulr397aq4ve472tgbr6'),
(82, 1, '::1', '2017-10-17 10:10:08', '2017-10-17 12:45:35', '3gvmk9pulr397aq4ve472tgbr6'),
(83, 1, '::1', '2017-10-23 10:58:35', '2017-10-23 10:59:05', 'bc8pl7c4fisdm1j9nm8o5854r2'),
(84, 1, '::1', '2017-10-23 12:50:22', '2017-10-23 12:52:41', 'bc8pl7c4fisdm1j9nm8o5854r2'),
(85, 1, '::1', '2017-10-24 04:16:51', '2017-10-24 10:52:11', '2a29h8774ij3igqatta2ts5004'),
(86, 1, '::1', '2017-10-28 04:43:37', '2017-10-28 04:50:12', '110gnqg87jk9oumrrc84lk5t92'),
(87, 1, '::1', '2017-10-28 05:06:46', NULL, 'bc547q2qb5aupstlen12rg7580'),
(88, 1, '::1', '2017-10-28 09:59:14', '2017-10-28 09:59:24', 'qsg2h3v4phlob9n08n4qr6hr17');

-- --------------------------------------------------------

--
-- Table structure for table `admin_master`
--

CREATE TABLE `admin_master` (
  `admin_id` int(11) NOT NULL,
  `admin_name` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `admin_type_id` int(11) DEFAULT NULL,
  `email_id` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `fyear_start` date DEFAULT '2016-04-01',
  `fyear_end` date DEFAULT '2017-03-31',
  `status` enum('0','1') DEFAULT '0',
  `pin` varchar(255) DEFAULT NULL,
  `live` enum('0','1') DEFAULT '0',
  `token` varchar(255) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `license` int(11) DEFAULT '0',
  `addedby` int(11) DEFAULT NULL,
  `addeddate` datetime DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `modifieddate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_master`
--

INSERT INTO `admin_master` (`admin_id`, `admin_name`, `username`, `password`, `admin_type_id`, `email_id`, `mobile`, `fyear_start`, `fyear_end`, `status`, `pin`, `live`, `token`, `company_id`, `license`, `addedby`, `addeddate`, `modifiedby`, `modifieddate`) VALUES
(1, 'Admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, 'admin@admin.com', '9876543244', '2017-04-01', '2018-04-01', '1', '012016', '0', NULL, 1, 0, 1, '2016-07-01 00:00:00', 1, '2017-10-17 15:40:38'),
(2, 'Admin1', 'admin1', '21232f297a57a5a743894a0e4a801fc3', 1, 'admin@admin.com', '9845746312', '2017-04-01', '2018-04-01', '1', '012026', '0', NULL, 2, 1, 1, '2017-08-02 00:00:00', 2, '2017-08-03 12:13:32'),
(3, 'Admin2', 'admin2', '21232f297a57a5a743894a0e4a801fc3', 1, 'admin@admin.com', '8745962315', '2017-08-02', '2017-08-02', '1', '012036', '0', NULL, 2, 1, 1, '2017-08-02 00:00:00', 3, '2017-08-02 14:18:32');

-- --------------------------------------------------------

--
-- Table structure for table `admin_type_master`
--

CREATE TABLE `admin_type_master` (
  `admin_type_id` int(11) NOT NULL,
  `admin_type` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_type_master`
--

INSERT INTO `admin_type_master` (`admin_type_id`, `admin_type`) VALUES
(1, 'Admin'),
(2, 'Account'),
(3, 'Store');

-- --------------------------------------------------------

--
-- Table structure for table `banking_master`
--

CREATE TABLE `banking_master` (
  `banking_id` bigint(20) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `bv_no` varchar(255) DEFAULT NULL,
  `transaction_date` date DEFAULT NULL,
  `pay_from` bigint(20) DEFAULT NULL,
  `pay_to` bigint(20) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT '0.00',
  `description` text,
  `addedby` int(11) DEFAULT NULL,
  `addeddate` datetime DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `modifieddate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banking_master`
--

INSERT INTO `banking_master` (`banking_id`, `company_id`, `bv_no`, `transaction_date`, `pay_from`, `pay_to`, `amount`, `description`, `addedby`, `addeddate`, `modifiedby`, `modifieddate`) VALUES
(1, 1, '1', '2017-10-28', 30, 1, '100.00', 'Test', 1, '2017-10-28 11:07:40', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `company_master`
--

CREATE TABLE `company_master` (
  `company_id` int(11) NOT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `company_alias` varchar(255) DEFAULT NULL,
  `company_code` varchar(255) DEFAULT NULL,
  `address` text,
  `phone` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `gstin` varchar(255) DEFAULT NULL,
  `pan_no` varchar(255) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `state_code` varchar(50) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_ac_no` varchar(255) DEFAULT NULL,
  `bank_ifsc` varchar(255) DEFAULT NULL,
  `bank_branch` varchar(255) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '0',
  `addedby` int(11) DEFAULT NULL,
  `addeddate` datetime DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `modifieddate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `company_master`
--

INSERT INTO `company_master` (`company_id`, `company_name`, `company_alias`, `company_code`, `address`, `phone`, `mobile`, `email`, `gstin`, `pan_no`, `state`, `state_code`, `bank_name`, `bank_ac_no`, `bank_ifsc`, `bank_branch`, `active`, `addedby`, `addeddate`, `modifiedby`, `modifieddate`) VALUES
(1, 'Nirant Enterprise Pvt. Ltd.', 'nirant', '01', '414, Titaniam City Center Mall, Prahladnagar, Ahmedabad.', '0798000386227', '9825563412', 'nepl@nepl.com', 'GS182896', '280196', 'GUJARAT', '024', 'ICICI', '1828110196', 'IC1828IC96', 'C', 0, 2, '2017-08-03 10:37:21', NULL, NULL),
(2, 'NEPL', 'NEPL', '02', 'AanandNagar Ahmedabad.', '07928856379', '9852746312', 'nepl@nepl.com', 'GS182896', '4561323', 'GUJARAT', '24', 'SBI', '87979', '7485', 'C.T.M.', 0, 2, '2017-08-03 10:38:29', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `group_master`
--

CREATE TABLE `group_master` (
  `group_id` int(11) NOT NULL,
  `group_name` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '0',
  `edit` tinyint(1) DEFAULT '0',
  `remove` tinyint(1) DEFAULT '0',
  `addedby` int(11) DEFAULT NULL,
  `addeddate` datetime DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `modifieddate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `group_master`
--

INSERT INTO `group_master` (`group_id`, `group_name`, `parent_id`, `visible`, `edit`, `remove`, `addedby`, `addeddate`, `modifiedby`, `modifieddate`) VALUES
(1, 'ASSETS', NULL, 0, 0, 0, NULL, NULL, NULL, NULL),
(2, 'LIABILITIES', NULL, 0, 0, 0, NULL, NULL, NULL, NULL),
(3, 'INCOME', NULL, 0, 0, 0, NULL, NULL, NULL, NULL),
(4, 'EXPENSES', NULL, 0, 0, 0, NULL, NULL, NULL, NULL),
(5, 'CURRENT ASSETS', 1, 0, 0, 0, NULL, NULL, NULL, NULL),
(6, 'FIXED ASSETS', 1, 0, 0, 0, NULL, NULL, NULL, NULL),
(7, 'INVESTMENTS', 1, 0, 0, 0, NULL, NULL, NULL, NULL),
(8, 'MISC. EXPENSES (ASSET)', 1, 0, 0, 0, NULL, NULL, NULL, NULL),
(9, 'CAPITAL ACCOUNT', 2, 0, 0, 0, NULL, NULL, NULL, NULL),
(10, 'CURRENT LIABILITIES', 2, 0, 0, 0, NULL, NULL, NULL, NULL),
(11, 'LOANS (LIABILITY)', 2, 0, 0, 0, NULL, NULL, NULL, NULL),
(12, 'SUSPENSE A/C', 2, 0, 0, 0, NULL, NULL, NULL, NULL),
(13, 'BRANCH / DIVISIONS', 2, 0, 0, 0, NULL, NULL, NULL, NULL),
(14, 'SALES ACCOUNTS', 3, 0, 0, 0, NULL, NULL, NULL, NULL),
(15, 'DIRECT INCOMES [INCOME (DIRECT)]', 3, 1, 0, 0, NULL, NULL, NULL, NULL),
(16, 'INDIRECT INCOMES [INCOME (INDIRECT)]', 3, 1, 0, 0, NULL, NULL, NULL, NULL),
(17, 'PURCHASE ACCOUNTS', 4, 0, 0, 0, NULL, NULL, NULL, NULL),
(18, 'DIRECT EXPENSES [EXPENSES (DIRECT)]', 4, 1, 0, 0, NULL, NULL, NULL, NULL),
(19, 'INDIRECT EXPENSES [EXPENSES (INDIRECT)]', 4, 1, 0, 0, NULL, NULL, NULL, NULL),
(20, 'BANK ACCOUNTS', 5, 1, 0, 0, NULL, NULL, 1, '2017-06-23 14:41:44'),
(21, 'CASH IN HAND', 5, 1, 0, 0, NULL, NULL, NULL, NULL),
(22, 'DEPOSITS (ASSET)', 5, 0, 0, 0, NULL, NULL, NULL, NULL),
(23, 'LOANS & ADVANCES (ASSET)', 5, 0, 0, 0, NULL, NULL, NULL, NULL),
(24, 'STOCK IN HAND', 5, 0, 0, 0, NULL, NULL, NULL, NULL),
(25, 'SUNDRY DEBTORS', 5, 0, 0, 0, NULL, NULL, NULL, NULL),
(26, 'RESERVES & SURPLUS [RETAINED EARNINGS]', 9, 0, 0, 0, NULL, NULL, NULL, NULL),
(27, 'DUTIES & TAXES', 10, 0, 0, 0, NULL, NULL, NULL, NULL),
(28, 'PROVISIONS', 10, 0, 0, 0, NULL, NULL, NULL, NULL),
(29, 'SUNDRY CREDITORS', 10, 0, 0, 0, NULL, NULL, NULL, NULL),
(30, 'BANK OD A/C [BANK OCC A/C]', 11, 0, 0, 0, NULL, NULL, NULL, NULL),
(31, 'SECURED LOANS', 11, 0, 0, 0, NULL, NULL, NULL, NULL),
(32, 'UNSECURED LOANS', 11, 0, 0, 0, NULL, NULL, NULL, NULL),
(33, 'CUSTOMERS', 3, 0, 0, 0, NULL, NULL, NULL, NULL),
(34, 'VENDORS', 4, 0, 0, 0, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_item_master`
--

CREATE TABLE `invoice_item_master` (
  `invoice_item_id` int(11) NOT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `item_name` varchar(255) DEFAULT NULL,
  `description` text,
  `hsn_code` varchar(255) DEFAULT NULL,
  `qty` float DEFAULT '0',
  `unit` varchar(50) DEFAULT NULL,
  `rate` decimal(10,2) DEFAULT '0.00',
  `discount` decimal(10,2) DEFAULT '0.00',
  `amount` decimal(10,2) DEFAULT '0.00',
  `taxable_value` decimal(10,2) DEFAULT '0.00',
  `tax1_id` int(11) DEFAULT NULL,
  `tax1_rate` decimal(10,2) DEFAULT '0.00',
  `tax1_amount` decimal(10,2) DEFAULT '0.00',
  `tax2_id` int(11) DEFAULT NULL,
  `tax2_rate` decimal(10,2) DEFAULT '0.00',
  `tax2_amount` decimal(10,2) DEFAULT '0.00',
  `tax3_id` int(11) DEFAULT NULL,
  `tax3_rate` decimal(10,2) DEFAULT '0.00',
  `tax3_amount` decimal(10,2) DEFAULT '0.00',
  `total` decimal(10,2) DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invoice_item_master`
--

INSERT INTO `invoice_item_master` (`invoice_item_id`, `invoice_id`, `item_id`, `item_name`, `description`, `hsn_code`, `qty`, `unit`, `rate`, `discount`, `amount`, `taxable_value`, `tax1_id`, `tax1_rate`, `tax1_amount`, `tax2_id`, `tax2_rate`, `tax2_amount`, `tax3_id`, `tax3_rate`, `tax3_amount`, `total`) VALUES
(1, 1, 1, 'WEBSITE', 'TEst', 'H001', 1, 'NOs', '10000.00', '0.00', '10000.00', '10000.00', 2, '2.50', '250.00', 6, '6.00', '600.00', 7, '12.00', '1200.00', '12050.00'),
(2, 1, 2, 'SOFTWARE', 'Test', 'H002', 1, 'NOs', '15000.00', '0.00', '15000.00', '15000.00', 8, '9.00', '1350.00', 12, '14.00', '2100.00', 10, '18.00', '2700.00', '21150.00'),
(3, 1, 1, 'WEBSITE', 'Tests', 'H001', 1, 'NOs', '10000.00', '0.00', '10000.00', '10000.00', 2, '2.50', '250.00', 12, '14.00', '1400.00', 10, '18.00', '1800.00', '13450.00'),
(4, 2, 1, 'WEBSITE', 'test', 'H001', 1, 'NOs', '10000.00', '0.00', '10000.00', '10000.00', -1, '0.00', '0.00', -1, '0.00', '0.00', -1, '0.00', '0.00', '10000.00'),
(5, 2, 2, 'SOFTWARE', 'test', 'H002', 1, 'NOs', '15000.00', '0.00', '15000.00', '15000.00', -1, '0.00', '0.00', -1, '0.00', '0.00', -1, '0.00', '0.00', '15000.00');

-- --------------------------------------------------------

--
-- Table structure for table `invoice_master`
--

CREATE TABLE `invoice_master` (
  `invoice_id` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `invoice_no` varchar(255) DEFAULT NULL,
  `invoice_date` date DEFAULT NULL,
  `challan_no` varchar(255) DEFAULT NULL,
  `challan_date` date DEFAULT NULL,
  `po_no` varchar(255) DEFAULT NULL,
  `po_date` date DEFAULT NULL,
  `place_of_supply` varchar(255) DEFAULT NULL,
  `transportation_mode` text,
  `vehicle_no` varchar(255) DEFAULT NULL,
  `date_of_supply` date DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `billing_name` varchar(255) DEFAULT NULL,
  `billing_mobile` varchar(50) DEFAULT NULL,
  `billing_gstin` longtext,
  `billing_address` longtext,
  `billing_city` longtext,
  `billing_state` longtext,
  `billing_state_code` longtext,
  `billing_country` longtext,
  `billing_pin_code` longtext,
  `shipping_name` varchar(255) DEFAULT NULL,
  `shipping_gstin` longtext,
  `shipping_address` longtext,
  `shipping_city` longtext,
  `shipping_state` longtext,
  `shipping_state_code` longtext,
  `shipping_country` longtext,
  `shipping_pin_code` longtext,
  `total_amount_before_tax` decimal(10,2) DEFAULT '0.00',
  `discount` decimal(10,2) DEFAULT '0.00',
  `total_tax_amount` decimal(10,2) DEFAULT '0.00',
  `roundoff` decimal(10,2) DEFAULT '0.00',
  `grand_total` decimal(10,2) DEFAULT '0.00',
  `terms_conditions` text,
  `oc_name` varchar(255) DEFAULT NULL,
  `oc_address` text,
  `oc_phone` varchar(50) DEFAULT NULL,
  `oc_mobile` varchar(50) DEFAULT NULL,
  `oc_email` varchar(50) DEFAULT NULL,
  `oc_pan_no` varchar(50) DEFAULT NULL,
  `oc_gstin` varchar(50) DEFAULT NULL,
  `oc_state` varchar(50) DEFAULT NULL,
  `oc_state_code` varchar(50) DEFAULT NULL,
  `oc_bank_name` varchar(255) DEFAULT NULL,
  `oc_bank_ac_no` varchar(255) DEFAULT NULL,
  `oc_bank_ifsc` varchar(255) DEFAULT NULL,
  `oc_bank_branch` varchar(255) DEFAULT NULL,
  `addedby` int(11) DEFAULT NULL,
  `addeddate` datetime DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `modifieddate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invoice_master`
--

INSERT INTO `invoice_master` (`invoice_id`, `company_id`, `invoice_no`, `invoice_date`, `challan_no`, `challan_date`, `po_no`, `po_date`, `place_of_supply`, `transportation_mode`, `vehicle_no`, `date_of_supply`, `due_date`, `client_id`, `billing_name`, `billing_mobile`, `billing_gstin`, `billing_address`, `billing_city`, `billing_state`, `billing_state_code`, `billing_country`, `billing_pin_code`, `shipping_name`, `shipping_gstin`, `shipping_address`, `shipping_city`, `shipping_state`, `shipping_state_code`, `shipping_country`, `shipping_pin_code`, `total_amount_before_tax`, `discount`, `total_tax_amount`, `roundoff`, `grand_total`, `terms_conditions`, `oc_name`, `oc_address`, `oc_phone`, `oc_mobile`, `oc_email`, `oc_pan_no`, `oc_gstin`, `oc_state`, `oc_state_code`, `oc_bank_name`, `oc_bank_ac_no`, `oc_bank_ifsc`, `oc_bank_branch`, `addedby`, `addeddate`, `modifiedby`, `modifieddate`) VALUES
(1, 1, '1', '2017-09-10', '1', '2017-09-18', '1', '2017-09-19', 'Ahmedabad', 'ROAD', 'GH45863', '2017-09-23', '0000-00-00', 16, 'SADIKBHAI', '9852741623', '', 'GOMTIPUR,', 'AHMEDABAD', 'GUJARAT', '24', 'INDIA', '380021', 'SADIKBHAI', '', 'GOMTIPUR,', 'AHMEDABAD', 'GUJARAT', '24', 'INDIA', '380021', '35000.00', '0.00', '11650.00', '0.00', '46650.00', 'TEst', 'Nirant Enterprise Pvt. Ltd.', '414, Titaniam City Center Mall, Prahladnagar, Ahmedabad.', '0798000386227', 'nepl@nepl.com', 'Nirant Enterprise Pvt. Ltd.', '280196', 'GS182896', 'GUJARAT', '024', 'ICICI', '1828110196', 'IC1828IC96', 'C', 1, '2017-09-09 10:58:41', 1, '2017-09-14 16:17:28'),
(2, 1, '2', '2017-09-16', '2', '0000-00-00', '2', '2017-09-20', 'Ahmedabad', 'ROAD', 'GJ4563', '2017-09-17', '0000-00-00', 14, 'SADHNA AUTOMOBILES', '9414419794', '08AAIPP6049G1ZF', '33-A, Friends Colony,\r\nChamti Kheda Road,\r\nNr. New Pulia, Chittorgarh,\r\nRajasthan', 'Chittorgarh', 'Rajasthan', '08', 'India', '312001', 'SADHNA AUTOMOBILES', '08AAIPP6049G1ZF', '33-A, Friends Colony,\r\nChamti Kheda Road,\r\nNr. New Pulia, Chittorgarh,\r\nRajasthan', 'Chittorgarh', 'Rajasthan', '08', 'India', '312001', '25000.00', '0.00', '0.00', '0.00', '25000.00', 'Test', 'Nirant Enterprise Pvt. Ltd.', '414, Titaniam City Center Mall, Prahladnagar, Ahmedabad.', '0798000386227', 'nepl@nepl.com', 'Nirant Enterprise Pvt. Ltd.', '280196', 'GS182896', 'GUJARAT', '024', 'ICICI', '1828110196', 'IC1828IC96', 'C', 1, '2017-09-14 16:18:56', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `item_master`
--

CREATE TABLE `item_master` (
  `item_id` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `item_name` varchar(255) DEFAULT NULL,
  `item_alias` varchar(255) DEFAULT NULL,
  `item_code` varchar(255) DEFAULT NULL,
  `hsn_code` varchar(255) DEFAULT NULL,
  `item_rate` decimal(10,2) DEFAULT '0.00',
  `item_unit` varchar(255) DEFAULT NULL,
  `initial_stock` float DEFAULT '0',
  `min_stock` float DEFAULT '0',
  `max_stock` float DEFAULT '0',
  `addedby` int(11) DEFAULT NULL,
  `addeddate` datetime DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `modifieddate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `item_master`
--

INSERT INTO `item_master` (`item_id`, `company_id`, `item_name`, `item_alias`, `item_code`, `hsn_code`, `item_rate`, `item_unit`, `initial_stock`, `min_stock`, `max_stock`, `addedby`, `addeddate`, `modifiedby`, `modifieddate`) VALUES
(1, 1, 'WEBSITE', 'Alias', '001', 'H001', '10000.00', 'NOs', 0, 0, 0, 1, '2017-09-09 10:56:53', NULL, NULL),
(2, 1, 'SOFTWARE', 'Software', '002', 'H002', '15000.00', 'NOs', 0, 0, 0, 1, '2017-09-09 10:57:26', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ledger_custom_fields`
--

CREATE TABLE `ledger_custom_fields` (
  `lcf_id` bigint(20) NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  `lcf_name` varchar(255) DEFAULT NULL,
  `lcf_type` varchar(255) DEFAULT NULL,
  `is_required` tinyint(1) DEFAULT '0',
  `placeholder` varchar(255) DEFAULT NULL,
  `lcf_values` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ledger_custom_fields`
--

INSERT INTO `ledger_custom_fields` (`lcf_id`, `group_id`, `lcf_name`, `lcf_type`, `is_required`, `placeholder`, `lcf_values`) VALUES
(1, 33, 'MOBILE NO', 'number', 0, NULL, NULL),
(2, 33, 'PHONE NO', 'number', 0, NULL, NULL),
(3, 33, 'EMAIL', 'email', 0, NULL, NULL),
(4, 33, 'WEBSITE', 'text', 0, NULL, NULL),
(5, 33, 'BILLING GSTIN', 'text', 0, NULL, NULL),
(6, 33, 'PAN', 'text', 0, NULL, NULL),
(7, 33, 'BILLING ADDRESS', 'textarea', 0, NULL, NULL),
(8, 33, 'BILLING CITY', 'text', 0, NULL, NULL),
(9, 33, 'BILLING STATE', 'text', 0, NULL, NULL),
(10, 33, 'BILLING COUNTRY', 'text', 0, NULL, NULL),
(11, 33, 'BILLING PIN CODE', 'number', 0, NULL, NULL),
(12, 33, 'SHIPPING ADDRESS', 'textarea', 0, NULL, NULL),
(13, 33, 'SHIPPING CITY', 'text', 0, NULL, NULL),
(14, 33, 'SHIPPING STATE', 'text', 0, NULL, NULL),
(15, 33, 'SHIPPING COUNTRY', 'text', 0, NULL, NULL),
(16, 33, 'SHIPPING PIN CODE', 'number', 0, NULL, NULL),
(17, 33, 'AS ON DATE', 'date', 1, NULL, NULL),
(18, 34, 'MOBILE NO', 'number', 0, NULL, NULL),
(19, 34, 'PHONE NO', 'number', 0, NULL, NULL),
(20, 34, 'EMAIL', 'email', 0, NULL, NULL),
(21, 34, 'WEBSITE', 'text', 0, NULL, NULL),
(22, 34, 'BILLING GSTIN', 'text', 0, NULL, NULL),
(23, 34, 'PAN', 'text', 0, NULL, NULL),
(24, 34, 'BILLING ADDRESS', 'textarea', 0, NULL, NULL),
(25, 34, 'BILLING CITY', 'text', 0, NULL, NULL),
(26, 34, 'BILLING STATE', 'text', 0, NULL, NULL),
(27, 34, 'BILLING COUNTRY', 'text', 0, NULL, NULL),
(28, 34, 'BILLING PIN CODE', 'number', 0, NULL, NULL),
(29, 34, 'SHIPPING ADDRESS', 'textarea', 0, NULL, NULL),
(30, 34, 'SHIPPING CITY', 'text', 0, NULL, NULL),
(31, 34, 'SHIPPING STATE', 'text', 0, NULL, NULL),
(32, 34, 'SHIPPING COUNTRY', 'text', 0, NULL, NULL),
(33, 34, 'SHIPPING PIN CODE', 'number', 0, NULL, NULL),
(34, 34, 'AS ON DATE', 'date', 1, NULL, NULL),
(35, 34, 'PAYMENT DETAILS', 'textarea', 0, NULL, NULL),
(36, 27, 'TAX TYPE', 'select', 1, NULL, '{\"SGST\":\"SGST\",\"CGST\":\"CGST\",\"IGST\":\"IGST\"}'),
(37, 27, 'TAX RATE', 'text', 1, NULL, NULL),
(41, 20, 'BANK AC NO', 'text', 1, NULL, NULL),
(42, 20, 'BANK AC HOLDER', 'text', 1, NULL, NULL),
(43, 20, 'BANK NAME', 'text', 1, NULL, NULL),
(44, 20, 'BANK BRANCH', 'text', 1, NULL, NULL),
(45, 20, 'RTGS CODE', 'text', 0, NULL, NULL),
(46, 20, 'MICR CODE', 'text', 0, NULL, NULL),
(47, 20, 'IFSC CODE', 'text', 0, NULL, NULL),
(48, 9, 'NAME', 'text', 1, NULL, NULL),
(49, 9, 'EMAIL', 'email', 0, NULL, NULL),
(50, 9, 'ADDRESS', 'textarea', 0, NULL, NULL),
(51, 9, 'CITY', 'text', 0, NULL, NULL),
(52, 9, 'STATE', 'text', 0, NULL, NULL),
(53, 9, 'PIN CODE', 'number', 0, NULL, NULL),
(54, 31, 'LOAN AC NO', 'text', 0, NULL, NULL),
(55, 31, 'BANK NAME', 'text', 0, NULL, NULL),
(56, 31, 'BANK BRANCH', 'text', 0, NULL, NULL),
(57, 31, 'LOAN TYPE', 'text', 0, NULL, NULL),
(58, 31, 'INTEREST RATE', 'number', 0, NULL, NULL),
(59, 31, 'DETAILS', 'textarea', 0, NULL, NULL),
(60, 32, 'LOAN AC NO', 'text', 0, NULL, NULL),
(61, 32, 'BANK NAME', 'text', 0, NULL, NULL),
(62, 32, 'BANK BRANCH', 'text', 0, NULL, NULL),
(63, 32, 'LOAN TYPE', 'text', 0, NULL, NULL),
(64, 32, 'INTEREST RATE', 'number', 0, NULL, NULL),
(65, 32, 'DETAILS', 'textarea', 0, NULL, NULL),
(66, 23, 'INTEREST BEARING DEPOSIT', 'radio', 0, NULL, '{\"YES\":\"1\",\"NO\":\"0\"}'),
(67, 23, 'INTERST RATE', 'number', 0, NULL, NULL),
(68, 23, 'COMPOUNDING TYPE', 'select', 1, NULL, '{\"DAILY\":\"DAILY\",\"MONTHLY\":\"MONTHLY\",\"QUARTERLY\":\"QUARTERLY\",\"HALF-YEARLY\":\"HALF-YEARLY\",\"YEARLY\":\"YEARLY\",\"PAYOUT\":\"PAYOUT\"}'),
(69, 22, 'INTEREST BEARING DEPOSIT', 'radio', 0, NULL, '{\"YES\":\"1\",\"NO\":\"0\"}'),
(70, 22, 'INTERST RATE', 'number', 0, NULL, NULL),
(71, 22, 'COMPOUNDING TYPE', 'select', 1, NULL, '{\"DAILY\":\"DAILY\",\"MONTHLY\":\"MONTHLY\",\"QUARTERLY\":\"QUARTERLY\",\"HALF-YEARLY\":\"HALF-YEARLY\",\"YEARLY\":\"YEARLY\",\"PAYOUT\":\"PAYOUT\"}'),
(72, 6, 'DEPRECIABLE', 'radio', 0, NULL, '{\"YES\":\"1\",\"NO\":\"0\"}'),
(73, 6, 'DEPRECIATION RATE', 'number', 0, NULL, NULL),
(74, 33, 'SHIPPING GSTIN', 'text', 0, NULL, NULL),
(75, 34, 'SHIPPING GSTIN', 'text', 0, NULL, NULL),
(76, 33, 'BILLING STATE CODE', 'number', 0, NULL, NULL),
(77, 33, 'SHIPPING STATE CODE', 'number', 0, NULL, NULL),
(78, 34, 'BILLING STATE CODE', 'number', 0, NULL, NULL),
(79, 34, 'SHIPPING STATE CODE', 'number', 0, NULL, NULL),
(80, 33, 'TRANSPORTER NAME', 'text', 0, NULL, NULL),
(81, 33, 'TRANSPORTER ADDRESS', 'textarea', 0, NULL, NULL),
(82, 33, 'TRANSPORTER STATE', 'text', 0, NULL, NULL),
(83, 33, 'TRANSPORTER STATE CODE', 'number', 0, NULL, NULL),
(84, 33, 'TRANSPORTER GSTIN', 'text', 0, NULL, NULL),
(85, 33, 'TRANSPORTER PHONE', 'number', 0, NULL, NULL),
(86, 34, 'TRANSPORTER NAME', 'text', 0, NULL, NULL),
(87, 34, 'TRANSPORTER ADDRESS', 'textarea', 0, NULL, NULL),
(88, 34, 'TRANSPORTER STATE', 'text', 0, NULL, NULL),
(89, 34, 'TRANSPORTER STATE CODE', 'number', 0, NULL, NULL),
(90, 34, 'TRANSPORTER GSTIN', 'text', 0, NULL, NULL),
(91, 34, 'TRANSPORTER PHONE', 'number', 0, NULL, NULL),
(92, 33, 'BILLING NAME', 'text', 0, NULL, NULL),
(93, 33, 'SHIPPING NAME', 'text', 0, NULL, NULL),
(94, 34, 'BILLING NAME', 'text', 0, NULL, NULL),
(95, 34, 'SHIPPING NAME', 'text', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ledger_item_master`
--

CREATE TABLE `ledger_item_master` (
  `li_id` bigint(20) NOT NULL,
  `ledger_id` bigint(20) DEFAULT NULL,
  `lcf_id` bigint(20) DEFAULT NULL,
  `li_value` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ledger_item_master`
--

INSERT INTO `ledger_item_master` (`li_id`, `ledger_id`, `lcf_id`, `li_value`) VALUES
(1, 2, 36, 'SGST'),
(2, 2, 37, '2.5'),
(3, 3, 36, 'CGST'),
(4, 3, 37, '2.5'),
(5, 4, 36, 'IGST'),
(6, 4, 37, '5.0'),
(7, 5, 36, 'SGST'),
(8, 5, 37, '6.0'),
(9, 6, 36, 'CGST'),
(10, 6, 37, '6.0'),
(11, 7, 36, 'IGST'),
(12, 7, 37, '12.0'),
(13, 8, 36, 'SGST'),
(14, 8, 37, '9.0'),
(15, 9, 36, 'CGST'),
(16, 9, 37, '9.0'),
(17, 10, 36, 'IGST'),
(18, 10, 37, '18.00'),
(19, 11, 36, 'SGST'),
(20, 11, 37, '14.0'),
(21, 12, 36, 'CGST'),
(22, 12, 37, '14.0'),
(23, 13, 36, 'IGST'),
(24, 13, 37, '28.0'),
(25, 14, 17, '12-08-2017'),
(26, 14, 1, '9414419794'),
(27, 14, 2, '01472246534'),
(28, 14, 3, 'sadhnaautomobiles@gmail.com'),
(29, 14, 4, ''),
(30, 14, 6, 'AAIPP6049G1ZF'),
(31, 14, 92, 'SADHNA AUTOMOBILES'),
(32, 14, 5, '08AAIPP6049G1ZF'),
(33, 14, 7, '33-A, Friends Colony,\r\nChamti Kheda Road,\r\nNr. New Pulia, Chittorgarh,\r\nRajasthan'),
(34, 14, 8, 'Chittorgarh'),
(35, 14, 9, 'Rajasthan'),
(36, 14, 76, '08'),
(37, 14, 10, 'India'),
(38, 14, 11, '312001'),
(39, 14, 93, 'SADHNA AUTOMOBILES'),
(40, 14, 74, '08AAIPP6049G1ZF'),
(41, 14, 12, '33-A, Friends Colony,\r\nChamti Kheda Road,\r\nNr. New Pulia, Chittorgarh,\r\nRajasthan'),
(42, 14, 13, 'Chittorgarh'),
(43, 14, 14, 'Rajasthan'),
(44, 14, 77, '08'),
(45, 14, 15, 'India'),
(46, 14, 16, '312001'),
(47, 15, 17, '01-07-2017'),
(48, 15, 1, ''),
(49, 15, 2, ''),
(50, 15, 3, ''),
(51, 15, 4, ''),
(52, 15, 6, ''),
(53, 15, 92, 'TALIBBHAI'),
(54, 15, 5, ''),
(55, 15, 7, 'USHA TOCKIG\r\nGOMTIPUR'),
(56, 15, 8, 'AHMEDABAD'),
(57, 15, 9, 'GUJARAT'),
(58, 15, 76, '24'),
(59, 15, 10, 'INDIA'),
(60, 15, 11, '380021'),
(61, 15, 93, 'TALIBBHAI'),
(62, 15, 74, ''),
(63, 15, 12, 'USHA TOCKIG\r\nGOMTIPUR'),
(64, 15, 13, 'AHMEDABAD'),
(65, 15, 14, 'GUJARAT'),
(66, 15, 77, '24'),
(67, 15, 15, 'INDIA'),
(68, 15, 16, '380021'),
(69, 16, 17, '01-07-2017'),
(70, 16, 1, ''),
(71, 16, 2, ''),
(72, 16, 3, ''),
(73, 16, 4, ''),
(74, 16, 6, ''),
(75, 16, 92, 'SADIKBHAI'),
(76, 16, 5, ''),
(77, 16, 7, 'GOMTIPUR,'),
(78, 16, 8, 'AHMEDABAD'),
(79, 16, 9, 'GUJARAT'),
(80, 16, 76, '24'),
(81, 16, 10, 'INDIA'),
(82, 16, 11, '380021'),
(83, 16, 93, 'SADIKBHAI'),
(84, 16, 74, ''),
(85, 16, 12, 'GOMTIPUR,'),
(86, 16, 13, 'AHMEDABAD'),
(87, 16, 14, 'GUJARAT'),
(88, 16, 77, '24'),
(89, 16, 15, 'INDIA'),
(90, 16, 16, '380021'),
(91, 17, 17, '01-07-2017'),
(92, 17, 1, ''),
(93, 17, 2, ''),
(94, 17, 3, ''),
(95, 17, 4, ''),
(96, 17, 6, ''),
(97, 17, 92, 'RAVIBHAI'),
(98, 17, 5, ''),
(99, 17, 7, 'ANUPAM CINEMA,\r\n'),
(100, 17, 8, 'AHMEDABAD'),
(101, 17, 9, 'GUJARAT'),
(102, 17, 76, '24'),
(103, 17, 10, 'INDIA'),
(104, 17, 11, '380021'),
(105, 17, 93, ''),
(106, 17, 74, ''),
(107, 17, 12, ''),
(108, 17, 13, ''),
(109, 17, 14, ''),
(110, 17, 77, ''),
(111, 17, 15, ''),
(112, 17, 16, ''),
(113, 18, 17, '01-07-2017'),
(114, 18, 1, ''),
(115, 18, 2, ''),
(116, 18, 3, ''),
(117, 18, 4, ''),
(118, 18, 6, ''),
(119, 18, 92, 'KANTIBHAI'),
(120, 18, 5, ''),
(121, 18, 7, 'JAIN MILAN SOCEITY,\r\nSURELIYA ROAD,\r\n'),
(122, 18, 8, 'AHMEDABAD'),
(123, 18, 9, '24'),
(124, 18, 76, 'GUJARAT'),
(125, 18, 10, 'INDIA'),
(126, 18, 11, '380026'),
(127, 18, 93, 'KANTIBHAI'),
(128, 18, 74, ''),
(129, 18, 12, 'JAIN MILAN SOCEITY,\r\nSURELIYA ROAD,\r\n'),
(130, 18, 13, 'AHMEDABAD'),
(131, 18, 14, '24'),
(132, 18, 77, 'GUJARAT'),
(133, 18, 15, 'INDIA'),
(134, 18, 16, '380026'),
(135, 19, 17, '01-07-2017'),
(136, 19, 1, ''),
(137, 19, 2, ''),
(138, 19, 3, ''),
(139, 19, 4, ''),
(140, 19, 6, ''),
(141, 19, 92, 'SAFIBHAI'),
(142, 19, 5, ''),
(143, 19, 7, 'GOMTIPUR,'),
(144, 19, 8, 'AHMEDABAD'),
(145, 19, 9, 'GUJARAT'),
(146, 19, 76, '24'),
(147, 19, 10, 'INDIA'),
(148, 19, 11, '380021'),
(149, 19, 93, 'SAFIBHAI'),
(150, 19, 74, ''),
(151, 19, 12, 'GOMTIPUR,'),
(152, 19, 13, 'AHMEDABAD'),
(153, 19, 14, 'GUJARAT'),
(154, 19, 77, '24'),
(155, 19, 15, 'INDIA'),
(156, 19, 16, '380021'),
(157, 20, 17, '01-07-2017'),
(158, 20, 1, ''),
(159, 20, 2, ''),
(160, 20, 3, ''),
(161, 20, 4, ''),
(162, 20, 6, ''),
(163, 20, 92, 'RAFIKBHAI'),
(164, 20, 5, ''),
(165, 20, 7, 'GOMTIPUR,'),
(166, 20, 8, 'AHMEDABAD'),
(167, 20, 9, 'GUJARAT'),
(168, 20, 76, '24'),
(169, 20, 10, 'INDIA'),
(170, 20, 11, '380021'),
(171, 20, 93, 'RAFIKBHAI'),
(172, 20, 74, ''),
(173, 20, 12, 'GOMTIPUR,'),
(174, 20, 13, 'AHMEDABAD'),
(175, 20, 14, 'GUJARAT'),
(176, 20, 77, '24'),
(177, 20, 15, 'INDIA'),
(178, 20, 16, '380021'),
(179, 21, 17, '01-07-2017'),
(180, 21, 1, ''),
(181, 21, 2, ''),
(182, 21, 3, ''),
(183, 21, 4, ''),
(184, 21, 6, ''),
(185, 21, 92, 'JITENDRA JANI'),
(186, 21, 5, ''),
(187, 21, 7, 'A14 SHIV SHAMBHU SOCEITY,\r\nVASTRAL ROAD,\r\nAMRAIWADI'),
(188, 21, 8, 'AHMEDABAD'),
(189, 21, 9, 'GUJARAT'),
(190, 21, 76, '24'),
(191, 21, 10, 'INDIA'),
(192, 21, 11, '380026'),
(193, 21, 93, 'JITENDRA JANI'),
(194, 21, 74, ''),
(195, 21, 12, 'A14 SHIV SHAMBHU SOCEITY,\r\nVASTRAL ROAD,\r\nAMRAIWADI'),
(196, 21, 13, 'AHMEDABAD'),
(197, 21, 14, 'GUJARAT'),
(198, 21, 77, '24'),
(199, 21, 15, 'INDIA'),
(200, 21, 16, '380026'),
(201, 22, 17, '01-07-2017'),
(202, 22, 1, ''),
(203, 22, 2, ''),
(204, 22, 3, ''),
(205, 22, 4, ''),
(206, 22, 6, ''),
(207, 22, 92, 'RAFIKBHAI'),
(208, 22, 5, ''),
(209, 22, 7, 'GOMTIPUR'),
(210, 22, 8, 'AHMEDABAD'),
(211, 22, 9, 'GUJARAT'),
(212, 22, 76, '24'),
(213, 22, 10, 'INDIA'),
(214, 22, 11, '380021'),
(215, 22, 93, 'RAFIKBHAI'),
(216, 22, 74, ''),
(217, 22, 12, 'GOMTIPUR'),
(218, 22, 13, 'AHMEDABAD'),
(219, 22, 14, 'GUJARAT'),
(220, 22, 77, '24'),
(221, 22, 15, 'INDIA'),
(222, 22, 16, '380021'),
(223, 23, 17, '01-07-2017'),
(224, 23, 1, '7046005986'),
(225, 23, 2, ''),
(226, 23, 3, ''),
(227, 23, 4, ''),
(228, 23, 6, ''),
(229, 23, 92, 'VIJAYBHAI (PAKO)'),
(230, 23, 5, ''),
(231, 23, 7, 'GITAMANDIRE\r\n'),
(232, 23, 8, 'AHMEDABAD'),
(233, 23, 9, 'GUJARAT'),
(234, 23, 76, '24'),
(235, 23, 10, 'INDIA'),
(236, 23, 11, ''),
(237, 23, 93, 'VIJAYBHAI'),
(238, 23, 74, ''),
(239, 23, 12, 'GITAMANDIRE\r\n'),
(240, 23, 13, 'AHMEDABAD'),
(241, 23, 14, 'GUJARAT'),
(242, 23, 77, '24'),
(243, 23, 15, 'INDIA'),
(244, 23, 16, ''),
(245, 24, 17, '01-07-2017'),
(246, 24, 1, '9909894193'),
(247, 24, 2, ''),
(248, 24, 3, ''),
(249, 24, 4, ''),
(250, 24, 6, ''),
(251, 24, 92, 'RINKESHBHAI'),
(252, 24, 5, ''),
(253, 24, 7, 'MANINAGAR'),
(254, 24, 8, 'AHMEDABAD'),
(255, 24, 9, 'GUJARAT'),
(256, 24, 76, '24'),
(257, 24, 10, 'INDIA'),
(258, 24, 11, ''),
(259, 24, 93, 'RINKESHBHAI'),
(260, 24, 74, ''),
(261, 24, 12, 'MANINAGAR'),
(262, 24, 13, 'AHMEDABAD'),
(263, 24, 14, 'GUJARAT'),
(264, 24, 77, '24'),
(265, 24, 15, 'INDIA'),
(266, 24, 16, ''),
(267, 25, 17, '01-07-2017'),
(268, 25, 1, ''),
(269, 25, 2, ''),
(270, 25, 3, ''),
(271, 25, 4, ''),
(272, 25, 6, ''),
(273, 25, 92, 'AYUBBHAI'),
(274, 25, 5, ''),
(275, 25, 7, 'MARIUM NI CHAL,\r\nGOMTIPUR'),
(276, 25, 8, 'AHMEDABAD'),
(277, 25, 9, 'GUJARAT'),
(278, 25, 76, '24'),
(279, 25, 10, 'INDIA'),
(280, 25, 11, '380021'),
(281, 25, 93, 'AYUBBHAI'),
(282, 25, 74, ''),
(283, 25, 12, 'MARIUM NI CHAL,\r\nGOMTIPUR'),
(284, 25, 13, 'AHMEDABAD'),
(285, 25, 14, 'GUJARAT'),
(286, 25, 77, '24'),
(287, 25, 15, 'INDIA'),
(288, 25, 16, '380021'),
(289, 26, 17, '01-07-2017'),
(290, 26, 1, ''),
(291, 26, 2, ''),
(292, 26, 3, ''),
(293, 26, 4, ''),
(294, 26, 6, ''),
(295, 26, 92, 'RAVIBHAI NANDA'),
(296, 26, 5, ''),
(297, 26, 7, 'JAMNAGAR,'),
(298, 26, 8, 'JAMNAGAR'),
(299, 26, 9, 'GUJARAT'),
(300, 26, 76, '24'),
(301, 26, 10, 'INDIA'),
(302, 26, 11, ''),
(303, 26, 93, 'RAVIBHAI NANDA'),
(304, 26, 74, ''),
(305, 26, 12, 'JAMNAGAR,'),
(306, 26, 13, 'JAMNAGAR'),
(307, 26, 14, 'GUJARAT'),
(308, 26, 77, '24'),
(309, 26, 15, 'INDIA'),
(310, 26, 16, ''),
(311, 27, 34, '01-07-2017'),
(312, 27, 18, '9824349650'),
(313, 27, 19, ''),
(314, 27, 20, 'harishsalescorp@gmail.com'),
(315, 27, 21, ''),
(316, 27, 23, ''),
(317, 27, 35, ''),
(318, 27, 94, 'HARISH SALES CORPORATION'),
(319, 27, 22, '24AEMPN1690P1ZV'),
(320, 27, 24, 'HAWAI CHOWK,\r\nOUTSIDE KHAMBHLIA GATE,\r\n'),
(321, 27, 25, 'JAMNAGAR'),
(322, 27, 26, 'GUJARAT'),
(323, 27, 78, '24'),
(324, 27, 27, 'INDIA'),
(325, 27, 28, '361001'),
(326, 27, 95, 'HARISH SALES CORPORATION'),
(327, 27, 75, '24AEMPN1690P1ZV'),
(328, 27, 29, 'HAWAI CHOWK,\r\nOUTSIDE KHAMBHLIA GATE,\r\n'),
(329, 27, 30, 'JAMNAGAR'),
(330, 27, 31, 'GUJARAT'),
(331, 27, 79, '24'),
(332, 27, 32, 'INDIA'),
(333, 27, 33, '361001'),
(334, 28, 34, '01-07-2017'),
(335, 28, 18, '9825347980'),
(336, 28, 19, ''),
(337, 28, 20, ''),
(338, 28, 21, ''),
(339, 28, 23, ''),
(340, 28, 35, ''),
(341, 28, 94, 'GHANSHYAM BATTERIES'),
(342, 28, 22, '24CJPPS4787LIZ6'),
(343, 28, 24, '33, HIREN CHAMBERS,\r\nTHAKKAR NAGAR CROSS ROAD,\r\nBAPUNAGAR,\r\nN.H.NO-08\r\n'),
(344, 28, 25, 'AHMEDABAD'),
(345, 28, 26, 'GUJARAT'),
(346, 28, 78, '24'),
(347, 28, 27, 'INDIA'),
(348, 28, 28, ''),
(349, 28, 95, 'GHANSHYAM BATTERIES'),
(350, 28, 75, '24CJPPS4787LIZ6'),
(351, 28, 29, '33, HIREN CHAMBERS,\r\nTHAKKAR NAGAR CROSS ROAD,\r\nBAPUNAGAR,\r\nN.H.NO-08\r\n'),
(352, 28, 30, 'AHMEDABAD'),
(353, 28, 31, 'GUJARAT'),
(354, 28, 79, '24'),
(355, 28, 32, 'INDIA'),
(356, 28, 33, ''),
(357, 29, 17, '01-07-2017'),
(358, 29, 1, '9413473815'),
(359, 29, 2, ''),
(360, 29, 3, 'shriakshayyobykes@yahoo.com'),
(361, 29, 4, ''),
(362, 29, 6, 'BCQPS8904J'),
(363, 29, 92, 'SHRI AKSHAY SOLAR'),
(364, 29, 5, '08BCQPS8904J1ZS'),
(365, 29, 7, '\'\'INDESPRASTH\'\' Bhilwara Road,By Pass Kankroli-Rajsamand'),
(366, 29, 8, 'RAJASTHAN'),
(367, 29, 9, 'RAJASTHAN'),
(368, 29, 76, '08'),
(369, 29, 10, 'INDIA'),
(370, 29, 11, '313324'),
(371, 29, 93, 'SHRI AKSHAY SOLAR'),
(372, 29, 74, '08BCQPS8904J1ZS'),
(373, 29, 12, '\'\'INDESPRASTH\'\' Bhilwara Road,By Pass Kankroli-Rajsamand'),
(374, 29, 13, 'RAJASTHAN'),
(375, 29, 14, 'RAJASTHAN'),
(376, 29, 77, '08'),
(377, 29, 15, 'INDIA'),
(378, 29, 16, '313324'),
(379, 30, 41, '182811'),
(380, 30, 42, 'SELF'),
(381, 30, 43, 'ICICI'),
(382, 30, 44, 'PRAHLADNAGAR'),
(383, 30, 45, '48578'),
(384, 30, 46, '455789'),
(385, 30, 47, '1524587');

-- --------------------------------------------------------

--
-- Table structure for table `ledger_master`
--

CREATE TABLE `ledger_master` (
  `ledger_id` bigint(20) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `ledger_name` varchar(255) DEFAULT NULL,
  `ledger_alias` varchar(255) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `opening_balance` decimal(10,2) DEFAULT '0.00',
  `current_balance` decimal(10,2) DEFAULT '0.00',
  `addedby` int(11) DEFAULT NULL,
  `addeddate` datetime DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `modifieddate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ledger_master`
--

INSERT INTO `ledger_master` (`ledger_id`, `company_id`, `ledger_name`, `ledger_alias`, `group_id`, `opening_balance`, `current_balance`, `addedby`, `addeddate`, `modifiedby`, `modifieddate`) VALUES
(1, 1, 'PETTY CASH', 'PETTY CASH', 21, '0.00', '1410.00', 1, '2017-08-05 11:08:34', NULL, NULL),
(2, 1, 'SGST @2.5', 'SGST @2.5', 27, '0.00', '-14750.00', 1, '2017-08-05 11:09:00', NULL, NULL),
(3, 1, 'CGST @2.5', 'CGST @2.5', 27, '0.00', '0.00', 1, '2017-08-05 11:09:26', NULL, NULL),
(4, 1, 'IGST @5.0', 'IGST @5.0', 27, '0.00', '1750.00', 1, '2017-08-05 11:09:49', NULL, NULL),
(5, 1, 'SGST @6.0', 'SGST @6.0', 27, '0.00', '900.00', 1, '2017-08-05 11:10:54', NULL, NULL),
(6, 1, 'CGST @6.0', 'CGST @6.0', 27, '0.00', '1200.00', 1, '2017-08-05 11:11:15', NULL, NULL),
(7, 1, 'IGST @12.0', 'IGST @12.0', 27, '0.00', '0.00', 1, '2017-08-05 11:12:04', NULL, NULL),
(8, 1, 'SGST @9.0', 'SGST @9.0', 27, '0.00', '-1350.00', 1, '2017-08-05 11:13:03', NULL, NULL),
(9, 1, 'CGST @9.0', 'CGST @9.0', 27, '0.00', '2250.00', 1, '2017-08-05 11:13:22', NULL, NULL),
(10, 1, 'IGST @18.0', 'IGST @18.0', 27, '0.00', '-4500.00', 1, '2017-08-05 11:13:43', NULL, NULL),
(11, 1, 'SGST @14.0', 'SGST @14.0', 27, '0.00', '8726.00', 1, '2017-08-05 11:14:19', NULL, NULL),
(12, 1, 'CGST @14.0', 'CGST @14.0', 27, '0.00', '5226.00', 1, '2017-08-05 11:14:34', NULL, NULL),
(13, 1, 'IGST @28.0', 'IGST @28.0', 27, '0.00', '-19320.00', 1, '2017-08-05 11:14:50', NULL, NULL),
(14, 1, 'SADHNA AUTOMOBILES', '', 33, '0.00', '86740.00', 1, '2017-08-12 14:27:45', NULL, NULL),
(15, 1, 'TALIBBHAI', '', 33, '0.00', '3776.00', 1, '2017-08-15 18:50:12', NULL, NULL),
(16, 1, 'SADIKBHAI', '', 33, '0.00', '47399.00', 1, '2017-08-15 18:51:23', NULL, NULL),
(17, 1, 'RAVIBHAI', '', 33, '0.00', '5255.00', 1, '2017-08-15 18:52:12', NULL, NULL),
(18, 1, 'KANTIBHAI', '', 33, '0.00', '1050.00', 1, '2017-08-15 18:53:33', NULL, NULL),
(19, 1, 'SAFIBHAI', '', 33, '0.00', '4558.00', 1, '2017-08-15 18:58:17', NULL, NULL),
(20, 1, 'RAFIKBHAI', '', 33, '0.00', '3529.00', 1, '2017-08-15 19:02:01', NULL, NULL),
(21, 1, 'JITENDRA JANI', '', 33, '0.00', '1863.00', 1, '2017-08-15 19:04:36', NULL, NULL),
(22, 1, 'RAFIKBHAI BATTERY', '', 33, '0.00', '0.00', 1, '2017-08-15 19:06:24', NULL, NULL),
(23, 1, 'VIJAYBHAI (PAKO)', '', 33, '0.00', '3873.00', 1, '2017-08-15 19:08:28', NULL, NULL),
(24, 1, 'RINKESHBHAI', '', 33, '0.00', '4928.00', 1, '2017-08-15 19:09:29', NULL, NULL),
(25, 1, 'AYUBBHAI', '', 33, '0.00', '775.00', 1, '2017-08-15 19:11:02', NULL, NULL),
(26, 1, 'RAVIBHAI NANDA', '', 33, '0.00', '5101.00', 1, '2017-08-15 19:13:02', NULL, NULL),
(27, 1, 'HARISH SALES CORPORATION', 'RAVI BHAI', 34, '0.00', '-145182.00', 1, '2017-08-15 19:18:02', NULL, NULL),
(28, 1, 'GHANSHYAM BATTERIES', 'RAJESHBHAI', 34, '0.00', '-177461.00', 1, '2017-08-15 19:20:03', NULL, NULL),
(29, 1, 'SHRI AKSHAY SOLAR', 'RAJ', 33, '0.00', '26880.00', 1, '2017-08-17 10:25:31', NULL, NULL),
(30, 1, 'Bank Accounts', 'Bank Account', 20, '0.00', '0.00', 1, '2017-08-28 09:42:12', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `payment_vouchers`
--

CREATE TABLE `payment_vouchers` (
  `pv_id` bigint(20) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `pv_no` varchar(255) DEFAULT NULL,
  `pv_date` date DEFAULT NULL,
  `pay_to` bigint(20) DEFAULT NULL,
  `pay_from` bigint(20) DEFAULT NULL,
  `tds_section` bigint(20) DEFAULT NULL,
  `payment_mode` varchar(255) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `cheque_no` varchar(255) DEFAULT NULL,
  `cheque_date` date DEFAULT NULL,
  `transaction_date` date DEFAULT NULL,
  `card_no` varchar(255) DEFAULT NULL,
  `transaction_ref` varchar(255) DEFAULT NULL,
  `payment_date` date DEFAULT NULL,
  `total_amount` decimal(10,2) DEFAULT '0.00',
  `total_tds_amount` decimal(10,2) DEFAULT '0.00',
  `pv_type` tinyint(4) DEFAULT '0' COMMENT '1- Against Invoice, 2-Advance Payment, 3-Other Expense',
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payment_voucher_items`
--

CREATE TABLE `payment_voucher_items` (
  `pvi_id` bigint(20) NOT NULL,
  `pv_id` bigint(20) DEFAULT NULL,
  `purchase_id` int(11) DEFAULT NULL,
  `tds_amount` decimal(10,2) DEFAULT '0.00',
  `amount` decimal(10,2) DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_item_master`
--

CREATE TABLE `purchase_item_master` (
  `purchase_item_id` int(11) NOT NULL,
  `purchase_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `item_name` varchar(255) DEFAULT NULL,
  `description` text,
  `hsn_code` varchar(255) DEFAULT NULL,
  `qty` decimal(10,0) DEFAULT '0',
  `unit` varchar(50) DEFAULT NULL,
  `rate` decimal(10,2) DEFAULT '0.00',
  `discount` decimal(10,0) DEFAULT '0',
  `amount` decimal(10,2) DEFAULT '0.00',
  `taxable_value` decimal(10,2) DEFAULT '0.00',
  `tax1_id` int(11) DEFAULT NULL,
  `tax1_rate` decimal(10,2) DEFAULT '0.00',
  `tax1_amount` decimal(10,2) DEFAULT '0.00',
  `tax2_id` int(11) DEFAULT NULL,
  `tax2_rate` decimal(10,2) DEFAULT '0.00',
  `tax2_amount` decimal(10,2) DEFAULT '0.00',
  `tax3_id` int(11) DEFAULT NULL,
  `tax3_rate` decimal(10,2) DEFAULT '0.00',
  `tax3_amount` decimal(10,2) DEFAULT '0.00',
  `total` decimal(10,2) DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `purchase_item_master`
--

INSERT INTO `purchase_item_master` (`purchase_item_id`, `purchase_id`, `item_id`, `item_name`, `description`, `hsn_code`, `qty`, `unit`, `rate`, `discount`, `amount`, `taxable_value`, `tax1_id`, `tax1_rate`, `tax1_amount`, `tax2_id`, `tax2_rate`, `tax2_amount`, `tax3_id`, `tax3_rate`, `tax3_amount`, `total`) VALUES
(1, 1, 1, 'WEBSITE', 'Test', 'H001', '1', 'NOs', '10000.00', '0', '10000.00', '10000.00', 2, '2.50', '250.00', 6, '6.00', '600.00', 7, '12.00', '1200.00', '12050.00'),
(2, 1, 2, 'SOFTWARE', 'Test', 'H002', '1', 'NOs', '15000.00', '0', '15000.00', '15000.00', 5, '6.00', '900.00', 9, '9.00', '1350.00', 4, '5.00', '750.00', '18000.00'),
(3, 1, 1, 'WEBSITE', 'Test', 'H001', '2', 'NOs', '10000.00', '0', '20000.00', '20000.00', 2, '2.50', '500.00', 6, '6.00', '1200.00', 4, '5.00', '1000.00', '22700.00');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_master`
--

CREATE TABLE `purchase_master` (
  `purchase_id` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `voucher_no` varchar(255) DEFAULT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `invoice_no` varchar(255) DEFAULT NULL,
  `invoice_date` date DEFAULT NULL,
  `challan_no` varchar(255) DEFAULT NULL,
  `challan_date` date DEFAULT NULL,
  `po_no` varchar(255) DEFAULT NULL,
  `po_date` date DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `vendor_name` varchar(255) DEFAULT NULL,
  `vendor_gstin` varchar(50) DEFAULT NULL,
  `vendor_state` varchar(50) DEFAULT NULL,
  `vendor_state_code` varchar(50) DEFAULT NULL,
  `total_amount_before_tax` decimal(10,2) DEFAULT '0.00',
  `discount` decimal(10,2) DEFAULT '0.00',
  `total_tax_amount` decimal(10,2) DEFAULT '0.00',
  `roundoff` decimal(10,2) DEFAULT '0.00',
  `grand_total` decimal(10,2) DEFAULT '0.00',
  `terms_conditions` text,
  `addedby` int(11) DEFAULT NULL,
  `addeddate` datetime DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `modifieddate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `purchase_master`
--

INSERT INTO `purchase_master` (`purchase_id`, `company_id`, `voucher_no`, `vendor_id`, `invoice_no`, `invoice_date`, `challan_no`, `challan_date`, `po_no`, `po_date`, `due_date`, `vendor_name`, `vendor_gstin`, `vendor_state`, `vendor_state_code`, `total_amount_before_tax`, `discount`, `total_tax_amount`, `roundoff`, `grand_total`, `terms_conditions`, `addedby`, `addeddate`, `modifiedby`, `modifieddate`) VALUES
(1, 1, '1', 28, '1', '2017-09-11', '1', '2017-09-10', '', '0000-00-00', '0000-00-00', 'GHANSHYAM BATTERIES', '24CJPPS4787LIZ6', 'GUJARAT', '24', '45000.00', '0.00', '7750.00', '0.00', '52750.00', '', 1, '2017-09-09 12:29:19', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `receipt_vouchers`
--

CREATE TABLE `receipt_vouchers` (
  `rv_id` bigint(20) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `rv_no` varchar(255) DEFAULT NULL,
  `rv_date` date DEFAULT NULL,
  `received_from` bigint(20) DEFAULT NULL,
  `received_to` bigint(20) DEFAULT NULL,
  `receipt_mode` varchar(255) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `cheque_no` varchar(255) DEFAULT NULL,
  `cheque_date` date DEFAULT NULL,
  `transaction_date` date DEFAULT NULL,
  `card_no` varchar(255) DEFAULT NULL,
  `transaction_ref` varchar(255) DEFAULT NULL,
  `payment_date` date DEFAULT NULL,
  `description` text,
  `total_amount` decimal(10,2) DEFAULT '0.00',
  `total_tds_amount` decimal(10,2) DEFAULT '0.00',
  `rv_type` tinyint(4) DEFAULT '0' COMMENT '1- Against Invoice, 2- Advance Receipt, 3- Other Income'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `receipt_voucher_items`
--

CREATE TABLE `receipt_voucher_items` (
  `rvi_id` bigint(20) NOT NULL,
  `rv_id` bigint(20) DEFAULT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `tds_amount` decimal(10,2) DEFAULT '0.00',
  `amount` decimal(10,2) DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_login_history`
--
ALTER TABLE `admin_login_history`
  ADD PRIMARY KEY (`login_id`),
  ADD KEY `admin_id` (`admin_id`);

--
-- Indexes for table `admin_master`
--
ALTER TABLE `admin_master`
  ADD PRIMARY KEY (`admin_id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `pin` (`pin`),
  ADD KEY `admin_type_id` (`admin_type_id`),
  ADD KEY `company_id` (`company_id`);

--
-- Indexes for table `admin_type_master`
--
ALTER TABLE `admin_type_master`
  ADD PRIMARY KEY (`admin_type_id`);

--
-- Indexes for table `banking_master`
--
ALTER TABLE `banking_master`
  ADD PRIMARY KEY (`banking_id`);

--
-- Indexes for table `company_master`
--
ALTER TABLE `company_master`
  ADD PRIMARY KEY (`company_id`);

--
-- Indexes for table `group_master`
--
ALTER TABLE `group_master`
  ADD PRIMARY KEY (`group_id`),
  ADD UNIQUE KEY `group_name` (`group_name`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Indexes for table `invoice_item_master`
--
ALTER TABLE `invoice_item_master`
  ADD PRIMARY KEY (`invoice_item_id`),
  ADD KEY `invoice_item_master_ibfk_1` (`invoice_id`),
  ADD KEY `item_id` (`item_id`);

--
-- Indexes for table `invoice_master`
--
ALTER TABLE `invoice_master`
  ADD PRIMARY KEY (`invoice_id`),
  ADD UNIQUE KEY `invoice_no` (`invoice_no`),
  ADD KEY `company_id` (`company_id`);

--
-- Indexes for table `item_master`
--
ALTER TABLE `item_master`
  ADD PRIMARY KEY (`item_id`),
  ADD UNIQUE KEY `item_name` (`item_name`),
  ADD KEY `company_id` (`company_id`);

--
-- Indexes for table `ledger_custom_fields`
--
ALTER TABLE `ledger_custom_fields`
  ADD PRIMARY KEY (`lcf_id`),
  ADD KEY `ledger_custom_fields_ibfk_1` (`group_id`);

--
-- Indexes for table `ledger_item_master`
--
ALTER TABLE `ledger_item_master`
  ADD PRIMARY KEY (`li_id`),
  ADD KEY `ledger_item_master_ibfk_1` (`ledger_id`),
  ADD KEY `ledger_item_master_ibfk_2` (`lcf_id`);

--
-- Indexes for table `ledger_master`
--
ALTER TABLE `ledger_master`
  ADD PRIMARY KEY (`ledger_id`),
  ADD UNIQUE KEY `ledger_name` (`ledger_name`),
  ADD KEY `ledger_master_ibfk_1` (`group_id`),
  ADD KEY `company_id` (`company_id`);

--
-- Indexes for table `payment_vouchers`
--
ALTER TABLE `payment_vouchers`
  ADD PRIMARY KEY (`pv_id`),
  ADD KEY `company_id` (`company_id`);

--
-- Indexes for table `payment_voucher_items`
--
ALTER TABLE `payment_voucher_items`
  ADD PRIMARY KEY (`pvi_id`),
  ADD KEY `pv_id` (`pv_id`);

--
-- Indexes for table `purchase_item_master`
--
ALTER TABLE `purchase_item_master`
  ADD PRIMARY KEY (`purchase_item_id`),
  ADD KEY `purchase_id` (`purchase_id`),
  ADD KEY `item_id` (`item_id`);

--
-- Indexes for table `purchase_master`
--
ALTER TABLE `purchase_master`
  ADD PRIMARY KEY (`purchase_id`),
  ADD KEY `company_id` (`company_id`);

--
-- Indexes for table `receipt_vouchers`
--
ALTER TABLE `receipt_vouchers`
  ADD PRIMARY KEY (`rv_id`),
  ADD KEY `company_id` (`company_id`);

--
-- Indexes for table `receipt_voucher_items`
--
ALTER TABLE `receipt_voucher_items`
  ADD PRIMARY KEY (`rvi_id`),
  ADD KEY `rv_id` (`rv_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_login_history`
--
ALTER TABLE `admin_login_history`
  MODIFY `login_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;
--
-- AUTO_INCREMENT for table `admin_master`
--
ALTER TABLE `admin_master`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `admin_type_master`
--
ALTER TABLE `admin_type_master`
  MODIFY `admin_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `banking_master`
--
ALTER TABLE `banking_master`
  MODIFY `banking_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `company_master`
--
ALTER TABLE `company_master`
  MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `group_master`
--
ALTER TABLE `group_master`
  MODIFY `group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `invoice_item_master`
--
ALTER TABLE `invoice_item_master`
  MODIFY `invoice_item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `invoice_master`
--
ALTER TABLE `invoice_master`
  MODIFY `invoice_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `item_master`
--
ALTER TABLE `item_master`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ledger_custom_fields`
--
ALTER TABLE `ledger_custom_fields`
  MODIFY `lcf_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;
--
-- AUTO_INCREMENT for table `ledger_item_master`
--
ALTER TABLE `ledger_item_master`
  MODIFY `li_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=386;
--
-- AUTO_INCREMENT for table `ledger_master`
--
ALTER TABLE `ledger_master`
  MODIFY `ledger_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `payment_vouchers`
--
ALTER TABLE `payment_vouchers`
  MODIFY `pv_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `payment_voucher_items`
--
ALTER TABLE `payment_voucher_items`
  MODIFY `pvi_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `purchase_item_master`
--
ALTER TABLE `purchase_item_master`
  MODIFY `purchase_item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `purchase_master`
--
ALTER TABLE `purchase_master`
  MODIFY `purchase_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `receipt_vouchers`
--
ALTER TABLE `receipt_vouchers`
  MODIFY `rv_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `receipt_voucher_items`
--
ALTER TABLE `receipt_voucher_items`
  MODIFY `rvi_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin_login_history`
--
ALTER TABLE `admin_login_history`
  ADD CONSTRAINT `admin_login_history_ibfk_1` FOREIGN KEY (`admin_id`) REFERENCES `admin_master` (`admin_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `admin_master`
--
ALTER TABLE `admin_master`
  ADD CONSTRAINT `admin_master_ibfk_1` FOREIGN KEY (`admin_type_id`) REFERENCES `admin_type_master` (`admin_type_id`),
  ADD CONSTRAINT `admin_master_ibfk_2` FOREIGN KEY (`company_id`) REFERENCES `company_master` (`company_id`);

--
-- Constraints for table `group_master`
--
ALTER TABLE `group_master`
  ADD CONSTRAINT `group_master_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `group_master` (`group_id`);

--
-- Constraints for table `invoice_item_master`
--
ALTER TABLE `invoice_item_master`
  ADD CONSTRAINT `invoice_item_master_ibfk_1` FOREIGN KEY (`invoice_id`) REFERENCES `invoice_master` (`invoice_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `invoice_item_master_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `item_master` (`item_id`);

--
-- Constraints for table `invoice_master`
--
ALTER TABLE `invoice_master`
  ADD CONSTRAINT `invoice_master_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `company_master` (`company_id`);

--
-- Constraints for table `item_master`
--
ALTER TABLE `item_master`
  ADD CONSTRAINT `item_master_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `company_master` (`company_id`);

--
-- Constraints for table `ledger_custom_fields`
--
ALTER TABLE `ledger_custom_fields`
  ADD CONSTRAINT `ledger_custom_fields_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `group_master` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ledger_item_master`
--
ALTER TABLE `ledger_item_master`
  ADD CONSTRAINT `ledger_item_master_ibfk_1` FOREIGN KEY (`ledger_id`) REFERENCES `ledger_master` (`ledger_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ledger_item_master_ibfk_2` FOREIGN KEY (`lcf_id`) REFERENCES `ledger_custom_fields` (`lcf_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ledger_master`
--
ALTER TABLE `ledger_master`
  ADD CONSTRAINT `ledger_master_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `group_master` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ledger_master_ibfk_2` FOREIGN KEY (`company_id`) REFERENCES `company_master` (`company_id`);

--
-- Constraints for table `payment_vouchers`
--
ALTER TABLE `payment_vouchers`
  ADD CONSTRAINT `payment_vouchers_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `company_master` (`company_id`);

--
-- Constraints for table `payment_voucher_items`
--
ALTER TABLE `payment_voucher_items`
  ADD CONSTRAINT `payment_voucher_items_ibfk_1` FOREIGN KEY (`pv_id`) REFERENCES `payment_vouchers` (`pv_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `purchase_item_master`
--
ALTER TABLE `purchase_item_master`
  ADD CONSTRAINT `purchase_item_master_ibfk_1` FOREIGN KEY (`purchase_id`) REFERENCES `purchase_master` (`purchase_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `purchase_item_master_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `item_master` (`item_id`);

--
-- Constraints for table `purchase_master`
--
ALTER TABLE `purchase_master`
  ADD CONSTRAINT `purchase_master_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `company_master` (`company_id`);

--
-- Constraints for table `receipt_vouchers`
--
ALTER TABLE `receipt_vouchers`
  ADD CONSTRAINT `receipt_vouchers_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `company_master` (`company_id`);

--
-- Constraints for table `receipt_voucher_items`
--
ALTER TABLE `receipt_voucher_items`
  ADD CONSTRAINT `receipt_voucher_items_ibfk_1` FOREIGN KEY (`rv_id`) REFERENCES `receipt_vouchers` (`rv_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
