<?php
ob_start();
$Page = "Invoice"; 
require_once('head.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Xenon ERP</title>
    
    <!-- Favicon -->
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="assets/plugins/bootstrap/dist/css/bootstrap.css" rel="stylesheet" />

    <!-- Bootstrap DateRangePicker Css -->
    <link href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />

    <!-- Animate.css Css -->
    <link href="assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Font Awesome Css -->
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

    <!-- iCheck Css -->
    <link href="assets/plugins/iCheck/skins/flat/_all.css" rel="stylesheet" />

    <!-- Switchery Css -->
    <link href="assets/plugins/switchery/dist/switchery.css" rel="stylesheet" />

    <!-- Metis Menu Css -->
    <link href="assets/plugins/metisMenu/dist/metisMenu.css" rel="stylesheet" />

    <!-- Pace Loader Css -->
    <link href="assets/plugins/pace/themes/white/pace-theme-flash.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="assets/css/style.css" rel="stylesheet" />
    
</head>
<body>
    <div class="all-content-wrapper">
        <!-- Top Bar -->
        <header>
            <?php include_once('header.php'); ?>
        </header>
        <?php
            if(isset($_GET['invoice_id']))
            {
                $InvoiceID = $_GET['invoice_id'];
                $SelectInvoice = "SELECT * FROM invoice_master WHERE invoice_id='".$InvoiceID."'";
                $SelectInvoiceQuery = mysqli_query($con,$SelectInvoice);
                if(!$SelectInvoiceQuery)
                {
                    //die(mysqli_error($con));
                    header("Location: view_all_invoice.php");
                    exit();
                }
                $count = mysqli_num_rows($SelectInvoiceQuery);
                if($count != 1)
                {
                    header("Location: view_all_invoice.php");
                    exit();
                }
                
                $Invoice = mysqli_fetch_array($SelectInvoiceQuery);
                
                $InvoiceID = $Invoice['invoice_id'];
                $InvoiceNo = $Invoice['invoice_no'];
                $ClientID = $Invoice['client_id'];
                $RecordDate = date("d-m-Y",strtotime($Invoice['record_date']));
                $DueDate = date("d-m-Y",strtotime($Invoice['due_date']));
                $PoRef = $Invoice['po_ref'];
                $PlaceOfSupply = $Invoice['place_of_supply'];
                $TotalAmountBeforeTax = $Invoice['subtotal'];
                $Discount = $Invoice['discount'];
                $TotalTaxAmount = $Invoice['totak_tax_amount'];
                $GrandTotal = $Invoice['grand_total'];
                $TermsCondition = $Invoice['terms_conditions'];

                $BillingGSTIN = $Invoice['billing_gstin'];
                $BillingAddress = $Invoice['billing_address'];
                $BillingCity = $Invoice['billing_city'];
                $BillingState = $Invoice['billing_state'];
                $BillingStateCode = $Invoice['billing_state_code'];
                $BillingCountry = $Invoice['billing_country'];
                $BillingPinCode = $Invoice['billing_pin_code'];
                $ShippingGSTIN = $Invoice['shipping_gstin'];
                $ShippingAddress = $Invoice['shipping_address'];
                $ShippingCity = $Invoice['shipping_city'];
                $ShippingState = $Invoice['shipping_state'];
                $ShippingStateCode = $Invoice['shipping_state_code'];
                $ShippingCountry = $Invoice['shipping_country'];
                $ShippingPinCode = $Invoice['shipping_pin_code'];
                $TransporterName = $Invoice['transporter_name'];
                $TransporterAddress = $Invoice['transporter_address'];
                $TransporterState = $Invoice['transporter_state'];
                $TransporterStateCode = $Invoice['transporter_state_code'];
                $TransporterGSTIN = $Invoice['transporter_gstin'];
                $TransporterPhone = $Invoice['transporter_phone'];

                $ClientDetail = $ledgerObject->selectClientDetail($ClientID);
                $ClientDetail = json_decode($ClientDetail,true);
                $ClientName = $ClientDetail['ClientName'];

                $AddedBy = '';
                $AddedDate = '';
                $ModifiedBy = '';
                $ModifiedDate = '';

                if(!empty($Invoice['addedby']))
                {
                    $AddedBy = $profileObject->selectUserName($Invoice['addedby']);
                    $AddedDate = '( '.date("h:i:s A d-m-Y",strtotime($Invoice['addeddate'])).' )';
                }
                if(!empty($Invoice['modifiedby']))
                {
                    $ModifiedBy = $profileObject->selectUserName($Invoice['modifiedby']);
                    $ModifiedDate = '( '.date("h:i:s A d-m-Y",strtotime($Invoice['modifieddate'])).' )';
                }
            }
            else
            {
                header("Location: view_all_invoice.php");
                exit();
            }
        ?>
        <!-- #END# Top Bar -->
        <!-- Left Menu -->
        <aside class="sidebar">
            <?php include_once('menu.php'); ?>
        </aside>
        <!-- #END# Left Menu -->
        <section class="content">
            <div class="page-heading">
                <h1>Invoice Details</h1>
                <ol class="breadcrumb">
                    <li><a href="dashboard.php">Home</a></li>
                    <li><a href="view_all_invoice.php">Invoice</a></li>
                    <li class="active">Invoice Details</li>
                </ol>
            </div>
            
            <div class="page-body">
                <!-- -------------- ERROR SECTION START -------------- -->
                        
                <div id="flash" class="alert alert hidden">
                    <strong>
                        <i class="ace-icon fa fa-spinner"></i>
                    </strong>
                    &nbsp; &nbsp;
                    <span></span>
                </div>
                
                <!-- -------------- ERROR SECTION END -------------- -->
            
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Invoice Details</div>
                            <div class="panel-body">
                                <div class="row">
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <th width="20%">Client Name</th>
                                                <td width="30%"><?php echo $ClientName; ?></td>
                                                <th width="20%">PO Ref.</th>
                                                <td width="30%"><?php echo $PoRef; ?></td>
                                            </tr>
                                            <tr>
                                                <th width="20%">Invoice No</th>
                                                <td width="30%"><?php echo $InvoiceNo; ?></td>
                                                <th width="20%">Place Of Supply</th>
                                                <td width="30%"><?php echo $PlaceOfSupply; ?></td>
                                            </tr>
                                            <tr>
                                                <th width="20%">Record Date</th>
                                                <td width="30%"><?php echo $RecordDate; ?></td>
                                                <th width="20%">Terms & Condition</th>
                                                <td width="30%"><?php echo $TermsCondition; ?></td>
                                            </tr>
                                            <tr>
                                                <th width="20%">Due Date</th>
                                                <td width="30%"><?php echo $DueDate; ?></td>
                                            </tr>
                                            <tr>
                                                <th width="20%">AddedBy</th>
                                                <td width="30%"><?php echo $AddedBy; ?></td>
                                                <th width="20%">ModifiedBy</th>
                                                <td width="30%"><?php echo $ModifiedBy; ?></td>
                                            </tr>
                                            <tr>
                                                <th width="20%">AddedDate</th>
                                                <td width="30%"><?php echo $AddedDate; ?></td>
                                                <th width="20%">ModifiedDate</th>
                                                <td width="30%"><?php echo $ModifiedDate; ?></td>
                                            </tr>
                                            <tr>
                                                
                                            </tr>
                                            <tr>
                                                
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <label class="col-md-12 text-center" style="font-size: 17px;">Billing Details</label>
                                        </div>
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <th width="20%">Billing GSTIN</th>
                                                    <td width="30%"><?php echo $BillingGSTIN; ?></td>
                                                </tr>
                                                <tr>
                                                    <th width="20%">Billing Address</th>
                                                    <td width="30%"><?php echo $BillingAddress; ?></td>
                                                </tr>
                                                <tr>
                                                    <th width="20%">Billing City</th>
                                                    <td width="30%"><?php echo $BillingCity; ?></td>
                                                </tr>
                                                <tr>
                                                    <th width="20%">Billing State</th>
                                                    <td width="30%"><?php echo $BillingState; ?></td>
                                                </tr>
                                                <tr>
                                                    <th width="20%">Billing State Code</th>
                                                    <td width="30%"><?php echo $BillingStateCode; ?></td>
                                                </tr>
                                                <tr>
                                                    <th width="20%">Billing Country</th>
                                                    <td width="30%"><?php echo $BillingCountry; ?></td>
                                                </tr>
                                                <tr>
                                                    <th width="20%">Billing Pin Code</th>
                                                    <td width="30%"><?php echo $BillingPinCode; ?></td>
                                                </tr>
                                                <tr>
                                                    
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <label class="col-md-12 text-center" style="font-size: 17px;">Shipping Details</label>
                                        </div>
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <th width="20%">Shipping GSTIN</th>
                                                    <td width="30%"><?php echo $ShippingGSTIN; ?></td>
                                                </tr>
                                                <tr>
                                                    <th width="20%">Shipping Address</th>
                                                    <td width="30%"><?php echo $ShippingAddress; ?></td>
                                                </tr>
                                                <tr>
                                                    <th width="20%">Shipping City</th>
                                                    <td width="30%"><?php echo $ShippingCity; ?></td>
                                                </tr>
                                                <tr>
                                                    <th width="20%">Shipping State</th>
                                                    <td width="30%"><?php echo $ShippingState; ?></td>
                                                </tr>
                                                <tr>
                                                    <th width="20%">Shipping State Code</th>
                                                    <td width="30%"><?php echo $ShippingStateCode; ?></td>
                                                </tr>
                                                <tr>
                                                    <th width="20%">Shipping Country</th>
                                                    <td width="30%"><?php echo $ShippingCountry; ?></td>
                                                </tr>
                                                <tr>
                                                    <th width="20%">Shipping Pin Code</th>
                                                    <td width="30%"><?php echo $ShippingPinCode; ?></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="form-group">
                                            <label class="col-md-12 text-center" style="font-size: 17px;">Transporter Details</label>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                            <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <th width="20%">Transporter Name</th>
                                                        <td width="30%"><?php echo $TransporterName; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th width="20%">Transporter Address</th>
                                                        <td width="30%"><?php echo $TransporterAddress; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th width="20%">Transporter State</th>
                                                        <td width="30%"><?php echo $TransporterState; ?></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                            <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <th width="20%">Transporter State Code</th>
                                                        <td width="30%"><?php echo $TransporterStateCode; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th width="20%">Transporter GSTIN</th>
                                                        <td width="30%"><?php echo $TransporterGSTIN; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th width="20%">Transporter Phone</th>
                                                        <td width="30%"><?php echo $TransporterPhone; ?></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12">
                                        <table id="bill-table" class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th colspan="15" class="text-center" width="100%">Item Detail</th>
                                                </tr>
                                                <tr>
                                                    <th class="text-center">#</th>
                                                    <th class="text-center">Item Name</th>
                                                    <th class="text-center">Description</th>
                                                    <th class="text-center">Quantity</th>
                                                    <th class="text-center">Rate</th>
                                                    <th class="text-center">Amount</th>
                                                    <th class="text-center">Discount</th>
                                                    <th class="text-center">Taxable Value</th>
                                                    <th class="text-center">SGST Rate</th>
                                                    <th class="text-center">SGST Value</th>
                                                    <th class="text-center">CGST Rate</th>
                                                    <th class="text-center">CGST Value</th>
                                                    <th class="text-center">IGST Rate</th>
                                                    <th class="text-center">IGST Value</th>
                                                    <th class="text-center">Total</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                                $SelectInvoiceDetails = mysqli_query($con,"SELECT * FROM `invoice_item_master` WHERE `invoice_id`='".$InvoiceID."'");
                                                while($InvoiceDetail = mysqli_fetch_array($SelectInvoiceDetails))
                                                {
                                                    $InvoiceItemID = $InvoiceDetail['invoice_item_id'];
                                                    $ItemID = $InvoiceDetail['item_id'];
                                                    $Description = $InvoiceDetail['description'];
                                                    $Quantity = $InvoiceDetail['qty'];
                                                    $Rate = $InvoiceDetail['rate'];
                                                    $Discount1 = $InvoiceDetail['discount'];
                                                    $Amount = $InvoiceDetail['amount'];
                                                    $TaxableValue = $InvoiceDetail['taxable_value'];
                                                    $Tax1 = $InvoiceDetail['tax1_id'];
                                                    $Tax1Rate = $InvoiceDetail['tax1_rate'];
                                                    $Tax1Value = $InvoiceDetail['tax1_amount'];
                                                    $Tax2 = $InvoiceDetail['tax2_id'];
                                                    $Tax2Rate = $InvoiceDetail['tax2_rate'];
                                                    $Tax2Value = $InvoiceDetail['tax2_amount'];
                                                    $Tax3 = $InvoiceDetail['tax3_id'];
                                                    $Tax3Rate = $InvoiceDetail['tax3_rate'];
                                                    $Tax3Value = $InvoiceDetail['tax3_amount'];         
                                                    $Total = $InvoiceDetail['total'];
                                                    
                                                    $ItemDetail = $itemObject->selectItemDetail($ItemID);
                                                    $ItemDetail = json_decode($ItemDetail,true);
                                                    $ItemName = $ItemDetail['ItemName'];

                                            ?>
                                                <tr>
                                                    <td class="text-center"><?php echo $count; $count++; ?></td>
                                                    <td class="text-center"><?php echo $ItemName; ?></td>
                                                    <td class="text-center"><?php echo $Description; ?></td>
                                                    <td class="text-center"><?php echo $Quantity; ?></td>
                                                    <td class="text-center"><?php echo $Rate; ?></td>
                                                    <td class="text-center"><?php echo $Amount; ?></td>
                                                    <td class="text-center"><?php echo $Discount1; ?></td>
                                                    <td class="text-center"><?php echo $TaxableValue; ?></td>
                                                    <td class="text-center"><?php echo $Tax1Rate; ?></td>
                                                    <td class="text-center"><?php echo $Tax1Value; ?></td>
                                                    <td class="text-center"><?php echo $Tax2Rate; ?></td>
                                                    <td class="text-center"><?php echo $Tax2Value; ?></td>
                                                    <td class="text-center"><?php echo $Tax3Rate; ?></td>
                                                    <td class="text-center"><?php echo $Tax3Value; ?></td>
                                                    <td class="text-center"><?php echo $Total; ?></td>
                                                </tr>
                                            <?php
                                                }
                                            ?>  
                                                <tr>
                                                    <td colspan="11"></td>
                                                    <td colspan="3">Amount Before Tax</td>
                                                    <td class="text-center"><?php echo $TotalAmountBeforeTax; ?></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="11"></td>
                                                    <td colspan="3">Total Tax Amount</td>
                                                    <td class="text-center"><?php echo $TotalTaxAmount; ?></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="11"></td>
                                                    <td colspan="3">Discount</td>
                                                    <td class="text-center"><?php echo $Discount; ?></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="11"></td>
                                                    <td colspan="3">Grand Total</td>
                                                    <td class="text-center"><?php echo $GrandTotal; ?></td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="text-center">
                                        <a class="btn btn-sm btn-success" href="update_invoice.php?invoice_id=<?php echo $InvoiceID; ?>">
                                            <i class="fa fa-refresh bigger-110"></i>
                                            Update Invoice Detail
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Footer -->
        
        <!-- #END# Footer -->
    </div>

    <!-- Jquery Core Js -->
    <script src="assets/plugins/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Bootstrap DateRangePicker Js -->
    <script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Pace Loader Js -->
    <script src="assets/plugins/pace/pace.js"></script>

    <!-- Screenfull Js -->
    <script src="assets/plugins/screenfull/src/screenfull.js"></script>

    <!-- Metis Menu Js -->
    <script src="assets/plugins/metisMenu/dist/metisMenu.js"></script>

    <!-- Jquery Slimscroll Js -->
    <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    

    <!-- Custom Js -->
    <script src="assets/js/admin.js"></script>
    

    <script type="text/javascript">
        jQuery(function($) {
                                            
            $('.js-daterange-picker').daterangepicker({
                format: 'dd-mm-yyyy',
                autoclose:true
            });
            
        });
    </script>
    
    
</body>
</html>
<?php
ob_flush();
?>