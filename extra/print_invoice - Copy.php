<?php
ob_start();
$Page = "Invoice"; 
require_once('head.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Xenon ERP</title>
    
    <!-- Favicon -->
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="assets/plugins/bootstrap/dist/css/bootstrap.css" rel="stylesheet" />

    <!-- Bootstrap DateRangePicker Css -->
    <link href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />

    <!-- Animate.css Css -->
    <link href="assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Font Awesome Css -->
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

    <!-- iCheck Css -->
    <link href="assets/plugins/iCheck/skins/flat/_all.css" rel="stylesheet" />

    <!-- Switchery Css -->
    <link href="assets/plugins/switchery/dist/switchery.css" rel="stylesheet" />

    <!-- Metis Menu Css -->
    <link href="assets/plugins/metisMenu/dist/metisMenu.css" rel="stylesheet" />

    <!-- Pace Loader Css -->
    <link href="assets/plugins/pace/themes/white/pace-theme-flash.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="assets/css/style.css" rel="stylesheet" />

    <style type="text/css">
		.print_invoice {
			width:9.3in !important; 
			/*height:11.7in !important; */
		}
		
		.print_invoice table 
		{
			margin: 0mm 0mm 0mm 0mm;
			width: 100%;
			font-family:Arial, Helvetica, sans-serif;
		}
		.print_invoice table tr td, .print_invoice table tr th
		{
			width: 2% !important;
			font-weight:normal;
		}
		.b_l 
		{
			border-left:none !important;
		}
		.b_r 
		{
			border-right:none !important;
		}
		.b_b 
		{
			border-bottom:none !important;
		}
		.b_t 
		{
			border-top:none !important;
		}
    </style>
    <style type="text/css" media="print">
	@media print {
		@page {
			size: A4 portrait;
			margin:0mm 0mm 0mm 0mm;
			font-size:11px !important;
			font-family:Arial, Helvetica, sans-serif;
			color:#000;
		}
		.invoice_inner {
			
		}
		.bill_main
		{
			margin: 0mm 5mm 0mm 5mm;
			width: 94%;
			/*border: 1px solid #000;*/
			border-collapse:collapse;
			font-size:11px !important;
			font-family:Arial, Helvetica, sans-serif;
		}
		.bill_main tr td, .bill_main tr th
		{
			width: 2% !important;
			height: 15px;
			border: 1px solid #000;
			border-collapse:collapse;
			padding:3px 2px;;
			font-size:11px !important;
		}
		.bill_main tr th
		{
			font-weight:normal;
		}
		
		.bill_table_2
		{
			width: 100.25%;
			border: 0px solid #000;
			border-collapse:collapse;
			font-size:11px !important;
			font-family:Arial, Helvetica, sans-serif;
		}
		.bill_table_2 tr td, .bill_table_2 tr th
		{
			width: 2% !important;
			height: 15px;
			border: 1px solid #000;
			border-collapse:collapse;
			padding:3px 2px;;
			font-size:11px !important;
		}
		
		.b_l 
		{
			border-left:none !important;
		}
		.b_r 
		{
			border-right:none !important;
		}
		.b_b 
		{
			border-bottom:none !important;
		}
		.b_t 
		{
			border-top:none !important;
		}
		.my_l_h 
		{
			line-height: 16px !important;
		}
		.proforma 
		{
			display:none;
		}
	}
	</style>
    
</head>
<body>
    <div class="all-content-wrapper">
        <!-- Top Bar -->
        <header>
            <?php include_once('header.php'); ?>
        </header>
        <?php
            if(isset($_GET['invoice_id']))
            {
                $InvoiceID = $_GET['invoice_id'];
                $SelectInvoice = "SELECT * FROM invoice_master WHERE invoice_id='".$InvoiceID."'";
                $SelectInvoiceQuery = mysqli_query($con,$SelectInvoice);
                if(!$SelectInvoiceQuery)
                {
                    //die(mysqli_error($con));
                    header("Location: view_all_invoice.php");
                    exit();
                }
                $count = mysqli_num_rows($SelectInvoiceQuery);
                if($count != 1)
                {
                    header("Location: view_all_invoice.php");
                    exit();
                }
                
                $Invoice = mysqli_fetch_array($SelectInvoiceQuery);
                
                $InvoiceID = $Invoice['invoice_id'];
                $InvoiceNo = $Invoice['invoice_no'];
                $InvoiceDate = '';
                $ChallanNo = $Invoice['challan_no'];
                $ChallanDate = '';
                $PONo = $Invoice['po_no'];
                $PODate = '';
                $PlaceOfSupply = $Invoice['place_of_supply'];
                $TransportationMode = $Invoice['transportation_mode'];
                $VehicleNo = $Invoice['vehicle_no'];
                $DateOfSupply = '';
                $DueDate = '';
				
                $TotalAmountBeforeTax = $Invoice['total_amount_before_tax'];
                $Discount = $Invoice['discount'];
                $TotalTaxAmount = $Invoice['total_tax_amount'];
                $RoundOff = $Invoice['roundoff'];
                $GrandTotal = $Invoice['grand_total'];
                $TermsCondition = $Invoice['terms_conditions'];

                $ClientID = $Invoice['client_id'];
                $BillingName = $Invoice['billing_name'];
                $BillingGSTIN = $Invoice['billing_gstin'];
                $BillingAddress = $Invoice['billing_address'];
                $BillingCity = $Invoice['billing_city'];
                $BillingState = $Invoice['billing_state'];
                $BillingStateCode = $Invoice['billing_state_code'];
                $BillingCountry = $Invoice['billing_country'];
                $BillingPinCode = $Invoice['billing_pin_code'];
				
				$ShippingName = $Invoice['shipping_name'];
                $ShippingGSTIN = $Invoice['shipping_gstin'];
                $ShippingAddress = $Invoice['shipping_address'];
                $ShippingCity = $Invoice['shipping_city'];
                $ShippingState = $Invoice['shipping_state'];
                $ShippingStateCode = $Invoice['shipping_state_code'];
                $ShippingCountry = $Invoice['shipping_country'];
                $ShippingPinCode = $Invoice['shipping_pin_code'];
				
                $TransporterName = $Invoice['transporter_name'];
                $TransporterAddress = $Invoice['transporter_address'];
                $TransporterState = $Invoice['transporter_state'];
                $TransporterStateCode = $Invoice['transporter_state_code'];
                $TransporterGSTIN = $Invoice['transporter_gstin'];
                $TransporterPhone = $Invoice['transporter_phone'];
				
				$OCName = $Invoice['oc_name'];
				$OCAddress = $Invoice['oc_address'];
				$OCPhone = $Invoice['oc_phone'];
				$OCMobile = $Invoice['oc_mobile'];
				$OCEmail = $Invoice['oc_email'];
				$OCPANNo = $Invoice['oc_pan_no'];
				$OCGSTIN = $Invoice['oc_gstin'];
				$OCState = $Invoice['oc_state'];
				$OCStateCode = $Invoice['oc_state_code'];
				
				if(!empty($Invoice['invoice_date']) && $Invoice['invoice_date'] != '0000-00-00') { $InvoiceDate = date("d-m-Y",strtotime($Invoice['invoice_date'])); }
				if(!empty($Invoice['challan_date']) && $Invoice['challan_date'] != '0000-00-00') { $ChallanDate = date("d-m-Y",strtotime($Invoice['challan_date'])); }
				if(!empty($Invoice['po_date']) && $Invoice['po_date'] != '0000-00-00') { $PODate = date("d-m-Y",strtotime($Invoice['po_date'])); }
				if(!empty($Invoice['due_date']) && $Invoice['due_date'] != '0000-00-00') { $DueDate = date("d-m-Y",strtotime($Invoice['due_date'])); }
				if(!empty($Invoice['date_of_supply']) && $Invoice['date_of_supply'] != '0000-00-00') { $DateOfSupply = date("d-m-Y",strtotime($Invoice['date_of_supply'])); }
            }
            else
            {
                header("Location: view_all_invoice.php");
                exit();
            }
        ?>
        <!-- #END# Top Bar -->
        <!-- Left Menu -->
        <aside class="sidebar">
            <?php include_once('menu.php'); ?>
        </aside>
        <!-- #END# Left Menu -->
        <section class="content">
            <div class="page-heading">
                <h1>Invoice</h1>
                <ol class="breadcrumb">
                    <li><a href="view_all_invoice.php">Invoice</a></li>
                    <li><a href="update_invoice.php?invoice_id=<?php echo $InvoiceID; ?>">Update Invoice</li>
                    <li><a href="print_invoice.php?invoice_id=<?php echo $InvoiceID; ?>">Print Invoice</a></li>
                </ol>
            </div>
            
            <div class="page-body">
                <div class="panel panel-default">   
                    <div class="panel-body">
                    	<div class="row clearfix">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<button id="btnPrint1" class="btn btn-success">
									<i class="ace-icon fa fa-print bigger-125"></i>
									ORIGINAL FOR RECIPIENT
								</button>
								<button id="btnPrint2" class="btn btn-primary">
									<i class="ace-icon fa fa-print bigger-125"></i>
									DUPLICATE FOR TRANSPORTER 
								</button>
								<button id="btnPrint3" class="btn btn-info">
									<i class="ace-icon fa fa-print bigger-125"></i>
									TRIPLICATE FOR SUPPLIER 
								</button>
								<br /><br /><br />
								<div id="print_invoice" class="print_invoice">
                                	<div class="invoice_inner">
                                    	<table class="table table-bordered bill_main">
                                    		<tr>
                                                <td style="border:none;"></td><td style="border:none;"></td>
                                                <td style="border:none;"></td><td style="border:none;"></td>
                                                <td style="border:none;"></td><td style="border:none;"></td>
                                                <td style="border:none;"></td><td style="border:none;"></td>
                                                <td style="border:none;"></td><td style="border:none;"></td>
                                                <td style="border:none;"></td><td style="border:none;"></td>
                                                <td style="border:none;"></td><td style="border:none;"></td>
                                                <td style="border:none;"></td><td style="border:none;"></td>
                                                <td style="border:none;"></td><td style="border:none;"></td>
                                                <td style="border:none;"></td><td style="border:none;"></td>
                                                <td style="border:none;"></td><td style="border:none;"></td>
                                                <td style="border:none;"></td><td style="border:none;"></td>
                                                <td style="border:none;"></td><td style="border:none;"></td>
                                                <td style="border:none;"></td><td style="border:none;"></td>
                                                <td style="border:none;"></td><td style="border:none;"></td>
                                                <td style="border:none;"></td><td style="border:none;"></td>
                                                <td style="border:none;"></td><td style="border:none;"></td>
                                                <td style="border:none;"></td><td style="border:none;"></td>
                                                <td style="border:none;"></td><td style="border:none;"></td>
                                                <td style="border:none;"></td><td style="border:none;"></td>
                                                <td style="border:none;"></td><td style="border:none;"></td>
                                                <td style="border:none;"></td><td style="border:none;"></td>
                                                <td style="border:none;"></td><td style="border:none;"></td>
                                                <td style="border:none;"></td><td style="border:none;"></td>
                                                <td style="border:none;"></td><td style="border:none;"></td>
                                            </tr>
                                            <tr>
                                                <td id="InvoicePrintType" colspan="50" align="right" style="border:none; font-weight:bold;">ORIGINAL FOR RECIPIENT</td>
                                            </tr>
                                            <tr>
                                            	<td colspan="50" valign="top" style="text-align: center;">
                                                    <p style="text-transform:uppercase; font-weight:bold; font-size:24px !important; margin-top:0px; margin-bottom:5px;"><?php echo $OCName; ?></p>
                                                    <p style="margin:0px; font-size: 13px !important; margin-bottom: 5px;" class="my_l_h">
                                                        <?php echo $OCAddress; ?>
                                                        <br/>
                                                        <?php if(!empty($OCPhone)) { echo 'Phone : '.$OCPhone.' | '; } ?>
                                                        <?php if(!empty($OCMobile)) { echo 'Mobile : '.$OCMobile.' | '; } ?>
                                                        <?php if(!empty($OCEmail)) { echo 'Email : '.$OCEmail; } ?>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                            	<td colspan="50" style="text-align: center; font-size: 13px !important; font-weight: bold;">TAX INVOICE</td>
                                            </tr>
                                            <tr>
                                            	<td colspan="6" class="b_b b_t b_r" style="font-weight: bold;">Invoice No <span style="float: right;">:</span></td>
                                            	<td colspan="10" class="b_b b_t b_r b_l" style="font-weight: bold;"><?php echo $InvoiceNo; ?></td>
                                            	<td colspan="4" class="b_b b_t b_r b_l" style="font-weight: bold;">Dated <span style="float: right;">:</span></td>
                                            	<td colspan="8" class="b_b b_t b_l" style="font-weight: bold;"><?php echo $InvoiceDate; ?></td>
                                            	<td colspan="8" class="b_b b_t b_r">Transportation Mode <span style="float: right;">:</span></td>
                                            	<td colspan="14" class="b_b b_t b_l"><?php echo $TransportationMode ?></td>
                                            </tr>
                                            <tr>
                                            	<td colspan="6" class="b_b b_t b_r">Challan No <span style="float: right;">:</span></td>
                                            	<td colspan="10" class="b_b b_t b_r b_l"><?php echo $ChallanNo; ?></td>
                                            	<td colspan="4" class="b_b b_t b_r b_l">Dated <span style="float: right;">:</span></td>
                                            	<td colspan="8" class="b_b b_t b_l"><?php echo $ChallanDate; ?></td>
                                            	<td colspan="8" class="b_b b_t b_r">Vehicle No <span style="float: right;">:</span></td>
                                            	<td colspan="14" class="b_b b_t b_l"><?php echo $VehicleNo ?></td>
                                            </tr>
                                            <tr>
                                            	<td colspan="6" class="b_b b_t b_r">PO No <span style="float: right;">:</span></td>
                                            	<td colspan="10" class="b_b b_t b_r b_l"><?php echo $PONo; ?></td>
                                            	<td colspan="4" class="b_b b_t b_r b_l">Dated <span style="float: right;">:</span></td>
                                            	<td colspan="8" class="b_b b_t b_l"><?php echo $PODate; ?></td>
                                            	<td colspan="8" class="b_b b_t b_r">Place Of Supply <span style="float: right;">:</span></td>
                                            	<td colspan="14" class="b_b b_t b_l"><?php echo $PlaceOfSupply ?></td>
                                            </tr>
                                            <tr>
                                            	<td colspan="6" class="b_b b_t b_r">GSTIN <span style="float: right;">:</span></td>
                                            	<td colspan="22" class="b_b b_t b_l"><?php echo $OCGSTIN; ?></td>
                                            	<td colspan="8" class="b_b b_t b_r">Date Of Supply <span style="float: right;">:</span></td>
                                            	<td colspan="14" class="b_b b_t b_l"><?php echo $DateOfSupply ?></td>
                                            </tr>
                                            <tr>
                                            	<td colspan="6" class="b_b b_t b_r">State <span style="float: right;">:</span></td>
                                            	<td colspan="22" class="b_b b_t b_l"><?php echo $OCState; ?></td>
                                            	<td colspan="8" class="b_b b_t b_r">Due Date <span style="float: right;">:</span></td>
                                            	<td colspan="14" class="b_b b_t b_l"><?php echo $DueDate ?></td>
                                            </tr>
                                            <tr>
                                            	<td colspan="6" valign="top" class="b_b b_t b_r">State Code <span style="float: right;">:</span></td>
                                            	<td colspan="22" valign="top" class="b_b b_t b_l"><?php echo $OCStateCode; ?></td>
                                            	<td colspan="8" valign="top" class="b_b b_t b_r">Terms &amp; Condition <span style="float: right;">:</span></td>
                                            	<td colspan="14" valign="top" class="b_b b_t b_l"><?php echo $TermsCondition ?></td>
                                            </tr>
                                            <tr>
                                            	<td colspan="28" style="text-align: center; font-weight: bold;">Details of Recipient | Billed To</td>
                                            	<td colspan="22" style="text-align: center; font-weight: bold;">Details of Consignee | Shipped To</td>
                                            </tr>
                                            <tr>
                                            	<td colspan="6" valign="top" class="b_b b_r b_t">Name <span style="float: right;">:</span></td>
                                            	<td colspan="22" valign="top" class="b_b b_l b_t"><?php echo $BillingName; ?></td>
                                            	<td colspan="6" valign="top" class="b_b b_r b_t">Name <span style="float: right;">:</span></td>
                                            	<td colspan="16" valign="top" class="b_b b_l b_t"><?php echo $ShippingName; ?> </td>
                                            </tr>
                                            <tr>
                                            	<td colspan="6" valign="top" class="b_b b_r b_t">Address <span style="float: right;">:</span></td>
                                            	<td colspan="22" valign="top" class="b_b b_l b_t my_l_h">
                                            	<?php echo $BillingAddress; ?> <br/>
                                            	<?php echo $BillingCity; ?>
                                            	<?php echo $BillingCountry; ?>
                                            	<?php echo $BillingPinCode; ?>
                                            	</td>
                                            	<td colspan="6" valign="top" class="b_b b_r b_t">Address <span style="float: right;">:</span></td>
                                            	<td colspan="16" valign="top" class="b_b b_l b_t my_l_h">
                                            	<?php echo $ShippingAddress; ?>  <br/>
                                            	<?php echo $ShippingCity; ?>
                                            	<?php echo $ShippingCountry; ?>
                                            	<?php echo $ShippingPinCode; ?>
                                            	</td>
                                            </tr>
                                            <tr>
                                            	<td colspan="6" valign="top" class="b_b b_r b_t">State <span style="float: right;">:</span></td>
                                            	<td colspan="10" valign="top" class="b_b b_l b_r b_t"><?php echo $BillingState; ?> </td>
                                            	<td colspan="12" valign="top" class="b_b b_l b_t">State Code : <?php echo $BillingStateCode; ?></td>
                                            	
                                            	<td colspan="6" valign="top" class="b_b b_r b_t">State <span style="float: right;">:</span></td>
                                            	<td colspan="6" valign="top" class="b_b b_l b_r b_t"><?php echo $ShippingState; ?> </td>
                                            	<td colspan="10" valign="top" class="b_b b_l b_t">State Code : <?php echo $BillingStateCode; ?></td>
                                            </tr>
                                            <tr>
                                            	<td colspan="6" valign="top" class="b_b b_r b_t">GSTIN <span style="float: right;">:</span></td>
                                            	<td colspan="22" valign="top" class="b_b b_l b_t"><?php echo $BillingGSTIN; ?> </td>
                                            	<td colspan="6" valign="top" class="b_b b_r b_t">GSTIN <span style="float: right;">:</span></td>
                                            	<td colspan="16" valign="top" class="b_b b_l b_t"><?php echo $ShippingGSTIN; ?> </td>
                                            </tr>
												
                                            <tr>
                                            	<td colspan="2" rowspan="2" align="center" style="font-weight: bold;">Sr.No.</td>
                                            	<td colspan="8" rowspan="2" align="center" style="font-weight: bold;">Description of Goods</td>
                                            	<td colspan="4" rowspan="2" align="center" style="font-weight: bold;">HSN</td>
                                            	<td colspan="3" rowspan="2" align="center" style="font-weight: bold;">Qty</td>
                                            	<td colspan="2" rowspan="2" align="center" style="font-weight: bold;">Unit</td>
                                            	<td colspan="3" rowspan="2" align="center" style="font-weight: bold;">Rate</td>
                                            	<td colspan="4" rowspan="2" align="center" style="font-weight: bold;">Amount</td>
                                            	<td colspan="3" rowspan="2" align="center" style="font-weight: bold;">Disc</td>
                                            	<td colspan="4" rowspan="2" align="center" style="font-weight: bold;">Taxable Value</td>
                                            	<td colspan="4" align="center" style="font-weight: bold;">SGST</td>
                                            	<td colspan="4" align="center" style="font-weight: bold;">CGST</td>
                                            	<td colspan="4" align="center" style="font-weight: bold;">IGST</td>
                                            	<td colspan="5" rowspan="2" align="center" style="font-weight: bold;">Total</td>
                                            </tr>
                                            <tr>
                                            	<td colspan="2" align="center" style="font-weight: bold;">Rate</td>
                                            	<td colspan="2" align="center" style="font-weight: bold;">Amount</td>
                                            	<td colspan="2" align="center" style="font-weight: bold;">Rate</td>
                                            	<td colspan="2" align="center" style="font-weight: bold;">Amount</td>
                                            	<td colspan="2" align="center" style="font-weight: bold;">Rate</td>
                                            	<td colspan="2" align="center" style="font-weight: bold;">Amount</td>
                                            </tr>
                                            <?php
												$SelectGSTInvoiceItem = mysqli_query($con,"SELECT * FROM invoice_item_master WHERE invoice_id='".$InvoiceID."'");
												$Count = 0;
												$TotalSGST = 0;
												$TotalCGST = 0;
												$TotalIGST = 0;
												$TotalQty = 0;
												while($GSTInvoiceItem = mysqli_fetch_array($SelectGSTInvoiceItem))
												{
													$Count++;
													$InvoiceItemID = $GSTInvoiceItem['invoice_item_id'];
													$ItemID = $GSTInvoiceItem['item_id'];
													$ItemName = $GSTInvoiceItem['item_name'];
													$ItemSpecification = $GSTInvoiceItem['description'];
													$HSNCode = $GSTInvoiceItem['hsn_code'];
													$Unit = $GSTInvoiceItem['unit'];
													$Quantity = $GSTInvoiceItem['qty'];
													$Rate = $GSTInvoiceItem['rate'];
													$Amount = $GSTInvoiceItem['amount'];
													$Discount = $GSTInvoiceItem['discount'];
													$TaxableValue = $GSTInvoiceItem['taxable_value'];
													$Tax1 = $GSTInvoiceItem['tax1_id'];
													$Tax1Rate = $GSTInvoiceItem['tax1_rate'];
													$Tax1Value = $GSTInvoiceItem['tax1_amount'];
													$Tax2 = $GSTInvoiceItem['tax2_id'];
													$Tax2Rate = $GSTInvoiceItem['tax2_rate'];
													$Tax2Value = $GSTInvoiceItem['tax2_amount'];
													$Tax3 = $GSTInvoiceItem['tax3_id'];
													$Tax3Rate = $GSTInvoiceItem['tax3_rate'];
													$Tax3Value = $GSTInvoiceItem['tax3_amount'];
													$Total = $GSTInvoiceItem['total'];
											?>
                                            <tr>
                                            	<td colspan="2" valign="top" align="center" class="b_t b_b"><?php echo $Count; ?></td>
                                            	<td colspan="8" valign="top" align="left" class="b_t b_b"><?php echo $ItemName; ?><br/><small><?php echo $ItemSpecification; ?></small> </td>
                                            	<td colspan="4" valign="top" align="center" class="b_t b_b"><?php echo $HSNCode; ?></td>
                                            	<td colspan="3" valign="top" align="center" class="b_t b_b"><?php echo $Quantity; ?></td>
                                            	<td colspan="2" valign="top" align="center" class="b_t b_b"><?php echo $Unit; ?></td>
                                            	<td colspan="3" valign="top" align="right" class="b_t b_b"><?php echo $Rate; ?></td>
                                            	<td colspan="4" valign="top" align="right" class="b_t b_b"><?php echo $Amount; ?></td>
                                            	<td colspan="3" valign="top" align="right" class="b_t b_b"><?php echo $Discount; ?></td>
                                            	<td colspan="4" valign="top" align="right" class="b_t b_b"><?php echo $TaxableValue; ?></td>
                                            	<td colspan="2" valign="top" align="right" class="b_t b_b"><?php echo $Tax1Rate; ?></td>
                                            	<td colspan="2" valign="top" align="right" class="b_t b_b"><?php echo $Tax1Value; ?></td>
                                            	<td colspan="2" valign="top" align="right" class="b_t b_b"><?php echo $Tax2Rate; ?></td>
                                            	<td colspan="2" valign="top" align="right" class="b_t b_b"><?php echo $Tax2Value; ?></td>
                                            	<td colspan="2" valign="top" align="right" class="b_t b_b"><?php echo $Tax3Rate; ?></td>
                                            	<td colspan="2" valign="top" align="right" class="b_t b_b"><?php echo $Tax3Value; ?></td>
                                            	<td colspan="5" valign="top" align="right" class="b_t b_b"><?php echo $Total; ?></td>
                                            </tr>
                                            <?php
													$TotalSGST += $Tax1Value;
													$TotalCGST += $Tax2Value;
													$TotalIGST += $Tax3Value;
													$TotalQty += $Quantity;
												}
	
												$Set = 10 - $Count;
												for($i=0;$i<$Set;$i++)
												{
												?>
											<tr>
                                            	<td colspan="2" valign="top" align="center" class="b_t b_b" style="height: 15px;"></td>
                                            	<td colspan="8" valign="top" align="center" class="b_t b_b"></td>
                                            	<td colspan="4" valign="top" align="center" class="b_t b_b"></td>
                                            	<td colspan="3" valign="top" align="center" class="b_t b_b"></td>
                                            	<td colspan="2" valign="top" align="center" class="b_t b_b"></td>
                                            	<td colspan="3" valign="top" align="center" class="b_t b_b"></td>
                                            	<td colspan="4" valign="top" align="center" class="b_t b_b"></td>
                                            	<td colspan="3" valign="top" align="center" class="b_t b_b"></td>
                                            	<td colspan="4" valign="top" align="center" class="b_t b_b"></td>
                                            	<td colspan="2" valign="top" align="center" class="b_t b_b"></td>
                                            	<td colspan="2" valign="top" align="center" class="b_t b_b"></td>
                                            	<td colspan="2" valign="top" align="center" class="b_t b_b"></td>
                                            	<td colspan="2" valign="top" align="center" class="b_t b_b"></td>
                                            	<td colspan="2" valign="top" align="center" class="b_t b_b"></td>
                                            	<td colspan="2" valign="top" align="center" class="b_t b_b"></td>
                                            	<td colspan="5" valign="top" align="center" class="b_t b_b"></td>
                                            </tr>
												<?php
												}
											?>
											<tr style="font-weight: bold;">
                                            	<td colspan="2" align="center"></td>
                                            	<td colspan="8" align="center"></td>
                                            	<td colspan="4" align="center">Total</td>
                                            	<td colspan="3" align="center"><?php echo $TotalQty; ?></td>
                                            	<td colspan="2" align="center"></td>
                                            	<td colspan="3" align="center"></td>
                                            	<td colspan="4" align="center"></td>
                                            	<td colspan="3" align="center"></td>
                                            	<td colspan="4" align="center"></td>
                                            	<td colspan="2" align="center"></td>
                                            	<td colspan="2" align="center"></td>
                                            	<td colspan="2" align="center"></td>
                                            	<td colspan="2" align="center"></td>
                                            	<td colspan="2" align="center"></td>
                                            	<td colspan="2" align="center"></td>
                                            	<td colspan="5" align="center"></td>
                                            </tr>
                                       		<tr>
                                       			<td colspan="50"></td>
                                       		</tr>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
               	</div>
            </div>
        </section>
        <!-- Footer -->
        
        <!-- #END# Footer -->
    </div>

    <!-- Jquery Core Js -->
    <script src="assets/plugins/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Pace Loader Js -->
    <script src="assets/plugins/pace/pace.js"></script>

    <!-- Screenfull Js -->
    <script src="assets/plugins/screenfull/src/screenfull.js"></script>

    <!-- Metis Menu Js -->
    <script src="assets/plugins/metisMenu/dist/metisMenu.js"></script>

    <!-- Jquery Slimscroll Js -->
    <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>    

    <!-- Custom Js -->
    <script src="assets/js/admin.js"></script>
    
    <script src="assets/js/jquery.print.js"></script>
    

    <script type="text/javascript">
        jQuery(function($) {
            
            $("#btnPrint1").click(function() {
				$("#InvoicePrintType").html('ORIGINAL FOR RECIPIENT');
				$("#print_invoice").print();
				return (false);
			});
			$("#btnPrint2").click(function() {
				$("#InvoicePrintType").html('DUPLICATE FOR TRANSPORTER');
				$("#print_invoice").print();
				return (false);
			});
			$("#btnPrint3").click(function() {
				$("#InvoicePrintType").html('TRIPLICATE FOR SUPPLIER');
				$("#print_invoice").print();
				return (false);
			});
        });
    </script>
    
    
</body>
</html>
<?php
ob_flush();
?>