<?php
ob_start();
$Page = "Form"; 
require_once('head.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Xenon ERP</title>
    
    <!-- Favicon -->
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="assets/plugins/bootstrap/dist/css/bootstrap.css" rel="stylesheet" />

    <!-- Animate.css Css -->
    <link href="assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Font Awesome Css -->
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

    <!-- iCheck Css Libraries | You can choose a theme from plugins/iCheck/skins instead of get all themes -->
    <link href="assets/plugins/iCheck/skins/minimal/_all.css" rel="stylesheet" />
    <link href="assets/plugins/iCheck/skins/square/_all.css" rel="stylesheet" />
    <link href="assets/plugins/iCheck/skins/flat/_all.css" rel="stylesheet" />

    <!-- Switchery Css -->
    <link href="assets/plugins/switchery/dist/switchery.css" rel="stylesheet" />

    <!-- Metis Menu Css -->
    <link href="assets/plugins/metisMenu/dist/metisMenu.css" rel="stylesheet" />

    <!-- Pace Loader Css -->
    <link href="assets/plugins/pace/themes/white/pace-theme-flash.css" rel="stylesheet" />

    <!-- Bootstrap Select Css -->
    <link href="assets/plugins/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" />

    <!-- DateTimePicker Css -->
    <link href="assets/plugins/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="assets/css/style.css" rel="stylesheet" />

    <!-- Summernote Css -->
    <link href="assets/plugins/summernote/dist/summernote.css" rel="stylesheet" />

</head>
<body>
    <div class="all-content-wrapper">
        <!-- Top Bar -->
        <header>
            <?php include_once('header.php'); ?>
        </header>
        <!-- #END# Top Bar -->
        <!-- Left Menu -->
        <aside class="sidebar">
            <?php include_once('menu.php'); ?>
        </aside>
        <!-- #END# Left Menu -->
        <section class="content">
            <div class="page-heading">
                <h1>
                    <a href="form.php">
                        Form
                    </a>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="dashboard.php">Home</a></li>
                    <li><a href="form.php">Form</a></li>
                    <li class="active">Formp</li>
                </ol>
            </div>
            
            <div class="page-body">
            <!-- -------------- ERROR SECTION START -------------- -->
                        
                <div id="flash" class="alert alert hidden">
                    <strong>
                        <i class="ace-icon fa fa-spinner"></i>
                    </strong>
                    &nbsp; &nbsp;
                    <span></span>
                </div>
            
            <!-- -------------- ERROR SECTION END -------------- -->
                <div class="row clearfix">
                    <!-- Horizontal Layout  -->
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Form</div>
                            <div class="panel-body p-b-25">
                                <form id="Add-Product-Form" method="post" class="form-horizontal" action="#">
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <input type="hidden" id="AdminID" name="AdminID" value="<?php echo $_SESSION['AdminID']; ?>" required />
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label"> Text Field</label>
                                            <div class="col-sm-6">
                                                <input type="text" id="Text" name="Text" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Password</label>
                                            <div class="col-sm-6">
                                                <input type="password" id="password" name="password" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label"> readonly</label>
                                            <div class="col-sm-6">
                                                <input type="text" id="Readonly" name="Readonly" class="form-control" placeholder="Readonly" readonly />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label"> Disabled</label>
                                            <div class="col-sm-6">
                                                <input type="text" placeholder="Disabled" class="form-control" disabled="disabled">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Url</label>
                                            <div class="col-sm-6">
                                                <input type="text" id="Url" name="Url" class="form-control" required />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Title</label>
                                            <div class="col-sm-6">
                                                <input type="text" id="Title" name="Title" class="form-control" required />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Status</label>
                                            <div class="col-sm-6">
                                                <select class="selectpicker form-control show-tick" id="Status" name="Status">
                                                    <option value="1">Active</option>
                                                    <option value="2">In Active</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label"> Popular </label>
                                            <div class="col-sm-2">
                                                <input type="checkbox" class="js-switch" id="Popular" name="Popular" value='1' data-size="small" />
                                            </div>
                                            <label class="col-sm-2 control-label"> Featured </label>
                                            <div class="col-sm-2">
                                                <input type="checkbox" class="js-switch" id="Featured" name="Featured" value='1' data-size="small" />
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label"> New Arrival </label>
                                            <div class="col-sm-6">
                                                <input type="checkbox" class="js-switch" id="NewArrival" name="NewArrival" value='1' data-size="small" />
                                            </div>
                                        </div>
                                        

                                    </div>

                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <div class="form-group note-group-select-from-files">
                                            <label class="col-sm-4 control-label"> Image</label>
                                            <div class="col-sm-6">
                                                <input class="note-image-input form-control" id="Image" name="Image" type="file" accept="image/*" placeholder="Image" required />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">TEXTAREA</label>
                                            <div class="col-sm-6">
                                                <textarea id="TEXTAREA" name="TEXTAREA" class="form-control no-resize" rows="5" required /></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Date</label>
                                            <div class="col-sm-6">
                                                <input type="text" id="Date" name="Date" placeholder="Please choose a date..." data-format="YYYY-MM-DD" class="form-control js-dtp">
                                                    <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Time</label>
                                            <div class="col-sm-6">
                                                <input type="text" id="Time" name="Time" placeholder="Please choose a time..." data-format="HH:mm" class="form-control js-dtp">
                                                <span class="glyphicon glyphicon-time form-control-feedback"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Date & Time</label>
                                            <div class="col-sm-6">
                                                <input type="text" id="DateTime" name="DateTime" placeholder="Please choose date &amp; time..." data-format="YYYY-MM-DD HH:mm" class="form-control js-dtp">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <label class="col-sm-2 control-label" for="Description"> Product Description</label>
                                        <div class="col-sm-10">
                                            <div class="summernote" id="Description"></div>
                                            <input type="hidden" id="ProductDescription" name="ProductDescription" required />
                                        </div>
                                    </div>
                                    <div>
                                        <div class="form-group">
                                            <div class="text-center">
                                                <button type="submit" class="btn btn-sm btn-success" onClick="return doSaveData();">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- #END# Horizontal Layout  -->
                </div>
            </div>
        </section>
        <!-- Footer -->
       
        <!-- #END# Footer -->
    </div>
        
    <!-- Jquery Core Js -->
    <script src="assets/plugins/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Pace Loader Js -->
    <script src="assets/plugins/pace/pace.js"></script>

    <!-- Screenfull Js -->
    <script src="assets/plugins/screenfull/src/screenfull.js"></script>

    <!-- Metis Menu Js -->
    <script src="assets/plugins/metisMenu/dist/metisMenu.js"></script>

    <!-- Jquery Slimscroll Js -->
    <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Switchery Js -->
    <script src="assets/plugins/switchery/dist/switchery.js"></script>

    <!-- iCheck Js -->
    <script src="assets/plugins/iCheck/icheck.js"></script>

    <!-- Autosize Js (Textarea auto growth plugin) -->
    <script src="assets/plugins/autosize/dist/autosize.js"></script>

    <!-- MomentJs Js -->
    <script src="assets/plugins/moment/moment.js"></script>

    <!-- DateTimePicker Js -->
    <script src="assets/plugins/eonasdan-bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js"></script>

    <!-- Bootstrap Select Js -->
    <script src="assets/plugins/bootstrap-select/dist/js/bootstrap-select.js"></script>

    <!-- Custom Js -->
    <script src="assets/js/admin.js"></script>
    <script src="assets/js/pages/forms/basic-form-elements.js"></script>

    <!-- Summernote Js -->
    <script src="assets/plugins/summernote/dist/summernote.js"></script>

    <script type="text/javascript">
       
        jQuery(document).ready(function(){
    
            $('#Description').summernote();
                            
       });
       
    </script>
    
    <script type="text/javascript">
        function doSaveData()
        {       
            Description = $('#Description').summernote('code');
            //Description = document.getElementById("Description").innerHTML; 
            //alert(Description);
            //Description = escape(Description);
            document.getElementById("ProductDescription").value = Description;
            return true;
        }   
    </script>
    <script>
        function NuMValidation(evt)
        {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
            return false;
    
            return true;
        } 
    </script>
        
</body>
</html>
<?php
ob_flush();
?>