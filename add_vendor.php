<?php
ob_start();
$Page = "AddVendor"; 
require_once('head.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Xenon ERP</title>
    
    <!-- Favicon -->
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="assets/plugins/bootstrap/dist/css/bootstrap.css" rel="stylesheet" />

    <!-- Animate.css Css -->
    <link href="assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Font Awesome Css -->
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

    <!-- iCheck Css Libraries | You can choose a theme from plugins/iCheck/skins instead of get all themes -->
    <link href="assets/plugins/iCheck/skins/minimal/_all.css" rel="stylesheet" />
    <link href="assets/plugins/iCheck/skins/square/_all.css" rel="stylesheet" />
    <link href="assets/plugins/iCheck/skins/flat/_all.css" rel="stylesheet" />

    <!-- Switchery Css -->
    <link href="assets/plugins/switchery/dist/switchery.css" rel="stylesheet" />

    <!-- Metis Menu Css -->
    <link href="assets/plugins/metisMenu/dist/metisMenu.css" rel="stylesheet" />

    <!-- Pace Loader Css -->
    <link href="assets/plugins/pace/themes/white/pace-theme-flash.css" rel="stylesheet" />

    <!-- Bootstrap Select Css -->
    <link href="assets/plugins/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" />

    <!-- DateTimePicker Css -->
    <link href="assets/plugins/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="assets/css/style.css" rel="stylesheet" />

</head>
<body class="ls-fixed navbar-fixed">
    <div class="all-content-wrapper">
        <!-- Top Bar -->
        <header>
            <?php include_once('header.php'); ?>
        </header>
        <!-- #END# Top Bar -->
        <!-- Left Menu -->
        <aside class="sidebar">
            <?php include_once('menu.php'); ?>
        </aside>
        <!-- #END# Left Menu -->
        <section class="content">
            <div class="page-heading">
                <h1>
                    <a href="view_all_vendor.php">
                        Vendor
                    </a>
                    <small>
                        <i class="fa fa-angle-double-right"></i>
                        <a class="font-bold" href="add_vendor.php">Add New Vendor</a>
                    </small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="dashboard.php">Home</a></li>
                    <li><a href="view_all_vendor.php">Vendor</a></li>
                </ol>
            </div>
            
            <div class="page-body">
            <!-- -------------- ERROR SECTION START -------------- -->
                        
                <div id="flash" class="alert alert hidden">
                    <strong>
                        <i class="fa fa-spinner fa-spin"></i>
                    </strong>
                    &nbsp; &nbsp;
                    <span></span>
                </div>
            
            <!-- -------------- ERROR SECTION END -------------- -->
                <div class="row clearfix">
                    <!-- Horizontal Layout  -->
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Add New Vendor</div>
                            <div class="panel-body p-b-25">
                                <form id="Add-Vendor-Form" method="post" class="form-horizontal" action="#">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                            <input type="hidden" id="AdminID" name="AdminID" value="<?php echo $_SESSION['AdminID']; ?>" required />
                                            <input type="hidden" id="CompanyID" name="CompanyID" value="<?php echo $CurrentCompanyID; ?>" required />
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Vendor Name</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="LedgerName" name="LedgerName" class="form-control" required />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Vendor Alias</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="LedgerAlias" name="LedgerAlias" class="form-control" required />
                                                    <input type="hidden" id="GroupID" name="GroupID" value="34" class="form-control" required />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Opening Balance</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="OpeningBalance" name="OpeningBalance" onKeyPress="return NuMValidation(event);" class="form-control" required />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">As On Date</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="LCF[34]" name="LCF[34]" placeholder="" data-format="DD-MM-YYYY" class="form-control js-dtp" required />
                                                        <span style="width: 50px; " class="glyphicon glyphicon-calendar form-control-feedback"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Moblie No</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="LCF[18]" name="LCF[18]" onKeyPress="return NuMValidation(event);" class="form-control" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Phone No</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="LCF[19]" name="LCF[19]" onKeyPress="return NuMValidation(event);" class="form-control" />
                                                </div>
                                            </div>
										</div>
                                     	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Email</label>
                                                <div class="col-sm-6">
                                                    <input type="email" id="LCF[20]" name="LCF[20]" class="form-control" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Website</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="LCF[21]" name="LCF[21]" class="form-control" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">PAN</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="LCF[23]" name="LCF[23]" class="form-control" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Payment Details</label>
                                                <div class="col-sm-6">
                                                    <textarea id="LCF[35]" name="LCF[35]" class="form-control no-resize" rows="5"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <label class="col-sm-9 control-label center" style="font-size: 15px;">Billing Details</label>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Billing Name</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="LCF[94]" name="LCF[94]" class="form-control b_name" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Billing GSTIN</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="LCF[22]" name="LCF[22]" class="form-control b_gstin" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Billing Address</label>
                                                <div class="col-sm-6">
                                                    <textarea id="LCF[24]" name="LCF[24]" class="form-control no-resize b_address" rows="5"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Billing City</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="LCF[25]" name="LCF[25]" class="form-control b_city" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Billing State</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="LCF[26]" name="LCF[26]" class="form-control b_state" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Billing State Code</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="LCF[78]" name="LCF[78]" class="form-control b_state_code" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Billing Country</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="LCF[27]" name="LCF[27]" class="form-control b_country" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Billing Pin Code</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="LCF[28]" name="LCF[28]" class="form-control b_pin_code" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <label class="col-sm-9 control-label no-pedding" style="font-size: 15px;">Shipping Details</label>
                                            </div>
                                            <div class="form-group">
												<label class="col-sm-5 control-label" for=""></label>
												<div class="col-sm-6">
													<div class="checkbox">
														<input type="checkbox" id="CopyBillingDetail" data-icheck-theme="minimal" data-icheck-color="grey">
														<label for="chb_4">Same As Billing Detail</label>
													</div>
												</div>
											</div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Shipping Name</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="LCF[95]" name="LCF[95]" class="form-control s_name" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Shipping GSTIN</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="LCF[75]" name="LCF[75]" class="form-control s_gstin" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Shipping Address</label>
                                                <div class="col-sm-6">
                                                   <textarea id="LCF[29]" name="LCF[29]" class="form-control no-resize s_address" rows="5"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Shipping City</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="LCF[30]" name="LCF[30]" class="form-control s_city" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Shipping State</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="LCF[31]" name="LCF[31]" class="form-control s_state" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Shipping State Code</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="LCF[79]" name="LCF[79]" class="form-control s_state_code" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Shipping Country</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="LCF[32]" name="LCF[32]" class="form-control s_country" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Shipping Pin Code</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="LCF[33]" name="LCF[33]" class="form-control s_pin_code" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">    
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="form-group">
                                                <div class="text-center">
                                                    <button type="submit" class="btn btn-success">
                                                        <i class="fa fa-check bigger-110"></i>
                                                        Submit
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- #END# Horizontal Layout  -->
                </div>
            </div>
        </section>
        <!-- Footer -->
       
        <!-- #END# Footer -->
    </div>
        
    <!-- Jquery Core Js -->
    <script src="assets/plugins/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Pace Loader Js -->
    <script src="assets/plugins/pace/pace.js"></script>

    <!-- Screenfull Js -->
    <script src="assets/plugins/screenfull/src/screenfull.js"></script>

    <!-- Metis Menu Js -->
    <script src="assets/plugins/metisMenu/dist/metisMenu.js"></script>

    <!-- Jquery Slimscroll Js -->
    <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Switchery Js -->
    <script src="assets/plugins/switchery/dist/switchery.js"></script>

    <!-- iCheck Js -->
    <script src="assets/plugins/iCheck/icheck.js"></script>

    <!-- Autosize Js (Textarea auto growth plugin) -->
    <script src="assets/plugins/autosize/dist/autosize.js"></script>

    <!-- MomentJs Js -->
    <script src="assets/plugins/moment/moment.js"></script>

    <!-- DateTimePicker Js -->
    <script src="assets/plugins/eonasdan-bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js"></script>

    <!-- Bootstrap Select Js -->
    <script src="assets/plugins/bootstrap-select/dist/js/bootstrap-select.js"></script>

    <!-- Custom Js -->
    <script src="assets/js/admin.js"></script>
<!--<script src="assets/js/pages/forms/basic-form-elements.js"></script>-->
   	
   	<script type="text/javascript">
        function NuMValidation(evt)
        {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
            return false;
    
            return true;
        } 
    </script>
    <script type="text/javascript">
        jQuery(function ($) 
        {
        	'use strict';
            $(document).ready(function () {
                //Init datetimepicker
                $('.js-dtp').each(function (i, key) {
                    var format = $(key).data('format');
                    $(key).datetimepicker({
                        format: format,
                        showClear: true
                    });
                });
				
				//Init checkboxes and radios
				$('input[data-icheck-theme]').each(function (i, key) {
					var color = $(key).data('icheckColor');
					var theme = $(key).data('icheckTheme');
					var baseCheckboxClass = 'icheckbox_' + theme;
					var baseRadioClass = 'iradio_' + theme;

					$(key).iCheck({
						checkboxClass: color === theme ? baseCheckboxClass : baseCheckboxClass + '-' + color,
						radioClass: color === theme ? baseRadioClass : baseRadioClass + '-' + color
					});
				});
				
				$("#CopyBillingDetail").on('ifClicked', function (event) 
				{
					var status = $("#CopyBillingDetail").prop('checked');
					
					if(status == false)
					{
						var BillingName = $(".b_name").val();
						var BillingGSTIN = $(".b_gstin").val();
						var BillingAddress = $(".b_address").val();
						var BillingCity = $(".b_city").val();
						var BillingState = $(".b_state").val();
						var BillingStateCode = $(".b_state_code").val();
						var BillingCountry = $(".b_country").val();
						var BillingPinCode = $(".b_pin_code").val();

						$(".s_name").val(BillingName);
						$(".s_gstin").val(BillingGSTIN);
						$(".s_address").val(BillingAddress);
						$(".s_city").val(BillingCity);
						$(".s_state").val(BillingState);
						$(".s_state_code").val(BillingStateCode);
						$(".s_country").val(BillingCountry);
						$(".s_pin_code").val(BillingPinCode);
					}
					else if(status == true)
					{
						$(".s_name").val('');
						$(".s_gstin").val('');
						$(".s_address").val('');
						$(".s_city").val('');
						$(".s_state").val('');
						$(".s_state_code").val('');
						$(".s_country").val('');
						$(".s_pin_code").val('');
					}
				});
            });
        });

    </script>

    <script type="text/javascript">
        $('#Add-Vendor-Form').on('submit', function(event) {
            
            event.preventDefault();

            var Action = 'AddLedger';

            //return false;
            var form_data = new FormData(this);
            form_data.append('Action',Action);
                            
            $("#flash").show();
            $("#flash i").addClass('fa-spinner');
            $("#flash i").addClass('fa-spin');
            $("#flash").removeClass('hidden');
            $('html,body').animate({ scrollTop: $(".content").offset().top},'slow');
            $("#flash span").html('Please Wait...');
            
            $.ajax({
                url: 'includes/ledger_script.php',
                type: 'POST',
                data: form_data,
                contentType: false,
                cache: false,
                processData:false,
                success: function(result)
                {
                    //alert(result);
                    var obj = JSON.parse(result);
                    var Status = obj.Status;
                    
                    if(Status == '2')
                    {
                        $("#flash").removeClass('alert alert-danger');
                        $("#flash").addClass('alert alert-success');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-times-circle');
                        $("#flash i").addClass('fa fa-fw fa-check-circle');
                        $("#flash span").html('Vendor Insert Successfully.');
                        document.getElementById("Add-Vendor-Form").reset();
                        $('#flash').delay(3000).fadeOut(500);
                        setTimeout(function() {
                          window.location.href = "add_vendor.php";
                        }, 1000);
                        return true;
                    }
                    else if(Status == '1')
                    {
                        $("#flash").removeClass('alert alert-success');
                        $("#flash").addClass('alert alert-danger');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash i").addClass('fa fa-fw fa-times-circle');
                        $("#flash span").html('Vendor Insert Not Successfully.');
                        $('#flash').delay(3000).fadeOut(500);
                        return true;
                    }
                    else if(Status == '0')
                    {
                        $("#flash").removeClass('alert alert-success');
                        $("#flash").addClass('alert alert-danger');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash i").addClass('fa fa-fw fa-times-circle');
                        $("#flash span").html('Vendor Already Exists.');
                        $('#flash').delay(3000).fadeOut(500);
                        return true;
                    }
                }
            });
        });
    </script>
        
</body>
</html>
<?php
ob_flush();
?>