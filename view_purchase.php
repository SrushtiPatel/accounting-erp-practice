<?php
ob_start();
$Page = "Purchase"; 
require_once('head.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Xenon ERP</title>
    
    <!-- Favicon -->
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="assets/plugins/bootstrap/dist/css/bootstrap.css" rel="stylesheet" />

    <!-- Bootstrap DateRangePicker Css -->
    <link href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />

    <!-- Animate.css Css -->
    <link href="assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Font Awesome Css -->
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

    <!-- iCheck Css -->
    <link href="assets/plugins/iCheck/skins/flat/_all.css" rel="stylesheet" />

    <!-- Switchery Css -->
    <link href="assets/plugins/switchery/dist/switchery.css" rel="stylesheet" />

    <!-- Metis Menu Css -->
    <link href="assets/plugins/metisMenu/dist/metisMenu.css" rel="stylesheet" />

    <!-- Pace Loader Css -->
    <link href="assets/plugins/pace/themes/white/pace-theme-flash.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="assets/css/style.css" rel="stylesheet" />
    
</head>
<body class="ls-fixed navbar-fixed">
    <div class="all-content-wrapper">
        <!-- Top Bar -->
        <header>
            <?php include_once('header.php'); ?>
        </header>
        <?php
            if(isset($_GET['purchase_id']))
            {
                $PurchaseID = $_GET['purchase_id'];
                $SelectPurchase = "SELECT * FROM purchase_master WHERE purchase_id='".$PurchaseID."'";
                $SelectPurchaseQuery = mysqli_query($con,$SelectPurchase);
                if(!$SelectPurchaseQuery)
                {
                    //die(mysqli_error($con));
                    header("Location: view_all_purchase.php");
                    exit();
                }
                $count = mysqli_num_rows($SelectPurchaseQuery);
                if($count != 1)
                {
                    header("Location: view_all_purchase.php");
                    exit();
                }
                
                $Purchase = mysqli_fetch_array($SelectPurchaseQuery);
                
                $PurchaseID = $Purchase['purchase_id'];
                $VoucherNo = $Purchase['voucher_no'];
                $VendorID = $Purchase['vendor_id'];
				$InvoiceNo = $Purchase['invoice_no'];
				$InvoiceDate = $Purchase['invoice_date'];
				$ChallanNo = $Purchase['challan_no'];
				$ChallanDate = $Purchase['challan_date'];
				$PONo = $Purchase['po_no'];
				$PODate = $Purchase['po_date'];
                $DueDate = $Purchase['due_date'];
               
				$TotalAmountBeforeTax = $Purchase['total_amount_before_tax'];	
                $Discount = $Purchase['discount'];
                $TotalTaxAmount = $Purchase['total_tax_amount'];
				$RoundOff = $Purchase['roundoff'];
                $GrandTotal = $Purchase['grand_total'];
                $TermsCondition = $Purchase['terms_conditions'];
				$Vendor_Name = $Purchase['vendor_name'];
				$Vendor_GSTIN = $Purchase['vendor_gstin'];
				$Vendor_State = $Purchase['vendor_state'];
				$Vendor_State_Code = $Purchase['vendor_state_code'];
				
				$VendorDetail = $ledgerObject->selectVendorDetail($VendorID);
				$VendorDetail = json_decode($VendorDetail,true);
				$VendorName = $VendorDetail['VendorName'];
				
                if(!empty($Purchase['invoice_date']) && $Purchase['invoice_date'] != '0000-00-00') { $InvoiceDate = date("d-m-Y",strtotime($Purchase['invoice_date'])); }
				if(!empty($Purchase['challan_date']) && $Purchase['challan_date'] != '0000-00-00') { $ChallanDate = date("d-m-Y",strtotime($Purchase['challan_date'])); }
				if(!empty($Purchase['po_date']) && $Purchase['po_date'] != '0000-00-00') { $PODate = date("d-m-Y",strtotime($Purchase['po_date'])); }
				if(!empty($Purchase['due_date']) && $Purchase['due_date'] != '0000-00-00') { $DueDate = date("d-m-Y",strtotime($Purchase['due_date'])); }
				

                $AddedBy = '';
                $AddedDate = '';
                $ModifiedBy = '';
                $ModifiedDate = '';

                if(!empty($Purchase['addedby']))
                {
                    $AddedBy = $profileObject->selectUserName($Purchase['addedby']);
                    $AddedDate = '( '.date("h:i:s A d-m-Y",strtotime($Purchase['addeddate'])).' )';
                }
                if(!empty($Purchase['modifiedby']))
                {
                    $ModifiedBy = $profileObject->selectUserName($Purchase['modifiedby']);
                    $ModifiedDate = '( '.date("h:i:s A d-m-Y",strtotime($Purchase['modifieddate'])).' )';
                }
            }
            else
            {
                header("Location: view_all_purchase.php");
                exit();
            }
        ?>
        <!-- #END# Top Bar -->
        <!-- Left Menu -->
        <aside class="sidebar">
            <?php include_once('menu.php'); ?>
        </aside>
        <!-- #END# Left Menu -->
        <section class="content">
            <div class="page-heading">
                <h1>Purchase Details</h1>
                <ol class="breadcrumb">
                    <li><a href="dashboard.php">Home</a></li>
                    <li><a href="view_all_purchase.php">Purchase</a></li>
                    <li class="active">Purchase Details</li>
                </ol>
            </div>
            
            <div class="page-body">
                <!-- -------------- ERROR SECTION START -------------- -->
                        
                <div id="flash" class="alert alert hidden">
                    <strong>
                        <i class="ace-icon fa fa-spinner"></i>
                    </strong>
                    &nbsp; &nbsp;
                    <span></span>
                </div>
                
                <!-- -------------- ERROR SECTION END -------------- -->
            
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Purchase Details</div>
								<div class="panel-body">
									<div class="row">
										<div class="col-xs-12 col-sm-6">
											<table class="table table-striped table-bordered">
												<tbody>
													<tr>
														<th width="40%">Voucher No</th>
														<td width="60%"><?php echo $VoucherNo; ?></td>
													</tr>
													<tr>
														<th>Vendor Name</th>
														<td><?php echo $VendorName; ?></td>
													</tr>
													<tr>
														<th>Invoice No</th>
														<td><?php echo $InvoiceNo; ?></td>
													</tr>
													<tr>
														<th>Invoice Date</th>
														<td><?php echo $InvoiceDate; ?></td>
													</tr>
													<tr>
														<th>Challan No</th>
														<td><?php echo $ChallanNo; ?></td>
													</tr>
													<tr>
														<th>Challan Date</th>
														<td><?php echo $ChallanDate; ?></td>
													</tr>
													<tr>
														<th>PO NO</th>
														<td><?php echo $PONo; ?></td>
													</tr>
												</tbody>
											</table>
										</div>
										<div class="col-xs-12 col-sm-6">
											<table class="table table-striped table-bordered">
												<tbody>
													<tr>
														<th width="40%">PO Date</th>
														<td width="60%"><?php echo $PODate; ?></td>
													</tr>
													<tr>
														<th>Due Date</th>
														<td><?php echo $DueDate; ?></td>
													</tr>
													<tr>
														<th>Terms & Condition</th>
														<td><?php echo $TermsCondition; ?></td>
													</tr>
													<tr>
														<th>Vendor Name</th>
														<td><?php echo $Vendor_Name; ?></td>
													</tr>
													<tr>
														<th>Vendor GSTIN</th>
														<td><?php echo $Vendor_GSTIN; ?></td>
													</tr>
													<tr>
														<th>Vendor State</th>
														<td><?php echo $Vendor_State; ?></td>
													</tr>
													<tr>
														<th>Vendor State Code</th>
														<td><?php echo $Vendor_State_Code; ?></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-sm-12">
											<table id="bill-table" class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th colspan="15" class="text-center" width="100%">Item Detail</th>
													</tr>
													<tr>
														<th width="5%" class="text-center">#</th>
														<th width="10%" class="text-center">Item Name</th>
														<th width="10%" class="text-center">Description</th>
														<th width="5%" class="text-center">Quantity</th>
														<th width="5%" class="text-center">Rate</th>
														<th width="5%" class="text-center">Amount</th>
														<th width="5%" class="text-center">Discount</th>
														<th width="5%" class="text-center">Taxable Value</th>
														<th width="10%" class="text-center">SGST Rate</th>
														<th width="5%" class="text-center">SGST Value</th>
														<th width="10%" class="text-center">CGST Rate</th>
														<th width="5%" class="text-center">CGST Value</th>
														<th width="10%" class="text-center">IGST Rate</th>
														<th width="5%" class="text-center">IGST Value</th>
														<th width="5%" class="text-center">Total</th>
													</tr>
												</thead>
												<tbody>
												<?php
													$SelectPurchaseDetails = mysqli_query($con,"SELECT * FROM `purchase_item_master` WHERE `purchase_id`='".$PurchaseID."'");
													while($PurDetail = mysqli_fetch_array($SelectPurchaseDetails))
													{
														$PurchaseItemID = $PurDetail['purchase_item_id'];
														$ItemID = $PurDetail['item_id'];
														$Description = $PurDetail['description'];
														$Quantity = $PurDetail['qty'];
														$Rate = $PurDetail['rate'];
														$Discount = $PurDetail['discount'];
														$Amount = $PurDetail['amount'];
														$TaxableValue = $PurDetail['taxable_value'];
														$Tax1 = $PurDetail['tax1_id'];
														$Tax1Rate = $PurDetail['tax1_rate'];
														$Tax1Value = $PurDetail['tax1_amount'];
														$Tax2 = $PurDetail['tax2_id'];
														$Tax2Rate = $PurDetail['tax2_rate'];
														$Tax2Value = $PurDetail['tax2_amount'];
														$Tax3 = $PurDetail['tax3_id'];
														$Tax3Rate = $PurDetail['tax3_rate'];
														$Tax3Value = $PurDetail['tax3_amount'];         
														$Total = $PurDetail['total'];

														$ItemDetail = $itemObject->selectItemDetail($ItemID);
														$ItemDetail = json_decode($ItemDetail,true);
														$ItemName = $ItemDetail['ItemName'];
														

												?>
													<tr>
														<td class="text-center"><?php echo $count; $count++; ?></td>
														<td class="text-center"><?php echo $ItemName; ?></td>
														<td class="text-center"><?php echo $Description; ?></td>
														<td class="text-center"><?php echo $Quantity; ?></td>
														<td class="text-center"><?php echo $Rate; ?></td>
														<td class="text-center"><?php echo $Amount; ?></td>
														<td class="text-center"><?php echo $Discount; ?></td>
														<td class="text-center"><?php echo $TaxableValue; ?></td>
														<td class="text-center"><?php echo $Tax1Rate; ?></td>
														<td class="text-center"><?php echo $Tax1Value; ?></td>
														<td class="text-center"><?php echo $Tax2Rate; ?></td>
														<td class="text-center"><?php echo $Tax2Value; ?></td>
														<td class="text-center"><?php echo $Tax3Rate; ?></td>
														<td class="text-center"><?php echo $Tax3Value; ?></td>
														<td class="text-center"><?php echo $Total; ?></td>
													</tr>
												<?php
													}
												?>  
													<tr>
														<td colspan="11"></td>
														<td colspan="3">Amount Before Tax</td>
														<td class="text-center"><?php echo $TotalAmountBeforeTax; ?></td>
													</tr>
													<tr>
														<td colspan="11"></td>
														<td colspan="3">Total Tax Amount</td>
														<td class="text-center"><?php echo $TotalTaxAmount; ?></td>
													</tr>
													<tr>
														<td colspan="11"></td>
														<td colspan="3">Discount</td>
														<td class="text-center"><?php echo $Discount; ?></td>
													</tr>
													<tr>
														<td colspan="11"></td>
														<td colspan="3">RoundOff</td>
														<td class="text-center"><?php echo $RoundOff; ?></td>
													</tr>
													<tr>
														<td colspan="11"></td>
														<td colspan="3">Grand Total</td>
														<td class="text-center"><?php echo $GrandTotal; ?></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
									<div class="form-group">
										<div class="text-center">
											<a class="btn btn-sm btn-success" href="update_purchase.php?purchase_id=<?php echo $PurchaseID; ?>">
												<i class="fa fa-refresh bigger-110"></i>
												Update Purchase Detail
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
        <!-- Footer -->
        
        <!-- #END# Footer -->
    </div>

    <!-- Jquery Core Js -->
    <script src="assets/plugins/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Bootstrap DateRangePicker Js -->
    <script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Pace Loader Js -->
    <script src="assets/plugins/pace/pace.js"></script>

    <!-- Screenfull Js -->
    <script src="assets/plugins/screenfull/src/screenfull.js"></script>

    <!-- Metis Menu Js -->
    <script src="assets/plugins/metisMenu/dist/metisMenu.js"></script>

    <!-- Jquery Slimscroll Js -->
    <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    

    <!-- Custom Js -->
    <script src="assets/js/admin.js"></script>
    

    <script type="text/javascript">
        jQuery(function($) {
                                            
            $('.js-daterange-picker').daterangepicker({
                format: 'dd-mm-yyyy',
                autoclose:true
            });
            
        });
    </script>
    
    
</body>
</html>
<?php
ob_flush();
?>