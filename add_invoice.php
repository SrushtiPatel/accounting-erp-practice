<?php
ob_start();
$Page = "AddInvoice"; 
require_once('head.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Xenon ERP</title>
    
    <!-- Favicon -->
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="assets/plugins/bootstrap/dist/css/bootstrap.css" rel="stylesheet" />

    <!-- Animate.css Css -->
    <link href="assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Font Awesome Css -->
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

    <!-- iCheck Css Libraries | You can choose a theme from plugins/iCheck/skins instead of get all themes -->
    <link href="assets/plugins/iCheck/skins/minimal/_all.css" rel="stylesheet" />
    <link href="assets/plugins/iCheck/skins/square/_all.css" rel="stylesheet" />
    <link href="assets/plugins/iCheck/skins/flat/_all.css" rel="stylesheet" />

    <!-- Switchery Css -->
    <link href="assets/plugins/switchery/dist/switchery.css" rel="stylesheet" />

    <!-- Metis Menu Css -->
    <link href="assets/plugins/metisMenu/dist/metisMenu.css" rel="stylesheet" />

    <!-- Pace Loader Css -->
    <link href="assets/plugins/pace/themes/white/pace-theme-flash.css" rel="stylesheet" />

    <!-- Bootstrap Select Css -->
    <link href="assets/plugins/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" />

    <!-- DateTimePicker Css -->
    <link href="assets/plugins/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="assets/css/style.css" rel="stylesheet" />

</head>
<body class="ls-fixed navbar-fixed">
    <div class="all-content-wrapper">
        <!-- Top Bar -->
        <header>
            <?php include_once('header.php'); ?>
        </header>
        <!-- #END# Top Bar -->
        <!-- Left Menu -->
        <aside class="sidebar">
            <?php include_once('menu.php'); ?>
        </aside>
        <!-- #END# Left Menu -->
        <section class="content">
            <div class="page-heading">
                <h1>
                    <a href="view_all_invoice.php">
                        Invoice
                    </a>
                    <small>
                        <i class="fa fa-angle-double-right"></i>
                        <a class="font-bold" href="add_invoice.php">Add New Invoice</a>
                    </small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="dashboard.php">Home</a></li>
                    <li><a href="view_all_invoice.php">Invoice</a></li>
                </ol>
            </div>
            
            <div class="page-body">
            <!-- -------------- ERROR SECTION START -------------- -->
                        
                <div id="flash" class="alert alert hidden">
                    <strong>
                        <i class="fa fa-spinner fa-spin"></i>
                    </strong>
                    &nbsp; &nbsp;
                    <span></span>
                </div>
            
            <!-- -------------- ERROR SECTION END -------------- -->
                <div class="row clearfix">
                    <!-- Horizontal Layout  -->
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Add New Invoice</div>
                            <div class="panel-body p-b-25">
                                <form id="Add-Invoice-Form" method="post" class="form-horizontal" action="#">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                            <input type="hidden" id="AdminID" name="AdminID" value="<?php echo $_SESSION['AdminID']; ?>" required />
                                            <input type="hidden" id="CompanyID" name="CompanyID" value="<?php echo $CurrentCompanyID; ?>" required />
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Client</label>
                                                <div class="col-sm-6">
                                                    <select class="col-xs-10 col-md-8 selectpicker form-control show-tick" id="ClientID" name="ClientID">
                                                        <option value="-1">-- SELECT --</option>
                                                        <?php
                                                            $SelectClient = "SELECT * FROM ledger_master WHERE group_id='33'  ORDER BY group_id";
                                                            $SelectClientQuery = mysqli_query($con,$SelectClient);
                                                                                    
                                                            while($Client = mysqli_fetch_array($SelectClientQuery))
                                                            {
                                                                echo '<option value="'.$Client['ledger_id'].'">'.$Client['ledger_name'].'</option>';
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Invoice No</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="InvoiceNo" name="InvoiceNo" value="<?php echo $salesObject->nextInvoiceNo($CurrentCompanyID); ?>" class="form-control" required readonly />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Invoice Date</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="InvoiceDate" name="InvoiceDate" data-format="DD-MM-YYYY" class="form-control js-dtp" required />
													<span style="width: 50px;" class="glyphicon glyphicon-calendar form-control-feedback"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Challan No</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="ChallanNo" name="ChallanNo" class="form-control" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Challan Date</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="ChallanDate" name="ChallanDate" data-format="DD-MM-YYYY" class="form-control js-dtp" />
                                                    <span style="width: 50px;" class="glyphicon glyphicon-calendar form-control-feedback"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">PO No</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="PONo" name="PONo" class="form-control" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">PO Date</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="PODate" name="PODate" data-format="DD-MM-YYYY" class="form-control js-dtp" />
                                                    <span style="width: 50px;" class="glyphicon glyphicon-calendar form-control-feedback"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Due Date</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="DueDate" name="DueDate" data-format="DD-MM-YYYY" class="form-control js-dtp" />
                                                    <span style="width: 50px;" class="glyphicon glyphicon-calendar form-control-feedback"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Place Of Supply</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="PlaceOfSupply" name="PlaceOfSupply" class="form-control" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Transportation Mode</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="TransportationMode" name="TransportationMode" class="form-control" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Vehicle No</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="VehicleNo" name="VehicleNo" class="form-control" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Date Of Supply</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="DateOfSupply" name="DateOfSupply" data-format="DD-MM-YYYY" class="form-control js-dtp" />
                                                    <span style="width: 50px;" class="glyphicon glyphicon-calendar form-control-feedback"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Terms & Condition</label>
                                                <div class="col-sm-6">
                                                    <textarea id="TermsCondition" name="TermsCondition" class="form-control no-resize" rows="4" /></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <label class="col-sm-offset-5 col-sm-6 control-label" style="font-size: 14px; text-align: left;">Billing Details</label>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Billing Name</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="BillingName" name="BillingName" class="form-control" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Billing Mobile</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="BillingMobile" name="BillingMobile" class="form-control" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Billing GSTIN</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="BillingGSTIN" name="BillingGSTIN" class="form-control" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Billing Address</label>
                                                <div class="col-sm-6">
                                                    <textarea id="BillingAddress" name="BillingAddress" class="form-control no-resize" rows="5" /></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Billing City</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="BillingCity" name="BillingCity" class="form-control" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Billing State</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="BillingState" name="BillingState" class="form-control" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Billing State Code</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="BillingStateCode" name="BillingStateCode" class="form-control" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Billing Country</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="BillingCountry" name="BillingCountry" class="form-control" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Billing Pin Code</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="BillingPinCode" name="BillingPinCode" class="form-control" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <label class="col-sm-offset-5 col-sm-6 control-label" style="font-size: 14px; text-align: left;">Shipping Details</label>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Shipping Name</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="ShippingName" name="ShippingName" class="form-control" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Shipping GSTIN</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="ShippingGSTIN" name="ShippingGSTIN" class="form-control" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Shipping Address</label>
                                                <div class="col-sm-6">
                                                    <textarea id="ShippingAddress" name="ShippingAddress" class="form-control no-resize" rows="5" /></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Shipping City</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="ShippingCity" name="ShippingCity" class="form-control" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Shipping State</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="ShippingState" name="ShippingState" class="form-control" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Shipping State Code</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="ShippingStateCode" name="ShippingStateCode" class="form-control" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Shipping Country</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="ShippingCountry" name="ShippingCountry" class="form-control" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Shipping Pin Code</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="ShippingPinCode" name="ShippingPinCode" class="form-control" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                    	<div class="form-group">
											<label class="col-xs-12 col-sm-12 control-label" style="font-size: 14px; text-align: center;">Our Details</label>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
											<?php
												$CompanyDetail = $profileObject->selectCompanyProfile($CurrentCompanyID);
												$CompanyDetail = json_decode($CompanyDetail,true);
												$CompanyName = $CompanyDetail['CompanyName'];
												$Address = $CompanyDetail['Address'];
												$Phone = $CompanyDetail['Phone'];
												$Mobile = $CompanyDetail['Mobile'];
												$GSTIN = $CompanyDetail['GSTIN'];
												$State = $CompanyDetail['State'];
												$StateCode = $CompanyDetail['StateCode'];
												$Email = $CompanyDetail['Email'];
												$PanNo = $CompanyDetail['PanNo'];
												$BankName = $CompanyDetail['BankName'];
												$BankAcNo = $CompanyDetail['BankAcNo'];
												$BankIFSC = $CompanyDetail['BankIFSC'];
												$BankBranch = $CompanyDetail['BankBranch'];
											?>
											<div class="form-group">
												<label class="col-sm-5 control-label">Company Name</label>
												<div class="col-sm-6">
													<input type="text" id="OCName" name="OCName" value="<?php echo $CompanyName; ?>" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-5 control-label">Company Address</label>
												<div class="col-sm-6">
													<textarea id="OCAddress" name="OCAddress" class="form-control" rows="3" /><?php echo $Address; ?></textarea>
												</div>
											</div>											
											<div class="form-group">
												<label class="col-sm-5 control-label">Phone</label>
												<div class="col-sm-6">
													<input type="text" id="OCPhone" name="OCPhone" value="<?php echo $Phone; ?>" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-5 control-label">Mobile</label>
												<div class="col-sm-6">
													<input type="text" id="OCMobile" name="OCMobile" value="<?php echo $Mobile; ?>" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-5 control-label">Email</label>
												<div class="col-sm-6">
													<input type="text" id="OCEmail" name="OCEmail" value="<?php echo $Email; ?>" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-5 control-label">PAN No</label>
												<div class="col-sm-6">
													<input type="text" id="OCPANNo" name="OCPANNo" value="<?php echo $PanNo; ?>" class="form-control" />
												</div>
											</div>
										</div>
                                   		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                   			<div class="form-group">
												<label class="col-sm-5 control-label">GSTIN</label>
												<div class="col-sm-6">
													<input type="text" id="GSTIN" name="GSTIN" value="<?php echo $GSTIN; ?>" class="form-control" />
												</div>
											</div>
                                   			<div class="form-group">
												<label class="col-sm-5 control-label">State</label>
												<div class="col-sm-6">
													<input type="text" id="State" name="State" value="<?php echo $State; ?>" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-5 control-label">State Code</label>
												<div class="col-sm-6">
													<input type="text" id="StateCode" name="StateCode" value="<?php echo $StateCode; ?>" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-5 control-label">Bank Name</label>
												<div class="col-sm-6">
													<input type="text" id="OCBankName" name="OCBankName" value="<?php echo $BankName; ?>" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-5 control-label">Bank Ac No</label>
												<div class="col-sm-6">
													<input type="text" id="OCBankAcNo" name="OCBankAcNo" value="<?php echo $BankAcNo;; ?>" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-5 control-label">Bank IFSC</label>
												<div class="col-sm-6">
													<input type="text" id="OCBankIFSC" name="OCBankIFSC" value="<?php echo $BankIFSC; ?>" class="form-control" />
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-5 control-label">Bank Branch</label>
												<div class="col-sm-6">
													<input type="text" id="OCBankBranch" name="OCBankBranch" value="<?php echo $BankBranch; ?>" class="form-control" />
												</div>
											</div>
										</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th colspan="10" class="text-center" width="100%">Item Detail</th>
                                                    </tr>
                                                    <tr>
                                                        <th class="text-center" width="15%">Item Name</th>
                                                        <th class="text-center" width="17%">Description</th>
                                                        <th class="text-center" width="8%">Quantity</th>
                                                        <th class="text-center" width="8%">Rate</th>
                                                        <th class="text-center" width="5%">Discount</th>
                                                        <th class="text-center" width="9%">SGST</th>
                                                        <th class="text-center" width="9%">CGST</th>
                                                        <th class="text-center" width="9%">IGST</th>
                                                        <th class="text-center" width="12%">Total</th>
                                                        <th class="text-center" width="8%">#</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr data-item-index="0">
                                                        <td class="padding-0-impo">
                                                            <select id="ItemID[]" name="ItemID[]" class="form-control no-border item item_0">
                                                                <option value="-1">-- SELECT --</option>
                                                                <?php
                                                                    $SelectItem = mysqli_query($con,"SELECT * FROM item_master WHERE company_id='".$CurrentCompanyID."'");
                                                                    while($Item = mysqli_fetch_array($SelectItem))
                                                                    {
                                                                        echo '<option value="'.$Item['item_id'].'">'.$Item['item_name'].'</option>';
                                                                    }
                                                                ?>
                                                            </select>
                                                            <input type="hidden" id="ItemName[]" name="ItemName[]" />
                                                            <input type="hidden" id="HSNCode[]" name="HSNCode[]" />
                                                            <input type="hidden" id="Unit[]" name="Unit[]" />
                                                        </td>
                                                        <td class="padding-0-impo">
                                                            <input type="text" id="Description[]" name="Description[]" class="form-control no-border" />
                                                        </td>
                                                        <td class="padding-0-impo">
                                                            <input type="text" id="Quantity[]" name="Quantity[]" class="form-control no-border" onKeyPress="return NuMValidation(event);" onFocus="return grandTotal();" onKeyUp="return grandTotal();" required />
                                                        </td>
                                                        <td class="padding-0-impo">
                                                            <input type="text" id="Rate[]" name="Rate[]" class="form-control no-border" onKeyPress="return NuMValidation(event);" onFocus="return grandTotal();" onKeyUp="return grandTotal();" required />
                                                            <input type="hidden" id="Amount[]" name="Amount[]" class="amount" required readonly />
                                                        </td>
                                                        <td class="padding-0-impo">
                                                            <input type="text" id="Discount[]" name="Discount[]" onKeyPress="return NuMValidation(event);" onFocus="return grandTotal();" onKeyUp="return grandTotal();" class="form-control no-border" />
                                                            <input type="hidden" id="TaxableValue[]" name="TaxableValue[]" class="form-control no-border" required readonly />
                                                        </td>
                                                        <td class="padding-0-impo">
                                                            <select id="Tax1[]" name="Tax1[]" class="tax1 tax1_0 form-control no-border" onFocus="return grandTotal();">
                                                                <option value="-1">-- SELECT --</option>
                                                                <?php
                                                                    $SelectTax = mysqli_query($con,"SELECT l.ledger_id,l.ledger_name, li.li_value FROM ledger_master l JOIN ledger_item_master li ON l.ledger_id=li.ledger_id WHERE l.group_id='27' AND li.lcf_id='36' AND li.li_value='SGST' AND company_id='".$CurrentCompanyID."'");
                                                                    while($Tax = mysqli_fetch_array($SelectTax))
                                                                    {
																		$Taxdetail = $taxObject->getTaxDetail($Tax['ledger_id']);
																		$Taxdetail = json_decode($Taxdetail,true);
																		$TaxRate = $Taxdetail['TaxValue'];
                                                                        echo '<option value="'.$Tax['ledger_id'].'">'.$TaxRate.'</option>';
                                                                    }
                                                                ?>
                                                            </select>
                                                            <input type="hidden" id="Tax1Rate[]" name="Tax1Rate[]" />
                                                            <input type="hidden" id="Tax1Value[]" name="Tax1Value[]" />
                                                        </td>
                                                        <td class="padding-0-impo">
                                                            <select id="Tax2[]" name="Tax2[]" class="tax2 tax2_0 form-control no-border" onFocus="return grandTotal();">
                                                                <option value="-1">-- SELECT --</option>
                                                                <?php
                                                                    $SelectTax = mysqli_query($con,"SELECT l.ledger_id,l.ledger_name, li.li_value FROM ledger_master l JOIN ledger_item_master li ON l.ledger_id=li.ledger_id WHERE l.group_id='27' AND li.lcf_id='36' AND li.li_value='CGST' AND company_id='".$CurrentCompanyID."'");
                                                                    while($Tax = mysqli_fetch_array($SelectTax))
                                                                    {
																		$Taxdetail = $taxObject->getTaxDetail($Tax['ledger_id']);
																		$Taxdetail = json_decode($Taxdetail,true);
																		$TaxRate = $Taxdetail['TaxValue'];
                                                                        echo '<option value="'.$Tax['ledger_id'].'">'.$TaxRate.'</option>';
                                                                    }
                                                                ?>
                                                            </select>
                                                            <input type="hidden" id="Tax2Rate[]" name="Tax2Rate[]" />
                                                            <input type="hidden" id="Tax2Value[]" name="Tax2Value[]" />
                                                        </td>
                                                        <td class="padding-0-impo">
                                                            <select id="Tax3[]" name="Tax3[]" class="tax3 tax3_0 form-control no-border" onFocus="return grandTotal();">
                                                                <option value="-1">-- SELECT --</option>
                                                                <?php
                                                                    $SelectTax = mysqli_query($con,"SELECT l.ledger_id,l.ledger_name, li.li_value FROM ledger_master l JOIN ledger_item_master li ON l.ledger_id=li.ledger_id WHERE l.group_id='27' AND li.lcf_id='36' AND li.li_value='IGST' AND company_id='".$CurrentCompanyID."'");
                                                                    while($Tax = mysqli_fetch_array($SelectTax))
                                                                    {
																		$Taxdetail = $taxObject->getTaxDetail($Tax['ledger_id']);
																		$Taxdetail = json_decode($Taxdetail,true);
																		$TaxRate = $Taxdetail['TaxValue'];
                                                                        echo '<option value="'.$Tax['ledger_id'].'">'.$TaxRate.'</option>';
                                                                    }
                                                                ?>
                                                            </select> 
                                                            <input type="hidden" id="Tax3Rate[]" name="Tax3Rate[]" />
                                                            <input type="hidden" id="Tax3Value[]" name="Tax3Value[]" />
                                                        </td>
                                                        <td class="padding-0-impo">
                                                            <input type="text" id="Total[]" name="Total[]" class="form-control no-border" onFocus="return grandTotal();" readonly required/>
                                                        </td>
                                                        <td class="padding-0-impo align-center" style="padding-left: 5px !important; padding-top: 2px !important;">
                                                            <button type="button" class="btn btn-default btn-sm addButton"><i class="fa fa-plus"></i></button>
                                                        </td>
                                                    </tr>
                                                    <!-- Item Hide Start -->
                                                    <tr class="hidden" id="itemTemplate">
                                                        <td class="padding-0-impo center">
                                                            <select id="item_id" name="item_id" class="form-control no-border item">
                                                                <option value="-1">-- SELECT --</option>
                                                                <?php
                                                                    $SelectItem = mysqli_query($con,"SELECT * FROM item_master WHERE company_id='".$CurrentCompanyID."'");
                                                                    while($Item = mysqli_fetch_array($SelectItem))
                                                                    {
                                                                        echo '<option value="'.$Item['item_id'].'">'.$Item['item_name'].'</option>';
                                                                    }
                                                                ?>
                                                            </select>
                                                            <input type="hidden" id="item_name" name="item_name" />
                                                            <input type="hidden" id="hsn_code" name="hsn_code" />
                                                            <input type="hidden" id="unit" name="unit" />
                                                        </td>
                                                        <td class="padding-0-impo center">
                                                            <input type="text" id="description" name="description" class="form-control no-border" />
                                                        </td>
                                                        <td class="padding-0-impo center">
                                                            <input type="text" id="quantity" name="quantity" class="form-control no-border" onKeyPress="return NuMValidation(event);" onFocus="return grandTotal();" onKeyUp="return grandTotal();" />
                                                        </td>
                                                        <td class="padding-0-impo center">
                                                            <input type="text" id="rate" name="rate" class="form-control no-border" onKeyPress="return NuMValidation(event);" onFocus="return grandTotal();" onKeyUp="return grandTotal();" />
                                                            <input type="hidden" id="amount" name="amount" />
                                                        </td>
                                                        <td class="padding-0-impo center">
                                                            <input type="text" id="discount" name="discount" class="form-control no-border" onKeyPress="return NuMValidation(event);" onFocus="return grandTotal();" onKeyUp="return grandTotal();" />
                                                            <input type="hidden" id="taxable_value" name="taxable_value" />
                                                        </td>
                                                        <td class="padding-0-impo center">
                                                            <select id="tax1" name="tax1" class="tax1 form-control no-border" onFocus="return grandTotal();">
                                                                <option value="-1">-- SELECT --</option>
                                                                <?php
                                                                    $SelectTax = mysqli_query($con,"SELECT l.ledger_id,l.ledger_name, li.li_value FROM ledger_master l JOIN ledger_item_master li ON l.ledger_id=li.ledger_id WHERE l.group_id='27' AND li.lcf_id='36' AND li.li_value='SGST' AND company_id='".$CurrentCompanyID."'");
                                                                    while($Tax = mysqli_fetch_array($SelectTax))
                                                                    {
																		$Taxdetail = $taxObject->getTaxDetail($Tax['ledger_id']);
																		$Taxdetail = json_decode($Taxdetail,true);
																		$TaxRate = $Taxdetail['TaxValue'];
                                                                        echo '<option value="'.$Tax['ledger_id'].'">'.$TaxRate.'</option>';
                                                                    }
                                                                ?>
                                                            </select>
                                                            <input type="hidden" id="tax1rate" name="tax1rate" />
                                                            <input type="hidden" id="tax1value" name="tax1value" />
                                                        </td>
                                                        <td class="padding-0-impo center">
                                                            <select id="tax2" name="tax2" class="tax2 form-control no-border" onFocus="return grandTotal();">
                                                                <option value="-1">-- SELECT --</option>
                                                                <?php
                                                                    $SelectTax = mysqli_query($con,"SELECT l.ledger_id,l.ledger_name, li.li_value FROM ledger_master l JOIN ledger_item_master li ON l.ledger_id=li.ledger_id WHERE l.group_id='27' AND li.lcf_id='36' AND li.li_value='CGST' AND company_id='".$CurrentCompanyID."'");
                                                                    while($Tax = mysqli_fetch_array($SelectTax))
                                                                    {
																		$Taxdetail = $taxObject->getTaxDetail($Tax['ledger_id']);
																		$Taxdetail = json_decode($Taxdetail,true);
																		$TaxRate = $Taxdetail['TaxValue'];
                                                                        echo '<option value="'.$Tax['ledger_id'].'">'.$TaxRate.'</option>';
                                                                    }
                                                                ?>
                                                            </select>
                                                            <input type="hidden" id="tax2rate" name="tax2rate" />
                                                            <input type="hidden" id="tax2value" name="tax2value" />
                                                        </td>
                                                        <td class="padding-0-impo center">
                                                            <select id="tax3" name="tax3" class="tax3 form-control no-border" onFocus="return grandTotal();">
                                                                <option value="-1">-- SELECT --</option>
                                                                <?php
                                                                    $SelectTax = mysqli_query($con,"SELECT l.ledger_id,l.ledger_name, li.li_value FROM ledger_master l JOIN ledger_item_master li ON l.ledger_id=li.ledger_id WHERE l.group_id='27' AND li.lcf_id='36' AND li.li_value='IGST' AND company_id='".$CurrentCompanyID."'");
                                                                    while($Tax = mysqli_fetch_array($SelectTax))
                                                                    {
																		$Taxdetail = $taxObject->getTaxDetail($Tax['ledger_id']);
																		$Taxdetail = json_decode($Taxdetail,true);
																		$TaxRate = $Taxdetail['TaxValue'];
                                                                        echo '<option value="'.$Tax['ledger_id'].'">'.$TaxRate.'</option>';
                                                                    }
                                                                ?>
                                                            </select>
                                                            <input type="hidden" id="tax3rate" name="tax3rate" />
                                                            <input type="hidden" id="tax3value" name="tax3value" />
                                                        </td>
                                                        <td class="padding-0-impo center">
                                                            <input type="text" id="total" name="total" class="form-control no-border" onFocus="return grandTotal();" readonly required />
                                                        </td>
                                                        <td class="padding-0-impo align-center" style="padding-left: 5px !important; padding-top: 2px !important;">
                                                            <button type="button" class="btn btn-default btn-sm addButton"><i class="fa fa-plus"></i></button>
                                                            <button type="button" class="btn btn-default btn-sm removeButton"><i class="fa fa-minus"></i></button>
                                                        </td>
                                                    </tr>
                                                    <!-- Item Hide End -->
                                                    <tr>
                                                        <td colspan="5"></td>
                                                        <td colspan="3">Amount Before Tax</td>
                                                        <td colspan="2" class="padding-0-impo center">
                                                            <input type="text" id="TotalAmountBeforeTax" name="TotalAmountBeforeTax" class="form-control no-border" onFocus="return grandTotal();" onKeyUp="return grandTotal();" readonly required />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="5"></td>
                                                        <td colspan="3">Discount</td>
                                                        <td colspan="2" class="padding-0-impo center">
                                                            <input type="text" id="TotalDiscount" name="TotalDiscount" class="form-control no-border" onFocus="return grandTotal();" onKeyUp="return grandTotal();" readonly required />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="5"></td>
                                                        <td colspan="3">Total Tax Amount</td>
                                                        <td colspan="2" class="padding-0-impo center">
                                                            <input type="text" id="TotalTaxAmount" name="TotalTaxAmount" class="form-control no-border" onFocus="return grandTotal();" onKeyUp="return grandTotal();" readonly required />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="5"></td>
                                                        <td colspan="3">Round Off</td>
                                                        <td colspan="2" class="padding-0-impo center">
                                                            <input type="text" id="RoundOff" name="RoundOff" class="form-control no-border" onFocus="return grandTotal();" onKeyUp="return grandTotal();" readonly required />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="5"></td>
                                                        <td colspan="3">Grand Total</td>
                                                        <td colspan="2" class="padding-0-impo center">
                                                            <input type="text" id="GrandTotal" name="GrandTotal" class="form-control no-border" onFocus="return grandTotal();" onKeyUp="return grandTotal();" readonly required />
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="row">    
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="form-group">
                                                <div class="text-center">
                                                    <button type="submit" class="btn btn-success">
                                                        <i class="fa fa-check bigger-110"></i>
                                                        Submit
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- #END# Horizontal Layout  -->
                </div>
            </div>
        </section>
        <!-- Footer -->
       
        <!-- #END# Footer -->
    </div>
        
    <!-- Jquery Core Js -->
    <script src="assets/plugins/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Pace Loader Js -->
    <script src="assets/plugins/pace/pace.js"></script>

    <!-- Screenfull Js -->
    <script src="assets/plugins/screenfull/src/screenfull.js"></script>

    <!-- Metis Menu Js -->
    <script src="assets/plugins/metisMenu/dist/metisMenu.js"></script>

    <!-- Jquery Slimscroll Js -->
    <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Switchery Js -->
    <script src="assets/plugins/switchery/dist/switchery.js"></script>

    <!-- iCheck Js -->
    <script src="assets/plugins/iCheck/icheck.js"></script>

    <!-- Autosize Js (Textarea auto growth plugin) -->
    <script src="assets/plugins/autosize/dist/autosize.js"></script>

    <!-- MomentJs Js -->
    <script src="assets/plugins/moment/moment.js"></script>

    <!-- DateTimePicker Js -->
    <script src="assets/plugins/eonasdan-bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js"></script>

    <!-- Bootstrap Select Js -->
    <script src="assets/plugins/bootstrap-select/dist/js/bootstrap-select.js"></script>

    <!-- Custom Js -->
    <script src="assets/js/admin.js"></script>
    
   	<script>
        function NuMValidation(evt)
        {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
            return false;
    
            return true;
        } 
        function grandTotal()
        {
            var QtyArray = document.querySelectorAll("#Add-Invoice-Form input[id='Quantity[]']");
            var RateArray = document.querySelectorAll("#Add-Invoice-Form input[id='Rate[]']");
            var DiscountArray = document.querySelectorAll("#Add-Invoice-Form input[id='Discount[]']");
            var Tax1RateArray = document.querySelectorAll("#Add-Invoice-Form input[id='Tax1Rate[]']");
            var Tax2RateArray = document.querySelectorAll("#Add-Invoice-Form input[id='Tax2Rate[]']");
            var Tax3RateArray = document.querySelectorAll("#Add-Invoice-Form input[id='Tax3Rate[]']");
			
            var AmountArray = document.querySelectorAll("#Add-Invoice-Form input[id='Amount[]']");
            var TaxablevalueArray = document.querySelectorAll("#Add-Invoice-Form input[id='TaxableValue[]']");
            var TotalArray = document.querySelectorAll("#Add-Invoice-Form input[id='Total[]']");
            var Tax1ValueArray = document.querySelectorAll("#Add-Invoice-Form input[id='Tax1Value[]']");
            var Tax2ValueArray = document.querySelectorAll("#Add-Invoice-Form input[id='Tax2Value[]']");
            var Tax3ValueArray = document.querySelectorAll("#Add-Invoice-Form input[id='Tax3Value[]']");
                
            var TotalAmountBeforeTax = 0.00;
            var TotalDiscount = 0.00;
            var TotalTaxAmount = 0.00;
            var RounOff = 0.00;
            var GrandTotal2 = 0.00;
            var GrandTotal = 0.00;

            for (var i = 0; i < QtyArray.length; i++) 
            {
                var Qty = QtyArray[i].value; 
                var Rate = RateArray[i].value;
                var Discount = DiscountArray[i].value;
                var Tax1Rate = Tax1RateArray[i].value;
                var Tax2Rate = Tax2RateArray[i].value;
                var Tax3Rate = Tax3RateArray[i].value;
                
                if(Qty == '' || isNaN(Qty)) { Qty = 0.00; }
                if(Rate == '' || isNaN(Rate)) { Rate = 0.00; }
                if(Discount == '' || isNaN(Discount)) { Discount = 0.00; }
                
                if(Tax1Rate == '' || isNaN(Tax1Rate)) { Tax1Rate = 0.00; }
                if(Tax2Rate == '' || isNaN(Tax2Rate)) { Tax2Rate = 0.00; }
                if(Tax3Rate == '' || isNaN(Tax3Rate)) { Tax3Rate = 0.00; }
                
                var Amount = parseFloat(Qty) * parseFloat(Rate);
                Amount = Math.round(Amount);
                Amount = Amount.toFixed(2);
                AmountArray[i].value = Amount;
                
                var TaxableValue = parseFloat(Amount) - parseFloat(Discount);
                TaxableValue = Math.round(TaxableValue);
                TaxableValue = TaxableValue.toFixed(2);
                TaxablevalueArray[i].value = TaxableValue;
                                                       
                var Tax1Value = parseFloat(TaxableValue) * parseFloat(Tax1Rate) / 100;
                Tax1Value = Math.round(Tax1Value);
                Tax1Value = Tax1Value.toFixed(2);
                Tax1ValueArray[i].value = Tax1Value;
               	 
                var Tax2Value = parseFloat(TaxableValue) * parseFloat(Tax2Rate) / 100;
                Tax2Value = Math.round(Tax2Value);
                Tax2Value = Tax2Value.toFixed(2);
                Tax2ValueArray[i].value = Tax2Value;
                                                       
                var Tax3Value = parseFloat(TaxableValue) * parseFloat(Tax3Rate) / 100;
                Tax3Value = Math.round(Tax3Value);
                Tax3Value = Tax3Value.toFixed(2);
                Tax3ValueArray[i].value = Tax3Value;
                
                var Total = parseFloat(TaxableValue) + parseFloat(Tax1Value) + parseFloat(Tax2Value) + parseFloat(Tax3Value);
                Total = Math.round(Total);
                Total = Total.toFixed(2);
                TotalArray[i].value = Total;
                
                TotalAmountBeforeTax += parseFloat(Amount);
                TotalDiscount += parseFloat(Discount);
                TotalTaxAmount += parseFloat(Tax1Value) + parseFloat(Tax2Value) + parseFloat(Tax3Value);
                GrandTotal2 += parseFloat(Total);
            }
            TotalAmountBeforeTax = Math.round(TotalAmountBeforeTax);
            TotalAmountBeforeTax = TotalAmountBeforeTax.toFixed(2);
            $("#TotalAmountBeforeTax").val(TotalAmountBeforeTax);
            
			TotalDiscount = Math.round(TotalDiscount);
            TotalDiscount = TotalDiscount.toFixed(2);
            $("#TotalDiscount").val(TotalDiscount);
			
            TotalTaxAmount = Math.round(TotalTaxAmount);
            TotalTaxAmount = TotalTaxAmount.toFixed(2);
            $("#TotalTaxAmount").val(TotalTaxAmount);
            
            GrandTotal2 = GrandTotal2.toFixed(2);
            GrandTotal = Math.round(GrandTotal2);
            GrandTotal = GrandTotal.toFixed(2);
            
            var Roundoff = GrandTotal - GrandTotal2;
            Roundoff = Roundoff.toFixed(2);

            $("#RoundOff").val(Roundoff);
            $("#GrandTotal").val(GrandTotal);
            
            return true;
        }

    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            
            var itemIndex = 0;
            
            $('#Add-Invoice-Form')
            
            // Add button click handler
            .on('click', '.addButton', function() {
				
				var checkItemIDValue = checkItemID();
				if(checkItemIDValue == -1)
				{
					return false;
				}
				
                itemIndex++;
                var $template = $('#itemTemplate'),
                    $clone    = $template
                                    .clone()
                                    .removeClass('hidden')
                                    .removeAttr('id')
                                    .attr('data-item-index', itemIndex)
                                    .insertBefore($template);
    
                // Update the name attributes
                $clone
                    .find('[name="item_id"]').attr('name', "ItemID[]").end()
					.find('[name="item_name"]').attr('name', "ItemName[]").end()
					.find('[name="hsn_code"]').attr('name', "HSNCode[]").end()
					.find('[name="unit"]').attr('name', "Unit[]").end()
                    .find('[name="description"]').attr('name', "Description[]").end()
                    .find('[name="quantity"]').attr('name', "Quantity[]").end()
                    .find('[name="rate"]').attr('name', "Rate[]").end()
                    .find('[name="amount"]').attr('name', "Amount[]").end()
                    .find('[name="discount"]').attr('name', "Discount[]").end()
                    .find('[name="taxable_value"]').attr('name', "TaxableValue[]").end()
                    .find('[name="tax1"]').attr('name', "Tax1[]").end()
                    .find('[name="tax1rate"]').attr('name', "Tax1Rate[]").end()
                    .find('[name="tax1value"]').attr('name', "Tax1Value[]").end()
                    .find('[name="tax2"]').attr('name', "Tax2[]").end()
                    .find('[name="tax2rate"]').attr('name', "Tax2Rate[]").end()
                    .find('[name="tax2value"]').attr('name', "Tax2Value[]").end()
                    .find('[name="tax3"]').attr('name', "Tax3[]").end()
                    .find('[name="tax3rate"]').attr('name', "Tax3Rate[]").end()
                    .find('[name="tax3value"]').attr('name', "Tax3Value[]").end()
                    .find('[name="total"]').attr('name', "Total[]").end()
                    .find('[id="item_id"]').attr('id', "ItemID[]").end()
					.find('[id="item_name"]').attr('id', "ItemName[]").end()
					.find('[id="hsn_code"]').attr('id', "HSNCode[]").end()
					.find('[id="unit"]').attr('id', "Unit[]").end()
                    .find('[id="description"]').attr('id', "Description[]").end()
                    .find('[id="quantity"]').attr('id', "Quantity[]").end()
                    .find('[id="rate"]').attr('id', "Rate[]").end()
                    .find('[id="amount"]').attr('id', "Amount[]").end()
                    .find('[id="discount"]').attr('id', "Discount[]").end()
                    .find('[id="taxable_value"]').attr('id', "TaxableValue[]").end()
                    .find('[id="tax1"]').attr('id', "Tax1[]").end()
                    .find('[id="tax1rate"]').attr('id', "Tax1Rate[]").end()
                    .find('[id="tax1value"]').attr('id', "Tax1Value[]").end()
                    .find('[id="tax2"]').attr('id', "Tax2[]").end()
                    .find('[id="tax2rate"]').attr('id', "Tax2Rate[]").end()
                    .find('[id="tax2value"]').attr('id', "Tax2Value[]").end()
                    .find('[id="tax3"]').attr('id', "Tax3[]").end()
                    .find('[id="tax3rate"]').attr('id', "Tax3Rate[]").end()
                    .find('[id="tax3value"]').attr('id', "Tax3Value[]").end()
                    .find('[id="total"]').attr('id', "Total[]").end()
                    .find('[name="Rate[]"]').attr('required', true).end()
                    .find('[name="Amount[]"]').attr('required', true).end()
					.find('[id="ItemID[]"]').addClass("item_"+itemIndex).end()
                    .find('[id="Tax1[]"]').addClass("tax1_"+itemIndex).end()
                    .find('[id="Tax2[]"]').addClass("tax2_"+itemIndex).end()
                    .find('[id="Tax3[]"]').addClass("tax3_"+itemIndex).end();
                    
                    grandTotal();
                    
            })
            // Remove button click handler
            .on('click', '.removeButton', function() {
                var $row  = $(this).parents('tr'),
                    index = $row.attr('data-item-index');
                        
                // Remove element containing the fields
                $row.remove();
                grandTotal();
            });
        });

        $('#Add-Invoice-Form')
            .on('change', '#ClientID', function() {
        
            var ClientID = $("#ClientID").val();
            
            if(ClientID == -1)
            {
                $("#Add-Invoice-Form #BillingName").val('');
				$("#Add-Invoice-Form #BillingMobile").val('');
                $("#Add-Invoice-Form #BillingGSTIN").val('');
                $("#Add-Invoice-Form #BillingAddress").val('');
                $("#Add-Invoice-Form #BillingCity").val('');
                $("#Add-Invoice-Form #BillingState").val('');
                $("#Add-Invoice-Form #BillingStateCode").val('');
                $("#Add-Invoice-Form #BillingCountry").val('');
                $("#Add-Invoice-Form #BillingPinCode").val('');
                $("#Add-Invoice-Form #ShippingName").val('');
                $("#Add-Invoice-Form #ShippingGSTIN").val('');
                $("#Add-Invoice-Form #ShippingAddress").val('');
                $("#Add-Invoice-Form #ShippingCity").val('');
                $("#Add-Invoice-Form #ShippingState").val('');
                $("#Add-Invoice-Form #ShippingStateCode").val('');
                $("#Add-Invoice-Form #ShippingCountry").val('');
                $("#Add-Invoice-Form #ShippingPinCode").val('');
                $("#Add-Invoice-Form #TransporterName").val('');
                $("#Add-Invoice-Form #TransporterAddress").val('');
                $("#Add-Invoice-Form #TransporterState").val('');
                $("#Add-Invoice-Form #TransporterStateCode").val('');
                $("#Add-Invoice-Form #TransporterGSTIN").val('');
                $("#Add-Invoice-Form #TransporterPhone").val('');
                $("#Add-Invoice-Form #ClientID").focus();
            }
            else
            {
                var Action = 'GetClientDetail';
                var dataString = 'Action='+ Action +'&ClientID='+ ClientID;
                //alert(dataString);

                $("#data_loader").show();
                $("#data_loader").fadeIn(400).html('<img src="assets/img/loading.gif" />');
                $.ajax({
                    type: "POST",
                    url: "includes/loader_functions.php",
                    data: dataString,
                    cache: false,
                    success: function(result)
                    {
                        //alert(result);
                        $("#data_loader").hide();
                        var obj = JSON.parse(result);
                                
                        $("#Add-Invoice-Form #BillingName").val(obj.BillingName);
						$("#Add-Invoice-Form #BillingMobile").val(obj.BillingMobile);
                        $("#Add-Invoice-Form #BillingGSTIN").val(obj.BillingGSTIN);
                        $("#Add-Invoice-Form #BillingAddress").val(obj.BillingAddress);
                        $("#Add-Invoice-Form #BillingCity").val(obj.BillingCity);
                        $("#Add-Invoice-Form #BillingState").val(obj.BillingState);
                        $("#Add-Invoice-Form #BillingStateCode").val(obj.BillingStateCode);
                        $("#Add-Invoice-Form #BillingCountry").val(obj.BillingCountry);
                        $("#Add-Invoice-Form #BillingPinCode").val(obj.BillingPinCode);
                        $("#Add-Invoice-Form #ShippingName").val(obj.ShippingName);
                        $("#Add-Invoice-Form #ShippingGSTIN").val(obj.ShippingGSTIN);
                        $("#Add-Invoice-Form #ShippingAddress").val(obj.ShippingAddress);
                        $("#Add-Invoice-Form #ShippingCity").val(obj.ShippingCity);
                        $("#Add-Invoice-Form #ShippingState").val(obj.ShippingState);
                        $("#Add-Invoice-Form #ShippingStateCode").val(obj.ShippingStateCode);
                        $("#Add-Invoice-Form #ShippingCountry").val(obj.ShippingCountry);
                        $("#Add-Invoice-Form #ShippingPinCode").val(obj.ShippingPinCode);
                        $("#Add-Invoice-Form #TransporterName").val(obj.TransporterName);
                        $("#Add-Invoice-Form #TransporterAddress").val(obj.TransporterAddress);
                        $("#Add-Invoice-Form #TransporterState").val(obj.TransporterState);
                        $("#Add-Invoice-Form #TransporterStateCode").val(obj.TransporterStateCode);
                        $("#Add-Invoice-Form #TransporterGSTIN").val(obj.TransporterGSTIN);
                        $("#Add-Invoice-Form #TransporterPhone").val(obj.TransporterPhone);
                        return true;
                    }
                });
            }
        });
        
        function checkItemID()
        {
            var checkItemID = '';
            $('select[name="ItemID[]"]').each(function() {
                var ItemID = $(this).val(); 
                if(ItemID < 1)
                {
                    checkItemID = ItemID;
                    $(this).css({ border: "1px solid #f2a696", color: "#d68273" });
                    $(this).focus;
                }
                else
                {
                    $(this).css({ border: "", color: "" });
                }
            });
            return checkItemID;
        }
		$('#Add-Invoice-Form').on( 'change', 'tbody td select.item', function () {
            
            var $row  = $(this).parents("tr"),
                index = $row.attr('data-item-index');
            

            var ItemID = $('.item_'+index).val();
			
            if(ItemID < 0)
            {
                $row.find('[id="ItemName[]"]').attr('value', "").end();
				$row.find('[id="HSNCode[]"]').attr('value', "").end();
				$row.find('[id="Unit[]"]').attr('value', "").end();
				$row.find('[id="Rate[]"]').attr('value', "").end();
                
                return false;
            }
            else
            {
                var Action = 'GetItemDetail';
                var dataString = 'Action='+ Action +'&ItemID='+ ItemID;
                $.ajax({
                    type: "POST",
                    url: "includes/loader_functions.php",
                    data: dataString,
                    cache: false,
                    success: function(result)
                    {
                        //alert(result);
                        var obj = JSON.parse(result);
                        $row.find('[id="ItemName[]"]').attr('value', obj.ItemName).end();
						$row.find('[id="HSNCode[]"]').attr('value', obj.HSNCode).end();
						$row.find('[id="Unit[]"]').attr('value', obj.ItemUnit).end();
						$row.find('[id="Rate[]"]').attr('value', obj.ItemRate).end();
                      
                        return true;
                    }
                });
            }
        });
        
        $('#Add-Invoice-Form').on( 'change', 'tbody td select.tax1', function () {
            
            var $row  = $(this).parents("tr"),
                index = $row.attr('data-item-index');
            

            var TaxID = $('.tax1_'+index).val();
            if(TaxID < 0)
            {
                $row.find('[id="Tax1Rate[]"]').attr('value', "").end();
                
                grandTotal();
                return false;
            }
            else
            {
                var Action = 'GetAllTaxDetail';
                var dataString = 'Action='+ Action +'&TaxID='+ TaxID;
                $.ajax({
                    type: "POST",
                    url: "includes/loader_functions.php",
                    data: dataString,
                    cache: false,
                    success: function(result)
                    {
                        //alert(result);
                        var obj = JSON.parse(result);
                        $row.find('[id="Tax1Rate[]"]').attr('value', obj.TaxValue).end();
                        
                        grandTotal();
                        return true;
                    }
                });
            }
        });

        
        $('#Add-Invoice-Form').on( 'change', 'tbody td select.tax2', function () {
            
            var $row  = $(this).parents("tr"),
                index = $row.attr('data-item-index');
            
            var TaxID = $('.tax2_'+index).val();
            
            if(TaxID < 0)
            {
                $row.find('[id="Tax2Rate[]"]').attr('value', "").end();
                
                grandTotal();
                return false;
            }
            else
            {
                var Action = 'GetAllTaxDetail';
                var dataString = 'Action='+ Action +'&TaxID='+ TaxID;
                //alert(dataString);
                $.ajax({
                    type: "POST",
                    url: "includes/loader_functions.php",
                    data: dataString,
                    cache: false,
                    success: function(result)
                    {
                        //alert(result);
                        var obj = JSON.parse(result);
                        $row.find('[id="Tax2Rate[]"]').attr('value', obj.TaxValue).end();
                        
                        grandTotal();
                        return true;
                    }
                });
            }
        });
        
        $('#Add-Invoice-Form').on( 'change', 'tbody td select.tax3', function () {
            
            var $row  = $(this).parents("tr"),
                index = $row.attr('data-item-index');
            
            var TaxID = $('.tax3_'+index).val();
            if(TaxID < 0)
            {
                $row.find('[id="Tax3Rate[]"]').attr('value', "").end();
                
                grandTotal();
                return false;
            }
            else
            {   
                var Action = 'GetAllTaxDetail';
                var dataString = 'Action='+ Action +'&TaxID='+ TaxID;
                $.ajax({
                    type: "POST",
                    url: "includes/loader_functions.php",
                    data: dataString,
                    cache: false,
                    success: function(result)
                    {
                        //alert(result);
                        var obj = JSON.parse(result);
                        $row.find('[id="Tax3Rate[]"]').attr('value', obj.TaxValue).end();
                        
                        grandTotal();
                        return true;
                    }
                });
            }
        });
    </script>
    <script type="text/javascript">

        jQuery(function ($) 
        {
            'use strict';
            $(document).ready(function () {
                //Init datetimepicker
                $('.js-dtp').each(function (i, key) {
                    var format = $(key).data('format');
                    $(key).datetimepicker({
                        format: format,
                        showClear: true
                    });
                });
            });
        });

    </script>

    <script type="text/javascript">
        $('#Add-Invoice-Form').on('submit', function(event) {
            
            event.preventDefault();
            var Action = 'AddInvoice';
            
            var ClientID = $("#ClientID").val();
            
            if(ClientID < 0)
            {
				alert("Please Select Any Client.");
                $("#ClientID").focus();
                return false;
            }
			
			var checkItemIDValue = checkItemID();
			if(checkItemIDValue == -1)
			{
				return false;
			}
            
            grandTotal();

            //return false;
            var form_data = new FormData(this);
            form_data.append('Action',Action);
            
           	$("#flash").show();
            $("#flash i").addClass('fa-spinner');
            $("#flash i").addClass('fa-spin');
            $("#flash").removeClass('hidden');
            $('html,body').animate({ scrollTop: $(".content").offset().top},'slow');
            $("#flash span").html('Please Wait...');
            
            $.ajax({
                url: 'includes/invoice_script.php',
                type: 'POST',
                data: form_data,
                contentType: false,
                cache: false,
                processData:false,
                success: function(result)
                {
                    //alert(result);
                    var obj = JSON.parse(result);
                    var Status = obj.Status;
                    
                    if(Status == '2')
                    {
                        $("#flash").removeClass('alert alert-danger');
                        $("#flash").addClass('alert alert-success');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-times-circle');
                        $("#flash i").addClass('fa fa-fw fa-check-circle');
                        $("#flash span").html('Invoice Insert Successfully.');
                        document.getElementById("Add-Invoice-Form").reset();
                        $('#flash').delay(3000).fadeOut(500);
                        setTimeout(function() {
                          window.location.href = "add_invoice.php";
                        }, 1000);
                        return true;
                    }
                    else if(Status == '1')
                    {
                        $("#flash").removeClass('alert alert-success');
                        $("#flash").addClass('alert alert-danger');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash i").addClass('fa fa-fw fa-times-circle');
                        $("#flash span").html('Invoice Insert Not Successfully.');
                        $('#flash').delay(3000).fadeOut(500);
                        return true;
                    }
                    else if(Status == '0')
                    {
                        $("#flash").removeClass('alert alert-success');
                        $("#flash").addClass('alert alert-danger');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash i").addClass('fa fa-fw fa-times-circle');
                        $("#flash span").html('Invoice No Already Exists.');
                        $('#flash').delay(3000).fadeOut(500);
                        return true;
                    }
                }
            });
        });
    </script>
    </body>
</html>
<?php
ob_flush();
?>