<?php
ob_start();
$Page = "ViewAgainstReceipt"; 
require_once('head.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Xenon ERP</title>
    
    <!-- Favicon -->
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="assets/plugins/bootstrap/dist/css/bootstrap.css" rel="stylesheet" />

    <!-- Animate.css Css -->
    <link href="assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Font Awesome Css -->
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

    <!-- iCheck Css -->
    <link href="assets/plugins/iCheck/skins/flat/_all.css" rel="stylesheet" />

    <!-- Switchery Css -->
    <link href="assets/plugins/switchery/dist/switchery.css" rel="stylesheet" />

    <!-- Metis Menu Css -->
    <link href="assets/plugins/metisMenu/dist/metisMenu.css" rel="stylesheet" />

    <!-- Pace Loader Css -->
    <link href="assets/plugins/pace/themes/white/pace-theme-flash.css" rel="stylesheet" />
    
    <!-- Bootstrap Select Css -->
    <link href="assets/plugins/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" />

    <!-- DateTimePicker Css -->
    <link href="assets/plugins/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="assets/css/style.css" rel="stylesheet" />
    
</head>
<body class="ls-fixed navbar-fixed">
    <div class="all-content-wrapper">
        <!-- Top Bar -->
        <header>
            <?php include_once('header.php'); ?>
        </header>
		<?php
            if(isset($_GET['rv_id']))
            {
                $RVID = $_GET['rv_id'];
                $SelectReceipt = "SELECT * FROM receipt_vouchers WHERE rv_id='".$RVID."'";
                $SelectReceiptQuery = mysqli_query($con,$SelectReceipt);
                if(!$SelectReceiptQuery)
                {
                    //die(mysqli_error($con));
                    header("Location: view_all_against_receipt.php");
                    exit();
                }
                $count = mysqli_num_rows($SelectReceiptQuery);
                if($count != 1)
                {
                    header("Location: view_all_against_receipt.php");
                    exit();
                }
                
                $Receipt = mysqli_fetch_array($SelectReceiptQuery);
				
				$CompanyID = $Receipt['company_id'];
				$RVNo = $Receipt['rv_no'];
				$RVDate = $Receipt['rv_date'];
				$ReceivedFrom = $Receipt['received_from'];
				$ReceivedTo = $Receipt['received_to'];
				$ReceiptMode = $Receipt['receipt_mode'];
				$BankName = $Receipt['bank_name'];
				$ChequeNo = $Receipt['cheque_no'];
				$ChequeDate = $Receipt['cheque_date'];
                $TransactionDate = $Receipt['transaction_date'];
				$CardNo = $Receipt['card_no'];
				$TransactionRef = $Receipt['transaction_ref'];
				$PaymentDate = $Receipt['payment_date'];
				$Description = $Receipt['description'];
				$TotalAmount = $Receipt['total_amount'];
				$TotalTDSAmount = $Receipt['total_tds_amount'];
				$RVType = $Receipt['rv_type'];
                
				if(!empty($Receipt['rv_date']) && $Receipt['rv_date'] != '0000-00-00') { $RVDate = date("d-m-Y",strtotime($Receipt['rv_date'])); }
				if(!empty($Receipt['cheque_date']) && $Receipt['cheque_date'] != '0000-00-00') { $ChequeDate = date("d-m-Y",strtotime($Receipt['cheque_date'])); }
				if(!empty($Receipt['transaction_date']) && $Receipt['transaction_date'] != '0000-00-00') { $TransactionDate = date("d-m-Y",strtotime($Receipt['transaction_date'])); }
				if(!empty($Receipt['payment_date']) && $Receipt['payment_date'] != '0000-00-00') { $PaymentDate = date("d-m-Y",strtotime($Receipt['payment_date'])); }
				
            }
            else
            {
                header("Location: view_all_against_receipt.php");
                exit();
            }
        ?>
        <!-- #END# Top Bar -->
        <!-- Left Menu -->
        <aside class="sidebar">
            <?php include_once('menu.php'); ?>
        </aside>
        <!-- #END# Left Menu -->
        <section class="content dashboard">
            <div class="page-heading">
                <h1>
                    <a href="view_all_against_receipt.php">
                        Against Receipt
                    </a>
                    <small>
                        <i class="fa fa-angle-double-right"></i>
                        <a class="font-bold" href="update_receipt_against_invoice.php?rv_id=<?php echo $RVID; ?>">Update Against Receipt</a>
                    </small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="dashboard.php">Home</a></li>
                    <li><a href="view_all_against_receipt.php">Against Receipt</a></li>
                </ol>
            </div>
            <div class="page-body">
            	<!-- -------------- ERROR SECTION START -------------- -->
                        
                <div id="flash" class="alert alert hidden">
                    <strong>
                        <i class="fa fa-spinner fa-spin"></i>
                    </strong>
                    &nbsp; &nbsp;
                    <span></span>
                </div>
            
            	<!-- -------------- ERROR SECTION END -------------- -->
				<div class="row clearfix">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Update Against Receipt</div>
							<div class="panel-body p-b-25">
								<form id="Update-AgainstReceipt-Form" method="post" class="form-horizontal" action="#">
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                            <input type="hidden" id="AdminID" name="AdminID" value="<?php echo $_SESSION['AdminID']; ?>" required />
                                            <input type="hidden" id="RVType" name="RVType" value="<?php echo $RVType; ?>" required />
											<input type="hidden" id="RVID" name="RVID" value="<?php echo $RVID; ?>" required />
											<input type="hidden" id="CompanyID" name="CompanyID" value="<?php echo $CompanyID; ?>" required />
											<input type="hidden" id="OldTotalAmount" name="OldTotalAmount" value="<?php echo $TotalAmount; ?>" required />
											<input type="hidden" id="OldTotalTDSAmount" name="OldTotalTDSAmount" value="<?php echo $TotalTDSAmount; ?>" required />
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Receipt No</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="RVNo" name="RVNo" value="<?php echo $RVNo; ?>" class="form-control" readonly required />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Received From</label>
                                                <div class="col-sm-6">
                                                    <select class="col-xs-10 col-md-8 selectpicker form-control show-tick" id="ReceivedFrom" name="ReceivedFrom">
                                                        <?php
                                                            $SelectClient = "SELECT * FROM ledger_master WHERE group_id='33' AND company_id='".$CurrentCompanyID."'  ORDER BY group_id";
                                                            $SelectClientQuery = mysqli_query($con,$SelectClient);
                                                            while($Client = mysqli_fetch_array($SelectClientQuery))
                                                            {
																if($ReceivedFrom == $Client['ledger_id'])
																{
																	echo '<option value="'.$Client['ledger_id'].'" selected>'.$Client['ledger_name'].'</option>';
																}
                                                                
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
										</div>
                                      	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Receipt Date</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="RVDate" name="RVDate" data-format="DD-MM-YYYY" class="form-control js-dtp" value="<?php echo $RVDate; ?>" required />
													<span style="width: 50px;" class="glyphicon glyphicon-calendar form-control-feedback"></span>
                                                </div>
                                            </div>
										</div>
									</div>
                                 	<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<table id="AgainstReceipt" class="table table-bordered">
												<thead>
													<tr>
														<th width="20%" class="text-center">Invoice No</th>
														<th width="20%" class="text-center">Recived</th>
														<th width="20%" class="text-center">Outstanding</th>
														<th width="20%" class="text-center">TDS Amount</th>
														<th width="20%" class="text-center">Amount</th>
													</tr>
												</thead>
												<tbody>
													<?php
														$SelectReceiptItems = mysqli_query($con,"SELECT * FROM receipt_voucher_items WHERE rv_id='".$RVID."' GROUP BY invoice_id");
														while($ReceiptItems = mysqli_fetch_array($SelectReceiptItems))
														{
															$RVIID = $ReceiptItems['rvi_id'];
															$ItemIndex = $ReceiptItems['rvi_id'];
															$InvoiceID = $ReceiptItems['invoice_id'];
															$R_TDSAmount = $ReceiptItems['tds_amount'];
															$R_Amount = $ReceiptItems['amount'];
															
															$SelectInvoice = mysqli_query($con,"SELECT * FROM invoice_master WHERE invoice_id='".$InvoiceID."'");
															$InvoiceArr = mysqli_fetch_array($SelectInvoice);
															$InvoiceNo = $InvoiceArr['invoice_no'];
															$GrandTotal = $InvoiceArr['grand_total'];
															
															$SelectReceiptItem = mysqli_query($con,"SELECT SUM(tds_amount), SUM(amount) FROM receipt_voucher_items WHERE invoice_id='".$InvoiceID."'");
															$ReceiptItem = mysqli_fetch_array($SelectReceiptItem);
															$TDSAmount = $ReceiptItem['SUM(tds_amount)'];
															$Amount = $ReceiptItem['SUM(amount)'];
															$Total = $TDSAmount + $Amount;
															$Received = $Total;
															$Outstanding = $GrandTotal - $Total;
															$OldOutstanding = $Received + $Outstanding;
													?>
													<tr data-item-index=<?php echo $ItemIndex; ?>>
														<td>
															<input type = "hidden" id="RVIID[]" name="RVIID[]" value="<?php echo $RVIID; ?>" required />
															<input type = "hidden" id="InvoiceID[]" name="InvoiceID[]" value="<?php echo $InvoiceID; ?>" required />
															<input type="text" id="InvoiceNo[]" name="InvoiceNo[]" value="<?php echo $InvoiceNo; ?>" style="border: none;" readonly />
														</td>
														<td>
															<input type="text" id="Received[]" name="Received[]" value="<?php echo $Received; ?>" style="border: none;" readonly />
														</td>
														<td>
															<input type="text" id="Outstanding[]" name="Outstanding[]" value="<?php echo $Outstanding; ?>" style="border: none;" readonly />
															<input type="hidden" id="OldOutstanding[]" name="OldOutstanding[]" value="<?php echo $OldOutstanding; ?>" />
														</td>
														<td class="no-padding text-center">
															<input type="text" id="TDSAmount[]" name="TDSAmount[]" value="<?php echo $R_TDSAmount; ?>" onKeyPress="return NuMValidation(event);" onKeyUp="return grandTotal();" style="border: none;" />	
														</td>
														<td class="no-padding text-center">
															<input type="text" id="Amount[]" name="Amount[]" value="<?php echo $R_Amount; ?>" onKeyPress="return NuMValidation(event);" onKeyUp="return grandTotal();" style="border: none;" />
														</td>
													</tr>
													<?php
														}
													?>
													<tr>
														<td></td>
														<td></td>
														<td class="text-center">Total</td>
														<td class="no-padding text-center">
															<input type = "text" id="TotalTDSAmount" name="TotalTDSAmount" value="<?php echo $TotalTDSAmount; ?>" onFocus="return grandTotal();"  style="border: none;" readonly />
														</td>
														<td class="no-padding text-center">
															<input type = "text" id="TotalAmount" name="TotalAmount" value="<?php echo $TotalAmount; ?>" onFocus="return grandTotal();" style="border: none;" readonly />
														</td>
													</tr>
												</tbody>
											</table>
										</div>	
									</div>
                                  	<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Received To</label>
                                                <div class="col-sm-6">
                                                    <select class="col-xs-10 col-md-8 selectpicker form-control show-tick" id="ReceivedTo" name="ReceivedTo">
                                                        <?php
                                                            $SelectBankAccount = "SELECT * FROM ledger_master WHERE group_id IN('20','21') AND company_id='".$CurrentCompanyID."'  ORDER BY group_id";
                                                            $SelectBankAccountQuery = mysqli_query($con,$SelectBankAccount);
                                                            while($BankAccount = mysqli_fetch_array($SelectBankAccountQuery))
                                                            {
																if($ReceivedTo == $BankAccount['ledger_id'])
																{
																	echo '<option value="'.$BankAccount['ledger_id'].'" selected>'.$BankAccount['ledger_name'].'</option>';
																}
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Description</label>
                                                <div class="col-sm-6">
                                                    <textarea id="Description" name="Description" class="form-control no-resize" rows="4" /><?php echo $Description; ?></textarea>
                                                </div>
                                            </div>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
											<div class="form-group">
                                                <label class="col-sm-5 control-label">Receipt Mode</label>
                                                <div class="col-sm-6">
                                                    <select class="col-xs-10 col-md-8 selectpicker form-control show-tick" id="ReceiptMode" name="ReceiptMode" onChange="return fetchData();">
                                                        <?php if($ReceiptMode == 'CASH') { echo '<option value="CASH">CASH</option>'; } ?>
                                                        <?php if($ReceiptMode == 'CHEQUE') { echo '<option value="CHEQUE">CHEQUE</option>'; } ?>
                                                        <?php if($ReceiptMode == 'CREDIT/DEBIT CARD') { echo '<option value="CREDIT/DEBIT CARD">CREDIT/DEBIT CARD</option>'; } ?>
                                                        <?php if($ReceiptMode == 'INTERNET BANKING') { echo '<option value="INTERNET BANKING">INTERNET BANKING</option>'; } ?>
                                                    </select>
                                                </div>
                                            </div>
											<?php
												if($ReceiptMode == 'CHEQUE')
												{
											?>
												<div class="form-group">
													<label class="col-sm-5 control-label">Bank Name</label>
													<div class="col-sm-6">
														<input type="text" id="BankName" name="BankName" value="<?php echo $BankName; ?>" class="form-control" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-5 control-label">Cheque No</label>
													<div class="col-sm-6">
														<input type="text" id="ChequeNo" name="ChequeNo" value="<?php echo $ChequeNo; ?>" class="form-control" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-5 control-label">Cheque Date</label>
													<div class="col-sm-6">
														<input type="text" id="ChequeDate" name="ChequeDate" data-format="DD-MM-YYYY" value="<?php echo $ChequeDate; ?>" class="form-control js-dtp" />
														<span style="width: 50px;" class="glyphicon glyphicon-calendar form-control-feedback"></span>
													</div>
												</div>
											<?php
												}
												else if($ReceiptMode == 'CREDIT/DEBIT CARD')
												{
											?>
												<div class="form-group">
													<label class="col-sm-5 control-label">Transaction Date</label>
													<div class="col-sm-6">
														<input type="text" id="TransactionDate" name="TransactionDate" value="<?php echo $TransactionDate; ?>" data-format="DD-MM-YYYY" class="form-control js-dtp" />
														<span style="width: 50px;" class="glyphicon glyphicon-calendar form-control-feedback"></span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-5 control-label">Card No</label>
													<div class="col-sm-6">
														<input type="text" id="CardNo" name="CardNo" value="<?php echo $CardNo; ?>" class="form-control" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-5 control-label">Transaction Ref</label>
													<div class="col-sm-6">
														<input type="text" id="TransactionRef" name="TransactionRef" value="<?php echo $TransactionRef; ?>" class="form-control" />
													</div>
												</div>
											<?php
												}
												else if($ReceiptMode == 'INTERNET BANKING')
												{
											?>
												<div class="form-group">
													<label class="col-sm-5 control-label">Bank Name</label>
													<div class="col-sm-6">
														<input type="text" id="BankName" name="BankName" value="<?php echo $BankName; ?>" class="form-control" />
													</div>
												</div> 
												<div class="form-group">
													<label class="col-sm-5 control-label">Payment Date</label>
													<div class="col-sm-6">
														<input type="text" id="PaymentDate" name="PaymentDate" data-format="DD-MM-YYYY" value="<?php echo $PaymentDate; ?>" class="form-control js-dtp" />
														<span style="width: 50px;" class="glyphicon glyphicon-calendar form-control-feedback"></span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-5 control-label">Transaction Ref</label>
													<div class="col-sm-6">
														<input type="text" id="TransactionRef" name="TransactionRef" value="<?php echo $TransactionRef; ?>" class="form-control" />
													</div>
												</div>
											<?php
												}
											?>
										</div>
									</div>
									
									<div class="row">    
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="form-group">
                                                <div class="text-center">
                                                    <button type="submit" id="Submit" name="Submit" class="btn btn-success">
                                                        <i class="fa fa-refresh bigger-110"></i>
                                                        Update
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
								</form>
							</div>
						</div>
					</div>	
				</div>
            </div>
        </section>
        <!-- Footer -->
       
        <!-- #END# Footer -->
    </div>

    <!-- Jquery Core Js -->
    <script src="assets/plugins/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Pace Loader Js -->
    <script src="assets/plugins/pace/pace.js"></script>

    <!-- Screenfull Js -->
    <script src="assets/plugins/screenfull/src/screenfull.js"></script>

    <!-- Metis Menu Js -->
    <script src="assets/plugins/metisMenu/dist/metisMenu.js"></script>

    <!-- Jquery Slimscroll Js -->
    <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
    
    <!-- Switchery Js -->
    <script src="assets/plugins/switchery/dist/switchery.js"></script>

    <!-- iCheck Js -->
    <script src="assets/plugins/iCheck/icheck.js"></script>
    
	<!-- MomentJs Js -->
    <script src="assets/plugins/moment/moment.js"></script>
    
   	<!-- DateTimePicker Js -->
    <script src="assets/plugins/eonasdan-bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js"></script>

    <!-- Bootstrap Select Js -->
    <script src="assets/plugins/bootstrap-select/dist/js/bootstrap-select.js"></script>


    <!-- Custom Js -->
    <script src="assets/js/admin.js"></script>
    
    <script>
        function NuMValidation(evt)
        {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
            return false;
    
            return true;
        }
		function BasivElement(evt)
        {
			//Init datetimepicker
			$('.js-dtp').each(function (i, key) {
				var format = $(key).data('format');
				$(key).datetimepicker({
					format: format,
					showClear: true
				});
			});
        }
	</script>
   <script type="text/javascript">

        jQuery(function ($) 
        {
            'use strict';
            $(document).ready(function () {
                //Init datetimepicker
                $('.js-dtp').each(function (i, key) {
                    var format = $(key).data('format');
                    $(key).datetimepicker({
                        format: format,
                        showClear: true
                    });
                });
            });
        });

    </script>
    <script type="text/javascript">
				
			$('#Add-AgainstReceipt-Form')
			.on('change', '#ReceivedFrom', function() {
				
				var ClientID = $('#ReceivedFrom').val();
				
				fetchInvoice(ClientID);
			});
			
			function fetchInvoice(ClientID)
			{
				var Action = 'LoadInvoice';
				var dataString = 'Action='+ Action +'&ClientID='+ ClientID;
				
				$.ajax({
					type: "POST",
					url: "includes/loader_functions.php",
					data: dataString,
					cache: false,
					success: function(result)
					{
						//alert(result);
						$('#AgainstReceipt tbody').html(result);
						
						return true;
						
					}
				});
			}
		</script>
		<script type="text/javascript">
			
			function grandTotal()
			{
				var TDSAmountArray = document.querySelectorAll("#Update-AgainstReceipt-Form input[id='TDSAmount[]']");
				var AmountArray = document.querySelectorAll("#Update-AgainstReceipt-Form input[id='Amount[]']");
				
				var TotalTDSAmount = 0.00;
				var TotalAmount = 0.00;
				
				for (var i = 0; i < TDSAmountArray.length; i++) 
				{
					var TDSAmount = TDSAmountArray[i].value; 
					var Amount = AmountArray[i].value;
					
					if(TDSAmount == '' || isNaN(TDSAmount)) { TDSAmount = 0.00; }
					if(Amount == '' || isNaN(Amount)) { Amount = 0.00; }
					
					TotalTDSAmount += parseFloat(TDSAmount);
					TotalAmount += parseFloat(Amount);
				}
				TotalTDSAmount = Math.round(TotalTDSAmount);
				TotalTDSAmount = TotalTDSAmount.toFixed(2);
				$("#TotalTDSAmount").val(TotalTDSAmount);
				
				TotalAmount = Math.round(TotalAmount);
				TotalAmount = TotalAmount.toFixed(2);
				$("#TotalAmount").val(TotalAmount);
				
				return true;
			}
			
			
			function checkAmount()
			{
				var OutstandingArray = document.querySelectorAll("#Update-AgainstReceipt-Form input[id='Outstanding[]']");
				var OldOutstandingArray = document.querySelectorAll("#Update-AgainstReceipt-Form input[id='OldOutstanding[]']");
				var AmountArray = document.querySelectorAll("#Update-AgainstReceipt-Form input[id='Amount[]']");
				var TDSAmountArray = document.querySelectorAll("#Update-AgainstReceipt-Form input[id='TDSAmount[]']");
				
				for(var i = 0; i < OutstandingArray.length; i++)
				{
					var Amount = AmountArray[i].value;
					var Outstanding = OutstandingArray[i].value;
					var OldOutstanding = OldOutstandingArray[i].value;
					var TDSAmount = TDSAmountArray[i].value;
					var Total = 0;
					
					if(TDSAmount == '' || isNaN(TDSAmount)) { TDSAmount = 0.00; }
					if(Amount == '' || isNaN(Amount)) { Amount = 0.00; }
					if(Outstanding == '' || isNaN(Outstanding)) { Outstanding = 0.00; }
					if(OldOutstanding == '' || isNaN(OldOutstanding)) { OldOutstanding = 0.00; }
					
					Total = parseFloat(TDSAmount) + parseFloat(Amount);
					var TotalOutstanding = parseFloat(Outstanding) + parseFloat(OldOutstanding);
					
					if(Total > TotalOutstanding)
					{
						alert("Amount Should Not be Greater Than Outstanding.");
						return false;
					}
				}
			}
			
			function fetchData()
			{
				var ReceiptMode = $("#ReceiptMode").val();

				if(ReceiptMode < 0)
				{
					$("#CustomFields").html('');
					$("#ReceiptMode").focus();
					return false;
				}

				else
				{ 
					var Action = 'GetBankCustomField';
					var dataString = 'Action='+ Action +'&ReceiptMode='+ ReceiptMode;
					
					$.ajax({
						type: "POST",
						url: "includes/loader_functions.php",
						data: dataString,
						cache: false,
						success: function(result)
						{
							//alert(result);
							$("#CustomFields").html(result);
							BasivElement();
							return true;
						}
					});
				}
			}
	</script>
   	<script type="text/javascript">
        $('#Update-AgainstReceipt-Form').on('submit', function(event) {
            
            event.preventDefault();
			
            var Action = 'UpdateReceipt';
			
			var ClientID = $("#ReceivedFrom").val();
            
            if(ClientID < 0)
            {
				alert("Please Select Any Client.");
                $("#ReceivedFrom").focus();
                return false;
            }
			
			var CheckAmount = checkAmount();
			if(CheckAmount == false)
			{
				return false;
			}
			grandTotal();
			
            var form_data = new FormData(this);
            form_data.append('Action',Action);
            
			$("#Submit").attr('disabled',true);
            $("#flash").show();
            $("#flash i").addClass('fa-spinner');
            $("#flash i").addClass('fa-spin');
            $("#flash").removeClass('hidden');
            $('html,body').animate({ scrollTop: $(".content").offset().top},'slow');
            $("#flash span").html('Please Wait...');
          	//return false;
            $.ajax({
                url: 'includes/receipt_script.php',
                type: 'POST',
                data: form_data,
                contentType: false,
                cache: false,
                processData:false,
                success: function(result)
                {
                    //alert(result);
                    var obj = JSON.parse(result);
                    var Status = obj.Status;
                    
                    if(Status == '4')
                    {
						$("#Submit").attr('disabled',false);
                        $("#flash").removeClass('alert alert-danger');
                        $("#flash").addClass('alert alert-success');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-times-circle');
                        $("#flash i").addClass('fa fa-fw fa-check-circle');
                        $("#flash span").html('Receipt Update Successfully.');
                        document.getElementById("Update-AgainstReceipt-Form").reset();
                        $('#flash').delay(3000).fadeOut(500);
                        setTimeout(function() {
                          window.location.href = "update_receipt_against_invoice.php?rv_id=<?php echo $RVID; ?>";
                        }, 1000);
                        return true;
                    }
                    else if(Status == '3')
                    {
						$("#Submit").attr('disabled',false);
                        $("#flash").removeClass('alert alert-success');
                        $("#flash").addClass('alert alert-danger');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash i").addClass('fa fa-fw fa-times-circle');
                        $("#flash span").html(' Receipt Update Not Successfully.');
                        $('#flash').delay(3000).fadeOut(500);
                        return true;
                    }
                    else if(Status == '0')
                    {
						$("#Submit").attr('disabled',false);
                        $("#flash").removeClass('alert alert-success');
                        $("#flash").addClass('alert alert-danger');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash i").addClass('fa fa-fw fa-times-circle');
                        $("#flash span").html('Receipt Already Exists.');
                        $('#flash').delay(3000).fadeOut(500);
                        return true;
                    }
                    else
                    {
						$("#Submit").attr('disabled',false);
                        $("#flash").removeClass('alert alert-success');
                        $("#flash").addClass('alert alert-danger');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash i").addClass('fa fa-fw fa-times-circle');
                        $("#flash span").html(result);
                        $('#flash').delay(3000).fadeOut(500);
                        return true;
                    }
                }
            });
        });
    </script>
</body>
</html>
<?php
ob_flush();
?>