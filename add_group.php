<?php
ob_start();
$Page = "AddGroup"; 
require_once('head.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Xenon ERP</title>
    
    <!-- Favicon -->
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="assets/plugins/bootstrap/dist/css/bootstrap.css" rel="stylesheet" />

    <!-- Animate.css Css -->
    <link href="assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Font Awesome Css -->
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

    <!-- iCheck Css Libraries | You can choose a theme from plugins/iCheck/skins instead of get all themes -->
    <link href="assets/plugins/iCheck/skins/minimal/_all.css" rel="stylesheet" />
    <link href="assets/plugins/iCheck/skins/square/_all.css" rel="stylesheet" />
    <link href="assets/plugins/iCheck/skins/flat/_all.css" rel="stylesheet" />

    <!-- Switchery Css -->
    <link href="assets/plugins/switchery/dist/switchery.css" rel="stylesheet" />

    <!-- Metis Menu Css -->
    <link href="assets/plugins/metisMenu/dist/metisMenu.css" rel="stylesheet" />

    <!-- Pace Loader Css -->
    <link href="assets/plugins/pace/themes/white/pace-theme-flash.css" rel="stylesheet" />

    <!-- Bootstrap Select Css -->
    <link href="assets/plugins/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" />

    <!-- DateTimePicker Css -->
    <link href="assets/plugins/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="assets/css/style.css" rel="stylesheet" />

</head>
<body class="ls-fixed navbar-fixed">
    <div class="all-content-wrapper">
        <!-- Top Bar -->
        <header>
            <?php include_once('header.php'); ?>
        </header>
        <!-- #END# Top Bar -->
        <!-- Left Menu -->
        <aside class="sidebar">
            <?php include_once('menu.php'); ?>
        </aside>
        <!-- #END# Left Menu -->
        <section class="content">
            <div class="page-heading">
                <h1>
                    <a href="view_all_group.php">
                        Group
                    </a>
                    <small>
                        <i class="fa fa-angle-double-right"></i>
                        <a class="font-bold" href="add_group.php">Add New Group</a>
                    </small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="dashboard.php">Home</a></li>
                    <li><a href="view_all_group.php">Group</a></li>
                </ol>
            </div>
            
            <div class="page-body">
                <!-- -------------- ERROR SECTION START -------------- -->
                        
				<div id="flash" class="alert alert hidden">
					<strong>
						<i class="fa fa-spinner fa-spin"></i>
					</strong>
					&nbsp; &nbsp;
					<span></span>
				</div>
            
            	<!-- -------------- ERROR SECTION END -------------- -->
                <div class="row clearfix">
                    <!-- Horizontal Layout  -->
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">Add New Group</div>
                            <div class="panel-body p-b-25">
                                <form id="Add-Group-Form" method="post" class="form-horizontal" action="#">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <input type="hidden" id="AdminID" name="AdminID" value="<?php echo $_SESSION['AdminID']; ?>" required />
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Group Name</label>
                                            <div class="col-sm-6">
                                                <input type="text" id="GroupName" name="GroupName" class="form-control" required />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Parent Group</label>
                                            <div class="col-sm-6">
                                                <select class="col-xs-10 col-md-8 selectpicker form-control show-tick" id="ParentID" name="ParentID">
                                                    <option value="NULL">-- SELECT --</option>
                                                    <?php
                                                        $SelectGroup = "SELECT * FROM group_master WHERE visible='1' ORDER BY group_id";
                                                        $SelectGroupQuery = mysqli_query($con,$SelectGroup);
                                                                                
                                                        while($PGroup = mysqli_fetch_array($SelectGroupQuery))
                                                        {
                                                            echo '<option value="'.$PGroup['group_id'].'">'.$PGroup['group_name'].'</option>';
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="form-group">
                                            <div class="col-sm-offset-4 col-sm-6">
                                                <button type="submit" class="btn btn-success">
                                                    <i class="fa fa-check bigger-110"></i>
                                                    Submit
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- #END# Horizontal Layout  -->
                </div>
            </div>
        </section>
        <!-- Footer -->
       
        <!-- #END# Footer -->
    </div>
        
    <!-- Jquery Core Js -->
    <script src="assets/plugins/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Pace Loader Js -->
    <script src="assets/plugins/pace/pace.js"></script>

    <!-- Screenfull Js -->
    <script src="assets/plugins/screenfull/src/screenfull.js"></script>

    <!-- Metis Menu Js -->
    <script src="assets/plugins/metisMenu/dist/metisMenu.js"></script>

    <!-- Jquery Slimscroll Js -->
    <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    

    <!-- iCheck Js -->
    <script src="assets/plugins/iCheck/icheck.js"></script>

    <!-- Autosize Js (Textarea auto growth plugin) -->
    <script src="assets/plugins/autosize/dist/autosize.js"></script>

    <!-- MomentJs Js -->
    <script src="assets/plugins/moment/moment.js"></script>

    <!-- DateTimePicker Js -->
    <script src="assets/plugins/eonasdan-bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js"></script>

    <!-- Bootstrap Select Js -->
    <script src="assets/plugins/bootstrap-select/dist/js/bootstrap-select.js"></script>

    <!-- Custom Js -->
    <script src="assets/js/admin.js"></script>
    
    <script src="assets/js/pages/forms/basic-form-elements.js"></script>

    
   <script>
        function NuMValidation(evt)
        {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
            return false;
    
            return true;
        } 

    </script>

    <script type="text/javascript">
        $('#Add-Group-Form').on('submit', function(event) {
            
            event.preventDefault();
			
			var ParentID = $("#ParentID").val();
			if(ParentID == 'NULL')
			{
				alert("Please Select Any Parent Group.");
				return false;
			}
			
            var Action = 'AddGroup';

            var form_data = new FormData(this);
            form_data.append('Action',Action);
                            
           	$("#flash").show();
            $("#flash i").addClass('fa-spinner');
            $("#flash i").addClass('fa-spin');
            $("#flash").removeClass('hidden');
            $('html,body').animate({ scrollTop: $(".content").offset().top},'slow');
            $("#flash span").html('Please Wait...');
            
            $.ajax({
                url: 'includes/group_script.php',
                type: 'POST',
                data: form_data,
                contentType: false,
                cache: false,
                processData:false,
                success: function(result)
                {
                    //alert(result);
                    var obj = JSON.parse(result);
                    var Status = obj.Status;
                    
                    if(Status == '2')
                    {
                        $("#flash").removeClass('alert alert-danger');
                        $("#flash").addClass('alert alert-success');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-times-circle');
                        $("#flash i").addClass('fa fa-fw fa-check-circle');
                        $("#flash span").html('Group Insert Successfully.');
                        document.getElementById("Add-Group-Form").reset();
                        $('#flash').delay(3000).fadeOut(500);
                        setTimeout(function() {
                          window.location.href = "add_group.php";
                        }, 1000);
                        return true;
                    }
                    else if(Status == '1')
                    {
                        $("#flash").removeClass('alert alert-success');
                        $("#flash").addClass('alert alert-danger');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash i").addClass('fa fa-fw fa-times-circle');
                        $("#flash span").html('Group Insert Not Successfully.');
                        $('#flash').delay(3000).fadeOut(500);
                        return true;
                    }
                    else if(Status == '0')
                    {
                        $('#EmployeeCode').focus();
                        $("#flash").removeClass('alert alert-success');
                        $("#flash").addClass('alert alert-danger');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash i").addClass('fa fa-fw fa-times-circle');
                        $("#flash span").html('Group Already Exists.');
                        $('#flash').delay(3000).fadeOut(500);
                        return true;
                    }
                }
            });
        });
    </script>
        
</body>
</html>
<?php
ob_flush();
?>