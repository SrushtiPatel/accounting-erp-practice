<?php
ob_start();
$Page = "DutiesTaxes"; 
require_once('head.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Xenon ERP</title>
    
    <!-- Favicon -->
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="assets/plugins/bootstrap/dist/css/bootstrap.css" rel="stylesheet" />

    <!-- Bootstrap DateRangePicker Css -->
    <link href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />

    <!-- Animate.css Css -->
    <link href="assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Font Awesome Css -->
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

    <!-- iCheck Css -->
    <link href="assets/plugins/iCheck/skins/flat/_all.css" rel="stylesheet" />

    <!-- Switchery Css -->
    <link href="assets/plugins/switchery/dist/switchery.css" rel="stylesheet" />

    <!-- Metis Menu Css -->
    <link href="assets/plugins/metisMenu/dist/metisMenu.css" rel="stylesheet" />

    <!-- Pace Loader Css -->
    <link href="assets/plugins/pace/themes/white/pace-theme-flash.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="assets/css/style.css" rel="stylesheet" />
    
</head>
<body class="ls-fixed navbar-fixed">
    <div class="all-content-wrapper">
        <!-- Top Bar -->
        <header>
            <?php include_once('header.php'); ?>
        </header>
        <?php
            if(isset($_GET['ledger_id']))
            {
                $LedgerID = $_GET['ledger_id'];
                $SelectLedger = "SELECT * FROM ledger_master WHERE ledger_id='".$LedgerID."'";
                $SelectLedgerQuery = mysqli_query($con,$SelectLedger);
                if(!$SelectLedgerQuery)
                {
                    //die(mysqli_error($con));
                    header("Location: view_all_taxes.php");
                    exit();
                }
                $count = mysqli_num_rows($SelectLedgerQuery);
                if($count != 1)
                {
                    header("Location: view_all_taxes.php");
                    exit();
                }
                
                $Ledger = mysqli_fetch_array($SelectLedgerQuery);
                
                $LedgerID = $Ledger['ledger_id'];
                $LedgerName = $Ledger['ledger_name'];
                $LedgerAlias = $Ledger['ledger_alias'];
                $GroupID = $Ledger['group_id'];
                $OpeningBalance = $Ledger['opening_balance'];
                $CurrentBalance = $Ledger['current_balance'];

                $GroupDetail = $groupObject->selectGroupDetail($GroupID);
                $GroupDetail = json_decode($GroupDetail,true);
                $GroupName = $GroupDetail['GroupName'];
				
                $AddedBy = '';
                $AddedDate = '';
                $ModifiedBy = '';
                $ModifiedDate = '';

                if(!empty($Ledger['addedby']))
                {
                    $AddedBy = $profileObject->selectUserName($Ledger['addedby']);
                    $AddedDate = '( '.date("h:i:s A d-m-Y",strtotime($Ledger['addeddate'])).' )';
                }
                if(!empty($Ledger['modifiedby']))
                {
                    $ModifiedBy = $profileObject->selectUserName($Ledger['modifiedby']);
                    $ModifiedDate = '( '.date("h:i:s A d-m-Y",strtotime($Ledger['modifieddate'])).' )';
                }
            }
            else
            {
                header("Location: view_all_taxes.php");
                exit();
            }
        ?>
        <!-- #END# Top Bar -->
        <!-- Left Menu -->
        <aside class="sidebar">
            <?php include_once('menu.php'); ?>
        </aside>
        <!-- #END# Left Menu -->
        <section class="content">
            <div class="page-heading">
                <h1>
                	Duties & Taxes Details
                </h1>
                <small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					<a class="font-bold" href="view_taxes.php?ledger_id=<?php echo $LedgerID; ?>">View Duties & Taxes Detail</a>
				</small>
                <ol class="breadcrumb">
                    <li><a href="dashboard.php">Home</a></li>
                    <li><a href="view_all_taxes.php">Duties & Taxes</a></li>
                </ol>
            </div>
            
            <div class="page-body">
                <!-- -------------- ERROR SECTION START -------------- -->
                        
                <div id="flash" class="alert alert hidden">
                    <strong>
                        <i class="ace-icon fa fa-spinner"></i>
                    </strong>
                    &nbsp; &nbsp;
                    <span></span>
                </div>
                
                <!-- -------------- ERROR SECTION END -------------- -->
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Taxes Details</div>
                            <div class="panel-body">
                                <div class="row">
                                   	<div class="col-xs-12 col-sm-6">
										<table class="table table-bordered table-striped table-hover">
											<tbody>
												<tr>
													<th width="40%">Taxes Name</th>
													<td width="60%"><?php echo $LedgerName; ?></td>
												</tr>
												<tr>
													<th>Taxes Alias</th>
													<td><?php echo $LedgerAlias; ?></td>
												</tr>
												<tr>
													<th>Group Name</th>
													<td><?php echo $GroupName; ?></td>
												</tr>
												<tr>
													<th>Opening Balance</th>
													<td><?php echo $OpeningBalance; ?></td>
												</tr>
												<tr>
													<th>CurrentBalance</th>
													<td><?php echo $CurrentBalance; ?></td>
												</tr>
												<tr>
													<th>AddedBy</th>
													<td><?php echo $AddedBy; ?></td>
												</tr>
												<tr>
													<th>AddedDate</th>
													<td><?php echo $AddedDate; ?></td>
												</tr>
												<tr>
													<th>ModifiedBy</th>
													<td><?php echo $ModifiedBy; ?></td>
												</tr>
												<tr>
													<th>ModifiedDate</th>
													<td><?php echo $ModifiedDate; ?></td>
												</tr>
											</tbody>
										</table>
									</div>
                               		<div class="col-xs-12 col-sm-6">
                                        <table id="Ledger-table" class="table table-striped table-bordered table-hover">
                                            <tbody>
                                            <?php
                                                $SelectLCFItem = mysqli_query($con,"SELECT * FROM ledger_item_master WHERE ledger_id='".$LedgerID."'");
                                                $count = 1;
                                                
                                                while($LCFItem = mysqli_fetch_array($SelectLCFItem))
                                                {                                                                                                                               
                                                    $LIID = $LCFItem['li_id'];
                                                    $LCFID = $LCFItem['lcf_id'];
                                                    $Value = $LCFItem['li_value'];

                                                    $LCFDetail = $ledgerObject->selectLedgerItemDetail($LCFID);
                                                    $LCFDetail = json_decode($LCFDetail,true);
                                                    $LCFName = $LCFDetail['LCFName'];
                                            ?>
                                                <tr>
                                                    <th width="40%"><?php echo $LCFName; ?></th>
                                                    <td width="60%"><?php echo $Value; ?></td>
                                                </tr>
                                            <?php
                                                }
                                            ?>  
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="text-center">
                                        <a class="btn btn-sm btn-success" href="update_taxes.php?ledger_id=<?php echo $LedgerID; ?>">
                                            <i class="fa fa-refresh bigger-110"></i>
                                            Update Taxes Detail
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Footer -->
        
        <!-- #END# Footer -->
    </div>

    <!-- Jquery Core Js -->
    <script src="assets/plugins/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Bootstrap DateRangePicker Js -->
    <script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Pace Loader Js -->
    <script src="assets/plugins/pace/pace.js"></script>

    <!-- Screenfull Js -->
    <script src="assets/plugins/screenfull/src/screenfull.js"></script>

    <!-- Metis Menu Js -->
    <script src="assets/plugins/metisMenu/dist/metisMenu.js"></script>

    <!-- Jquery Slimscroll Js -->
    <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    

    <!-- Custom Js -->
    <script src="assets/js/admin.js"></script>
    

    <script type="text/javascript">
        jQuery(function($) {
                                            
            $('.js-daterange-picker').daterangepicker({
                format: 'dd-mm-yyyy',
                autoclose:true
            });
            
        });
    </script>
    
    
</body>
</html>
<?php
ob_flush();
?>