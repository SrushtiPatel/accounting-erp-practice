<nav class="navbar navbar-default">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
				<i class="material-icons">swap_vert</i>
			</button>
			<a href="javascript:void(0);" class="left-toggle-left-sidebar js-left-toggle-left-sidebar">
				<i class="material-icons">menu</i>
			</a>
			<!-- Logo -->
			<a class="navbar-brand" href="dashboard.php">
				<span class="logo-minimized">XE</span>
				<span class="logo">Xenon - ERP</span>
			</a>
			<!-- #END# Logo -->
		</div>
		<div class="collapse navbar-collapse" id="navbar-collapse">
			<ul class="nav navbar-nav">
				<li>
					<a href="javascript:void(0);" class="toggle-left-sidebar js-toggle-left-sidebar">
						<i class="material-icons">menu</i>
					</a>
				</li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<!-- Fullscreen Request -->
				<li>
					<a href="javascript:void(0);" class="fullscreen js-fullscreen">
						<i class="menu-icon fa fa-arrows-alt"></i>
					</a>
				</li>
				<!-- #END# Fullscreen Request -->

				<!-- User Menu -->
				<li class="dropdown user-menu">
					<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
						<img src="assets/images/user.jpg" alt="User Avatar" />
						<span class="hidden-xs"><?php echo $_SESSION['AdminName']; ?></span>
					</a>
					<ul class="dropdown-menu">
						<li class="header">
							<img src="assets/images/user.jpg" alt="User Avatar" />
							<div class="user">
								<?php echo $_SESSION['AdminName']; ?>
							</div>
						</li>
						<li class="body">
							<ul>
								<li>
									<a href="profile.php">
										<i class="ace-icon fa fa-user"></i> Profile
									</a>
								</li>
								<?php
									$SelectAdmin = mysqli_query($con,"SELECT `license` FROM admin_master WHERE admin_id='".$_SESSION['AdminID']."'");
									$Admin = mysqli_fetch_array($SelectAdmin);
									$License = $Admin['license'];
									if($License == 0)
									{
								?>
								<li>
									<a href="company.php">
										<i class="ace-icon fa fa-list"></i> Company
									</a>
								</li>
								<?php } ?>
								<li>
									<a href="settings.php">
										<i class="ace-icon fa fa-cog"></i> Setting
									</a>
								</li>
							</ul>
						</li>
						<li class="footer">
							<div class="row clearfix">
								<div class="col-xs-5">
									<a href="log-off.php" class="btn btn-default btn-sm btn-block">Log Off</a>
								</div>
								<div class="col-xs-2"></div>
								<div class="col-xs-5">
									<a href="includes/logout.php" class="btn btn-default btn-sm btn-block">Log Out</a>
								</div>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</nav>