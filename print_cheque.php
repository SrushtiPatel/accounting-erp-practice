<?php
ob_start();
$Page = "ViewAgainstPayment"; 
require_once('head.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Xenon ERP</title>
    
    <!-- Favicon -->
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="assets/plugins/bootstrap/dist/css/bootstrap.css" rel="stylesheet" />

    <!-- Animate.css Css -->
    <link href="assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Font Awesome Css -->
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

    <!-- iCheck Css -->
    <link href="assets/plugins/iCheck/skins/flat/_all.css" rel="stylesheet" />

    <!-- Switchery Css -->
    <link href="assets/plugins/switchery/dist/switchery.css" rel="stylesheet" />

    <!-- Metis Menu Css -->
    <link href="assets/plugins/metisMenu/dist/metisMenu.css" rel="stylesheet" />

    <!-- Pace Loader Css -->
    <link href="assets/plugins/pace/themes/white/pace-theme-flash.css" rel="stylesheet" />
    
    <!-- Bootstrap Select Css -->
    <link href="assets/plugins/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" />

    <!-- DateTimePicker Css -->
    <link href="assets/plugins/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="assets/css/style.css" rel="stylesheet" />
    
    <style type="text/css">
	.frame {
		width: 764px;
		height: 355px;
		background-image: url(assets/images/cheque.jpg);
		background-repeat:no-repeat;
		background-size:contain;
		position:absolute;
		font-size:14px !important;
		overflow:hidden;
	}
	.cdate {
		/*border:1px solid black;*/
		margin-left:588px !important;
		margin-top:30px !important;
		width : 150px;
		height:20px;
		padding-left:6px;
		position:absolute;
		letter-spacing:11px;
	}
	.cname {
		/*border:1px solid black;*/
		width : 450px;
		height:20px;
		position:absolute;
		margin-top:75px;
		margin-left:70px;
		font-size:16px !important;
	}
	.cawords {
		/*border:1px solid black;*/
		width:700px;
		height:55px;
		position:absolute;
		font-size:16px !important;
		margin-top:97px;
		margin-left:35px;
		padding-top:5px;
		line-height:30px;
		text-indent:90px;
		text-transform:capitalize;
	}
	.camount {
		/*border:1px solid black;*/
		width : 140px;
		height:25px;
		position:absolute;
		margin-top:128px;
		margin-left:590px;
		padding-top:5px;
		padding-left:10px;
		font-size:16px !important;
	}
	.ctype {
		border:1px solid black;
		border-left:none;
		border-right:none;
		position:absolute;
		width:120px;
		font-size:14px !important;
		text-align:center;
		-webkit-transform: rotate(315deg);
		margin-top:30px;
		margin-left:-10px;
		padding-right:10px;
			
	}
    </style>
    <style type="text/css" media="print">
	@media print {
		@page {
			size: 9.4cm 20.2cm;
			margin:0cm;
			background:none !important;
		}
		.print_frame {
			width: 764px;
			height: 355px;
			margin-left:-212px;
			margin-top:198px;
			
			-webkit-transform: rotate(270deg);
			-moz-transform: rotate(270deg);
			-o-transform: rotate(270deg);
			-ms-transform: rotate(270deg);
			transform: rotate(270deg);
		}
		.frame {
			font-family:Arial, Helvetica, sans-serif;
			font-size:16px !important;
		}
		.frame .cdate {
			font-size:14px !important;
		}
	}
	</style>
    
</head>
<body>
    <div class="all-content-wrapper">
        <!-- Top Bar -->
        <header>
            <?php include_once('header.php'); ?>
        </header>
        <?php
            if(isset($_GET['pv_id']))
            {
                $PVID = $_GET['pv_id'];
                $SelectPayment = "SELECT * FROM payment_vouchers WHERE pv_id='".$PVID."'";
                $SelectPaymentQuery = mysqli_query($con,$SelectPayment);
                if(!$SelectPaymentQuery)
                {
                    //die(mysqli_error($con));
                    header("Location: view_all_advance_payment.php");
                    exit();
                }
                $count = mysqli_num_rows($SelectPaymentQuery);
                if($count != 1)
                {
                    header("Location: view_all_advance_payment.php");
                    exit();
                }
                
                $Payment = mysqli_fetch_array($SelectPaymentQuery);
				
				$CompanyID = $Payment['company_id'];
				$PVNo = $Payment['pv_no'];
				$PVDate = $Payment['pv_date'];
				$PayTo = $Payment['pay_to'];
				$PayFrom = $Payment['pay_from'];
				$PaymentMode = $Payment['payment_mode'];
				$BankName = $Payment['bank_name'];
				$ChequeNo = $Payment['cheque_no'];
				$ChequeDate = $Payment['cheque_date'];
                $TransactionDate = $Payment['transaction_date'];
				$CardNo = $Payment['card_no'];
				$TransactionRef = $Payment['transaction_ref'];
				$PaymentDate = $Payment['payment_date'];
				$Description = $Payment['description'];
				$TotalAmount = $Payment['total_amount'];
				$TotalTDSAmount = $Payment['total_tds_amount'];
				$PVType = $Payment['pv_type'];
                
				if(!empty($Payment['pv_date']) && $Payment['pv_date'] != '0000-00-00') { $PVDate = date("d-m-Y",strtotime($Payment['pv_date'])); }
				if(!empty($Payment['cheque_date']) && $Payment['cheque_date'] != '0000-00-00') { $ChequeDate = date("d-m-Y",strtotime($Payment['cheque_date'])); }
				if(!empty($Payment['transaction_date']) && $Payment['transaction_date'] != '0000-00-00') { $TransactionDate = date("d-m-Y",strtotime($Payment['transaction_date'])); }
				if(!empty($Payment['payment_date']) && $Payment['payment_date'] != '0000-00-00') { $PaymentDate = date("d-m-Y",strtotime($Payment['payment_date'])); }
				
				$PayToDetail = $ledgerObject->selectVendorDetail($PayTo);
				$PayToDetail = json_decode($PayToDetail,true);
				$PayToName = $PayToDetail['VendorName'];
				
				$BankDetail = $ledgerObject->selectBankDetail($PayFrom);
				$BankDetail = json_decode($BankDetail,true);
				
				$BankAcNo = $BankDetail['BankAcNo'];
				$BankName = $BankDetail['BankName'];
				$BankBranch = $BankDetail['BankBranch'];
				
				$AmountInWords = no_to_words($TotalAmount);
				$AmountInWords = ucwords($AmountInWords);
            }
            else
            {
                header("Location: view_all_advance_payment.php");
                exit();
            }
        ?>
        <!-- #END# Top Bar -->
        <!-- Left Menu -->
        <aside class="sidebar">
            <?php include_once('menu.php'); ?>
        </aside>
        <!-- #END# Left Menu -->
        <section class="content dashboard">
            <div class="page-heading">
                <h1>
                    <a href="view_all_against_payment.php">
                        Against Payment
                    </a>
                    <small>
                        <i class="fa fa-angle-double-right"></i>
                        <a class="font-bold" href="update_payment_against_invoice.php?pv_id=<?php echo $PVID; ?>">Update Against Payment</a>
                    </small>
                    <small>
                        <i class="fa fa-angle-double-right"></i>
                        <a class="font-bold" href="print_cheque.php?pv_id=<?php echo $PVID; ?>">Print Cheque</a>
                    </small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="dashboard.php">Home</a></li>
                    <li><a href="view_all_against_payment.php">Against Payment</a></li>
                </ol>
            </div>
            <div class="page-body">
            	<!-- -------------- ERROR SECTION START -------------- -->
                        
                <div id="flash" class="alert alert hidden">
                    <strong>
                        <i class="fa fa-spinner fa-spin"></i>
                    </strong>
                    &nbsp; &nbsp;
                    <span></span>
                </div>
            
            	<!-- -------------- ERROR SECTION END -------------- -->
				<div class="row clearfix">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Print Cheque</div>
							<div class="panel-body p-b-25">
								<div class="row">
                                   	<div class="col-xs-12 col-sm-6">
										<table class="table table-striped table-bordered table-hover">
											<tbody>
												<tr>
													<th width="40%">Voucher No</th>
													<td width="60%"><?php echo $PVNo; ?></td>
												</tr>
												<tr>
													<th>Voucher Date</th>
													<td><?php echo $PVDate; ?></td>
												</tr>
												<tr>
													<th>Pay To</th>
													<td><?php echo $PayToName; ?></td>
												</tr>
												<tr>
													<th>Amount</th>
													<td><?php echo number_format($TotalAmount,2); ?></td>
												</tr>
											</tbody>
										</table>
									</div>
                                   	<div class="col-xs-12 col-sm-6">
										<table class="table table-striped table-bordered table-hover">
											<tbody>
												<tr>
													<th width="40%">Bank Name</th>
													<td width="60%"><?php echo $BankName; ?></td>
												</tr>
												<tr>
													<th>Branch Name</th>
													<td><?php echo $BankBranch; ?></td>
												</tr>
												<tr>
													<th>Account Number</th>
													<td><?php echo $BankAcNo; ?></td>
												</tr>
												<tr>
													<th>Cheque No</th>
													<td><?php echo $ChequeNo; ?></td>
												</tr>
												<tr>
													<th>Cheque Date</th>
													<td><?php echo $ChequeDate; ?></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<hr style="border-top: 1px dotted #e2e2e2;" />
								<div class="row">
									<div class="col-xs-12" style="padding:10px 20px; margin-bottom:400px;">
										<button id="btnPrint1" class="btn btn-success">
											<i class="ace-icon fa fa-print bigger-125"></i>
											PRINT
										</button>
										<br/><br/>
										<div id="main" style="position:absolute;">
											<div class="frame print_frame">
												<div class="ctype">A/c Payee</div>
												<div class="cdate"><?php echo date("dmY",strtotime($ChequeDate)); ?></div>
												<div class="cname"><?php echo $PayToName; ?></div>
												<div class="cawords"><?php echo $AmountInWords; ?> Only</div>
												<div class="camount"><?php echo number_format($TotalAmount,2); ?> /-</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>	
				</div>
            </div>
        </section>
        <!-- Footer -->
       
        <!-- #END# Footer -->
    </div>

    <!-- Jquery Core Js -->
    <script src="assets/plugins/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Pace Loader Js -->
    <script src="assets/plugins/pace/pace.js"></script>

    <!-- Screenfull Js -->
    <script src="assets/plugins/screenfull/src/screenfull.js"></script>

    <!-- Metis Menu Js -->
    <script src="assets/plugins/metisMenu/dist/metisMenu.js"></script>

    <!-- Jquery Slimscroll Js -->
    <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
    
    <!-- Switchery Js -->
    <script src="assets/plugins/switchery/dist/switchery.js"></script>

    <!-- iCheck Js -->
    <script src="assets/plugins/iCheck/icheck.js"></script>
    
	<!-- MomentJs Js -->
    <script src="assets/plugins/moment/moment.js"></script>
    
   	<!-- DateTimePicker Js -->
    <script src="assets/plugins/eonasdan-bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js"></script>

    <!-- Bootstrap Select Js -->
    <script src="assets/plugins/bootstrap-select/dist/js/bootstrap-select.js"></script>

    <!-- Custom Js -->
    <script src="assets/js/admin.js"></script>
    <script src="assets/js/jquery.print.js"></script>
    
    <script type="text/javascript">
        jQuery(function($) {
            $("#btnPrint1").click(function() {
				$("#main").print();
				return (false);
			});
        });
    </script>
    
</body>
</html>
<?php
ob_flush();
?>