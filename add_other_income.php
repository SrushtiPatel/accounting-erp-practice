<?php
ob_start();
$Page = "AddOtherReceipt"; 
require_once('head.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Xenon ERP</title>
    
    <!-- Favicon -->
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="assets/plugins/bootstrap/dist/css/bootstrap.css" rel="stylesheet" />

    <!-- Animate.css Css -->
    <link href="assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Font Awesome Css -->
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

    <!-- iCheck Css -->
    <link href="assets/plugins/iCheck/skins/flat/_all.css" rel="stylesheet" />

    <!-- Switchery Css -->
    <link href="assets/plugins/switchery/dist/switchery.css" rel="stylesheet" />

    <!-- Metis Menu Css -->
    <link href="assets/plugins/metisMenu/dist/metisMenu.css" rel="stylesheet" />

    <!-- Pace Loader Css -->
    <link href="assets/plugins/pace/themes/white/pace-theme-flash.css" rel="stylesheet" />
    
    <!-- Bootstrap Select Css -->
    <link href="assets/plugins/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" />

    <!-- DateTimePicker Css -->
    <link href="assets/plugins/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="assets/css/style.css" rel="stylesheet" />
    
</head>
<body class="ls-fixed navbar-fixed">
    <div class="all-content-wrapper">
        <!-- Top Bar -->
        <header>
            <?php include_once('header.php'); ?>
        </header>
        <!-- #END# Top Bar -->
        <!-- Left Menu -->
        <aside class="sidebar">
            <?php include_once('menu.php'); ?>
        </aside>
        <!-- #END# Left Menu -->
        <section class="content dashboard">
            <div class="page-heading">
                <h1>
                    <a href="view_all_other_income.php">
                        Other Income
                    </a>
                    <small>
                        <i class="fa fa-angle-double-right"></i>
                        <a class="font-bold" href="add_other_income.php">Add New Other Income</a>
                    </small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="dashboard.php">Home</a></li>
                    <li><a href="view_all_other_income.php">Other Income</a></li>
                </ol>
            </div>
            <div class="page-body">
            	<!-- -------------- ERROR SECTION START -------------- -->
                        
                <div id="flash" class="alert alert hidden">
                    <strong>
                        <i class="fa fa-spinner fa-spin"></i>
                    </strong>
                    &nbsp; &nbsp;
                    <span></span>
                </div>
            
            	<!-- -------------- ERROR SECTION END -------------- -->
				<div class="row clearfix">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Add New Other Income</div>
							<div class="panel-body p-b-25">
								<form id="Add-OtherIncome-Form" method="post" class="form-horizontal" action="#">
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                            <input type="hidden" id="AdminID" name="AdminID" value="<?php echo $_SESSION['AdminID']; ?>" required />
                                            <input type="hidden" id="CompanyID" name="CompanyID" value="<?php echo $CurrentCompanyID; ?>" required />
                                            <input type="hidden" id="RVType" name="RVType" value="3" required />
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Receipt No</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="RVNo" name="RVNo" value="<?php echo $salesObject->nextInvoiceReceiptNo($CurrentCompanyID); ?>" class="form-control" readonly required />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Receipt Date</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="RVDate" name="RVDate" data-format="DD-MM-YYYY" class="form-control js-dtp" required />
													<span style="width: 50px;" class="glyphicon glyphicon-calendar form-control-feedback"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Received From</label>
                                                <div class="col-sm-6">
                                                    <select class="col-xs-10 col-md-8 selectpicker form-control show-tick" id="ReceivedFrom" name="ReceivedFrom">
                                                        <option value="-1">-- SELECT --</option>
                                                        <?php
                                                            $SelectClient = "SELECT * FROM ledger_master WHERE group_id IN('15','16','18','19','27','33','34') AND company_id='".$CurrentCompanyID."'  ORDER BY group_id";
                                                            $SelectClientQuery = mysqli_query($con,$SelectClient);
                                                                                    
                                                            while($Client = mysqli_fetch_array($SelectClientQuery))
                                                            {
                                                                echo '<option value="'.$Client['ledger_id'].'">'.$Client['ledger_name'].'</option>';
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Received To</label>
                                                <div class="col-sm-6">
                                                    <select class="col-xs-10 col-md-8 selectpicker form-control show-tick" id="ReceivedTo" name="ReceivedTo">
                                                        <option value="-1">-- SELECT --</option>
                                                        <?php
                                                            $SelectBankAccount = "SELECT * FROM ledger_master WHERE group_id IN('20','21') AND company_id='".$CurrentCompanyID."'  ORDER BY group_id";
                                                            $SelectBankAccountQuery = mysqli_query($con,$SelectBankAccount);
                                                                                    
                                                            while($BankAccount = mysqli_fetch_array($SelectBankAccountQuery))
                                                            {
                                                                echo '<option value="'.$BankAccount['ledger_id'].'">'.$BankAccount['ledger_name'].'</option>';
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Amount</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="TotalAmount" name="TotalAmount" onKeyPress="return NuMValidation(event);" class="form-control" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Description</label>
                                                <div class="col-sm-6">
                                                    <textarea id="Description" name="Description" class="form-control no-resize" rows="4" /></textarea>
                                                </div>
                                            </div>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
											<div class="form-group">
                                                <label class="col-sm-5 control-label">Receipt Mode</label>
                                                <div class="col-sm-6">
                                                    <select class="col-xs-10 col-md-8 selectpicker form-control show-tick" id="ReceiptMode" name="ReceiptMode" onChange="return fetchData();">
                                                        <option value="-1">-- SELECT --</option>
                                                        <option value="CASH">CASH</option>
                                                        <option value="CHEQUE">CHEQUE</option>
                                                        <option value="CREDIT/DEBIT CARD">CREDIT/DEBIT CARD</option>
                                                        <option value="INTERNET BANKING">INTERNET BANKING</option>
                                                    </select>
                                                </div>
                                            </div>
											<div id="CustomFields"></div>
										</div>
									</div>
									<div class="row">    
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="form-group">
                                                <div class="text-center">
                                                    <button type="submit" class="btn btn-success">
                                                        <i class="fa fa-check bigger-110"></i>
                                                        Submit
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
								</form>
							</div>
						</div>
					</div>	
				</div>
            </div>
        </section>
        <!-- Footer -->
       
        <!-- #END# Footer -->
    </div>

    <!-- Jquery Core Js -->
    <script src="assets/plugins/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Pace Loader Js -->
    <script src="assets/plugins/pace/pace.js"></script>

    <!-- Screenfull Js -->
    <script src="assets/plugins/screenfull/src/screenfull.js"></script>

    <!-- Metis Menu Js -->
    <script src="assets/plugins/metisMenu/dist/metisMenu.js"></script>

    <!-- Jquery Slimscroll Js -->
    <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
    
    <!-- Switchery Js -->
    <script src="assets/plugins/switchery/dist/switchery.js"></script>

    <!-- iCheck Js -->
    <script src="assets/plugins/iCheck/icheck.js"></script>
    
	<!-- MomentJs Js -->
    <script src="assets/plugins/moment/moment.js"></script>
    
   	<!-- DateTimePicker Js -->
    <script src="assets/plugins/eonasdan-bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js"></script>

    <!-- Bootstrap Select Js -->
    <script src="assets/plugins/bootstrap-select/dist/js/bootstrap-select.js"></script>


    <!-- Custom Js -->
    <script src="assets/js/admin.js"></script>
    
    <script>
        function NuMValidation(evt)
        {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
            return false;
    
            return true;
        }
		function BasivElement(evt)
        {
			//Init datetimepicker
			$('.js-dtp').each(function (i, key) {
				var format = $(key).data('format');
				$(key).datetimepicker({
					format: format,
					showClear: true
				});
			});
        }
		function fetchData()
			{
				var ReceiptMode = $("#ReceiptMode").val();

				if(ReceiptMode < 0)
				{
					$("#CustomFields").html('');
					$("#ReceiptMode").focus();
					return false;
				}

				else
				{ 
					var Action = 'GetBankCustomField';
					var dataString = 'Action='+ Action +'&ReceiptMode='+ ReceiptMode;
					
					$.ajax({
						type: "POST",
						url: "includes/loader_functions.php",
						data: dataString,
						cache: false,
						success: function(result)
						{
							//alert(result);
							$("#CustomFields").html(result);
							BasivElement();
							return true;
						}
					});
				}
			}
	</script>
   <script type="text/javascript">

        jQuery(function ($) 
        {
            'use strict';
            $(document).ready(function () {
                
                //Init datetimepicker
                $('.js-dtp').each(function (i, key) {
                    var format = $(key).data('format');
                    $(key).datetimepicker({
                        format: format,
                        showClear: true
                    });
                });
            });
        });

    </script>
    <script type="text/javascript">
        $('#Add-OtherIncome-Form').on('submit', function(event) {
            
            event.preventDefault();

            var Action = 'AddOtherReceipt';
			var ReceiptMode = $("#ReceiptMode").val();
			var ReceivedFrom = $("#ReceivedFrom").val();
			var ReceivedTo = $("#ReceivedTo").val();
			
			if(ReceivedFrom < 0)
			{
				alert("Please Select Any Client.");
				$("#ReceivedFrom").focus();
                return false;
			}
			if(ReceivedTo < 0)
			{
				alert("Please Select Any Bank Account.");
                $("#ReceivedTo").focus();
                return false;
			}
			if(ReceiptMode < 0)
			{
				alert("Please Select Any Receipt Mode");
				$("#ReceiptMode").focus();
                return false;
			}
            var form_data = new FormData(this);
            form_data.append('Action',Action);
                            
            $("#flash").show();
            $("#flash i").addClass('fa-spinner');
            $("#flash i").addClass('fa-spin');
            $("#flash").removeClass('hidden');
            $('html,body').animate({ scrollTop: $(".content").offset().top},'slow');
            $("#flash span").html('Please Wait...');
           
            $.ajax({
                url: 'includes/receipt_script.php',
                type: 'POST',
                data: form_data,
                contentType: false,
                cache: false,
                processData:false,
                success: function(result)
                {
                    //alert(result);
                    var obj = JSON.parse(result);
                    var Status = obj.Status;
                    
                    if(Status == '2')
                    {
                        $("#flash").removeClass('alert alert-danger');
                        $("#flash").addClass('alert alert-success');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-times-circle');
                        $("#flash i").addClass('fa fa-fw fa-check-circle');
                        $("#flash span").html('Receipt Insert Successfully.');
                        document.getElementById("Add-OtherIncome-Form").reset();
                        $('#flash').delay(3000).fadeOut(500);
                        setTimeout(function() {
                          window.location.href = "add_other_income.php";
                        }, 1000);
                        return true;
                    }
                    else if(Status == '1')
                    {
                        $("#flash").removeClass('alert alert-success');
                        $("#flash").addClass('alert alert-danger');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash i").addClass('fa fa-fw fa-times-circle');
                        $("#flash span").html('Receipt Insert Not Successfully.');
                        $('#flash').delay(3000).fadeOut(500);
                        return true;
                    }
                    else if(Status == '0')
                    {
                        $("#flash").removeClass('alert alert-success');
                        $("#flash").addClass('alert alert-danger');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash i").addClass('fa fa-fw fa-times-circle');
                        $("#flash span").html('Receipt Already Exists.');
                        $('#flash').delay(3000).fadeOut(500);
                        return true;
                    }
                    else
                    {
                        $("#flash").removeClass('alert alert-success');
                        $("#flash").addClass('alert alert-danger');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash i").addClass('fa fa-fw fa-times-circle');
                        $("#flash span").html(result);
                        $('#flash').delay(3000).fadeOut(500);
                        return true;
                    }
                }
            });
        });
    </script>
    
</body>
</html>
<?php
ob_flush();
?>