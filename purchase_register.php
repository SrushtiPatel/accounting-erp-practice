<?php
ob_start();
$Page = "PurchaseRegister"; 
require_once('head.php');

$CompanyID = '';
$VendorID = '';
$Date = '';
$StartDate = ''; 
$EndDate = ''; 
$StartDate1 = ''; 
$EndDate1 = ''; 

if(isset($_GET['CompanyID']))
{
	$CompanyID = $_GET['CompanyID'];
}
if(isset($_GET['VendorID']))
{
	$VendorID = $_GET['VendorID'];
}
if(isset($_GET['Date']))
{
	$Date = $_GET['Date'];
	$Date = explode('-',$Date);
		
	$StartDate = str_replace('/', '-', $Date[0]);
	$EndDate = str_replace('/', '-', $Date[1]);

	$StartDate = date("Y-m-d",strtotime($StartDate)); 
	$EndDate = date("Y-m-d",strtotime($EndDate)); 
	
	$StartDate1 = date("d-m-Y",strtotime($StartDate)); 
	$EndDate1 = date("d-m-Y",strtotime($EndDate));
}

$S_SelectAll = '';
$S_INVOICEDATE = '';
$S_COMPANYNAME = '';
$S_GSTIN = '';
$S_PARTICULARS = '';
$S_HSN = '';
$S_QTY = '';
$S_RATE = '';
$S_UNIT = '';
$S_AMOUNT = '';
$S_DISCOUNT = '';
$S_TAXABLEAMOUNT = '';
$S_SGST = '';
$S_CGST = '';
$S_IGST = '';
$S_TOTAL = '';
$S_TOTALTAXABLEAMOUNT = '';
$S_TOTALSGST = '';
$S_TOTALCGST = '';
$S_TOTALIGST = '';
$S_TOTALTAX = '';
$S_GRANDTOTAL = '';

if(isset($_GET['SelectAll'])) { $S_SelectAll = $_GET['SelectAll']; }
if(isset($_GET['INVOICEDATE'])) { $S_INVOICEDATE = $_GET['INVOICEDATE']; }
if(isset($_GET['COMPANYNAME'])) { $S_COMPANYNAME = $_GET['COMPANYNAME']; }
if(isset($_GET['GSTIN'])) { $S_GSTIN = $_GET['GSTIN']; }
if(isset($_GET['PARTICULARS'])) { $S_PARTICULARS = $_GET['PARTICULARS']; }
if(isset($_GET['HSN'])) { $S_HSN = $_GET['HSN']; }
if(isset($_GET['QTY'])) { $S_QTY = $_GET['QTY']; }
if(isset($_GET['RATE'])) { $S_RATE = $_GET['RATE']; }
if(isset($_GET['UNIT'])) { $S_UNIT = $_GET['UNIT']; }
if(isset($_GET['AMOUNT'])) { $S_AMOUNT = $_GET['AMOUNT']; }
if(isset($_GET['DISCOUNT'])) { $S_DISCOUNT = $_GET['DISCOUNT']; }
if(isset($_GET['TAXABLEAMOUNT'])) { $S_TAXABLEAMOUNT = $_GET['TAXABLEAMOUNT']; }
if(isset($_GET['SGST'])) { $S_SGST = $_GET['SGST']; }
if(isset($_GET['CGST'])) { $S_CGST = $_GET['CGST']; }
if(isset($_GET['IGST'])) { $S_IGST = $_GET['IGST']; }
if(isset($_GET['TOTAL'])) { $S_TOTAL = $_GET['TOTAL']; }
if(isset($_GET['TOTALTAXABLEAMOUNT'])) { $S_TOTALTAXABLEAMOUNT = $_GET['TOTALTAXABLEAMOUNT']; }
if(isset($_GET['TOTALSGST'])) { $S_TOTALSGST = $_GET['TOTALSGST']; }
if(isset($_GET['TOTALCGST'])) { $S_TOTALCGST = $_GET['TOTALCGST']; }
if(isset($_GET['TOTALIGST'])) { $S_TOTALIGST = $_GET['TOTALIGST']; }
if(isset($_GET['TOTALTAX'])) { $S_TOTALTAX = $_GET['TOTALTAX']; }
if(isset($_GET['GRANDTOTAL'])) { $S_GRANDTOTAL = $_GET['GRANDTOTAL']; }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Xenon ERP</title>
    
    <!-- Favicon -->
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="assets/plugins/bootstrap/dist/css/bootstrap.css" rel="stylesheet" />

    <!-- Bootstrap Select Css -->
    <link href="assets/plugins/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" />

    <!-- DateTimePicker Css -->
    <link href="assets/plugins/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet" />
	
   	<!-- Bootstrap DateRangePicker Css -->
    <link href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />

    <!-- Animate.css Css -->
    <link href="assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Font Awesome Css -->
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

    
    <!-- iCheck Css Libraries | You can choose a theme from plugins/iCheck/skins instead of get all themes -->
    <link href="assets/plugins/iCheck/skins/minimal/_all.css" rel="stylesheet" />

    <!-- Switchery Css -->
    <link href="assets/plugins/switchery/dist/switchery.css" rel="stylesheet" />

    <!-- Metis Menu Css -->
    <link href="assets/plugins/metisMenu/dist/metisMenu.css" rel="stylesheet" />

    <!-- Pace Loader Css -->
    <link href="assets/plugins/pace/themes/white/pace-theme-flash.css" rel="stylesheet" />
    
    <!-- Custom Css -->
    <link href="assets/css/style.css" rel="stylesheet" />
    
    <style type="text/css">
		#PrintReportDiv {
			/*border:2px solid #000;
			border-radius:10px;*/
		}
		.PrintReportTable {
			border-collapse:collapse;
			/*text-transform:uppercase;*/
		}
		.PrintReportTable tr td, .PrintReportTable tr th {
			font-family:Arial, Helvetica, sans-serif;
		}
		.b_l 
		{
			border-left:none !important;
		}
		.b_r 
		{
			border-right:none !important;
		}
		.b_b 
		{
			border-bottom:none !important;
		}
		.b_t 
		{
			border-top:none !important;
		}
	</style>
	<style type="text/css" media="print">
		@media print {		
			@page {
				/*size: A4 landscape;*/
				margin:0.25cm;
			}
			table tr td, table tr th {
				font-family:Arial, Helvetica, sans-serif;
				font-size:10px;
				padding:3px 4px;				
			}
			.PrintReportTable {
				border-collapse:collapse;
				border:1px solid #000 !important;
				/*page-break-inside:auto;*/
			}
			.PrintReportTable tr td table {
				min-width: 50%;
				border-collapse:collapse;
				border:1px solid #000 !important;
				/*page-break-inside:auto;*/
			}
			.PrintReportTable tr td table tr td {
				border-collapse:collapse;
			}
			.PrintReportTable tr {
				/*page-break-inside:auto;*/
			}
			.PrintReportTable tr td, .PrintReportTable tr th {
				font-family:Arial, Helvetica, sans-serif;
				font-size:10px;
				padding:3px 4px;
			}
			.page-break { display:block; page-break-before:always; }
			.hidden { display:none !important; }
			.hidden-print { display:none !important; }
			
			.b_l 
			{
				border-left:none !important;
			}
			.b_r 
			{
				border-right:none !important;
			}
			.b_b 
			{
				border-bottom:none !important;
			}
			.b_t 
			{
				border-top:none !important;
			}
		}
	</style>
    
</head>
<body class="ls-toggled">
    <div class="all-content-wrapper">
        <!-- Top Bar -->
        <header>
            <?php include_once('header.php'); ?>
        </header>
        <!-- #END# Top Bar -->
        <!-- Left Menu -->
        <aside class="sidebar">
            <?php include_once('menu.php'); ?>
        </aside>
        <!-- #END# Left Menu -->
        <section class="content dashboard">
            <div class="page-heading">
                <h1>
                    <a href="purchase_register.php">
                        Purchase Register
                    </a>
                </h1>
            </div>
            <div class="page-body">
             	<div class="row clearfix">
                    <div class="col-xs-12 col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Purchase Register</div>
                            <div class="panel-body p-b-25">
                                <form id="#" method="get" class="form-horizontal" action="purchase_register.php">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <input type="hidden" id="CompanyID" name="CompanyID" value="<?php echo $CurrentCompanyID; ?>" required />
											<div class="form-group">
												<label class="col-sm-3 col-md-2 control-label">Vendor</label>
												<div class="col-sm-4 col-md-3">
													<select class="col-xs-10 col-md-6 selectpicker form-control show-tick" id="VendorID" name="VendorID">
														<option value="0">-- ALL --</option>
														<?php
															$SelectVendor = "SELECT * FROM ledger_master WHERE group_id='34' AND company_id='".$CurrentCompanyID."'  ORDER BY group_id";
															$SelectVendorQuery = mysqli_query($con,$SelectVendor);

															while($Vendor = mysqli_fetch_array($SelectVendorQuery))
															{
																if($VendorID == $Vendor['ledger_id'])
																{
																	echo '<option value="'.$Vendor['ledger_id'].'" selected>'.$Vendor['ledger_name'].'</option>';
																}
																else
																{
																	echo '<option value="'.$Vendor['ledger_id'].'">'.$Vendor['ledger_name'].'</option>';
																}
															}
														?>
													</select>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-3 col-md-2 control-label">Financial Year</label>
												<div class="col-sm-4 col-md-3">
													<input type="text" class="form-control js-daterange-picker" id="Date" name="Date" value="<?php if(!empty($StartDate1)) { echo $StartDate1.' - '.$EndDate1; } ?>" required />
													<span style="width: 50px;" class="glyphicon glyphicon-calendar form-control-feedback"></span>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-3 col-md-2 control-label"></label>
												<div class="col-sm-4 col-md-2">
													<div class="checkbox">
														<input type="checkbox" id="SelectAll" name="SelectAll" data-icheck-theme="minimal" data-icheck-color="grey" onClick="return selectAll();" <?php if($S_SelectAll == 'on') { echo 'checked'; } ?>>
														<label for="chb_4">SELECT ALL</label>
													</div>
												</div>
												<div class="col-sm-4 col-md-2">
													<div class="checkbox">
														<input type="checkbox" id="INVOICEDATE" name="INVOICEDATE" data-icheck-theme="minimal" data-icheck-color="grey" <?php if($S_INVOICEDATE == 'on') { echo 'checked'; } ?>>
														<label for="chb_4">INVOICE DATE</label>
													</div>
												</div>
												<label class="col-sm-3 control-label hidden-md hidden-lg"></label>
												<div class="col-sm-4 col-md-2">
													<div class="checkbox">
														<input type="checkbox" id="COMPANYNAME" name="COMPANYNAME" data-icheck-theme="minimal" data-icheck-color="grey" <?php if($S_COMPANYNAME == 'on') { echo 'checked'; } ?>>
														<label for="chb_4">COMPANY NAME</label>
													</div>
												</div>
												<div class="col-sm-4 col-md-2">
													<div class="checkbox">
														<input type="checkbox" id="GSTIN" name="GSTIN" data-icheck-theme="minimal" data-icheck-color="grey" <?php if($S_GSTIN == 'on') { echo 'checked'; } ?>>
														<label for="chb_4">GSTIN</label>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-3 col-md-2 control-label"></label>
												<div class="col-sm-4 col-md-2">
													<div class="checkbox">
														<input type="checkbox" id="PARTICULARS" name="PARTICULARS" data-icheck-theme="minimal" data-icheck-color="grey" <?php if($S_PARTICULARS == 'on') { echo 'checked'; } ?>>
														<label for="chb_4">PARTICULARS</label>
													</div>
												</div>
												<div class="col-sm-4 col-md-2">
													<div class="checkbox">
														<input type="checkbox" id="HSN" name="HSN" data-icheck-theme="minimal" data-icheck-color="grey" <?php if($S_HSN == 'on') { echo 'checked'; } ?>>
														<label for="chb_4">HSN</label>
													</div>
												</div>
												<label class="col-sm-3 control-label hidden-md hidden-lg"></label>
												<div class="col-sm-4 col-md-2">
													<div class="checkbox">
														<input type="checkbox" id="QTY" name="QTY" data-icheck-theme="minimal" data-icheck-color="grey" <?php if($S_QTY == 'on') { echo 'checked'; } ?>>
														<label for="chb_4">QTY</label>
													</div>
												</div>
												<div class="col-sm-4 col-md-2">
													<div class="checkbox">
														<input type="checkbox" id="RATE" name="RATE" data-icheck-theme="minimal" data-icheck-color="grey" <?php if($S_RATE == 'on') { echo 'checked'; } ?>>
														<label for="chb_4">RATE</label>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-3 col-md-2 control-label"></label>
												<div class="col-sm-4 col-md-2">
													<div class="checkbox">
														<input type="checkbox" id="UNIT" name="UNIT" data-icheck-theme="minimal" data-icheck-color="grey" <?php if($S_UNIT == 'on') { echo 'checked'; } ?>>
														<label for="chb_4">UNIT</label>
													</div>
												</div>
												<div class="col-sm-4 col-md-2">
													<div class="checkbox">
														<input type="checkbox" id="AMOUNT" name="AMOUNT" data-icheck-theme="minimal" data-icheck-color="grey" <?php if($S_AMOUNT == 'on') { echo 'checked'; } ?>>
														<label for="chb_4">AMOUNT</label>
													</div>
												</div>
												<label class="col-sm-3 control-label hidden-md hidden-lg"></label>
												<div class="col-sm-4 col-md-2">
													<div class="checkbox">
														<input type="checkbox" id="DISCOUNT" name="DISCOUNT" data-icheck-theme="minimal" data-icheck-color="grey" <?php if($S_DISCOUNT == 'on') { echo 'checked'; } ?>>
														<label for="chb_4">DISCOUNT</label>
													</div>
												</div>
												<div class="col-sm-4 col-md-2">
													<div class="checkbox">
														<input type="checkbox" id="TAXABLEAMOUNT" name="TAXABLEAMOUNT" data-icheck-theme="minimal" data-icheck-color="grey" <?php if($S_TAXABLEAMOUNT == 'on') { echo 'checked'; } ?>>
														<label for="chb_4">TAXABLE AMOUNT</label>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-3 col-md-2 control-label"></label>
												<div class="col-sm-4 col-md-2">
													<div class="checkbox">
														<input type="checkbox" id="SGST" name="SGST" data-icheck-theme="minimal" data-icheck-color="grey" <?php if($S_SGST == 'on') { echo 'checked'; } ?>>
														<label for="chb_4">SGST</label>
													</div>
												</div>
												<div class="col-sm-4 col-md-2">
													<div class="checkbox">
														<input type="checkbox" id="CGST" name="CGST" data-icheck-theme="minimal" data-icheck-color="grey" <?php if($S_CGST == 'on') { echo 'checked'; } ?>>
														<label for="chb_4">CGST</label>
													</div>
												</div>
												<label class="col-sm-3 control-label hidden-md hidden-lg"></label>
												<div class="col-sm-4 col-md-2">
													<div class="checkbox">
														<input type="checkbox" id="IGST" name="IGST" data-icheck-theme="minimal" data-icheck-color="grey" <?php if($S_IGST == 'on') { echo 'checked'; } ?>>
														<label for="chb_4">IGST</label>
													</div>
												</div>
												<div class="col-sm-4 col-md-2">
													<div class="checkbox">
														<input type="checkbox" id="TOTAL" name="TOTAL" data-icheck-theme="minimal" data-icheck-color="grey" <?php if($S_TOTAL == 'on') { echo 'checked'; } ?>>
														<label for="chb_4">TOTAL</label>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-3 col-md-2 control-label"></label>
												<div class="col-sm-4 col-md-2">
													<div class="checkbox">
														<input type="checkbox" id="TOTALSGST" name="TOTALSGST" data-icheck-theme="minimal" data-icheck-color="grey" <?php if($S_TOTALSGST == 'on') { echo 'checked'; } ?>>
														<label for="chb_4">TOTAL SGST</label>
													</div>
												</div>
												<div class="col-sm-4 col-md-2">
													<div class="checkbox">
														<input type="checkbox" id="TOTALCGST" name="TOTALCGST" data-icheck-theme="minimal" data-icheck-color="grey" <?php if($S_TOTALCGST == 'on') { echo 'checked'; } ?>>
														<label for="chb_4">TOTAL CGST</label>
													</div>
												</div>
												<label class="col-sm-3 control-label hidden-md hidden-lg"></label>
												<div class="col-sm-4 col-md-2">
													<div class="checkbox">
														<input type="checkbox" id="TOTALIGST" name="TOTALIGST" data-icheck-theme="minimal" data-icheck-color="grey" <?php if($S_TOTALIGST == 'on') { echo 'checked'; } ?>>
														<label for="chb_4">TOTAL IGST</label>
													</div>
												</div>
												<div class="col-sm-4 col-md-2">
													<div class="checkbox">
														<input type="checkbox" id="TOTALTAX" name="TOTALTAX" data-icheck-theme="minimal" data-icheck-color="grey" <?php if($S_TOTALTAX == 'on') { echo 'checked'; } ?>>
														<label for="chb_4">TOTAL GST</label>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-3 col-md-2 control-label"></label>
												<div class="col-sm-8 col-md-4">
													<div class="checkbox">
														<input type="checkbox" id="TOTALTAXABLEAMOUNT" name="TOTALTAXABLEAMOUNT" data-icheck-theme="minimal" data-icheck-color="grey" <?php if($S_TOTALTAXABLEAMOUNT == 'on') { echo 'checked'; } ?>>
														<label for="chb_4">TOTAL TAXABLE AMOUNT</label>
													</div>
												</div>
												<label class="col-sm-3 control-label hidden-md hidden-lg"></label>
												<div class="col-sm-4 col-md-2">
													<div class="checkbox">
														<input type="checkbox" id="GRANDTOTAL" name="GRANDTOTAL" data-icheck-theme="minimal" data-icheck-color="grey" <?php if($S_GRANDTOTAL == 'on') { echo 'checked'; } ?>>
														<label for="chb_4">GRAND TOTAL</label>
													</div>
												</div>
											</div>
                                       		<div class="form-group">
                                                <div class="col-sm-offset-3 col-sm-4 col-md-offset-2 col-md-3">
                                                    <button type="submit" class="btn btn-success">
                                                        <i class="fa fa-check bigger-110"></i>
                                                        Submit
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <br/>
                <?php
				if($CompanyID>0 && $VendorID>-1 && !empty($StartDate) && !empty($EndDate))
				{
					if($VendorID < 1)
					{
						$VendorName = 'ALL';
					}
					else
					{
						$VendorDetail = $ledgerObject->selectVendorDetail($VendorID);
						$VendorDetail = json_decode($VendorDetail,true);
						$VendorName = $VendorDetail['VendorName'];
					}
				?>
                <div class="row clearfix">
                    <div class="col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Purchase Register Of Vendor : <?php echo $VendorName; ?>  [<?php echo $StartDate1.' To '.$EndDate1; ?>]</div>
                            <div class="panel-body p-b-25">
                            	 <div class="row clearfix">
                    				<div class="col-xs-12"> 
                      					<button id="btnPrint" class="btn btn-raised btn-primary">
											<i class="ace-icon fa fa-print bigger-125"></i>
											PRINT
										</button>
                     					<a id="btnXLS" onClick="javascript:fnExcelReport();" class="btn btn-raised btn-success">
											<i class="ace-icon fa fa-file-excel-o bigger-125"></i>
											EXCEL
										</a>
                      				
                      					<div id="PrintReport" style="margin-top:20px;">
											<div id="PrintReportDiv">
												<table width="100%" class="table">
													<tr>
														<th colspan="2" class="b_b b_l b_r b_t" style="text-align: center;font-size: 14px;">
															Purchase Register Of &nbsp; : <?php echo '&nbsp;'.$VendorName; ?>
														</th>
													</tr>	
													<tr>
														<th class="b_b b_l b_r b_t" style="text-align: left">
														 Date : <?php echo ' '.$StartDate1.' '.'To'.' '.$EndDate1.' '?>
														</th>
														<th class="b_b b_l b_r b_t" style="text-align: right">Report Generated On : <?php echo date("d-m-Y"); ?></th>
													</tr>		
												</table> 
												<table id="ReportTable" class="PrintReportTable table table-striped table-bordered table-hover" width="100%" border="1" style="margin-bottom:0px;">
													<thead>
														<tr>
															<th width="5%">INVOICE NO</th>
															<?php if($S_INVOICEDATE == 'on') { ?>
															<th width="6%">INVOICE DATE</th>
															<?php } ?>
															<?php if($S_COMPANYNAME == 'on') { ?>
															<th width="9%">COMPANY NAME</th>
															<?php } ?>
															<?php if($S_GSTIN == 'on') { ?>
															<th width="5%">GSTIN</th>
															<?php } ?>
															<?php if($S_PARTICULARS == 'on') { ?>
															<th width="6%">PARTICULARS</th>
															<?php } ?>
															<?php if($S_HSN == 'on') { ?>
															<th width="5%">HSN</th>
															<?php } ?>
															<?php if($S_QTY == 'on') { ?>
															<th width="5%">QUANTITY</th>
															<?php } ?>
															<?php if($S_RATE == 'on') { ?>
															<th width="5%">RATE</th>
															<?php } ?>
															<?php if($S_UNIT == 'on') { ?>
															<th width="5%">UNIT</th>
															<?php } ?>
															<?php if($S_AMOUNT == 'on') { ?>
															<th width="6%">AMOUNT</th>
															<?php } ?>
															<?php if($S_DISCOUNT == 'on') { ?>
															<th width="6%">DISCOUNT</th>
															<?php } ?>
															<?php if($S_TAXABLEAMOUNT == 'on') { ?>
															<th width="7%">TAXABLE AMOUNT</th>
															<?php } ?>
															<?php if($S_SGST == 'on') { ?>
															<th width="7%">SGST</th>
															<?php } ?>
															<?php if($S_CGST == 'on') { ?>
															<th width="7%">CGST</th>
															<?php } ?>
															<?php if($S_IGST == 'on') { ?>
															<th width="7%">IGST</th> 
															<?php } ?>
															<?php if($S_TOTAL == 'on') { ?>
															<th width="7%">TOTAL</th> 
															<?php } ?>
															<?php if($S_TOTALTAXABLEAMOUNT == 'on') { ?>
															<th width="7%">TOTAL TAXABLE AMOUNT</th>
															<?php } ?>
															<?php if($S_TOTALSGST == 'on') { ?>
															<th width="7%">TOTAL SGST</th>
															<?php } ?>
															<?php if($S_TOTALCGST == 'on') { ?>
															<th width="7%">TOTAL CGST</th>
															<?php } ?>
															<?php if($S_TOTALIGST == 'on') { ?>
															<th width="7%">TOTAL IGST</th>
															<?php } ?>
															<?php if($S_TOTALTAX == 'on') { ?>
															<th width="7%">TOTAL GST</th>
															<?php } ?>
															<?php if($S_GRANDTOTAL == 'on') { ?>
															<th width="8%">GRAND TOTAL</th>	
															<?php } ?>
														</tr>
													</thead>
													<tbody>
													<?php
													$T_Amount = 0;
													$T_Discount = 0;
													$T_TaxableAmount = 0;
													$T_SGST = 0;
													$T_CGST = 0;
													$T_IGST = 0;
													$T_Total = 0;
													$T_TotalTaxableAmount = 0;
													$T_TotalSGST = 0;
													$T_TotalCGST = 0;
													$T_TotalIGST = 0;
													$T_Tax = 0;
													$T_GrandTotal = 0;

													if($VendorID == 0)
													{
														$SelectInvoice = mysqli_query($con,"SELECT * FROM purchase_master WHERE invoice_date>='".$StartDate."' AND invoice_date<='".$EndDate."' ORDER BY invoice_date ASC");
													}
													else
													{
														$SelectInvoice = mysqli_query($con,"SELECT * FROM purchase_master WHERE vendor_id='".$VendorID."' AND invoice_date>='".$StartDate."' AND invoice_date<='".$EndDate."' ORDER BY invoice_date ASC");
													}
													while($Invoice = mysqli_fetch_array($SelectInvoice))
													{
														$PurchaseID = $Invoice['purchase_id'];
														$InvoiceNo = $Invoice['invoice_no'];
														$InvoiceDate = $Invoice['invoice_date'];
														$SupplierName = $Invoice['vendor_name'];
														$SupplierGSTIN = $Invoice['vendor_gstin'];
														$TotalTax = $Invoice['total_tax_amount'];
														$GrandTotal = $Invoice['grand_total'];

														$T_Tax += $TotalTax;
														$T_GrandTotal += $GrandTotal;

														$SelectInvoiceItem = mysqli_query($con,"SELECT * FROM purchase_item_master WHERE purchase_id='".$PurchaseID."'");
														$InvoiceItem = mysqli_fetch_array($SelectInvoiceItem);
														$Length = mysqli_num_rows($SelectInvoiceItem);

														$T_Amount += $InvoiceItem['amount'];
														$T_Discount += $InvoiceItem['discount'];
														$T_TaxableAmount += $InvoiceItem['taxable_value'];
														$T_SGST += $InvoiceItem['tax1_amount'];
														$T_CGST += $InvoiceItem['tax2_amount'];
														$T_IGST += $InvoiceItem['tax3_amount'];
														$T_Total += $InvoiceItem['total'];
														
														$TotalGSTData = $purchaseObject->getTotalGST($PurchaseID);
														$TotalGSTData = json_decode($TotalGSTData,true);
														
														$TotalTaxableAmount = $TotalGSTData['TotalTaxableAmount'];
														$TotalSGST = $TotalGSTData['TotalSGST'];
														$TotalCGST = $TotalGSTData['TotalCGST'];
														$TotalIGST = $TotalGSTData['TotalIGST'];
														$T_TotalTaxableAmount += $TotalTaxableAmount;
														$T_TotalSGST += $TotalSGST;
														$T_TotalCGST += $TotalCGST;
														$T_TotalIGST += $TotalIGST;
													?>
													<tr>
														<td class="b_b" align="left">
															<?php echo $InvoiceNo; ?>
														</td>
														<?php if($S_INVOICEDATE == 'on') { ?>
														<td class="b_b" align="left">
															<?php echo date("d-m-Y",strtotime($InvoiceDate)); ?>
														</td>
														<?php } ?>
														<?php if($S_COMPANYNAME == 'on') { ?>
														<td  class="b_b"align="left">
															<?php echo $SupplierName; ?>
														</td>
														<?php } ?>
														<?php if($S_GSTIN == 'on') { ?>
														<td class="b_b" align="left">
															<?php echo $SupplierGSTIN; ?>
														</td>
														<?php } ?>
														<?php if($S_PARTICULARS == 'on') { ?>
														<td align="left">
															<?php echo $InvoiceItem['item_name']; ?>
															<br/>
															<small><?php echo $InvoiceItem['description']; ?></small>
														</td>
														<?php } ?>
														<?php if($S_HSN == 'on') { ?>
														<td align="left">
															<?php echo $InvoiceItem['hsn_code']; ?>
														</td>
														<?php } ?>
														<?php if($S_QTY == 'on') { ?>
														<td align="right">
															<?php echo number_format($InvoiceItem['qty'],3); ?>
														</td>
														<?php } ?>
														<?php if($S_RATE == 'on') { ?>
														<td align="right">
															<?php echo number_format($InvoiceItem['rate'],2); ?>
														</td>
														<?php } ?>
														<?php if($S_UNIT == 'on') { ?>
														<td align="center">
															<?php echo $InvoiceItem['unit']; ?>
														</td>
														<?php } ?>
														<?php if($S_AMOUNT == 'on') { ?>
														<td align="right">
															<?php echo number_format($InvoiceItem['amount'],2); ?>
														</td>
														<?php } ?>
														<?php if($S_DISCOUNT == 'on') { ?>
														<td align="right">
															<?php echo number_format($InvoiceItem['discount'],2); ?>
														</td>
														<?php } ?>
														<?php if($S_TAXABLEAMOUNT == 'on') { ?>
														<td align="right">
															<?php echo number_format($InvoiceItem['taxable_value'],2); ?>
														</td>
														<?php } ?>
														<?php if($S_SGST == 'on') { ?>
														<td align="right">
															<?php echo number_format($InvoiceItem['tax1_amount'],2); ?>
														</td>
														<?php } ?>
														<?php if($S_CGST == 'on') { ?>
														<td align="right">
															<?php echo number_format($InvoiceItem['tax2_amount'],2); ?>
														</td>
														<?php } ?>
														<?php if($S_IGST == 'on') { ?>
														<td align="right">
															<?php echo number_format($InvoiceItem['tax3_amount'],2); ?>
														</td>
														<?php } ?>
														<?php if($S_TOTAL == 'on') { ?>
														<td align="right">
															<?php echo number_format($InvoiceItem['total'],2); ?>
														</td>
														<?php } ?>
														<?php if($S_TOTALTAXABLEAMOUNT == 'on') { ?>
														<td class="b_b" align="right">
															<?php echo number_format($TotalTaxableAmount,2); ?>
														</td>
														<?php } ?>
														<?php if($S_TOTALSGST == 'on') { ?>
														<td class="b_b" align="right">
															<?php echo number_format($TotalSGST,2); ?>
														</td>
														<?php } ?>
														<?php if($S_TOTALCGST == 'on') { ?>
														<td class="b_b" align="right">
															<?php echo number_format($TotalCGST,2); ?>
														</td>
														<?php } ?>
														<?php if($S_TOTALIGST == 'on') { ?>
														<td class="b_b" align="right">
															<?php echo number_format($TotalIGST,2); ?>
														</td>
														<?php } ?>
														<?php if($S_TOTALTAX == 'on') { ?>
														<td class="b_b" align="right">
															<?php echo number_format($TotalTax,2); ?>
														</td>
														<?php } ?>
														<?php if($S_GRANDTOTAL == 'on') { ?>
														<td class="b_b" align="right">
															<?php echo number_format($GrandTotal,2); ?>
														</td>
														<?php } ?>
													</tr>
													<?php
													if($S_PARTICULARS == 'on' || $S_HSN == 'on' || $S_QTY == 'on' || $S_RATE == 'on' || $S_UNIT == 'on' || $S_AMOUNT == 'on' || $S_DISCOUNT == 'on' || $S_TAXABLEAMOUNT == 'on' || $S_SGST == 'on' || $S_CGST == 'on' || $S_IGST == 'on' || $S_TOTAL == 'on')
													{
													for($i=1;$i<$Length;$i++)
													{
														$InvoiceItem = mysqli_fetch_array($SelectInvoiceItem);

														$T_Amount += $InvoiceItem['amount'];
														$T_Discount += $InvoiceItem['discount'];
														$T_TaxableAmount += $InvoiceItem['taxable_value'];
														$T_SGST += $InvoiceItem['tax1_amount'];
														$T_CGST += $InvoiceItem['tax2_amount'];
														$T_IGST += $InvoiceItem['tax3_amount'];
														$T_Total += $InvoiceItem['total'];
													?>
													<tr>
														<td class="b_b" align="left"></td>
														<?php if($S_INVOICEDATE == 'on') { ?>
														<td class="b_b" align="left"></td>
														<?php } ?>
														<?php if($S_COMPANYNAME == 'on') { ?>
														<td  class="b_b"align="left"></td>
														<?php } ?>
														<?php if($S_GSTIN == 'on') { ?>
														<td class="b_b" align="left"></td>
														<?php } ?>
														<?php if($S_PARTICULARS == 'on') { ?>
														<td align="left">
															<?php echo $InvoiceItem['item_name']; ?>
															<br/>
															<small><?php echo $InvoiceItem['description']; ?></small>
														</td>
														<?php } ?>
														<?php if($S_HSN == 'on') { ?>
														<td align="left">
															<?php echo $InvoiceItem['hsn_code']; ?>
														</td>
														<?php } ?>
														<?php if($S_QTY == 'on') { ?>
														<td align="right">
															<?php echo number_format($InvoiceItem['qty'],3); ?>
														</td>
														<?php } ?>
														<?php if($S_RATE == 'on') { ?>
														<td align="right">
															<?php echo number_format($InvoiceItem['rate'],2); ?>
														</td>
														<?php } ?>
														<?php if($S_UNIT == 'on') { ?>
														<td align="center">
															<?php echo $InvoiceItem['unit']; ?>
														</td>
														<?php } ?>
														<?php if($S_AMOUNT == 'on') { ?>
														<td align="right">
															<?php echo number_format($InvoiceItem['amount'],2); ?>
														</td>
														<?php } ?>
														<?php if($S_DISCOUNT == 'on') { ?>
														<td align="right">
															<?php echo number_format($InvoiceItem['discount'],2); ?>
														</td>
														<?php } ?>
														<?php if($S_TAXABLEAMOUNT == 'on') { ?>
														<td align="right">
															<?php echo number_format($InvoiceItem['taxable_value'],2); ?>
														</td>
														<?php } ?>
														<?php if($S_SGST == 'on') { ?>
														<td align="right">
															<?php echo number_format($InvoiceItem['tax1_amount'],2); ?>
														</td>
														<?php } ?>
														<?php if($S_CGST == 'on') { ?>
														<td align="right">
															<?php echo number_format($InvoiceItem['tax2_amount'],2); ?>
														</td>
														<?php } ?>
														<?php if($S_IGST == 'on') { ?>
														<td align="right">
															<?php echo number_format($InvoiceItem['tax3_amount'],2); ?>
														</td>
														<?php } ?>
														<?php if($S_TOTAL == 'on') { ?>
														<td align="right">
															<?php echo number_format($InvoiceItem['total'],2); ?>
														</td>
														<?php } ?>
														<?php if($S_TOTALTAXABLEAMOUNT == 'on') { ?>
														<td class="b_b" align="right"></td>
														<?php } ?>
														<?php if($S_TOTALSGST == 'on') { ?>
														<td class="b_b" align="right"></td>
														<?php } ?>
														<?php if($S_TOTALCGST == 'on') { ?>
														<td class="b_b" align="right"></td>
														<?php } ?>
														<?php if($S_TOTALIGST == 'on') { ?>
														<td class="b_b" align="right"></td>
														<?php } ?>
														<?php if($S_TOTALTAX == 'on') { ?>
														<td class="b_b" align="right"></td>
														<?php } ?>
														<?php if($S_GRANDTOTAL == 'on') { ?>
														<td class="b_b" align="right"></td>
														<?php } ?>
													</tr>
													<?php
													}
													}
													}
													?>
													<tr>
														<td align="center" style="font-weight: bold;">
															Total
														</td>
														<?php if($S_INVOICEDATE == 'on') { ?>
														<td class="b_b" align="left"></td>
														<?php } ?>
														<?php if($S_COMPANYNAME == 'on') { ?>
														<td  class="b_b"align="left"></td>
														<?php } ?>
														<?php if($S_GSTIN == 'on') { ?>
														<td class="b_b" align="left"></td>
														<?php } ?>
														<?php if($S_PARTICULARS == 'on') { ?>
														<td class="b_b" align="left"></td>
														<?php } ?>
														<?php if($S_HSN == 'on') { ?>
														<td class="b_b" align="left"></td>
														<?php } ?>
														<?php if($S_QTY == 'on') { ?>
														<td class="b_b" align="left"></td>
														<?php } ?>
														<?php if($S_RATE == 'on') { ?>
														<td class="b_b" align="left"></td>
														<?php } ?>
														<?php if($S_UNIT == 'on') { ?>
														<td class="b_b" align="left"></td>
														<?php } ?>
														<?php if($S_AMOUNT == 'on') { ?>
														<td align="right" style="font-weight: bold;">
															<?php echo number_format($T_Amount,2); ?>
														</td>
														<?php } ?>
														<?php if($S_DISCOUNT == 'on') { ?>
														<td align="right" style="font-weight: bold;">
															<?php echo number_format($T_Discount,2); ?>
														</td>
														<?php } ?>
														<?php if($S_TAXABLEAMOUNT == 'on') { ?>
														<td align="right" style="font-weight: bold;">
															<?php echo number_format($T_TaxableAmount,2); ?>
														</td>
														<?php } ?>
														<?php if($S_SGST == 'on') { ?>
														<td align="right" style="font-weight: bold;">
															<?php echo number_format($T_SGST,2); ?>
														</td>
														<?php } ?>
														<?php if($S_CGST == 'on') { ?>
														<td align="right" style="font-weight: bold;">
															<?php echo number_format($T_CGST,2); ?>
														</td>
														<?php } ?>
														<?php if($S_IGST == 'on') { ?>
														<td align="right" style="font-weight: bold;">
															<?php echo number_format($T_IGST,2); ?>
														</td>
														<?php } ?>
														<?php if($S_TOTAL == 'on') { ?>
														<td align="right" style="font-weight: bold;">
															<?php echo number_format($T_Total,2); ?>
														</td>
														<?php } ?>
														<?php if($S_TOTALTAXABLEAMOUNT == 'on') { ?>
														<td align="right" style="font-weight: bold;">
															<?php echo number_format($T_TotalTaxableAmount,2); ?>
														</td>
														<?php } ?>
														<?php if($S_TOTALSGST == 'on') { ?>
														<td align="right" style="font-weight: bold;">
															<?php echo number_format($T_TotalSGST,2); ?>
														</td>
														<?php } ?>
														<?php if($S_TOTALCGST == 'on') { ?>
														<td align="right" style="font-weight: bold;">
															<?php echo number_format($T_TotalCGST,2); ?>
														</td>
														<?php } ?>
														<?php if($S_TOTALIGST == 'on') { ?>
														<td align="right" style="font-weight: bold;">
															<?php echo number_format($T_TotalIGST,2); ?>
														</td>
														<?php } ?>
														<?php if($S_TOTALTAX == 'on') { ?>
														<td align="right" style="font-weight: bold;">
															<?php echo number_format($T_Tax,2); ?>
														</td>
														<?php } ?>
														<?php if($S_GRANDTOTAL == 'on') { ?>
														<td align="right" style="font-weight: bold;">
															<?php echo number_format($T_GrandTotal,2); ?>
														</td>
														<?php } ?>
													</tr>
												</tbody>
												</table>
											</div>
										</div>		
                       				</div>
                        		</div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
				}
				?>
            </div>
        </section>
        <!-- Footer -->
       
        <!-- #END# Footer -->
    </div>

    <!-- Jquery Core Js -->
    <script src="assets/plugins/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
    
    <!-- MomentJs Js -->
    <script src="assets/plugins/moment/moment.js"></script>
    
    <!-- DateTimePicker Js -->
    <script src="assets/plugins/eonasdan-bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js"></script>
    
    <!-- Bootstrap DateRangePicker Js -->
    <script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>


    <!-- Pace Loader Js -->
    <script src="assets/plugins/pace/pace.js"></script>

    <!-- Screenfull Js -->
    <script src="assets/plugins/screenfull/src/screenfull.js"></script>

    <!-- Metis Menu Js -->
    <script src="assets/plugins/metisMenu/dist/metisMenu.js"></script>

    <!-- Jquery Slimscroll Js -->
    <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   	
	<!-- Switchery Js -->
    <script src="assets/plugins/switchery/dist/switchery.js"></script>
    
    <!-- iCheck Js -->
    <script src="assets/plugins/iCheck/icheck.js"></script>
    
    <!-- Bootstrap Select Js -->
    <script src="assets/plugins/bootstrap-select/dist/js/bootstrap-select.js"></script>

    <!-- Custom Js -->
    <script src="assets/js/admin.js"></script>
    <script src="assets/js/jquery.print.js"></script>	
	<script src="assets/js/jquery.table2excel.min.js"></script>
    
    <script type="text/javascript">
        jQuery(function($) {
			
			$('.js-daterange-picker').daterangepicker({
				opens: "right",
				drops: "down",
				applyClass: "btn-primary",
				locale: {
					format: 'DD/MM/YYYY'
				}
			});
			
			$("#btnPrint").click(function() {
				$("#PrintReport").print();
				return (false);
			});
			
			//Init checkboxes and radios
			$('input[data-icheck-theme]').each(function (i, key) {
				var color = $(key).data('icheckColor');
				var theme = $(key).data('icheckTheme');
				var baseCheckboxClass = 'icheckbox_' + theme;
				var baseRadioClass = 'iradio_' + theme;

				$(key).iCheck({
					checkboxClass: color === theme ? baseCheckboxClass : baseCheckboxClass + '-' + color,
					radioClass: color === theme ? baseRadioClass : baseRadioClass + '-' + color
				});
			});
			
			$("#SelectAll").on('ifClicked', function (event) 
			{
				var status = $("#SelectAll").prop('checked');
				
				if(status == false)
				{
					$("input[type=checkbox]").prop('checked',true);
					$(".icheckbox_minimal-grey").addClass('checked');
				}
				else if(status == true)
				{
					$("input[type=checkbox]").prop('checked',false);
					$(".icheckbox_minimal-grey").removeClass('checked');
				}
			});
        });
		
		function fnExcelReport() {
			
			$("#ReportTable").table2excel({
				exclude: ".noExl",
				name: "Purchase Report",
				filename: "Purchase Report",
				fileext: ".xls",
				exclude_img: true,
				exclude_links: true,
				exclude_inputs: true
			});
		}
		
    </script>
</body>
</html>
<?php
ob_flush();
?>