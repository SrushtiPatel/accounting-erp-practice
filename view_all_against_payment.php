<?php
ob_start();
$Page = "ViewAgainstPayment"; 
require_once('head.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Xenon ERP</title>
    
    <!-- Favicon -->
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="assets/plugins/bootstrap/dist/css/bootstrap.css" rel="stylesheet" />

    <!-- Animate.css Css -->
    <link href="assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Font Awesome Css -->
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

    <!-- iCheck Css -->
    <link href="assets/plugins/iCheck/skins/flat/_all.css" rel="stylesheet" />

    <!-- Switchery Css -->
    <link href="assets/plugins/switchery/dist/switchery.css" rel="stylesheet" />

    <!-- Metis Menu Css -->
    <link href="assets/plugins/metisMenu/dist/metisMenu.css" rel="stylesheet" />

    <!-- Pace Loader Css -->
    <link href="assets/plugins/pace/themes/white/pace-theme-flash.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="assets/css/style.css" rel="stylesheet" />

    <!-- Jquery Datatables Css -->
    <link href="assets/plugins/DataTables/media/css/dataTables.bootstrap.css" rel="stylesheet" />
    
</head>
<body class="ls-fixed navbar-fixed">
    <div class="all-content-wrapper">
        <!-- Top Bar -->
        <header>
            <?php include_once('header.php'); ?>
        </header>
        <!-- #END# Top Bar -->
        <!-- Left Menu -->
        <aside class="sidebar">
            <?php include_once('menu.php'); ?>
        </aside>
        <!-- #END# Left Menu -->
        <section class="content dashboard">
             <div class="page-heading">
                <h1>
                    <a href="view_all_against_payment.php">
                        Against Payment
                    </a>
                    <small>
                        <i class="fa fa-angle-double-right"></i>
                        <a class="font-bold" href="add_payment_against_invoice.php">Add New Against Payment</a>
                    </small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="dashboard.php">Home</a></li>
                    <li><a href="view_all_against_payment.php">Against Payment</a></li>
                </ol>
            </div>

            <div class="page-body">
            	<!-- -------------- ERROR SECTION START -------------- -->
                        
	            <div id="flash" class="alert alert hidden">
	                <strong>
	                    <i class="fa fa-spinner fa-spin"></i>
	                </strong>
	                &nbsp; &nbsp;
	                <span></span>
	            </div>
            
            	<!-- -------------- ERROR SECTION END -------------- -->
                <div class="panel panel-default">
                    <div class="panel-heading">Against Payment</div>
                    <div class="panel-body">
                        <table id="Payment-Table" class="table table-striped table-hover js-basic-example dataTable">
                            <thead>
                                <tr>
                                    <th>#</th>
									<th>Payment Voucher No</th>
									<th>Payment Voucher Date</th>
									<th>Pay To</th>
									<th>Payment Mode</th>
									<th>Total Amount</th>
									<th></th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                    $SelectPayment = mysqli_query($con,"SELECT * FROM payment_vouchers WHERE pv_type='1' AND pv_date>='".$FYearStart."' AND pv_date<='".$FYearEnd."' AND company_id='".$CurrentCompanyID."'");
                                    if(!$SelectPayment)
                                    {
                                        die(mysqli_error($con));
                                    }
                                    $count = 1;
                                    while($Payment = mysqli_fetch_array($SelectPayment))
                                    {
                                        $PVID = $Payment['pv_id'];
										$PVNo = $Payment['pv_no'];
										$PVDate = $Payment['pv_date'];
										$PayTo = $Payment['pay_to'];
										$PayFrom = $Payment['pay_from'];
										$PaymentMode = $Payment['payment_mode'];
										$TotalAmount = $Payment['total_amount'];
										$TotalTDSAmount = $Payment['total_tds_amount'];
										
										$VendorDetail = $ledgerObject->selectVendorDetail($PayTo);
							            $VendorDetail = json_decode($VendorDetail,true);
							            $VendorName = $VendorDetail['VendorName'];
										
                                        if(!empty($PVDate)) { $PVDate = date("d-m-Y",strtotime($Payment['pv_date'])); }
                                    ?>
                                
                                    <tr>
                                        <td class="center"><?php echo $count; $count++; ?></td>
                                        <td><?php echo $PVNo;?></td>
                                        <td><?php echo $PVDate;?></td>
                                        <td><?php echo $VendorName;?></td>
                                        <td><?php echo $PaymentMode;?></td>
                                        <td><?php echo $TotalAmount;?></td>
                                        <td>
											<div class="action-buttons">
                                           		<?php if($PaymentMode == 'CHEQUE') { ?>
                                            	<a href="print_cheque.php?pv_id=<?php echo $PVID; ?>" class="col-primary" title="Print" target="_blank"><i class="fa fa-print"></i></a>
                                               	<?php } ?>
                                                <a href="update_payment_against_invoice.php?pv_id=<?php echo $PVID; ?>" class="col-success" title="Edit"><i class="fa fa-pencil-square-o"></i></a>
                                                <a data-toggle="modal" href="#myModel_Remove_Payment_<?php echo $PVID; ?>" class="col-danger" title="Remove"><i class="fa fa-trash-o"></i></a>
                                            </div>
<!-- ------------------------------ Remove Model Start ------------------------------ -->
    <div class="modal fade" id="myModel_Remove_Payment_<?php echo $PVID; ?>" tabindex="-1" role="dialog" aria-labelledby="myModelLable" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="Remove-Payment-<?php echo $PVID; ?>-Form" class="form-horizontal" method="post" action="#">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModelLable">Remove This Payment ?</h4>
                        <input type="hidden" id="PVID" name="PVID" value="<?php echo $PVID; ?>" required />
                        <input type="hidden" id="PayFrom" name="PayFrom" value="<?php echo $PayFrom; ?>" required />
                        <input type="hidden" id="PayTo" name="PayTo" value="<?php echo $PayTo; ?>" required />
                        <input type="hidden" id="TotalAmount" name="TotalAmount" value="<?php echo $TotalAmount; ?>" required />
                        <input type="hidden" id="TotalTDSAmount" name="TotalTDSAmount" value="<?php echo $TotalTDSAmount; ?>" required />
                        <br/>
                    </div>
                    <div class="modal-footer">
                        <button class="m-w-150 btn btn-danger" type="submit" id="Remove-Payment-<?php echo $PVID; ?>" onClick="return RemovePayment(this.id);">
                            <i class="fa fa-trash bigger-110"></i>
                            Remove
                        </button>
                        <button type="button" class="m-w-150 btn btn-default" data-dismiss="modal">
                            <i class="fa fa-remove bigger-110"></i>
                            Close
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

 <!-- ------------------------------ Remove Model End ------------------------------ -->
                                        </td>
                                    </tr>
                                <?php
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
             </div>
        </section>
        <!-- Footer -->
       
        <!-- #END# Footer -->
    </div>

    <!-- Jquery Core Js -->
    <script src="assets/plugins/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Pace Loader Js -->
    <script src="assets/plugins/pace/pace.js"></script>

    <!-- Screenfull Js -->
    <script src="assets/plugins/screenfull/src/screenfull.js"></script>

    <!-- Metis Menu Js -->
    <script src="assets/plugins/metisMenu/dist/metisMenu.js"></script>

    <!-- Jquery Slimscroll Js -->
    <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- JQuery Datatables Js -->
    <script src="assets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
    <script src="assets/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>

    <!-- Custom Js -->
    <script src="assets/js/admin.js"></script>
    <script src="assets/js/pages/tables/jquery-datatables.js"></script>
    <script src="assets/js/pages/ui/modals.js"></script>

    <script type="text/javascript">
        function RemovePayment(btnId) {
            
            var form_id = btnId+'-Form'; 
        
            $('#'+form_id).on('submit', function(event) {
            
            event.preventDefault();
            
            var PVID = $('#'+form_id+' #PVID').val();
			var PayFrom = $('#'+form_id+' #PayFrom').val();
			var PayTo = $('#'+form_id+' #PayTo').val();
			var TotalAmount = $('#'+form_id+' #TotalAmount').val();
			var TotalTDSAmount = $('#'+form_id+' #TotalTDSAmount').val();
            
            var Action = 'RemovePayment';
            
            var form_data = 'Action='+ Action +'&PVID='+ PVID +'&PayFrom='+ PayFrom +'&PayTo='+ PayTo +'&TotalAmount='+ TotalAmount +'&TotalTDSAmount='+ TotalTDSAmount;
            
            $("#flash").show();
			$("#flash i").addClass('fa-spinner');
			$("#flash i").addClass('fa-spin');
			$("#flash").removeClass('hidden');
			$('html,body').animate({ scrollTop: $(".content").offset().top},'slow');
			$("#flash span").html('Please Wait...');
           // alert(form_data);
            $.ajax({
                type: 'POST',
                url: 'includes/payment_script.php',
                data: form_data,
                cache: false,
                success: function(result)
                {
                    //alert(result);
                    var obj = JSON.parse(result);
                    var Status = obj.Status;
                    
                    if(Status == '6')
                    {
                        $('.modal').fadeOut();
                        $("#flash").removeClass('alert alert-danger');
                        $("#flash").addClass('alert alert-success');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-times-circle');
                        $("#flash i").addClass('fa fa-fw fa-check-circle');
                        $("#flash span").html('Payment Detail Remove Successfully.');
                        $("#flash").delay(2000).fadeOut(500);
                        setTimeout(function() {
                          window.location.href = "view_all_against_payment.php";
                        }, 1000);
                        return true;
                    }
                    else if(Status == '5')
                    {
                        $('.modal').fadeOut();
                        $("#flash").removeClass('alert alert-success');
                        $("#flash").addClass('alert alert-danger');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash i").addClass('fa fa-fw fa-times-circle');
                        $("#flash span").html('Payment Detail Remove Not Successfully.');
                        $("#flash").delay(3000).fadeOut(500);
                        return true;
                    }
                    else if(Status == '00')
                    {
                        $('.modal').fadeOut();
                        $("#flash").removeClass('alert alert-success');
                        $("#flash").addClass('alert alert-danger');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash i").addClass('fa fa-fw fa-times-circle');
                        $("#flash span").html('Cannot Delete Payment, There is a Reference Available For This Payment.');
                        $("#flash").delay(3000).fadeOut(500);
                        return true;
                    }
                    else
                    {
                        $("#flash").removeClass('alert alert-success');
                        $("#flash").addClass('alert alert-danger');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash i").addClass('fa fa-fw fa-times-circle');
                        $("#flash span").html(Status);
                        $('#flash').delay(4000).fadeOut(500);
                        return true;
                    }
                }
            });
            
            });
        }
    </script>
        
        
    </body>
</html>
<?php
ob_flush();
?>