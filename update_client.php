<?php
ob_start();
$Page = "Client"; 
require_once('head.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Xenon ERP</title>
    
    <!-- Favicon -->
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="assets/plugins/bootstrap/dist/css/bootstrap.css" rel="stylesheet" />

    <!-- Animate.css Css -->
    <link href="assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Font Awesome Css -->
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

    <!-- iCheck Css Libraries | You can choose a theme from plugins/iCheck/skins instead of get all themes -->
    <link href="assets/plugins/iCheck/skins/minimal/_all.css" rel="stylesheet" />
    <link href="assets/plugins/iCheck/skins/square/_all.css" rel="stylesheet" />
    <link href="assets/plugins/iCheck/skins/flat/_all.css" rel="stylesheet" />

    <!-- Switchery Css -->
    <link href="assets/plugins/switchery/dist/switchery.css" rel="stylesheet" />

    <!-- Metis Menu Css -->
    <link href="assets/plugins/metisMenu/dist/metisMenu.css" rel="stylesheet" />

    <!-- Pace Loader Css -->
    <link href="assets/plugins/pace/themes/white/pace-theme-flash.css" rel="stylesheet" />

    <!-- Bootstrap Select Css -->
    <link href="assets/plugins/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" />

    <!-- DateTimePicker Css -->
    <link href="assets/plugins/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="assets/css/style.css" rel="stylesheet" />

</head>
<body class="ls-fixed navbar-fixed">
    <div class="all-content-wrapper">
        <!-- Top Bar -->
        <header>
            <?php include_once('header.php'); ?>
        </header>
        <?php
            if(isset($_GET['ledger_id']))
            {
                $LedgerID = $_GET['ledger_id'];
                $SelectLedger = "SELECT * FROM ledger_master WHERE ledger_id='".$LedgerID."'";
                $SelectLedgerQuery = mysqli_query($con,$SelectLedger);
                if(!$SelectLedgerQuery)
                {
                    //die(mysqli_error($con));
                    header("Location: view_all_client.php");
                    exit();
                }
                $count = mysqli_num_rows($SelectLedgerQuery);
                if($count != 1)
                {
                    header("Location: view_all_client.php");
                    exit();
                }
                
                $Ledger = mysqli_fetch_array($SelectLedgerQuery);
                
                $LedgerID = $Ledger['ledger_id'];
				$CompanyID = $Ledger['company_id'];
                $LedgerName = $Ledger['ledger_name'];
                $GroupID = $Ledger['group_id'];
                $LedgerAlias = $Ledger['ledger_alias'];
                $OpeningBalance = $Ledger['opening_balance'];
                $OldCurrentBalance = $Ledger['current_balance'];
                $OldOpeningBalance = $Ledger['opening_balance'];

                $GroupDetail = $groupObject->selectGroupDetail($GroupID);
                $GroupDetail = json_decode($GroupDetail,true);
                $GroupName = $GroupDetail['GroupName'];
                
            }
            else
            {
                header("Location: view_all_client.php");
                exit();
            }
        ?>
        <!-- #END# Top Bar -->
        <!-- Left Menu -->
        <aside class="sidebar">
            <?php include_once('menu.php'); ?>
        </aside>
        <!-- #END# Left Menu -->
        <section class="content">
            <div class="page-heading">
                <h1>
                    <a href="view_all_client.php">Client</a>
               		<small>
                        <i class="fa fa-angle-double-right"></i>
                        <a class="font-bold" href="update_client.php?ledger_id=<?php echo $LedgerID; ?>">Update Client Detail</a>
                    </small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="dashboard.php">Home</a></li>
                    <li><a href="view_all_client.php">Client</a></li>
                </ol>
            </div>
            
            <div class="page-body">
                <!-- -------------- ERROR SECTION START -------------- -->
                        
                <div id="flash" class="alert alert hidden">
                    <strong>
                        <i class="fa fa-spinner fa-spin"></i>
                    </strong>
                    &nbsp; &nbsp;
                    <span></span>
                </div>
                
                <!-- -------------- ERROR SECTION END -------------- -->
                <div class="row clearfix">
                    <!-- Horizontal Layout  -->
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Update Client</div>
                            <div class="panel-body p-b-25">
                                <form id="Update-Client-Form" method="post" class="form-horizontal" action="#">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                            <input type="hidden" id="AdminID" name="AdminID" value="<?php echo $_SESSION['AdminID']; ?>" required />
                                            <input type="hidden" id="LedgerID" name="LedgerID" value="<?php echo $LedgerID; ?>" required />
                                            <input type="hidden" id="CompanyID" name="CompanyID" value="<?php echo $CompanyID; ?>" required />
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Client Name</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="LedgerName" name="LedgerName" value="<?php echo $LedgerName; ?>" class="col-xs-10 col-md-8 form-control" required />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Client Alias</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="LedgerAlias" name="LedgerAlias" value="<?php echo $LedgerAlias; ?>" class="form-control" />
                                                    <input type="hidden" id="GroupID" name="GroupID" value="<?php echo $GroupID; ?>" required />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Opening Balance</label>
                                                <div class="col-sm-6">
                                                    <input type="hidden" id="OldOpeningBalance" name="OldOpeningBalance" value="<?php echo $OldOpeningBalance; ?>" class="form-control" required />
                                                    <input type="text" id="OpeningBalance" name="OpeningBalance" value="<?php echo $OpeningBalance; ?>" class="form-control" onKeyPress="return NuMValidation2(event);" required />
                                                    <input type="hidden" id="OldCurrentBalance" name="OldCurrentBalance" value="<?php echo $OldCurrentBalance; ?>" class="form-control" required />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">As On Date</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="LCF[17]" name="LCF[17]" placeholder="" data-format="DD-MM-YYYY" value="<?php echo $Value = $ledgerObject->getcustomerFieldsFor1Ledger(17,$LedgerID); ?>" class="form-control js-dtp"  required /> 
													<span style="width: 50px; " class="glyphicon glyphicon-calendar form-control-feedback"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Moblie No</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="LCF[1]" name="LCF[1]" onKeyPress="return NuMValidation(event);" value="<?php echo $Value = $ledgerObject->getcustomerFieldsFor1Ledger(1,$LedgerID); ?>" class="form-control" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Phone No</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="LCF[2]" name="LCF[2]" onKeyPress="return NuMValidation(event);"  value="<?php echo $Value = $ledgerObject->getcustomerFieldsFor1Ledger(2,$LedgerID); ?>" class="form-control" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Email</label>
                                                <div class="col-sm-6">
                                                    <input type="email" id="LCF[3]" name="LCF[3]" value="<?php echo $Value = $ledgerObject->getcustomerFieldsFor1Ledger(3,$LedgerID); ?>" class="form-control" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Website</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="LCF[4]" name="LCF[4]" value="<?php echo $Value = $ledgerObject->getcustomerFieldsFor1Ledger(4,$LedgerID); ?>" class="form-control" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">PAN</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="LCF[6]" name="LCF[6]" value="<?php echo $Value = $ledgerObject->getcustomerFieldsFor1Ledger(6,$LedgerID); ?>" class="form-control" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <label class="col-sm-9 control-label center" style="font-size: 15px;">Billing Details</label>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Billing Name</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="LCF[92]" name="LCF[92]" value="<?php echo $Value = $ledgerObject->getcustomerFieldsFor1Ledger(92,$LedgerID); ?>" class="form-control b_name" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Billing GSTIN</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="LCF[5]" name="LCF[5]" value="<?php echo $Value = $ledgerObject->getcustomerFieldsFor1Ledger(5,$LedgerID); ?>" class="form-control b_gstin" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Billing Address</label>
                                                <div class="col-sm-6">
                                                    <textarea id="LCF[7]" name="LCF[7]" class="form-control no-resize b_address" rows="5" /><?php echo $Value = $ledgerObject->getcustomerFieldsFor1Ledger(7,$LedgerID); ?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Billing City</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="LCF[8]" name="LCF[8]" value="<?php echo $Value = $ledgerObject->getcustomerFieldsFor1Ledger(8,$LedgerID); ?>" class="form-control b_city" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Billing State</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="LCF[9]" name="LCF[9]" value="<?php echo $Value = $ledgerObject->getcustomerFieldsFor1Ledger(9,$LedgerID); ?>" class="form-control b_state" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Billing State Code</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="LCF[76]" name="LCF[76]" value="<?php echo $Value = $ledgerObject->getcustomerFieldsFor1Ledger(76,$LedgerID); ?>" class="form-control b_state_code" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Billing Country</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="LCF[10]" name="LCF[10]" value="<?php echo $Value = $ledgerObject->getcustomerFieldsFor1Ledger(10,$LedgerID); ?>" class="form-control b_country" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Billing Pin Code</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="LCF[11]" name="LCF[11]" value="<?php echo $Value = $ledgerObject->getcustomerFieldsFor1Ledger(11,$LedgerID); ?>" class="form-control b_pin_code" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <label class="col-sm-9 control-label no-pedding" style="font-size: 15px;">Shipping Details</label>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Shipping Name</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="LCF[93]" name="LCF[93]" value="<?php echo $Value = $ledgerObject->getcustomerFieldsFor1Ledger(93,$LedgerID); ?>" class="form-control s_name" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Shipping GSTIN</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="LCF[74]" name="LCF[74]" value="<?php echo $Value = $ledgerObject->getcustomerFieldsFor1Ledger(74,$LedgerID); ?>" class="form-control s_gstin" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Shipping Address</label>
                                                <div class="col-sm-6">
                                                    <textarea id="LCF[12]" name="LCF[12]" class="form-control no-resize s_address" rows="5" /><?php echo $Value = $ledgerObject->getcustomerFieldsFor1Ledger(12,$LedgerID); ?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Shipping City</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="LCF[13]" name="LCF[13]" value="<?php echo $Value = $ledgerObject->getcustomerFieldsFor1Ledger(13,$LedgerID); ?>" class="form-control s_city" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Shipping State</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="LCF[14]" name="LCF[14]" value="<?php echo $Value = $ledgerObject->getcustomerFieldsFor1Ledger(14,$LedgerID); ?>" class="form-control s_state" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Shipping State Code</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="LCF[77]" name="LCF[77]" value="<?php echo $Value = $ledgerObject->getcustomerFieldsFor1Ledger(77,$LedgerID); ?>" class="form-control s_state_code" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Shipping Country</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="LCF[15]" name="LCF[15]" value="<?php echo $Value = $ledgerObject->getcustomerFieldsFor1Ledger(15,$LedgerID); ?>" class="form-control s_country" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Shipping Pin Code</label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="LCF[16]" name="LCF[16]" value="<?php echo $Value = $ledgerObject->getcustomerFieldsFor1Ledger(16,$LedgerID); ?>" class="form-control s_pin_code" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="form-group">
                                                <div class="text-center">
                                                    <button type="submit" class="btn btn-success">
                                                        <i class="fa fa-refresh bigger-110"></i>
                                                        Update
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- #END# Horizontal Layout  -->
                </div>
            </div>
        </section>
        <!-- Footer -->
       
        <!-- #END# Footer -->
    </div>
        
    <!-- Jquery Core Js -->
    <script src="assets/plugins/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Pace Loader Js -->
    <script src="assets/plugins/pace/pace.js"></script>

    <!-- Screenfull Js -->
    <script src="assets/plugins/screenfull/src/screenfull.js"></script>

    <!-- Metis Menu Js -->
    <script src="assets/plugins/metisMenu/dist/metisMenu.js"></script>

    <!-- Jquery Slimscroll Js -->
    <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Switchery Js -->
    <script src="assets/plugins/switchery/dist/switchery.js"></script>

    <!-- iCheck Js -->
    <script src="assets/plugins/iCheck/icheck.js"></script>

    <!-- Autosize Js (Textarea auto growth plugin) -->
    <script src="assets/plugins/autosize/dist/autosize.js"></script>

    <!-- MomentJs Js -->
    <script src="assets/plugins/moment/moment.js"></script>

    <!-- DateTimePicker Js -->
    <script src="assets/plugins/eonasdan-bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js"></script>

    <!-- Bootstrap Select Js -->
    <script src="assets/plugins/bootstrap-select/dist/js/bootstrap-select.js"></script>

    <!-- Custom Js -->
    <script src="assets/js/admin.js"></script>

    <script>
        function NuMValidation(evt)
        {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
            return false;
    
            return true;
        } 
        function BasivElement(evt)
        {
            //Init switch button
			var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
			elems.forEach(function (e) {
				var size = $(e).data('size');
				var options = {};
				options['color'] = '#009688';
				if (size !== undefined) options['size'] = size;

				var switchery = new Switchery(e, options);
			});

			//Init datetimepicker
			$('.js-dtp').each(function (i, key) {
				var format = $(key).data('format');
				$(key).datetimepicker({
					format: format,
					showClear: true
				});
			});
        }

    </script>
    <script type="text/javascript">

        jQuery(function ($) 
        {
            'use strict';
            $(document).ready(function () {
                //Init switch button
                var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
                elems.forEach(function (e) {
                    var size = $(e).data('size');
                    var options = {};
                    options['color'] = '#009688';
                    if (size !== undefined) options['size'] = size;

                    var switchery = new Switchery(e, options);
                });

                //Init datetimepicker
                $('.js-dtp').each(function (i, key) {
                    var format = $(key).data('format');
                    $(key).datetimepicker({
                        format: format,
                        showClear: true
                    });
                });
            });
        });

    </script>

    <script type="text/javascript">
                    
        function fetchData()
        {
            var GroupID = $("#GroupID").val();

            if(GroupID < 0)
            {
                $("#CustomFields").html('');
                $("#GroupID").focus();
                return false;
            }
            else
            { 
                var Action = 'GetCustomField';
                var dataString = 'Action='+ Action +'&GroupID='+ GroupID;
                
                $.ajax({
                    type: "POST",
                    url: "includes/loader_functions.php",
                    data: dataString,
                    cache: false,
                    success: function(result)
                    {
                        //alert(result);
                        $("#CustomFields").html(result);
                        BasivElement();
                        return true;
                    }
                });
            }
        }       

    </script>
    
   <script type="text/javascript">
        $('#Update-Client-Form').on('submit', function(event) {
            
            event.preventDefault();
                
            var Action = 'UpdateLedger';
                           
            var form_data = new FormData(this);
            form_data.append('Action',Action); 
                
            $("#flash").show();
            $("#flash i").addClass('fa-spinner');
            $("#flash i").addClass('fa-spin');
            $("#flash").removeClass('hidden');
            $('html,body').animate({ scrollTop: $(".content").offset().top},'slow');
            $("#flash span").html('Please Wait...');
                
            $.ajax({
                url: 'includes/ledger_script.php',
                type: 'POST',
                data: form_data,
                contentType: false,
                cache: false,
                processData:false,
                success: function(result)
                {
                    //alert(result);
                    var obj = JSON.parse(result);
                    var Status = obj.Status;
                        
                    if(Status == '4')
                    {
                        $("#flash").removeClass('alert alert-danger');
                        $("#flash").addClass('alert alert-success');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-times-circle');
                        $("#flash i").addClass('fa fa-fw fa-check-circle');
                        $("#flash span").html('Client Update Successfully.');
                        document.getElementById("Update-Client-Form").reset();
                        $('#flash').delay(3000).fadeOut(500);
                        setTimeout(function() {
                          window.location.href = "update_client.php?ledger_id=<?php echo $LedgerID; ?>";
                        }, 1000);
                        return true;                            
                    }
                    else if(Status == '3')
                    {
                        $("#flash").removeClass('alert alert-success');
                        $("#flash").addClass('alert alert-danger');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash i").addClass('fa fa-fw fa-times-circle');
                        $("#flash span").html('Client Update Not Successfully.');
                        $('#flash').delay(3000).fadeOut(500);
                        return true;
                    }
                    else if(Status == '0')
                    {
                        $("#flash").removeClass('alert alert-success');
                        $("#flash").addClass('alert alert-danger');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash i").addClass('fa fa-fw fa-times-circle');
                        $("#flash span").html('Client Already Exist.');
                        $('#flash').delay(4000).fadeOut(500);
                        return true;
                    }
                    else if(Status == '00')
                    {
                        $("#flash").removeClass('alert alert-success');
                        $("#flash").addClass('alert alert-danger');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash i").addClass('fa fa-fw fa-times-circle');
                        $("#flash span").html('Cannot Update Client, There is a Reference Available For This Client.');
                        $('#flash').delay(4000).fadeOut(500);
                        return true;
                    }
                    else
                    {
                        $("#flash").removeClass('alert alert-success');
                        $("#flash").addClass('alert alert-danger');
                        $("#flash i").removeClass('fa-spinner');
                        $("#flash i").removeClass('fa-spin');
                        $("#flash i").removeClass('fa fa-fw fa-check-circle');
                        $("#flash i").addClass('fa fa-fw fa-times-circle');
                        $("#flash span").html(Status);
                        $('#flash').delay(4000).fadeOut(500);
                        return true;
                    }
                }
            });
        });
    </script>
        
    </body>
</html>
<?php
ob_flush();
?>