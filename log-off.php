﻿<?php
ob_start();
session_start();
if(!isset($_SESSION['AdminID']))
{
	header("Location: index.php");
	exit();
}
setcookie("LoginStatus", 0, strtotime( '+30 days' ), "/", "", "", TRUE);
require_once('includes/dbconfig.php');
$LogOutTime = date("Y-m-d H:i:s");
$UpdateLoginHistory = mysqli_query($con,"UPDATE `admin_login_history` SET `logout_datetime`='$LogOutTime' WHERE `login_id` = '".$_SESSION['LoginID']."'");

$UpdateLive = mysqli_query($con,"UPDATE `admin_master` SET `live`='0' WHERE admin_id='".$_SESSION['AdminID']."'");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Xenon ERP</title>
    
    <!-- Favicon -->
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet"/>

    <!-- Bootstrap Core Css -->
    <link href="assets/plugins/bootstrap/dist/css/bootstrap.css" rel="stylesheet"/>

    <!-- Font Awesome Css -->
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>

    <!-- Custom Css -->
    <link href="assets/css/style.css" rel="stylesheet"/>
</head>
<body class="locked-screen">
    <div class="watch"></div>
    <div class="date"></div>
	
    <div class="row">
    	<div class="col-xs-12 text-center m-t-15">
			<span id="login_error" class="col-danger"></span>
			<span id="login_success" class="col-success"></span>
		</div>
    </div>
    <div class="lockscreen-area clearfix">
        <div class="name">Nirant Enterprise</div>
        <div class="form">
            <form id="LogOff-Form" method="post" action="#">
                <div class="form-group has-feedback">
                   	<input type="hidden" id="Username" value="<?php echo $_SESSION['UserName']; ?>" required/>
                    <input type="password" id="Password" placeholder="Unlock Password" required/>
                    <span class="fa fa-arrow-right form-control-feedback"></span>
                </div>
            </form>
        </div>
    </div>
    <div class="profile-image">
        <img src="assets/images/user.jpg"/>
    </div>
                

    <!-- Jquery Core Js -->
    <script src="assets/plugins/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="assets/plugins/bootstrap/dist/js/bootstrap.js"></script>

    <!-- Momentjs -->
    <script src="assets/plugins/moment/moment.js"></script>

    <!-- Custom Js -->
    <script src="assets/js/pages/examples/locked-screen.js"></script>
    <script type="text/javascript">
        $('#LogOff-Form').on('submit', function(event) {
            
            event.preventDefault();
    
            var UserName = $("#Username").val();
            var Password = $("#Password").val();

            if(Password == '')
            {
                $('#login_success').fadeOut();
            	$("#login_error").html("Please Enter Password.");
               	$("#Password").focus();
               	return false;
            }
            
            var Action = "AdminLogin";
            var form_data = 'Action='+ Action +'&UserName='+ UserName +'&Password='+ Password;
            
            $('#login_success').fadeOut();
                $("#login_error").show();
                $("#login_error").fadeIn(400).html('<img src="assets/img/loading.gif" title="loading.gif">');
            
            $.ajax({
                url: "includes/login_script.php",
                type: 'POST',
                data: form_data,
                cache: false,
                processData:false,
                }).done(function(result) {
                    //alert(result);
                    var obj = JSON.parse(result);
                                            
                    var Status = obj.Status;
                    
                    if(Status == '1')
                    {
                        $('#login_success').fadeOut();
                            $("#login_error").html("Login Failed. Please Check Your Username And Password.");
                            $("#Username").focus();
                            return false;
                    }
                    else if(Status == '2')
                    {
                        $('#login_success').fadeOut();
                            $("#login_error").html("Login Failed. Account Does Not Exist.");
                            $("#Username").focus();
                            return false;
                    }
                    else if(Status == '3')
                    {
                        $('#login_success').fadeOut();
                            $("#login_error").html("Login Failed. Account Not Active.");
                            $("#Username").focus();
                            return false;
                    }
                    else if(Status == '4')
                    {
                        $('#login_success').fadeOut();
                            $("#login_error").html("Login Failed. Please Try Again Later.");
                            $("#Username").focus();
                            return false;
                    }
                    else if(Status == '5')
                    {
                        $('#login_error').fadeOut();
                            $("#login_success").show();
                            $("#login_success").html("Login Successful. Redirecting...");
                            window.location.assign("dashboard.php");
                            return true;
                    }
                    else
                    {
                        $('#login_success').fadeOut();
                            $("#login_error").html(Status);
                            $("#Username").focus();
                            return false;
                    }   
                });
            
        });
    </script>
</body>
</html>
<?php
ob_flush();
?>
